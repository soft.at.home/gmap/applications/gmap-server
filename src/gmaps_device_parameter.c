/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <debug/sahtrace.h>
#include "gmaps_priv.h"
#include <amxc/amxc_macros.h>

#define ME "dm_devices"

typedef struct _gmaps_traverse_first_parameter {
    const char* parameter_name;
    amxc_var_t* value;
} gmaps_traverse_first_parameter_t;

typedef struct _gmaps_traverse_parameters {
    const char* parameter_name;
    amxc_var_t* values;
} gmaps_traverse_parameters_t;

static gmap_traverse_status_t gmaps_traverse_first_parameter(amxd_object_t* device, gmap_traverse_action_t action, void* userdata) {
    gmap_traverse_status_t status = gmap_traverse_continue;
    gmaps_traverse_first_parameter_t* first_parameter_data = ( gmaps_traverse_first_parameter_t*) userdata;
    switch(action) {
    case gmap_traverse_device_matching: {
        SAH_TRACEZ_INFO(ME, "Traverse device matching %s", amxd_object_get_name(device, AMXD_OBJECT_NAMED));
        amxd_object_t* param_obj = NULL;
        amxd_path_t* path = NULL;
        const char* obj_path = NULL;
        const char* param_name = NULL;
        const amxc_var_t* value = NULL;
        if(strchr(first_parameter_data->parameter_name, '.')) {
            amxd_path_new(&path, first_parameter_data->parameter_name);
            obj_path = amxd_path_get(path, 0);
            if(obj_path) {
                param_obj = amxd_object_findf(device, "%s", obj_path);
                param_name = amxd_path_get_param(path);
            }
        } else {
            param_obj = device;
            param_name = first_parameter_data->parameter_name;
        }
        if(param_obj && param_name && *param_name) {
            SAH_TRACEZ_INFO(ME, "param_obj: %s param_name: %s", amxd_object_get_name(param_obj, AMXD_OBJECT_NAMED), param_name);

            value = amxd_object_get_param_value(param_obj,
                                                param_name);
            if(value) {
                amxc_var_new(&first_parameter_data->value);
                amxc_var_copy(first_parameter_data->value, value);
                status = gmap_traverse_done;
            }
        }
        amxd_path_delete(&path);
        break;
    }
    case gmap_traverse_start:
    case gmap_traverse_level_push:
    case gmap_traverse_device_not_matching:
    case gmap_traverse_device_done:
    case gmap_traverse_level_pop:
    case gmap_traverse_stop:
    default: {
        break;
    }
    }
    return status;
}

static gmap_status_t gmaps_add_param_value(amxd_object_t* device,
                                           void* priv) {
    gmap_status_t status = gmap_status_ok;
    gmaps_traverse_parameters_t* parameters_data = (gmaps_traverse_parameters_t*) priv;
    const amxc_var_t* obj_value = NULL;

    obj_value = amxd_object_get_param_value(device,
                                            parameters_data->parameter_name);
    if(obj_value) {
        amxc_var_t* value = amxc_var_add_new_key(parameters_data->values, amxd_object_get_name(device, AMXD_OBJECT_NAMED));
        amxc_var_copy(value, obj_value);
    }
    return status;
}

static gmap_traverse_status_t gmaps_traverse_parameters(amxd_object_t* device, gmap_traverse_action_t action, void* userdata) {
    gmap_traverse_status_t status = gmap_traverse_continue;
    gmaps_traverse_parameters_t* parameters_data = (gmaps_traverse_parameters_t*) userdata;
    switch(action) {
    case gmap_traverse_device_matching: {
        SAH_TRACEZ_INFO(ME, "Traverse device matching %s", amxd_object_get_name(device, AMXD_OBJECT_NAMED));
        gmaps_add_param_value(device, parameters_data);
        gmaps_device_for_all_alternatives(device, gmaps_add_param_value, parameters_data);
        break;
    }
    case gmap_traverse_start:
    case gmap_traverse_level_push:
    case gmap_traverse_device_not_matching:
    case gmap_traverse_device_done:
    case gmap_traverse_level_pop:
    case gmap_traverse_stop:
    default: {
        break;
    }
    }
    return status;
}

/**
   @ingroup gmap_device
   @brief
   Get the first parameter with the given name in the device tree of the device matching an expression.

   Server side imlementation to get the first parameter

   Fetches the first encounter parameter with the a specified name in the device tree for a device matching the given expression
   and traversing the tree in the specified traverse mode

   A master device matches the given expression if the device itself or one of its altrernative devices are matching.

   If the master device does not contain the parameter, but one or more of its alternatives do contain the parameter,
   the value is taken of the first alternative device that contains the parameter. The alternatives are inspected in
   the order they were added.

   When calling this function on an alternative device, the master device will be taken, unless the traverse mode is set
   to gmap_traverse_this

   @note
   The returned variant must be cleaned (use variant_cleanup) and freed (use free function)
   @param object pointer of the device
   @param parameter the name of the parameter the needs to be fetched
   @param expression the expression used for matching devices, the tags are only set on matching devices
   @param mode the traverse mode

   @return
   a variant containing the value of the parameter found or NULL when no such parameter was found.
 */

amxc_var_t* gmaps_device_get_first_parameter(amxd_object_t* device, const char* parameter, const char* expression, gmap_traverse_mode_t mode, gmap_status_t* status) {

    gmaps_traverse_first_parameter_t first_parameter_data;

    *status = gmap_status_unknown_error;

    first_parameter_data.parameter_name = parameter;
    first_parameter_data.value = NULL;

    when_str_empty(parameter, exit);

    // recursively traverse the tree
    if(gmaps_traverse_tree(device, mode, expression, gmaps_traverse_first_parameter, &first_parameter_data) == gmap_traverse_failed) {
        SAH_TRACEZ_ERROR(ME, "Failed to traverse tree");
        goto exit;
    }

    *status = gmap_status_ok;

exit:
    return first_parameter_data.value;
}

/**
   @ingroup gmap_device
   @brief
   Get all parameters with the given name in the device tree of the devices matching an expression.

   Server side imlementation to get the parameters

   Fetches all parameters with the a specified name in the device tree for all devices matching the given expression
   and traversing the tree in the specified traverse mode

   When the function is called on a master device, then the returned list will contain all values of all alternatives,
   if they contain the parameter.

   When the function is called on an alternative device, the starting point for the traverse will be the master device,
   unless the traverse mode is set to gmap_traverse_this.

   @note
   The returned variant map must be cleaned (use variant_map_cleanup) and freed (use free function)

   @param key the unique key that identifies the device
   @param parameter the name of the parameter the needs to be fetched
   @param expression the expression used for matching devices, the parameter is only fetched for matching devices
   @param mode the traverse mode

   @return
   a variant map containing all the values of the parameters found (the key is set to the key of the device) or an empty map when no such parameter was found.
 */

amxc_var_t* gmaps_device_get_parameters(amxd_object_t* device,
                                        const char* parameter,
                                        const char* expression,
                                        gmap_traverse_mode_t mode,
                                        gmap_status_t* status) {
    gmaps_traverse_parameters_t parameters_data;

    *status = gmap_status_unknown_error;

    parameters_data.parameter_name = parameter;
    parameters_data.values = NULL;

    when_str_empty(parameter, exit);

    amxc_var_new(&parameters_data.values);
    amxc_var_set_type(parameters_data.values, AMXC_VAR_ID_HTABLE);

    // recursively traverse the tree
    if(gmaps_traverse_tree(device, mode, expression, gmaps_traverse_parameters, &parameters_data) == gmap_traverse_failed) {
        SAH_TRACEZ_ERROR(ME, "Failed to traverse tree");
        goto exit;
    }

    *status = gmap_status_ok;

exit:
    return parameters_data.values;
}
