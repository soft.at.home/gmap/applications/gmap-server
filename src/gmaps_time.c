/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include <ctype.h>

#include "gmaps_priv.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#define ME "time"
#define TIME_OBJECT_PATH "Time."
#define TIME_RETRY_SEC 60
#define TIME_INIT_SEC 20

static bool ntp_synchronized = false;
static amxb_subscription_t* time_subscription = NULL;
static amxp_timer_t* init_timer = NULL;


static void gmap_update_last_connection(void) {
    SAH_TRACEZ_INFO(ME, "Updating last connection");
    amxd_object_for_each(instance, child_it, gmap_dm_get_devices_device()) {
        amxd_object_t* dev = amxc_llist_it_get_data(child_it, amxd_object_t, it);

        if(amxd_object_get_value(bool, dev, "Active", NULL)) {
            // Set the LastConnection time.
            amxc_ts_t now;
            amxc_ts_now(&now);
            amxd_object_set_value(amxc_ts_t, dev, "LastConnection", &now);
        }
    }
}

/**
 * When NTP is synchronized and the current time is set, set Devices.Device.*.FirstSeen to current
 * time for all devices where Devices.Device.[].FirstSeen == "0001-01-01T00:00:00Z" .
 */
static void gmaps_set_zerofirstseen_to_now(void) {
    amxd_status_t status = amxd_status_unknown_error;
    amxc_ts_t now;
    amxc_var_t current;
    amxc_var_t zero;

    amxc_var_init(&zero);
    amxc_var_init(&current);

    when_false_trace(ntp_synchronized, exit, ERROR, "Invalid state");

    amxc_ts_now(&now);
    amxc_var_set_type(&zero, AMXC_VAR_ID_TIMESTAMP);

    amxd_object_for_each(instance, it, gmap_dm_get_devices_device()) {
        amxd_object_t* dev = amxc_llist_it_get_data(it, amxd_object_t, it);
        int result = -1;
        status = amxd_object_get_param(dev, "FirstSeen", &current);
        when_failed_trace(status, next,
                          ERROR, "Failed to read 'FirstSeen' for device %s: %d",
                          amxd_object_get_name(dev, AMXD_OBJECT_NAMED), status);
        when_failed_trace(amxc_var_compare(&zero, &current, &result), next,
                          ERROR, "Failed to compare 'FirstSeen' for device %s",
                          amxd_object_get_name(dev, AMXD_OBJECT_NAMED));
        when_false(result == 0, next);

        amxd_object_set_value(amxc_ts_t, dev, "FirstSeen", &now);
next:
        continue;
    }

exit:
    amxc_var_clean(&zero);
    amxc_var_clean(&current);
}

static void gmap_update_ntp_synchronized(GMAPS_UNUSED const char* const sig_name,
                                         const amxc_var_t* const data,
                                         GMAPS_UNUSED void* const priv) {
    const char* status = GETP_CHAR(data, "parameters.Status.to");

    when_null_trace(status, exit, ERROR, "Status is a NULL pointer");

    if(strcmp(status, "Synchronized") == 0) {
        ntp_synchronized = true;
        gmap_update_last_connection();
        gmaps_set_zerofirstseen_to_now();
    } else {
        ntp_synchronized = false;
    }

exit:
    return;
}

static bool s_attempt_time_subscribe(void) {
    int ret = 0;
    amxb_bus_ctx_t* time_bus = NULL;
    amxc_var_t value;
    const char* status;

    amxc_var_init(&value);

    time_bus = amxb_be_who_has(TIME_OBJECT_PATH);
    when_null_trace(time_bus, exit, ERROR, "Failed to find bus for " TIME_OBJECT_PATH);

    if(time_subscription == NULL) {
        ret = amxb_subscription_new(&time_subscription,
                                    time_bus,
                                    TIME_OBJECT_PATH,
                                    "notification == 'dm:object-changed' && contains('parameters.Status')",
                                    gmap_update_ntp_synchronized,
                                    NULL);

        when_failed_trace(ret, exit, ERROR, "Could not subscribe to " TIME_OBJECT_PATH " events: %s (%d)", amxb_get_error(ret), ret);
    }

    // Check if Time.Status is already synchronized
    ret = amxb_get(time_bus, TIME_OBJECT_PATH "Status", 0, &value, 2);
    when_failed_trace(ret, exit, ERROR, "amxb_get on " TIME_OBJECT_PATH "Status failed: %s (%d)", amxb_get_error(ret), ret);

    ret = -1;
    status = GET_CHAR(amxc_var_get_first(amxc_var_get_first(&value)), "Status");
    when_null_trace(status, exit, ERROR, "amxb_get on " TIME_OBJECT_PATH "Status failed, parameter not in result.");

    ret = 0;
    SAH_TRACEZ_INFO(ME, "Time subscription success, current status '%s'", status);
    if(strcmp(status, "Synchronized") == 0) {
        ntp_synchronized = true;
        gmap_update_last_connection();
    }

exit:
    amxc_var_clean(&value);
    return ret == 0;
}

static void s_time_retry_cb(GMAPS_UNUSED amxp_timer_t* timer, GMAPS_UNUSED void* priv) {
    if(s_attempt_time_subscribe()) {
        SAH_TRACEZ_INFO(ME, "Subscription complete, removing retry timer.");
        amxp_timer_delete(&init_timer);
    }
}

static void s_deferred_wait_time_done(UNUSED const char* const s,
                                      UNUSED const amxc_var_t* const d,
                                      UNUSED void* const p) {
    amxp_slot_disconnect_all(s_deferred_wait_time_done);

    SAH_TRACEZ_INFO(ME, "Scheduling initial time subscription");
    /* Sometimes, Time. is fickle at startup, even after we know it should be
     * available. Therefore, give it some extra time before attempting to fetch. */
    amxp_timer_new(&init_timer, s_time_retry_cb, NULL);
    amxp_timer_set_interval(init_timer, (TIME_RETRY_SEC) * 1000);
    amxp_timer_start(init_timer, (TIME_INIT_SEC) * 1000);
}

static void s_deferred_init(UNUSED const amxc_var_t* const data,
                            UNUSED void* const priv) {
    int retval;
    /* Can't simply wait on "wait:done" signal in case a mib waits on an object as well. */
    retval = amxp_slot_connect_filtered(NULL, "^wait:Time\\.$", NULL, s_deferred_wait_time_done, NULL);
    when_failed_trace(retval, exit, ERROR, "subscribing wait failed for object '" TIME_OBJECT_PATH "': %d", retval);
    retval = amxb_wait_for_object(TIME_OBJECT_PATH);
    when_failed_trace(retval, exit, ERROR, "waiting failed for object '" TIME_OBJECT_PATH "'");
exit:
    return;
}

bool gmap_is_ntp_synchronized(void) {
    return ntp_synchronized;
}

void gmaps_time_init(void) {
    /* Time. is an optional dependency, so it can't be listed among the
     * requires in the odl. This entry point is called within a callback to the
     * wait:done event for the required objects from the odl file.
     * Therefore, waiting on Time. should be deferred to the next cycle
     * on the event loop. This allows libamxrt to properly handle the appearance
     * of all the odl requirements without confusing libamxb by adding a signal
     * handler from within a callback for that same signal.
     */
    if(0 != amxp_sigmngr_deferred_call(NULL, s_deferred_init, NULL, NULL)) {
        SAH_TRACEZ_ERROR(ME, "Failed to defer time init");
    }
}

void gmaps_time_cleanup(void) {
    amxp_timer_delete(&init_timer);
    amxb_subscription_delete(&time_subscription);
    ntp_synchronized = false;
}
