/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "gmaps_import_pcm.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include "gmaps_devices.h"

#define ME "gmaps"

static bool s_set_tags_of_device(const char* alias, amxd_object_t* device_template_obj, const amxc_var_t* device_data) {
    amxc_var_t params;
    bool ok = false;
    const char* tags = GET_CHAR(device_data, "Tags");
    amxc_var_init(&params);
    when_str_empty_trace(alias, exit, ERROR, "Device without alias");
    when_str_empty_trace(tags, exit, ERROR, "Device %s without tags", alias);
    when_null_trace(device_template_obj, exit, ERROR, "NULL argument");

    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &params, "Tags", tags);
    amxc_var_add_key(cstring_t, &params, "Alias", alias);
    amxc_var_add_key(cstring_t, &params, "Key", alias);

    amxd_object_t* device = NULL;
    when_failed_trace(amxd_object_new_instance(&device, device_template_obj, alias, 0, &params),
                      exit,
                      ERROR, "Error creating device object instance");
    amxd_object_set_params(device, &params);
    gmaps_store_device(device);

    /* Emit the expected 'dm:instance-added' event so that mod-pcm-svc remembers it.
     *
     * Internally, mod-pcm-svc relies on events sent by default by the transactions
     * it executes. This is normally fine when it creates all the objects itself.
     *
     * However, gmap-server has to pre-empt this so that the mibs are properly
     * loaded on the instance before mod-pcm-svc tries to restore mib parameters.
     *
     * Note that amxo_parser_apply_mibs emits events for all objects, instances
     * and parameters that are added from the mib, so logically, the containing
     * object must have been created already.
     */
    amxd_object_emit_add_inst(device);

    amxo_parser_apply_mibs(gmap_get_parser(), device, gmaps_device_matches);

    ok = true;

exit:
    amxc_var_clean(&params);
    return ok;
}

static bool s_set_tags_of_devices(const amxc_var_t* devices) {
    bool ok = true;
    when_null_trace(devices, exit, ERROR, "NULL argument");
    when_false_trace(amxc_var_type_of(devices) == AMXC_VAR_ID_HTABLE || amxc_var_type_of(devices) == AMXC_VAR_ID_LIST,
                     exit, ERROR, "Expected devices list or htable");

    amxd_object_t* device_template_obj = amxd_object_findf(&gmap_get_dm()->object, "Devices.Device");
    when_null_trace(device_template_obj, exit, ERROR, "Device template object not found");

    amxc_var_for_each(device, devices) {
        const amxc_var_t* device_parameters = amxc_var_get_first(device);
        const char* alias = amxc_var_key(device_parameters);
        ok &= s_set_tags_of_device(alias, device_template_obj, device_parameters);
    }

exit:
    return ok;
}

/**
 * Sets the tags of the devices contained in the given persistentconfiguration-manager data.
 *
 * We have to set the tags first, because they have to be set before setting parameters,
 * because some parameters are defined in mibs, and the mibs are only loaded after
 * the tags are set.
 */
static bool s_set_tags_of_devices_in_import_pcm(const amxc_var_t* pcm_data) {
    bool ok = true;
    const amxc_var_t* devices = GETP_ARG(pcm_data, "set.hgwconfig.Devices.Device.");

    if(devices != NULL) {
        ok &= s_set_tags_of_devices(devices);
    }

    return ok;
}

amxd_status_t gmaps_import_pcm(amxc_var_t* args, amxc_var_t* ret) {
    int status = amxd_status_unknown_error;
    bool ok = false;
    const amxc_var_t* pcm_data = GET_ARG(args, "data");
    when_null_trace(pcm_data, exit, ERROR, "'data' parameter not found");

    // First set the tags
    ok = s_set_tags_of_devices_in_import_pcm(pcm_data);
    when_false_trace(ok, exit, ERROR, "Error importing reboot-persistent data");

    // Now that the tags are set, and parameters defined in mibs exist for the device,
    // it's safe for PCM to fill in the other parameters:
    status = amxb_call(amxb_be_who_has("Devices"),
                       "Devices.",
                       "PcmImport",
                       args,
                       ret,
                       15);
    when_failed_trace(status, exit, ERROR, "Error calling PcmImport");

exit:
    return status;
}
