/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include <ctype.h>

#include "gmaps_priv.h"
#include "gmaps_uuid.h"

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#define ME "merge"

#define GET_INT64(a, n) amxc_var_dyncast(int64_t, n == NULL ? a : GET_ARG(a, n))

typedef enum _devices_merge_shift_type {
    devices_merge_shift_type_right,
    devices_merge_shift_type_left
} devices_merge_shift_type_t;

typedef enum _devices_merge_type {
    devices_merge_type_unknown,
    devices_merge_type_address,
    devices_merge_type_offset,
    devices_merge_type_filter,
    devices_merge_type_filter_offset,
    devices_merge_type_filter_right_shift,
    devices_merge_type_filter_left_shift
} devices_merge_type_t;

typedef struct _devices_merge_address {
    int64_t number;
    amxc_var_t address;
} devices_merge_address_t;

typedef struct _devices_merge_offset {
    int64_t number;
    amxc_var_t offset;
} devices_merge_offset_t;

typedef struct _devices_merge_filter {
    uint64_t mask;
    uint64_t value;
} devices_merge_filter_t;

typedef struct _devices_merge_filter_offset {
    int64_t number;
    uint64_t mask;
    uint64_t value;
    amxc_var_t offset;
} devices_merge_filter_offset_t;

typedef struct _devices_merge_filter_shift {
    uint64_t mask;
    uint64_t value;
    uint64_t shift;
} devices_merge_filter_shift_t;

typedef struct _devices_merge_rule {
    char* key;
    devices_merge_type_t type;
    void (* execute)(amxd_object_t*, struct _devices_merge_rule*);
    union {
        devices_merge_address_t* ma;
        devices_merge_offset_t* mo;
        devices_merge_filter_t* mf;
        devices_merge_filter_offset_t* mfo;
        devices_merge_filter_shift_t* mfs;
    } merge;
    amxc_llist_it_t it;
} devices_merge_rule_t;

static amxc_llist_t gmap_devices_merge_rules;

static devices_merge_type_t merge_get_type(const char* type) {
    when_str_empty_trace(type, exit, ERROR, "Missing Parameter Type");
    if(!strcmp(type, "Address")) {
        return devices_merge_type_address;
    } else if(!strcmp(type, "Offset")) {
        return devices_merge_type_offset;
    } else if(!strcmp(type, "Filter")) {
        return devices_merge_type_filter;
    } else if(!strcmp(type, "FilterOffset")) {
        return devices_merge_type_filter_offset;
    } else if(!strcmp(type, "FilterRightShift")) {
        return devices_merge_type_filter_right_shift;
    } else if(!strcmp(type, "FilterLeftShift")) {
        return devices_merge_type_filter_left_shift;
    }
    SAH_TRACEZ_ERROR(ME, "Unknown parameter Type : %s", type);
exit:
    return devices_merge_type_unknown;
}

static void merge_delete_rule(devices_merge_rule_t* rule) {
    switch(rule->type) {
    case devices_merge_type_address:
        amxc_var_clean(&rule->merge.ma->address);
        free(rule->merge.ma);
        break;

    case devices_merge_type_offset:
        amxc_var_clean(&rule->merge.mo->offset);
        free(rule->merge.mo);
        break;

    case devices_merge_type_filter:
        free(rule->merge.mf);
        break;

    case devices_merge_type_filter_offset:
        amxc_var_clean(&rule->merge.mfo->offset);
        free(rule->merge.mfo);
        break;

    case devices_merge_type_filter_right_shift:
    case devices_merge_type_filter_left_shift:
        free(rule->merge.mfs);
        break;

    default:
        break;
    }

    amxc_llist_it_take(&rule->it);
    SAH_TRACEZ_INFO(ME, "Delete rule for device %s, rule list size = %zu", rule->key, amxc_llist_size(&gmap_devices_merge_rules));
    free(rule->key);
    free(rule);
}

bool gmaps_devices_merge_delete_rules(const char* key) {
    bool res = false;
    when_str_empty_trace(key, exit, ERROR, "NULL pointer/empty string");
    amxc_llist_for_each(it, &gmap_devices_merge_rules) {
        devices_merge_rule_t* merge_rule = amxc_container_of(it, devices_merge_rule_t, it);
        if(!strcmp(key, merge_rule->key)) {
            merge_delete_rule(merge_rule);
        }
    }
    res = true;
exit:
    return res;
}

static void merge_add_rule(devices_merge_rule_t* rule) {
    amxc_llist_it_init(&rule->it);
    amxc_llist_append(&gmap_devices_merge_rules, &rule->it);
    SAH_TRACEZ_INFO(ME,
                    "Create merge rule for device %s, rule list size = %zu",
                    rule->key, amxc_llist_size(&gmap_devices_merge_rules));
}

static bool merge_set_alternative(amxd_object_t* master, amxd_object_t* alternative) {
    bool result = false;
    char* masterkey = NULL;
    char* altkey = NULL;
    amxd_status_t status;

    when_null_trace(master, exit, ERROR, "NULL parameter");
    when_null_trace(alternative, exit, ERROR, "NULL parameter");

    masterkey = amxd_object_get_cstring_t(master, "Key", &status);
    when_str_empty_trace(masterkey, exit, ERROR, "NULL/empty parameter status: %d", status);
    altkey = amxd_object_get_cstring_t(alternative, "Key", &status);
    when_str_empty_trace(altkey, exit, ERROR, "NULL/empty parameter status: %d", status);

    if(amxd_object_findf(master, "Alternative.%s.", altkey)) {
        SAH_TRACEZ_INFO(ME, "%s and %s are already master and alternative", masterkey, altkey);
        result = true;
        goto exit;
    }

    if(amxd_object_findf(alternative, "Alternative.%s", masterkey)) {
        SAH_TRACEZ_INFO(ME, "%s and %s are already master and alternative", altkey, masterkey);
        result = true;
        goto exit;
    }

    // Consider the following situation:
    //                      dev1
    //                       |
    //                       v
    //  dev2(master)~~~~~~dev4(alternative)
    //    |                  |
    //    v                  v
    //   dev3              dev5
    //
    // When making dev4 an alternative of dev2, then Devices.Device.dev1.topology() should include
    // dev3.
    // This is taken care of already by topology() and is_linked_to(), so we do not have to start
    // adding and removing links here.
    // If this changes, i.e. if topology() and friends stops taking care of this and instead we want
    // to add/remove links on merging, then it should not happen here in the specific case of
    // executing rules, but in a more generic place such as set_alternative().

    SAH_TRACEZ_INFO(ME, "Merge master %s and alternative %s devices", masterkey, altkey);
    gmaps_device_set_alternative(master, alternative);
    gmaps_device_clear_tag(altkey, "edev", NULL, gmap_traverse_this);
    result = true;
exit:
    free(masterkey);
    free(altkey);
    return result;
}

static uint64_t merge_macTouint64(const char* key) {
    char mac[13];
    uint64_t res;
    sscanf(key, "%c%c:%c%c:%c%c:%c%c:%c%c:%c%c", &mac[0], &mac[1], &mac[2], &mac[3], &mac[4], &mac[5],
           &mac[6], &mac[7], &mac[8], &mac[9], &mac[10], &mac[11]);
    mac[12] = 0;
    sscanf(mac, "%" SCNx64, &res);
    return res;
}

static const char* merge_apply_offset(const char* mac, int64_t offset) {

    uint64_t num;
    char result[18];
    num = merge_macTouint64(mac);
    num += offset;

    snprintf(result, 18, "%02" PRIX64 ":%02" PRIX64 ":%02" PRIX64 ":%02" PRIX64 ":%02" PRIX64 ":%02" PRIX64, (num >> 40) & 0xFF, (num >> 32) & 0xFF, (num >> 24) & 0xFF, (num >> 16) & 0xFF, (num >> 8) & 0xFF, num & 0xFF);
    return (gmaps_uuid_get_key_by_mac(result));
}

static void execute_address_rule(amxd_object_t* master, devices_merge_rule_t* rule) {

    int64_t number = 0;

    SAH_TRACEZ_INFO(ME, "Execute address rule");

    amxc_var_for_each(address_var, &rule->merge.ma->address) {
        char* mac = NULL;
        mac = amxc_var_dyncast(cstring_t, address_var);
        when_str_empty_trace(mac, exit, ERROR, "NULL pointer/empty string");
        SAH_TRACEZ_INFO(ME, "alternative mac: %s", mac);
        if(merge_set_alternative(master,
                                 gmaps_get_device(gmaps_uuid_get_key_by_mac(mac)))) {
            number++;
        }
exit:
        free(mac);
        continue;
    }

    if(number < rule->merge.mo->number) {
        return;
    }

    merge_delete_rule(rule);
}

static void execute_offset_rule(amxd_object_t* master, devices_merge_rule_t* rule) {

    int64_t number = 0;
    int64_t offset;
    amxd_status_t status;
    SAH_TRACEZ_INFO(ME, "Execute offset rule");
    amxc_var_for_each(offset_var, &rule->merge.mo->offset) {
        const char* key = NULL;
        char* mac = NULL;
        offset = amxc_var_dyncast(int64_t, offset_var);
        mac = amxd_object_get_cstring_t(master, "PhysAddress", &status);
        when_str_empty_trace(mac, exit, ERROR, "NULL pointer/empty string status: %d", status);
        key = merge_apply_offset(mac, offset);
        SAH_TRACEZ_INFO(ME, "alternative key: %s", key);
        if(merge_set_alternative(master, gmaps_get_device(key))) {
            number++;
        }
exit:
        free(mac);
        continue;
    }

    if(number < rule->merge.mo->number) {
        return;
    }

    merge_delete_rule(rule);
}

static char* merge_apply_filter(const char* mac, uint64_t mask, uint64_t value, bool key) {

    uint64_t num;
    char result[18];
    num = merge_macTouint64(mac);

    num &= mask;
    num |= value;

    snprintf(result, 18, "%02" PRIX64 ":%02" PRIX64 ":%02" PRIX64 ":%02" PRIX64 ":%02" PRIX64 ":%02" PRIX64, (num >> 40) & 0xFF, (num >> 32) & 0xFF, (num >> 24) & 0xFF, (num >> 16) & 0xFF, (num >> 8) & 0xFF, num & 0xFF);
    if(key) {
        const char* devkey = gmaps_uuid_get_key_by_mac(result);
        if(devkey) {
            return (strdup(devkey));
        }
    } else {
        if(*result) {
            return(strdup(result));
        }
    }
    return NULL;
}

static void execute_filter_rule(amxd_object_t* master, devices_merge_rule_t* rule) {
    char* key = NULL;
    char* mac = NULL;
    amxd_status_t status;

    SAH_TRACEZ_INFO(ME, "Execute filter rule");

    mac = amxd_object_get_cstring_t(master, "PhysAddress", &status);
    when_str_empty_trace(mac, exit, ERROR, "NULL pointer/empty string status: %d", status);
    key = merge_apply_filter(mac, rule->merge.mf->mask, rule->merge.mf->value, true);
    SAH_TRACEZ_INFO(ME, "alternative key: %s", key);
    if(!merge_set_alternative(master, gmaps_get_device(key))) {
        goto exit;
    }
    merge_delete_rule(rule);
exit:
    free(mac);
    free(key);
}

static void execute_filter_offset_rule(amxd_object_t* master, devices_merge_rule_t* rule) {
    const char* key = NULL;
    char* mac = NULL;
    char* mac_filtered = NULL;
    amxd_status_t status;
    int64_t number = 0;
    mac = amxd_object_get_cstring_t(master, "PhysAddress", &status);
    when_str_empty_trace(mac, exit, ERROR, "NULL pointer/empty string status: %d", status);

    SAH_TRACEZ_INFO(ME, "Execute filter offset rule\n");
    mac_filtered = merge_apply_filter(mac, rule->merge.mfo->mask, rule->merge.mfo->value, false);
    amxc_var_for_each(offset_var, &rule->merge.mfo->offset) {
        int64_t offset = amxc_var_dyncast(int64_t, offset_var);
        key = merge_apply_offset(mac_filtered, offset);
        SAH_TRACEZ_INFO(ME, "alternative key: %s", key);
        if(merge_set_alternative(master, gmaps_get_device(key))) {
            number++;
        }
        continue;
    }

    if(number < rule->merge.mo->number) {
        goto exit;
    }

    merge_delete_rule(rule);

exit:
    free(mac);
    free(mac_filtered);
}

static const char* merge_apply_shift(const char* mac, devices_merge_shift_type_t type, uint64_t shift) {

    uint64_t num;
    uint64_t tmp;
    char result[18];

    num = merge_macTouint64(mac);
    tmp = num;

    tmp &= shift;
    tmp = (type == devices_merge_shift_type_right) ? (tmp / 2) : (tmp * 2);
    tmp &= shift;

    num &= ~shift;
    num |= tmp;

    snprintf(result, 18, "%02" PRIX64 ":%02" PRIX64 ":%02" PRIX64 ":%02" PRIX64 ":%02" PRIX64 ":%02" PRIX64, (num >> 40) & 0xFF, (num >> 32) & 0xFF, (num >> 24) & 0xFF, (num >> 16) & 0xFF, (num >> 8) & 0xFF, num & 0xFF);

    return (gmaps_uuid_get_key_by_mac(result));
}

static void execute_filter_shift_rule(amxd_object_t* master, devices_merge_rule_t* rule) {
    const char* key = NULL;
    char* mac = NULL;
    char* mac_filtered = NULL;
    devices_merge_shift_type_t shift_type;
    amxd_status_t status;

    shift_type = (rule->type == devices_merge_type_filter_right_shift) ? devices_merge_shift_type_right : devices_merge_shift_type_left;

    mac = amxd_object_get_cstring_t(master, "PhysAddress", &status);
    when_str_empty_trace(mac, exit, ERROR, "NULL pointer/empty string status: %d", status);

    SAH_TRACEZ_INFO(ME, "Execute filter shift rule");
    mac_filtered = merge_apply_filter(mac, rule->merge.mfs->mask, rule->merge.mfs->value, false);

    key = merge_apply_shift(mac_filtered, shift_type, rule->merge.mfs->shift);
    SAH_TRACEZ_INFO(ME, "alternative key: %s", key);
    if(!merge_set_alternative(master, gmaps_get_device(key))) {
        goto exit;
    }
    merge_delete_rule(rule);
exit:
    free(mac);
    free(mac_filtered);
}

static bool create_address_rule(const char* key, amxc_var_t* rule) {
    bool result = false;
    int64_t number = -1;
    const amxc_var_t* address = NULL;
    size_t list_size = 0;
    devices_merge_rule_t* new_rule = NULL;
    char* new_rule_key = NULL;
    devices_merge_address_t* new_rule_merge_ma = NULL;

    /* Number: defines the number of alternatives devices that should be merged by this rule */
    number = GET_INT64(rule, "Number");
    when_false_trace((number > 0), error, ERROR, "Invalid number %" PRId64 "of mac to be merged", number);

    address = GET_ARG(rule, "Address");
    when_null_trace(address, error, ERROR, "Missing address parameter");
    when_false_trace((amxc_var_type_of(address) == AMXC_VAR_ID_LIST), error, ERROR, "Invalid address parameter");
    when_null_trace(amxc_var_get_first(address), error, ERROR, "Invalid address parameter");
    list_size = amxc_llist_size(amxc_var_constcast(amxc_llist_t, address));
    when_false_trace((number <= (int64_t) list_size), error, ERROR, "Invalid address parameter");

    new_rule = calloc(1, sizeof(devices_merge_rule_t));
    when_null_trace(new_rule, error, ERROR, "No MEM failure");
    new_rule_key = strdup(key);
    when_null_trace(new_rule_key, error, ERROR, "No MEM failure");
    new_rule_merge_ma = calloc(1, sizeof(devices_merge_address_t));
    when_null_trace(new_rule_merge_ma, error, ERROR, "No MEM failure");
    new_rule->type = devices_merge_type_address;
    new_rule->key = new_rule_key;
    new_rule->execute = execute_address_rule;
    new_rule->merge.ma = new_rule_merge_ma;
    new_rule->merge.ma->number = number;
    amxc_var_init(&new_rule->merge.ma->address);
    amxc_var_set_type(&new_rule->merge.ma->address, AMXC_VAR_ID_LIST);
    amxc_var_copy(&new_rule->merge.ma->address, address);
    merge_add_rule(new_rule);
    result = true;
    return result;
error:
    free(new_rule_merge_ma);
    free(new_rule_key);
    free(new_rule);
    return result;
}
static bool create_offset_rule(const char* key, amxc_var_t* rule) {
    bool result = false;
    int64_t number = -1;
    const amxc_var_t* offset = NULL;
    size_t list_size = 0;
    devices_merge_rule_t* new_rule = NULL;
    char* new_rule_key = NULL;
    devices_merge_offset_t* new_rule_merge_mo = NULL;

    SAH_TRACEZ_INFO(ME, "create offset rule");

    /* Number: defines the number of alternatives devices that should be merged by this rule */
    number = GET_INT64(rule, "Number");
    when_false_trace((number > 0), error, ERROR, "Invalid number %" PRId64 "of mac to be merged", number);

    offset = GET_ARG(rule, "Offset");
    when_null_trace(offset, error, ERROR, "Missing offset parameter");
    when_false_trace((amxc_var_type_of(offset) == AMXC_VAR_ID_LIST), error, ERROR, "Invalid offset parameter");
    when_null_trace(amxc_var_get_first(offset), error, ERROR, "Invalid offset parameter");
    list_size = amxc_llist_size(amxc_var_constcast(amxc_llist_t, offset));
    when_false_trace((number <= (int64_t) list_size), error, ERROR, "Invalid offset parameter");

    new_rule = calloc(1, sizeof(devices_merge_rule_t));
    when_null_trace(new_rule, error, ERROR, "No MEM failure");
    new_rule_key = strdup(key);
    when_null_trace(new_rule_key, error, ERROR, "No MEM failure");
    new_rule_merge_mo = calloc(1, sizeof(devices_merge_offset_t));
    when_null_trace(new_rule_merge_mo, error, ERROR, "No MEM failure");
    new_rule->type = devices_merge_type_offset;
    new_rule->key = new_rule_key;
    new_rule->execute = execute_offset_rule;
    new_rule->merge.mo = new_rule_merge_mo;
    new_rule->merge.mo->number = number;
    amxc_var_init(&new_rule->merge.mo->offset);
    amxc_var_set_type(&new_rule->merge.mo->offset, AMXC_VAR_ID_LIST);
    amxc_var_copy(&new_rule->merge.mo->offset, offset);
    merge_add_rule(new_rule);
    result = true;
    return result;
error:
    free(new_rule_merge_mo);
    free(new_rule_key);
    free(new_rule);
    return result;
}

static bool create_filter_rule(GMAPS_UNUSED const char* key, amxc_var_t* rule) {
    bool result = false;
    const char* mask = NULL;
    const char* value = NULL;
    uint64_t mask_64 = 0;
    uint64_t value_64 = 0;
    devices_merge_rule_t* new_rule = NULL;
    char* new_rule_key = NULL;
    devices_merge_filter_t* new_rule_merge_mf = NULL;

    SAH_TRACEZ_INFO(ME, "create filter rule");

    mask = GET_CHAR(rule, "Mask");
    when_false_trace(((mask != NULL) && (strlen(mask) == 12)), error, ERROR, "Invalid mask parameter");

    value = GET_CHAR(rule, "Value");
    when_false_trace(((value != NULL) && (strlen(value) == 12)), error, ERROR, "Invalid value parameter");

    new_rule = calloc(1, sizeof(devices_merge_rule_t));
    when_null_trace(new_rule, error, ERROR, "No MEM failure");
    new_rule_key = strdup(key);
    when_null_trace(new_rule_key, error, ERROR, "No MEM failure");
    new_rule_merge_mf = calloc(1, sizeof(devices_merge_filter_t));
    when_null_trace(new_rule_merge_mf, error, ERROR, "No MEM failure");
    new_rule->type = devices_merge_type_filter;
    new_rule->key = new_rule_key;
    new_rule->execute = execute_filter_rule;
    new_rule->merge.mf = new_rule_merge_mf;
    sscanf(mask, "%" SCNx64, &mask_64);
    sscanf(value, "%" SCNx64, &value_64);
    new_rule->merge.mf->value = value_64;
    new_rule->merge.mf->mask = mask_64;
    merge_add_rule(new_rule);
    result = true;
    return result;
error:
    free(new_rule_merge_mf);
    free(new_rule_key);
    free(new_rule);
    return result;
}

static bool create_filter_offset_rule(const char* key,
                                      amxc_var_t* rule) {
    bool result = false;
    const char* mask = NULL;
    const char* value = NULL;
    uint64_t mask_64 = 0;
    uint64_t value_64 = 0;

    int64_t number = -1;
    const amxc_var_t* offset = NULL;
    size_t list_size = 0;
    devices_merge_rule_t* new_rule = NULL;
    char* new_rule_key = NULL;
    devices_merge_filter_offset_t* new_rule_merge_mfo = NULL;

    SAH_TRACEZ_INFO(ME, "create filter offset rule");

    mask = GET_CHAR(rule, "Mask");
    when_false_trace(((mask != NULL) && (strlen(mask) == 12)), error, ERROR, "Invalid mask parameter");

    value = GET_CHAR(rule, "Value");
    when_false_trace(((value != NULL) && (strlen(value) == 12)), error, ERROR, "Invalid value parameter");


    /* Number: defines the number of alternatives devices that should be merged by this rule */
    number = GET_INT64(rule, "Number");
    when_false_trace((number > 0), error, ERROR, "Invalid number %" PRId64 "of mac to be merged", number);

    offset = GET_ARG(rule, "Offset");
    when_null_trace(offset, error, ERROR, "Missing offset parameter");
    when_false_trace((amxc_var_type_of(offset) == AMXC_VAR_ID_LIST), error, ERROR, "Invalid offset parameter");
    when_null_trace(amxc_var_get_first(offset), error, ERROR, "Invalid offset parameter");
    list_size = amxc_llist_size(amxc_var_constcast(amxc_llist_t, offset));
    when_false_trace((number <= (int64_t) list_size), error, ERROR, "Invalid offset parameter");

    new_rule = calloc(1, sizeof(devices_merge_rule_t));
    when_null_trace(new_rule, error, ERROR, "No MEM failure");
    new_rule_key = strdup(key);
    when_null_trace(new_rule_key, error, ERROR, "No MEM failure");
    new_rule_merge_mfo = calloc(1, sizeof(devices_merge_filter_offset_t));
    when_null_trace(new_rule_merge_mfo, error, ERROR, "No MEM failure");
    new_rule->type = devices_merge_type_filter_offset;
    new_rule->key = new_rule_key;
    new_rule->execute = execute_filter_offset_rule;
    new_rule->merge.mfo = new_rule_merge_mfo;
    sscanf(mask, "%" SCNx64, &mask_64);
    sscanf(value, "%" SCNx64, &value_64);
    new_rule->merge.mfo->value = value_64;
    new_rule->merge.mfo->mask = mask_64;
    new_rule->merge.mfo->number = number;
    amxc_var_init(&new_rule->merge.mfo->offset);
    amxc_var_set_type(&new_rule->merge.mfo->offset, AMXC_VAR_ID_LIST);
    amxc_var_copy(&new_rule->merge.mfo->offset, offset);
    merge_add_rule(new_rule);
    result = true;
    return result;
error:
    free(new_rule_merge_mfo);
    free(new_rule_key);
    free(new_rule);
    return result;
}

static bool create_filter_shift_rule(GMAPS_UNUSED const char* key,
                                     GMAPS_UNUSED amxc_var_t* rule,
                                     GMAPS_UNUSED devices_merge_type_t type) {
    bool result = false;
    const char* mask = NULL;
    const char* value = NULL;
    const char* shift = NULL;
    uint64_t mask_64 = 0;
    uint64_t value_64 = 0;
    uint64_t shift_64 = 0;
    devices_merge_rule_t* new_rule = NULL;
    char* new_rule_key = NULL;
    devices_merge_filter_shift_t* new_rule_merge_mfs = NULL;

    SAH_TRACEZ_INFO(ME, "create filter shift rule");

    mask = GET_CHAR(rule, "Mask");
    when_false_trace(((mask != NULL) && (strlen(mask) == 12)), error, ERROR, "Invalid mask parameter");

    value = GET_CHAR(rule, "Value");
    when_false_trace(((value != NULL) && (strlen(value) == 12)), error, ERROR, "Invalid value parameter");

    shift = GET_CHAR(rule, "Shift");
    when_false_trace(((shift != NULL) && (strlen(shift) == 12)), error, ERROR, "Invalid shift parameter");

    new_rule = calloc(1, sizeof(devices_merge_rule_t));
    when_null_trace(new_rule, error, ERROR, "No MEM failure");
    new_rule_key = strdup(key);
    when_null_trace(new_rule_key, error, ERROR, "No MEM failure");
    new_rule_merge_mfs = calloc(1, sizeof(devices_merge_filter_shift_t));
    when_null_trace(new_rule_merge_mfs, error, ERROR, "No MEM failure");
    new_rule->type = type;
    new_rule->key = new_rule_key;
    new_rule->execute = execute_filter_shift_rule;
    new_rule->merge.mfs = new_rule_merge_mfs;
    sscanf(mask, "%" SCNx64, &mask_64);
    sscanf(value, "%" SCNx64, &value_64);
    sscanf(shift, "%" SCNx64, &shift_64);
    new_rule->merge.mfs->value = value_64;
    new_rule->merge.mfs->mask = mask_64;
    new_rule->merge.mfs->shift = shift_64;

    merge_add_rule(new_rule);
    result = true;
    return result;
error:
    free(new_rule_merge_mfs);
    free(new_rule_key);
    free(new_rule);
    return result;

    return false;
}

static void merge_execute_rule(devices_merge_rule_t* rule) {

    amxd_object_t* device = gmaps_get_device(rule->key);
    when_null_trace(device, exit, ERROR, "Failed to find device %s", rule->key);

    if(rule->execute) {
        rule->execute(device, rule);
    } else {
        merge_delete_rule(rule);
    }
exit:
    return;
}

bool gmaps_devices_merge_execute_rules(const char* key) {

    bool has_tag = false;
    bool res = false;

    when_str_empty_trace(key, exit, ERROR, "NULL pointer/empty string");

    gmaps_device_has_tag(key, "mac", "", gmap_traverse_this, &has_tag);
    when_false_trace(has_tag, exit, ERROR, "device %s does not contain mac tag", key);
    when_false_trace(amxc_llist_size(&gmap_devices_merge_rules) != 0, exit, ERROR,
                     "No rules provided for device %s", key);
    // further we don t care about the key, all rules parsed
    amxc_llist_for_each(it, &gmap_devices_merge_rules) {
        devices_merge_rule_t* merge_rule = amxc_container_of(it, devices_merge_rule_t, it);
        merge_execute_rule(merge_rule);
    }
    res = true;
exit:
    return res;
}

bool gmaps_devices_merge_add_rules(const char* key, amxc_var_t* rules) {

    bool has_tag = false;

    when_str_empty_trace(key, error2, ERROR, "NULL pointer/empty string");
    when_null_trace(rules, error2, ERROR, "NULL pointer");

    SAH_TRACEZ_INFO(ME, "add rules for device: %s", key);

    gmaps_devices_merge_delete_rules(key);

    gmaps_device_has_tag(key, "mac", "", gmap_traverse_this, &has_tag);
    when_false_trace(has_tag, error, ERROR, "device %s does not contain mac tag", key);

    when_null_trace(amxc_var_get_first(rules), error, ERROR,
                    "No rules provided for device %s", key);

    amxc_var_for_each(rule, rules) {
        const char* type_c = GET_CHAR(rule, "Type");
        devices_merge_type_t type = merge_get_type(type_c);
        switch(type) {
        case devices_merge_type_address:
            if(!create_address_rule(key, rule)) {
                goto error;
            }
            break;
        case devices_merge_type_offset:
            if(!create_offset_rule(key, rule)) {
                goto error;
            }
            break;

        case devices_merge_type_filter:
            if(!create_filter_rule(key, rule)) {
                goto error;
            }
            break;

        case devices_merge_type_filter_offset:
            if(!create_filter_offset_rule(key, rule)) {
                goto error;
            }
            break;

        case devices_merge_type_filter_right_shift:
        case devices_merge_type_filter_left_shift:
            if(!create_filter_shift_rule(key, rule, type)) {
                goto error;
            }
            break;

        default:
            goto error;
            break;
        }
    }
    SAH_TRACEZ_INFO(ME, "Merge execute rules");
    gmaps_devices_merge_execute_rules(key);
    return true;

error:
    gmaps_devices_merge_delete_rules(key);
error2:
    return false;
}

void gmaps_devices_merge_init(void) {
    amxc_llist_init(&gmap_devices_merge_rules);
}

static void merge_list_it_clean(amxc_llist_it_t* it) {
    devices_merge_rule_t* rule = amxc_container_of(it, devices_merge_rule_t, it);
    merge_delete_rule(rule);
}

void gmaps_devices_merge_cleanup(void) {
    amxc_llist_clean(&gmap_devices_merge_rules, merge_list_it_clean);
}

amxc_llist_t* gmaps_get_devices_merge_rules(void) {
    return &gmap_devices_merge_rules;
}

