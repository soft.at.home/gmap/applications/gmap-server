/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include <ctype.h>

#include "gmaps_priv.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include "gmaps_dm_topologybuilder.h"

#define ME "common"
#define GLOBAL_CONFIG_PATH "Devices.Config.global."


typedef struct _gmap_server {
    amxd_dm_t* gmap_dm;             /**< the gMap data model object */
    amxo_parser_t* parser;          /**< the odl parser */
    amxc_htable_t devices;          /**< k,v = key string, instance object pointer */
    amxc_htable_t blocked_devices;  /**< hash set of device keys that are blocked */

    amxd_object_t* dm_devices;      /**< Handle to the Devices. object */
    amxd_object_t* dm_config;       /**< Handle to the Devices.Config. object */
    amxd_object_t* dm_device_templ; /**< Handle to the Devices.Device. template */
    amxd_object_t* dm_query;        /**< Handle to the Devices.Query. template */
} gmap_server_t;

static gmap_server_t server;
static amxb_subscription_t* interval_timer_subscription = NULL;
static amxp_timer_t* cleanup_timer = NULL;

static void gmap_server_set_config_if_unset(const char* name,
                                            const char* value) {
    amxc_var_t* option = amxo_parser_get_config(gmap_get_parser(), name);
    if(amxc_var_is_null(option)) {
        amxc_var_t value_var;
        amxc_var_init(&value_var);
        amxc_var_set(cstring_t, &value_var, value);
        amxo_parser_set_config(gmap_get_parser(), name, &value_var);
        amxc_var_clean(&value_var);
    }
}

static void gmap_server_populate_config(void) {
    gmap_server_set_config_if_unset("gmap_config_defaults", "/etc/amx/gmap-server/config");
    gmap_server_set_config_if_unset("gmap_config_save", "/etc/config/gmap-server/config");
}

static void gmap_server_create_savedir(void) {
    amxc_string_t* savedir = NULL;
    amxc_string_new(&savedir, 0);
    struct stat st;

    memset(&st, 0, sizeof(st));

    amxc_string_append(savedir, "${gmap_save_path}", 17);
    amxc_string_resolve(savedir, gmap_get_config());

    if(stat(amxc_string_get(savedir, 0), &st) == -1) {
        mkdir(amxc_string_get(savedir, 0), S_IRWXU);
    }

    amxc_string_delete(&savedir);
}

static void gmaps_clear_blocked_device(GMAPS_UNUSED const char* key,
                                       amxc_htable_it_t* it) {
    free(it);
}

static int gmaps_rebuild_devices_table_entry(GMAPS_UNUSED amxd_object_t* object, amxd_object_t* mobject, GMAPS_UNUSED void* priv) {
    when_null(mobject, exit);
    when_false(amxd_object_get_type(mobject) == amxd_object_instance, exit);
    gmaps_store_device(mobject);
exit:
    return 0;
}

static void gmaps_rebuild_devices_table(void) {
    amxd_object_for_all(server.dm_device_templ, ".[Alias != ''].", gmaps_rebuild_devices_table_entry, NULL);
}

static void cleanup_inactive_interval_cb(GMAPS_UNUSED amxp_timer_t* timer, GMAPS_UNUSED void* priv) {
    uint32_t max_inactive_time = 0;
    uint32_t max_inactive_time_treshold = 0;
    uint32_t lan_devs = 0;
    uint32_t treshold = 0;
    amxc_var_t max_time;
    amxc_var_t max_time_treshold;
    amxc_var_t treshold_var;
    amxc_var_t nr_of_lan_devs;
    gmap_status_t status = gmap_status_unknown_error;

    amxc_var_init(&max_time);
    amxc_var_init(&max_time_treshold);
    amxc_var_init(&treshold_var);
    amxc_var_init(&nr_of_lan_devs);

    when_null_trace(server.dm_config, exit, ERROR, "can not get config obj");

    status = gmaps_get_config(server.dm_config, "global", "MaxInactiveTime", &max_time);
    when_failed_trace(status, exit, ERROR, "can not get MaxInactiveTime from Config");
    max_inactive_time = amxc_var_dyncast(uint32_t, &max_time);

    status = gmaps_get_config(server.dm_config, "global", "MaxInactiveTimeTreshold", &max_time_treshold);
    when_failed_trace(status, exit, ERROR, "can not get MaxInactiveTimeTreshold from Config");
    max_inactive_time_treshold = amxc_var_dyncast(uint32_t, &max_time_treshold);

    status = gmaps_get_config(server.dm_config, "global", "InactiveCheckThreshold", &treshold_var);
    when_failed_trace(status, exit, ERROR, "can not get InactiveCheckThreshold from Config");
    treshold = amxc_var_dyncast(uint32_t, &treshold_var);

    when_failed_trace(amxd_object_get_param(server.dm_devices, "LANDeviceNumberOfEntries", &nr_of_lan_devs),
                      exit, ERROR, "Could not fetch Devices.LANDeviceNumberOfEntries");
    lan_devs = amxc_var_dyncast(uint32_t, &nr_of_lan_devs);

    if(max_inactive_time != 0) {
        remove_inactive_devices(max_inactive_time, NULL);
    }
    if((lan_devs >= treshold) && (treshold != 0) &&
       (max_inactive_time_treshold != 0) &&
       ((max_inactive_time_treshold < max_inactive_time) ||
        (max_inactive_time == 0))) {
        remove_inactive_devices(max_inactive_time_treshold, NULL);
    }

exit:
    amxc_var_clean(&max_time);
    amxc_var_clean(&max_time_treshold);
    amxc_var_clean(&treshold_var);
    amxc_var_clean(&nr_of_lan_devs);
}

static void timer_change(UNUSED const char* const sig_name,
                         const amxc_var_t* const data,
                         UNUSED void* const priv) {
    uint32_t interval = GETP_UINT32(data, "parameters.InactiveCheckInterval.to");

    when_true((interval == 0), exit);

    when_failed_trace(amxp_timer_stop(cleanup_timer), exit, ERROR, "Could not stop timer for 'cleanup_inactive_interval_cb', to update value");
    when_failed_trace(amxp_timer_set_interval(cleanup_timer, interval * 1000), exit, ERROR, "Could not set new interval-timer value for 'cleanup_inactive_interval_cb'");
    when_failed_trace(amxp_timer_start(cleanup_timer, interval * 1000), exit, ERROR, "Could not restart the timer with new value for 'cleanup_inactive_interval_cb'");
exit:
    return;
}

static void start_inactive_cleanup_timer(void) {
    uint32_t timer_msec = 0;
    int ret = 0;
    amxc_var_t interval_time;
    gmap_status_t status = gmap_status_unknown_error;
    amxb_bus_ctx_t* gmap_ctx = NULL;
    const char* expr = "notification == 'dm:object-changed' && path == '" GLOBAL_CONFIG_PATH "'";

    amxc_var_init(&interval_time);

    when_null_trace(server.dm_config, exit, ERROR, "can not get config obj");

    status = gmaps_get_config(server.dm_config, "global", "InactiveCheckInterval", &interval_time);
    when_failed_trace(status, exit, ERROR, "can not get InactiveCheckInterval from Config");
    timer_msec = (amxc_var_dyncast(uint32_t, &interval_time) * 1000);

    amxp_timer_new(&cleanup_timer, cleanup_inactive_interval_cb, NULL);
    when_failed_trace(amxp_timer_set_interval(cleanup_timer, timer_msec), exit, ERROR, "Could not start the interval-timer for 'cleanup_inactive_interval_cb'");
    when_failed_trace(amxp_timer_start(cleanup_timer, timer_msec), exit, ERROR, "Could not start the timer for 'cleanup_inactive_interval_cb'");

    gmap_ctx = amxb_be_who_has(GLOBAL_CONFIG_PATH);
    when_null_trace(gmap_ctx, exit, ERROR, "Could not find '%s'", GLOBAL_CONFIG_PATH);

    ret = amxb_subscription_new(&interval_timer_subscription, gmap_ctx, GLOBAL_CONFIG_PATH, expr, timer_change, NULL);
    when_failed_trace(ret, exit, ERROR, "Could not subscribe to the '%s' object", GLOBAL_CONFIG_PATH);

exit:
    amxc_var_clean(&interval_time);
}

static void stop_inactive_cleanup_timer(void) {
    amxp_timer_stop(cleanup_timer);
    amxp_timer_delete(&cleanup_timer);
    amxb_subscription_delete(&interval_timer_subscription);
}

void _gmap_server_start(GMAPS_UNUSED const char* const sig_name,
                        GMAPS_UNUSED const amxc_var_t* const data,
                        GMAPS_UNUSED void* const priv) {
    gmaps_rebuild_devices_table();
    start_inactive_cleanup_timer();
    gmaps_dm_tpb_init();
}

void gmap_server_init(amxd_dm_t* dm,
                      amxo_parser_t* parser) {
    amxc_htable_init(&server.devices, 10);
    amxc_htable_init(&server.blocked_devices, 10);

    server.gmap_dm = dm;
    server.parser = parser;

    gmap_client_set_mib_server_dm(dm);

    gmap_server_populate_config();
    gmap_server_create_savedir();
    server.dm_devices = amxd_dm_get_object(gmap_get_dm(), "Devices");
    server.dm_config = amxd_object_get_child(server.dm_devices, "Config");
    server.dm_device_templ = amxd_object_get_child(server.dm_devices, "Device");
    server.dm_query = amxd_object_get_child(server.dm_devices, "Query");

    if(server.dm_devices == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to find Devices. root object");
    }
    if(server.dm_config == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to find Devices.Config. object");
    }
    if(server.dm_device_templ == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to find Devices.Device. object");
    }
    if(server.dm_query == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to find Devices.Query. object");
    }

    gmaps_new_config(server.dm_config, "global");
    gmaps_load_config(NULL, "global");

    gmaps_query_init();
    gmaps_device_name_table_init();
    gmaps_devices_merge_init();

    gmaps_device_match_init();

    gmaps_time_init();
}

void gmap_server_cleanup(void) {
    amxc_htable_clean(&server.devices, gmaps_clear_device);
    amxc_htable_clean(&server.blocked_devices, gmaps_clear_blocked_device);
    gmaps_device_name_table_cleanup();
    gmaps_devices_merge_cleanup();
    gmaps_query_cleanup();
    gmaps_dm_tpb_cleanup();
    stop_inactive_cleanup_timer();
    gmaps_time_cleanup();
    server.parser = NULL;
    server.gmap_dm = NULL;
    server.dm_devices = NULL;
    server.dm_config = NULL;
    server.dm_device_templ = NULL;
    server.dm_query = NULL;
}

amxd_dm_t* gmap_get_dm(void) {
    return server.gmap_dm;
}

amxo_parser_t* gmap_get_parser(void) {
    return server.parser;
}

amxc_var_t* gmap_get_config(void) {
    return &(server.parser->config);
}

amxc_htable_t* gmap_get_table(void) {
    return &server.devices;
}

amxc_htable_t* gmap_get_blocked_devices(void) {
    return &server.blocked_devices;
}

amxd_object_t* GMAPS_PRIVATE gmap_dm_get_devices(void) {
    return server.dm_devices;
}

amxd_object_t* GMAPS_PRIVATE gmap_dm_get_devices_device(void) {
    return server.dm_device_templ;
}

amxd_object_t* GMAPS_PRIVATE gmap_dm_get_devices_query(void) {
    return server.dm_query;
}

amxd_object_t* GMAPS_PRIVATE gmap_dm_get_devices_config(void) {
    return server.dm_config;
}


static bool s_is_hex_nb(const char* str, char expected_end) {
    return isxdigit(str[0]) && isxdigit(str[1]) && str[2] == expected_end;
}

bool gmaps_common_is_mac(const char* str) {
    if(str == NULL) {
        return false;
    }
    for(int i = 0; i < 5; i++) {
        if(!s_is_hex_nb(&str[i * 3], ':')) {
            return false;
        }
    }
    return s_is_hex_nb(&str[15], '\0');
}

/**
 *
 * Preconditions:
 *   - `tag` and 'taglist' only contains "normal" characters (i.e. a-zA-Z0-9_- ), so no '%' or '\''.
 *   - `tag` is not empty
 */
bool gmaps_common_contains_tag(const ssv_string_t taglist, const char* tag) {
    amxc_set_t* parsed = gmaps_device_tag_parse(taglist);
    bool retval = amxc_set_has_flag(parsed, tag);

    amxc_set_delete(&parsed);

    return retval;
}

/**
 * Checks if the max amount of gmap-devices is reached.
 * If so, it tries to remove the oldes inactive device.
 * If no inactive devices are present, it returns false.
 */
bool check_max_device_entries(void) {
    uint32_t max_devices = 0;
    uint32_t nr_of_devices = 0;
    bool retval = false;
    gmap_status_t status = gmap_status_unknown_error;
    amxd_object_t* oldest_inactive_device = NULL;
    amxc_ts_t oldest = { 0 };
    amxc_var_t value;

    amxc_var_init(&value);

    when_null_trace(server.dm_devices, exit, ERROR, "can not get 'Devices' obj");
    when_null_trace(server.dm_config, exit, ERROR, "can not get config obj");

    when_failed_trace(amxd_object_get_param(server.dm_devices, "DeviceNumberOfEntries", &value),
                      exit, ERROR, "Could not fetch Devices.DeviceNumberOfEntries");
    nr_of_devices = amxc_var_dyncast(uint32_t, &value);

    status = gmaps_get_config(server.dm_config, "global", "MaxDevices", &value);
    when_failed_trace(status, exit, ERROR, "can not get MaxDevices from Config");
    max_devices = amxc_var_dyncast(uint32_t, &value);

    when_false_status(nr_of_devices >= max_devices, exit, retval = true);

    amxd_object_iterate(instance, it, gmap_dm_get_devices_device()) {
        amxd_object_t* dev = amxc_llist_it_get_data(it, amxd_object_t, it);
        amxc_var_t current_last_connection;
        const amxc_ts_t* current = NULL;

        if(gmaps_device_tag_has(dev, "protected") ||
           (gmaps_device_tag_has(dev, "lan") && gmaps_device_tag_has(dev, "physical")) ||
           gmaps_device_is_active(dev)) {
            continue;
        }

        amxc_var_init(&current_last_connection);
        amxd_object_get_param(dev, "LastConnection", &current_last_connection);
        current = amxc_var_constcast(amxc_ts_t, &current_last_connection);

        when_null(current, next_item);

        if((oldest_inactive_device == NULL) || (oldest.sec > current->sec)) {
            oldest_inactive_device = dev;
            oldest = *current;
        }

next_item:
        amxc_var_clean(&current_last_connection);
    }

    when_null_trace(oldest_inactive_device, exit, WARNING, "No inactive devices present. New device can not be added.");

    gmaps_delete_device(server.dm_device_templ, oldest_inactive_device->name);

    retval = true;

exit:
    amxc_var_clean(&value);
    return retval;
}

/*
 * Check if the maximum amount of devices is reached.
 * If so, it removes all the devices that are inactive for longer than MaxInactiveTimeTreshold.
 * If no devices are removed the functions returns false.
 */
bool check_max_lan_devices_entries(void) {
    uint32_t max_devices = 0;
    uint32_t max_seconds = 0;
    uint32_t nr_of_lan_devices = 0;
    bool retval = false;
    gmap_status_t status = gmap_status_unknown_error;
    uint32_t nr_of_removed_devices = 0;
    amxc_var_t value;

    amxc_var_init(&value);

    when_null_trace(server.dm_devices, exit, ERROR, "can not get 'Devices' obj");
    when_null_trace(server.dm_config, exit, ERROR, "can not get config obj");

    when_failed_trace(amxd_object_get_param(server.dm_devices, "LANDeviceNumberOfEntries", &value),
                      exit, ERROR, "Could not fetch Devices.LANDeviceNumberOfEntries");
    nr_of_lan_devices = amxc_var_dyncast(uint32_t, &value);

    status = gmaps_get_config(server.dm_config, "global", "MaxLanDevices", &value);
    when_failed_trace(status, exit, ERROR, "can not get MaxLanDevices from Config");
    max_devices = amxc_var_dyncast(uint32_t, &value);

    status = gmaps_get_config(server.dm_config, "global", "MaxInactiveTimeTreshold", &value);
    when_failed_trace(status, exit, ERROR, "can not get MaxInactiveTimeTreshold from Config");
    max_seconds = amxc_var_dyncast(uint32_t, &value);

    if(nr_of_lan_devices < max_devices) {
        retval = true;
    } else if(remove_inactive_devices(max_seconds, &nr_of_removed_devices)) {
        retval = nr_of_removed_devices > 0;
    }

exit:
    amxc_var_clean(&value);
    return retval;
}

/*
 * This function removes all lan devices without the 'protected' tag, inactive for longer than 'min_inactive'
 * nr_of_removed_devices should be empty, but initialised when called.
 */
bool remove_inactive_devices(uint32_t min_inactive, uint32_t* out_nr_of_removed_devices) {
    amxc_llist_t dev_list;
    amxc_ts_t now = {0, 0, 0};
    bool retval = true;
    uint32_t nr_of_removed = 0;
    bool ntp_synchronized = gmap_is_ntp_synchronized();

    amxc_llist_init(&dev_list);

    if(ntp_synchronized) {
        amxc_ts_now(&now);
        now.sec -= min_inactive;
    } else if(min_inactive != 0) {
        SAH_TRACEZ_ERROR(ME, "Cannot remove oldest devices as Time is not synchronised and minimumInactiveInterval is not '0'");
        retval = false;
        goto exit;
    }

    amxd_object_iterate(instance, it, gmap_dm_get_devices_device()) {
        amxd_object_t* dev = amxc_llist_it_get_data(it, amxd_object_t, it);
        bool remove = true;


        if(gmaps_device_tag_has(dev, "protected") ||
           gmaps_device_tag_has(dev, "self") ||
           !(gmaps_device_tag_has(dev, "lan") && gmaps_device_tag_has(dev, "physical")) ||
           gmaps_device_is_active(dev)) {
            continue;
        }

        if(ntp_synchronized) {
            const amxc_ts_t* connection_time = NULL;
            amxc_var_t last_connection;
            amxc_var_init(&last_connection);
            amxd_object_get_param(dev, "LastConnection", &last_connection);
            connection_time = amxc_var_constcast(amxc_ts_t, &last_connection);

            if(amxc_ts_compare(connection_time, &now) == 1) {
                remove = false;
            }

            amxc_var_clean(&last_connection);
        }
        if(remove) {
            gmap_device_priv_data_t* priv = gmaps_device_private_data(dev);
            when_null_trace(priv, next_device, ERROR, "BUG: Device has no private data");
            when_true_trace(priv->traversal.llist != NULL, next_device, ERROR, "BUG: Device is being traversed");
            amxc_llist_append(&dev_list, &priv->traversal);
        }

next_device:
        continue;
    }

    amxc_llist_for_each(it, &dev_list) {
        gmap_device_priv_data_t* priv = amxc_llist_it_get_data(it, gmap_device_priv_data_t, traversal);
        amxd_object_t* dev = gmaps_device_from_private_data(priv);
        gmap_status_t status;

        amxc_llist_it_take(it);

        if(dev == NULL) {
            SAH_TRACEZ_ERROR(ME, "BUG: device from private device data is NULL");
            continue;
        }

        status = gmaps_delete_device(server.dm_device_templ, dev->name);
        if(status != gmap_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Could not remove inactive device '%s'", dev->name);
        } else {
            nr_of_removed++;
        }
    }

    if(out_nr_of_removed_devices != NULL) {
        *out_nr_of_removed_devices = nr_of_removed;
    }

exit:
    amxc_llist_clean(&dev_list, NULL);
    return retval;
}

/**
 * Increase the value of Devices.LANDeviceNumberOfEntries
 */
void increase_lan_device_count(void) {
    amxc_var_t value;
    uint32_t nr_of_lan_devices = 0;

    amxc_var_init(&value);

    when_failed_trace(amxd_object_get_param(server.dm_devices, "LANDeviceNumberOfEntries", &value),
                      exit, ERROR, "Could not fetch Devices.LANDeviceNumberOfEntries");
    nr_of_lan_devices = amxc_var_dyncast(uint32_t, &value) + 1;
    amxc_var_set(uint32_t, &value, nr_of_lan_devices);

    when_failed_trace(amxd_object_set_param(server.dm_devices, "LANDeviceNumberOfEntries", &value),
                      exit, ERROR, "Could not increase the value of 'LANDeviceNumberOfEntries'");

exit:
    amxc_var_clean(&value);
    return;
}

/**
 * Decrease the value of Devices.LANDeviceNumberOfEntries
 */
void decrease_lan_device_count(void) {
    amxc_var_t value;
    uint32_t nr_of_lan_devices = 0;

    amxc_var_init(&value);

    when_failed_trace(amxd_object_get_param(server.dm_devices, "LANDeviceNumberOfEntries", &value),
                      exit, ERROR, "Could not fetch Devices.LANDeviceNumberOfEntries");
    nr_of_lan_devices = amxc_var_dyncast(uint32_t, &value);

    if(nr_of_lan_devices != 0) {
        nr_of_lan_devices--;
        amxc_var_set(uint32_t, &value, nr_of_lan_devices);
        when_failed_trace(amxd_object_set_param(server.dm_devices, "LANDeviceNumberOfEntries", &value),
                          exit, ERROR, "Could not decrease the value of 'LANDeviceNumberOfEntries'");
    } else {
        SAH_TRACEZ_ERROR(ME, "'LANDeviceNumberOfEntries' was already '0' when another decrease was requested");
    }

exit:
    amxc_var_clean(&value);
    return;
}
