/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include "gmaps_priv.h"


gmap_status_t gmaps_device_set_alternative(amxd_object_t* device, amxd_object_t* alt) {
    uint32_t retv = gmap_status_ok;
    amxd_object_t* alternatives = NULL;
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    alternatives = amxd_object_get_child(device, "Alternative");
    const char* altname = amxd_object_get_name(alt, AMXD_OBJECT_NAMED);
    const char* mastername = amxd_object_get_name(device, AMXD_OBJECT_NAMED);

    when_null_status(altname, exit, retv = gmap_status_device_not_found);
    when_null_status(device, exit, retv = gmap_status_invalid_parameter);
    when_null_status(alternatives, exit, retv = gmap_status_internal_error);

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, alternatives);
    amxd_trans_add_inst(&trans, 0, altname);
    amxd_trans_select_object(&trans, alt);
    amxd_trans_set_cstring_t(&trans, "Master", mastername);

    when_failed_status(amxd_trans_apply(&trans, gmap_get_dm()), exit, retv = gmap_status_internal_error);

exit:
    amxd_trans_clean(&trans);

    return retv;
}

gmap_status_t gmaps_device_remove_alternative(amxd_object_t* device, amxd_object_t* altdevice) {
    uint32_t retv = gmap_status_device_not_found;
    amxd_object_t* alternatives = NULL;
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    alternatives = amxd_object_get_child(device, "Alternative");

    when_null_status(device, exit, retv = gmap_status_invalid_parameter);
    when_null_status(alternatives, exit, retv = gmap_status_internal_error);
    when_null_status(altdevice, exit, retv = gmap_status_invalid_parameter);

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, alternatives);
    amxd_trans_del_inst(&trans, 0, amxd_object_get_name(altdevice, AMXD_OBJECT_NAMED));
    amxd_trans_select_object(&trans, altdevice);
    amxd_trans_set_cstring_t(&trans, "Master", "");

    when_failed_status(amxd_trans_apply(&trans, gmap_get_dm()), exit, retv = gmap_status_internal_error);

    retv = gmap_status_ok;

exit:
    amxd_trans_clean(&trans);

    return retv;

}

/**
 * Returns true if the @param device is an alternative device.
 * An alternative device is a device for which a master has been set (other than self).
 */
bool gmaps_device_is_alternative(amxd_object_t* device) {
    bool retval = false;
    char* master = amxd_object_get_value(cstring_t, device, "Master", NULL);
    const char* key = amxd_object_get_name(device, AMXD_OBJECT_NAMED);
    if((master != NULL) && (*master != 0) && (strcmp(key, master) != 0)) {
        retval = true;
    }
    free(master);
    return retval;
}

/**
 * Helper function to check if altdevice is alternative from device in one direction
 */
bool gmaps_device_is_alternative_from(amxd_object_t* device, amxd_object_t* altdevice) {
    amxd_object_t* alternatives = NULL;

    when_null(device, exit);
    when_null(altdevice, exit);

    alternatives = amxd_object_get_child(device, "Alternative");

    when_null(alternatives, exit);

    amxd_object_for_each(instance, lit, alternatives) {
        amxd_object_t* alt = amxc_llist_it_get_data(lit, amxd_object_t, it);
        if(strcmp(amxd_object_get_name(alt, AMXD_OBJECT_NAMED), amxd_object_get_name(altdevice, AMXD_OBJECT_NAMED)) == 0) {
            return true;
        }
    }

exit:

    return false;
}

/**
 * Returns true if there is an alternative device relationship
 * between @param device1 and @param device2.
 */
bool gmaps_device_are_alternatives(amxd_object_t* device1,
                                   amxd_object_t* device2) {
    bool retv = false;

    when_null(device1, exit);
    when_null(device2, exit);

    if(gmaps_device_is_alternative_from(device1, device2)) {
        retv = true;
    } else if(gmaps_device_is_alternative_from(device2, device1)) {
        retv = true;
    }

exit:

    return retv;
}

/**
 * Calls @param fn for every alternative device of @param device.
 * The loop can be stopped early by returning a non-zero value in @param cb.
 * @return Zero if all alternatives were handled, else the value returned in @param cb
 * when the loop was stopped early.
 */
uint32_t gmaps_device_for_all_alternatives(amxd_object_t* device,
                                           gmaps_device_task_fn_t fn,
                                           void* priv) {
    uint32_t retval = 0;
    amxd_object_t* alternatives = NULL;

    alternatives = amxd_object_get_child(device, "Alternative");

    amxd_object_for_each(instance, lit, alternatives) {
        amxd_object_t* altdevice = amxc_llist_it_get_data(lit, amxd_object_t, it);
        altdevice = gmaps_get_device(amxd_object_get_name(altdevice, AMXD_OBJECT_NAMED));
        retval = fn(altdevice, priv);
        when_failed(retval, exit);
    }

exit:
    return retval;
}
