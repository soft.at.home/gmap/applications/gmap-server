/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "gmaps_topologybuilder.h"
#include "gmaps_dm_topologybuilder.h"
#include <stdlib.h>
#include <string.h>

#include "gmaps_priv.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include "gmaps_devices_link.h"

#define ME "link"

#include <amxc/amxc_aqueue.h>
#include <amxc/amxc.h>


/** if a pointer to an edge becomes invalid because the edge is removed, but NULL has another meaning, a pointer to this edge can be used. */
static gmaps_tpb_edge_t invalid_edge_placeholder;

/**
 * Edge between two nodes in the graph.
 *
 * The reason we use a normal datastructure here (instead of using the datamodel)
 * is because using the datamodel had severe performance problems.
 */
struct _gmaps_tpb_edge {
    /** bookkeeping for this_edge.upper_node.edges_down */
    amxc_llist_it_t it_of_node_up;
    /** bookkeeping for this_edge.lower_node.edges_up */
    amxc_llist_it_t it_of_node_down;
    /** bookkeeping for gmaps_tpb_t.edges. */
    amxc_llist_it_t it_all;
    /** node this edge points from */
    gmaps_tpb_node_t* lower_node;
    /** node this edge points to */
    gmaps_tpb_node_t* upper_node;
    void* userdata;
};

/**
 * Node in the graph.
 *
 * The reason we use a normal datastructure here (instead of using the datamodel)
 * is because using the datamodel had severe performance problems.
 */
struct _gmaps_tpb_node {
    /** bookkeeping f or gmaps_tpb_t.nodes */
    amxc_llist_it_t it_all;
    /** Devices.Device.[] instance object */
    amxd_object_t* dev_obj;
    /** elements of type gmaps_tpb_edge_t. */
    amxc_llist_t edges_up;
    amxc_llist_t edges_down;
    /** To reduce callbacks of edge selection, cache selected edge.
     * Is `invalid_edge_placeholder` in case the previously selected edge does not exist anymore. */
    gmaps_tpb_edge_t* selected_edge;
    int32_t distance;
    void* userdata;
};

/**
 * Bookkeeping for building topology from graph.
 *
 * The reason we use a normal datastructure here (instead of using the datamodel)
 * is because using the datamodel had severe performance problems.
 */
struct _gmaps_tpb {
    /** element of type gmaps_tpb_edge_t. */
    amxc_llist_t edges;
    /** elements are of type node_t. */
    amxc_llist_t nodes;
};

static gmaps_tpb_edge_t* s_choose_link(const gmaps_tpb_node_t* node) {
    int32_t max = -1;
    gmaps_tpb_edge_t* max_edge = NULL;
    amxc_llist_for_each(it, &node->edges_up) {
        gmaps_tpb_edge_t* candidate = amxc_llist_it_get_data(it, gmaps_tpb_edge_t, it_of_node_down);
        if(candidate == NULL) {
            SAH_TRACEZ_ERROR(ME, "NULL - CRITICAL BUG");
        } else if(candidate->upper_node->distance > max) {
            max = candidate->upper_node->distance;
            max_edge = candidate;
        }
    }
    return max_edge;
}

static void s_select_all(gmaps_tpb_t* topologybuilder) {
    amxc_llist_for_each(it, &topologybuilder->nodes) {
        gmaps_tpb_node_t* node = amxc_llist_it_get_data(it, gmaps_tpb_node_t, it_all);
        gmaps_tpb_edge_t* edge = s_choose_link(node);
        if(node->selected_edge != edge) {
            node->selected_edge = edge;
            if(edge != NULL) {
                gmaps_dm_tpb_on_link_selected(edge);
            } else {
                gmaps_dm_tpb_on_no_link_selected(node);
            }
        }
    }
}

static void s_update_all_distances(gmaps_tpb_t* topologybuilder) {
    size_t nb_edges = amxc_llist_size(&topologybuilder->edges);
    for(size_t i = 0; i < nb_edges; i++) {
        bool updated = false;
        amxc_llist_for_each(it, &topologybuilder->edges) {
            gmaps_tpb_edge_t* edge = amxc_llist_it_get_data(it, gmaps_tpb_edge_t, it_all);
            int32_t current_distance = edge->lower_node->distance;
            int32_t candidate_distance = edge->upper_node->distance + 1;
            if(candidate_distance > current_distance) {
                edge->lower_node->distance = candidate_distance;
                updated = true;
            }
        }
        if(!updated) {
            break;
        }
    }
}

static void s_clear_all_distances(gmaps_tpb_t* topologybuilder) {
    amxc_llist_for_each(it, &topologybuilder->nodes) {
        gmaps_tpb_node_t* node = amxc_llist_it_get_data(it, gmaps_tpb_node_t, it_all);
        node->distance = 0;
    }
}

gmaps_tpb_node_t* gmaps_tpb_node_new(gmaps_tpb_t* topologybuilder, void* userdata) {
    gmaps_tpb_node_t* node = NULL;
    when_null_trace(topologybuilder, exit, ERROR, "NULL argument");

    node = calloc(1, sizeof(gmaps_tpb_node_t));
    when_null_trace(node, exit, ERROR, "Out of mem");
    node->userdata = userdata;
    amxc_llist_init(&node->edges_up);
    amxc_llist_init(&node->edges_down);
    amxc_llist_append(&topologybuilder->nodes, &node->it_all);

exit:
    return node;
}

void gmaps_tpb_node_delete(gmaps_tpb_node_t** node) {
    gmaps_tpb_node_t* node_ptr = NULL;
    when_null(node, exit);

    node_ptr = *node;
    when_null(node_ptr, exit);

    amxc_llist_it_take(&node_ptr->it_all);
    amxc_llist_for_each(it, &node_ptr->edges_up) {
        gmaps_tpb_edge_t* edge = amxc_llist_it_get_data(it, gmaps_tpb_edge_t, it_of_node_down);
        gmaps_tpb_edge_delete(&edge);
    }
    amxc_llist_for_each(it, &node_ptr->edges_down) {
        gmaps_tpb_edge_t* edge = amxc_llist_it_get_data(it, gmaps_tpb_edge_t, it_of_node_up);
        gmaps_tpb_edge_delete(&edge);
    }

    gmaps_dm_tpb_on_node_delete(node_ptr);

    free(node_ptr);
    *node = NULL;

exit:
    return;
}

gmaps_tpb_edge_t* gmaps_tpb_edge_new(gmaps_tpb_t* topologybuilder, gmaps_tpb_node_t* upper_node,
                                     gmaps_tpb_node_t* lower_node, void* userdata) {

    gmaps_tpb_edge_t* edge = NULL;
    when_null_trace(topologybuilder, exit, ERROR, "NULL argument");
    when_null_trace(upper_node, exit, ERROR, "NULL argument");
    when_null_trace(lower_node, exit, ERROR, "NULL argument");

    edge = calloc(1, sizeof(gmaps_tpb_edge_t));
    when_null_trace(edge, exit, ERROR, "Out of mem");
    edge->userdata = userdata;
    edge->upper_node = upper_node;
    edge->lower_node = lower_node;
    amxc_llist_append(&upper_node->edges_down, &edge->it_of_node_up);
    amxc_llist_append(&lower_node->edges_up, &edge->it_of_node_down);
    amxc_llist_append(&topologybuilder->edges, &edge->it_all);

exit:
    return edge;
}

void gmaps_tpb_edge_delete(gmaps_tpb_edge_t** edge) {
    gmaps_tpb_edge_t* edge_ptr = NULL;
    when_null(edge, exit);

    edge_ptr = *edge;
    when_null(edge_ptr, exit);

    if(edge_ptr->lower_node->selected_edge == edge_ptr) {
        edge_ptr->lower_node->selected_edge = &invalid_edge_placeholder;
    }

    gmaps_dm_tpb_on_edge_delete(edge_ptr);

    amxc_llist_it_take(&edge_ptr->it_of_node_down);
    amxc_llist_it_take(&edge_ptr->it_of_node_up);
    amxc_llist_it_take(&edge_ptr->it_all);

    free(edge_ptr);
    *edge = NULL;
exit:
    return;
}

/** @implements amxc_llist_it_delete_t */
static void s_edge_delete_by_it(amxc_llist_it_t* it) {
    gmaps_tpb_edge_t* edge = amxc_llist_it_get_data(it, gmaps_tpb_edge_t, it_all);
    gmaps_tpb_edge_delete(&edge);
}

/** @implements amxc_llist_it_delete_t */
static void s_node_delete_by_it(amxc_llist_it_t* it) {
    gmaps_tpb_node_t* node = amxc_llist_it_get_data(it, gmaps_tpb_node_t, it_all);
    gmaps_tpb_node_delete(&node);
}

gmaps_tpb_t* gmaps_tpb_new(void) {
    gmaps_tpb_t* topologybuilder = (gmaps_tpb_t*) calloc(1, sizeof(gmaps_tpb_t));
    when_null_trace(topologybuilder, exit, ERROR, "Out of mem");
    amxc_llist_init(&topologybuilder->edges);
    amxc_llist_init(&topologybuilder->nodes);

exit:
    return topologybuilder;
}

void gmaps_tpb_delete(gmaps_tpb_t** topologybuilder) {
    when_null(topologybuilder, exit);
    when_null(*topologybuilder, exit);
    amxc_llist_clean(&(*topologybuilder)->edges, s_edge_delete_by_it);
    amxc_llist_clean(&(*topologybuilder)->nodes, s_node_delete_by_it);
    free(*topologybuilder);
    *topologybuilder = NULL;

exit:
    return;
}

void gmaps_tpb_update(gmaps_tpb_t* topologybuilder) {
    when_null_trace(topologybuilder, exit, ERROR, "NULL argument");
    s_clear_all_distances(topologybuilder);
    s_update_all_distances(topologybuilder);
    s_select_all(topologybuilder);
exit:
    return;
}

gmaps_tpb_node_t* gmaps_tpb_edge_get_upper_node(gmaps_tpb_edge_t* edge) {
    when_null_trace(edge, error, ERROR, "NULL argument");
    return edge->upper_node;
error:
    return NULL;
}

gmaps_tpb_node_t* gmaps_tpb_edge_get_lower_node(gmaps_tpb_edge_t* edge) {
    when_null_trace(edge, error, ERROR, "NULL argument");
    return edge->lower_node;
error:
    return NULL;
}

void* gmaps_tpb_node_userdata(gmaps_tpb_node_t* node) {
    when_null_trace(node, error, ERROR, "NULL argument");
    return node->userdata;
error:
    return NULL;
}

void* gmaps_tpb_edge_userdata(gmaps_tpb_edge_t* edge) {
    when_null_trace(edge, error, ERROR, "NULL argument");
    return edge->userdata;
error:
    return NULL;
}
