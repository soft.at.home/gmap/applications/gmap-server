/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "gmaps_dm_topologybuilder.h"
#include "gmaps_topologybuilder.h"
#include <stdlib.h>
#include <string.h>

#include "gmaps_priv.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include "gmaps_devices_link.h"
#include <amxd/amxd_action.h>

#define ME "link"

#include <amxc/amxc_aqueue.h>
#include <amxc/amxc.h>

typedef struct {
    gmaps_tpb_t* topologybuilder;
} gmaps_topologybuilder_dm_t;
static gmaps_topologybuilder_dm_t* s_topologybuilder_dm;

static gmaps_topologybuilder_dm_t* s_get_topologybuilder_dm (void) {
    if(s_topologybuilder_dm != NULL) {
        return s_topologybuilder_dm;
    }
    s_topologybuilder_dm = (gmaps_topologybuilder_dm_t*) calloc(1, sizeof(gmaps_topologybuilder_dm_t));
    when_null_trace(s_topologybuilder_dm, exit, ERROR, "Out of mem");
    s_topologybuilder_dm->topologybuilder = gmaps_tpb_new();

exit:
    return s_topologybuilder_dm;
}

static gmaps_tpb_t* s_get_tpb(void) {
    gmaps_topologybuilder_dm_t* topologybuilder_dm = s_get_topologybuilder_dm();
    when_null_trace(topologybuilder_dm, error, ERROR, "NULL");
    return topologybuilder_dm->topologybuilder;

error:
    return NULL;
}

static amxd_object_t* s_get_ldev_of_linkudev (amxd_object_t* link_obj) {
    return
        amxd_object_get_parent(
        amxd_object_get_parent(
            amxd_object_get_parent(
                amxd_object_get_parent(link_obj))));
}

void gmaps_dm_tpb_update(void) {
    gmaps_tpb_update(s_get_tpb());
}

/**
 * Initializes the graph-from-which-we-build-the-topology based on the datamodel.
 *
 * This is useful for loading the reboot-persistent configuration.
 *
 * Note: This is a heavy function, so do not use this for regular updates while gmap is running.
 */
void gmaps_dm_tpb_init(void) {
    amxc_llist_t paths;
    amxc_llist_init(&paths);
    amxd_status_t status = amxd_object_resolve_pathf(amxd_dm_get_root(gmap_get_dm()), &paths,
                                                     "Devices.Device.*.Link.*.UDevice.*.");
    when_failed_trace(status, exit, ERROR, "Error getting Link.*.UDevice");

    amxc_llist_for_each(it, &paths) {
        const char* linkudev_path = amxc_string_get(amxc_string_from_llist_it(it), 0);
        amxd_object_t* linkudev = amxd_object_findf(amxd_dm_get_root(gmap_get_dm()), "%s", linkudev_path);
        when_null_trace(linkudev, exit, ERROR, "Object '%s' not found", linkudev_path);

        if(linkudev->priv != NULL) {
            SAH_TRACEZ_ERROR(ME, "%s already has an edge - is loading from dm called 2x?", linkudev_path);
            continue;
        }

        amxd_object_t* udev = gmaps_get_device(amxd_object_get_name(linkudev, AMXD_OBJECT_NAMED));
        amxd_object_t* ldev = s_get_ldev_of_linkudev(linkudev);
        when_null_trace(udev, exit, ERROR, "Error getting udev of Link.[].UDevice");
        when_null_trace(ldev, exit, ERROR, "Error getting ldev of Link.[].UDevice");

        gmaps_dm_tpb_create_edge_for_linkudev(linkudev, amxd_object_get_name(udev, AMXD_OBJECT_NAMED), amxd_object_get_name(ldev, AMXD_OBJECT_NAMED));
    }

exit:
    amxc_llist_clean(&paths, amxc_string_list_it_free);
    gmaps_dm_tpb_update();
}

void gmaps_dm_tpb_cleanup (void) {
    if(s_topologybuilder_dm == NULL) {
        return;
    }
    // Note: this calls gmaps_dm_tpb_on_edge_delete and gmaps_dm_tpb_on_node_delete such that the
    //       priv pointers from the objects are set to NULL. So we don't have to set those NULL
    //       pointers again in gmaps_dm_tpb_cleanup.
    gmaps_tpb_delete(&s_topologybuilder_dm->topologybuilder);
    free(s_topologybuilder_dm);
    s_topologybuilder_dm = NULL;
}

void PRIVATE gmaps_dm_tpb_device_init_priv(UNUSED const amxd_object_t* device, gmap_device_priv_data_t* priv) {
    when_null(priv, exit);

    priv->tpb_node = NULL;

exit:
    return;
}

void PRIVATE gmaps_dm_tpb_device_clean_priv(UNUSED const amxd_object_t* device, gmap_device_priv_data_t* priv) {
    when_null(priv, exit);

    gmaps_tpb_node_delete(&priv->tpb_node);

exit:
    return;
}

static gmaps_tpb_node_t* s_get_or_create_node_of_device(const char* dev_name) {
    amxd_object_t* dev_obj = gmaps_get_device(dev_name);
    gmap_device_priv_data_t* priv = gmaps_device_private_data(dev_obj);
    when_null_trace(priv, error, ERROR, "No device found for %s", dev_name);

    if(priv->tpb_node == NULL) {
        gmaps_tpb_node_t* node = gmaps_tpb_node_new(s_get_tpb(), dev_obj);
        priv->tpb_node = node;
        return node;
    } else {
        return priv->tpb_node;
    }

error:
    return NULL;
}

void gmaps_dm_tpb_create_edge_for_linkudev(amxd_object_t* linkudev, const char* upper_dev, const char* lower_dev) {
    when_null_trace(linkudev, exit, ERROR, "NULL argument");
    char* path = amxd_object_get_path(linkudev, AMXD_OBJECT_INDEXED);
    gmaps_tpb_edge_t* edge = gmaps_tpb_edge_new(s_get_tpb(),
                                                s_get_or_create_node_of_device(upper_dev),
                                                s_get_or_create_node_of_device(lower_dev),
                                                path);
    linkudev->priv = edge;
exit:
    return;
}

void gmaps_dm_tpb_delete_edge_for_linkudev(amxd_object_t* linkudev) {
    when_null_trace(linkudev, exit, ERROR, "NULL argument");
    gmaps_tpb_edge_t* edge = (gmaps_tpb_edge_t*) linkudev->priv;
    if(edge == NULL) {
        SAH_TRACEZ_ERROR(ME, "NULL object userdata for edge");
    }
    linkudev->priv = NULL;
    gmaps_tpb_edge_delete(&edge);
exit:
    return;
}

static amxd_object_t* s_linkudev_obj_of_edge(gmaps_tpb_edge_t* edge) {
    const char* linkudev_path = NULL;
    amxd_object_t* linkudev_obj = NULL;
    when_null_trace(edge, error, ERROR, "NULL argument");
    linkudev_path = (const char*) gmaps_tpb_edge_userdata(edge);
    linkudev_obj = amxd_dm_findf(gmap_get_dm(), "%s", linkudev_path);
    when_null_trace(linkudev_obj, error, ERROR, "NULL obj for %s for edge", linkudev_path);
    return linkudev_obj;

error:
    return NULL;
}

static amxd_object_t* s_dev_obj_of_node(gmaps_tpb_node_t* node) {
    amxd_object_t* dev = (amxd_object_t*) gmaps_tpb_node_userdata(node);
    when_null_trace(dev, error, ERROR, "No dev found for node");
    return dev;

error:
    return NULL;
}


void gmaps_dm_tpb_on_link_selected (gmaps_tpb_edge_t* edge) {
    when_null_trace(edge, exit, ERROR, "NULL argument");
    amxd_object_t* linkudev_obj = s_linkudev_obj_of_edge(edge);
    char* linktype = NULL;
    const char* udev_name = NULL;
    amxd_object_t* ldev_obj = NULL;
    const char* ldev_name = NULL;
    gmap_status_t status = gmap_status_unknown_error;

    when_null_trace(linkudev_obj, exit, ERROR, "No Link.UDevice found for edge");
    linktype = amxd_object_get_cstring_t(linkudev_obj, "Type", NULL);
    udev_name = amxd_object_get_name(linkudev_obj, AMXD_OBJECT_NAMED);
    ldev_obj = s_get_ldev_of_linkudev(linkudev_obj);
    ldev_name = amxd_object_get_name(ldev_obj, AMXD_OBJECT_NAMED);
    status = gmaps_devices_chosenlink_make(
        udev_name,
        ldev_name,
        linktype,
        true);
    free(linktype);
    if(status != gmap_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Error selecting link %s %s", udev_name, ldev_name);
    }
exit:
    return;
}

void gmaps_dm_tpb_on_no_link_selected (gmaps_tpb_node_t* node) {
    amxd_object_t* dev_obj = s_dev_obj_of_node(node);
    when_null_trace(dev_obj, exit, ERROR, "No dev object found for node");

    gmaps_devices_chosenlink_removeAllULink(dev_obj, NULL);

exit:
    return;
}

void gmaps_dm_tpb_on_node_delete (gmaps_tpb_node_t* node) {
    amxd_object_t* dev_obj = s_dev_obj_of_node(node);
    gmap_device_priv_data_t* priv = gmaps_device_private_data(dev_obj);

    when_null(priv, exit);
    if((priv->tpb_node != NULL) && (priv->tpb_node != node)) {
        SAH_TRACEZ_ERROR(ME, "Node with device that has different node!");
    }
    priv->tpb_node = NULL;

exit:
    return;
}

void gmaps_dm_tpb_on_edge_delete(gmaps_tpb_edge_t* edge) {
    amxd_object_t* linkudev_obj = s_linkudev_obj_of_edge(edge);
    free(gmaps_tpb_edge_userdata(edge));

    when_null(linkudev_obj, exit);
    if((linkudev_obj->priv != NULL) && (linkudev_obj->priv != edge)) {
        SAH_TRACEZ_ERROR(ME, "link's edge has different link!");
    }
    linkudev_obj->priv = NULL;

exit:
    return;
}

