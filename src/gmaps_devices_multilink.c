/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "gmaps_devices_multilink.h"
#include "gmaps_dm_topologybuilder.h"
#include <stdlib.h>
#include <string.h>
#include "gmaps_priv.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#define ME "link"
#define DATAMODEL_NAME_LINK "Link"
#define DATAMODEL_NAME_UDEVICE "UDevice"
#define DATAMODEL_NAME_LDEVICE "LDevice"
#define DATAMODEL_NAME_LINK_TYPE "Type"
#define DATAMODEL_NAME_LINK_ALIAS "Alias"
#define DATAMODEL_LINK_DEFAULT_TYPE "default"

/** Get Devices.Device.[dev_name].Link. */
static amxd_object_t* s_get_link_template(const char* dev_name) {
    amxd_object_t* link_templ = NULL;
    amxd_object_t* dev_obj = gmaps_get_device(dev_name);
    when_null_trace(dev_obj, exit, ERROR, "dev not found with name '%s'", dev_name);
    link_templ = amxd_object_get(dev_obj, DATAMODEL_NAME_LINK);
    when_null_trace(link_templ, exit, ERROR, "'%s' not found for %s", DATAMODEL_NAME_LINK, dev_name);

exit:
    return link_templ;
}

/** Get or create Devices.Device.[dev_name].Link.[datasource]. */
static amxd_object_t* s_create_or_get_datasource_instance(const char* dev_name, const char* datasource) {
    amxd_object_t* link_templ = s_get_link_template(dev_name);
    amxd_object_t* datasource_obj = NULL;
    when_str_empty_trace(datasource, exit, ERROR, "NULL datasource for dev name %s", dev_name);
    when_null_trace(link_templ, exit, ERROR, "No Link template for %s", dev_name);

    datasource_obj = amxd_object_get(link_templ, datasource);
    if(datasource_obj == NULL) {
        amxc_var_t params;
        amxc_var_init(&params);
        amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, &params, "Alias", datasource);
        amxd_object_add_instance(&datasource_obj, link_templ, datasource, 0, &params);
        amxc_var_clean(&params);
    }

exit:
    return datasource_obj;
}

/** Get Devices.Device.[dev_name].Link.[datasource].UDevice (or LDevice) */
static amxd_object_t* s_get_uldevice_template(const char* dev_name, const char* datasource, gmaps_link_type_t direction) {
    amxd_object_t* datasource_obj = s_create_or_get_datasource_instance(dev_name, datasource);
    const char* template_name = direction == gmaps_link_type_upper_link ? DATAMODEL_NAME_UDEVICE : DATAMODEL_NAME_LDEVICE;
    amxd_object_t* uldevice_template = amxd_object_get(datasource_obj, template_name);
    return uldevice_template;
}

static const char* s_type_empty_to_default(const char* type) {
    if((type == NULL) || (type[0] == '\0')) {
        return DATAMODEL_LINK_DEFAULT_TYPE;
    } else {
        return type;
    }
}

static bool s_should_be_persistent(const char* from_dev, const char* to_dev) {
    amxd_object_t* from_dev_obj = gmaps_get_device(from_dev);
    amxd_object_t* to_dev_obj = gmaps_get_device(to_dev);
    when_null_trace(from_dev_obj, error, ERROR, "Device %s not found", from_dev);
    when_null_trace(to_dev_obj, error, ERROR, "Device %s not found", to_dev);
    return
        IS_BIT_SET(amxd_object_get_attrs(from_dev_obj), amxd_oattr_persistent)
        && IS_BIT_SET(amxd_object_get_attrs(to_dev_obj), amxd_oattr_persistent);

error:
    return false;
}


/**
 * Adds one direction of the bidirectional link - so must be called a second time for the other direction.
 */
static bool s_link_add_unidir(const char* from_dev, const char* to_dev, gmaps_link_type_t direction, const char* type, const char* datasource) {
    bool retval = false;
    amxc_var_t params;
    amxd_object_t* uldevice_templ = s_get_uldevice_template(from_dev, datasource, direction);
    amxd_object_t* link_obj = amxd_object_get_instance(uldevice_templ, to_dev, 0);
    amxc_var_init(&params);
    when_null_trace(uldevice_templ, exit, ERROR, "Error getting link template object for %s", from_dev);
    when_str_empty_trace(from_dev, exit, ERROR, "NULL/Empty from_dev");
    when_str_empty_trace(to_dev, exit, ERROR, "NULL/Empty to_dev");
    if(link_obj == NULL) {
        amxd_status_t status = amxd_status_unknown_error;
        amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, &params, DATAMODEL_NAME_LINK_ALIAS, to_dev);
        amxc_var_add_key(cstring_t, &params, DATAMODEL_NAME_LINK_TYPE, s_type_empty_to_default(type));
        status = amxd_object_add_instance(&link_obj, uldevice_templ, to_dev, 0, &params);
        when_failed_trace(status, exit, ERROR, "Error adding link '%s'-'%s'", from_dev, to_dev);
        amxd_object_set_attr(link_obj, amxd_oattr_persistent, s_should_be_persistent(from_dev, to_dev));

        if(direction == gmaps_link_type_upper_link) {
            gmaps_dm_tpb_create_edge_for_linkudev(link_obj, to_dev, from_dev);
        }
    } else {
        amxd_status_t status = amxd_object_set_value(cstring_t, link_obj, DATAMODEL_NAME_LINK_TYPE, s_type_empty_to_default(type));
        when_failed_trace(status, exit, ERROR, "Error setting link type");
    }

    retval = true;
exit:
    amxc_var_clean(&params);
    return retval;
}

/**
 * Not supported be called multiple times for the same devices within the same transaction.
 */
static bool s_link_add_bidir(const char* upper_dev_name, const char* lower_dev_name, const char* type, const char* datasource) {
    bool retval = false;
    bool ok = false;
    when_str_empty_trace(upper_dev_name, exit, ERROR, "NULL/empty argument (%s, %s, %s)", upper_dev_name, lower_dev_name, datasource);
    when_str_empty_trace(lower_dev_name, exit, ERROR, "NULL/empty argument (%s, %s, %s)", upper_dev_name, lower_dev_name, datasource);
    when_str_empty_trace(datasource, exit, ERROR, "NULL/empty argument (%s, %s, %s)", upper_dev_name, lower_dev_name, datasource);

    ok = s_link_add_unidir(lower_dev_name, upper_dev_name, gmaps_link_type_upper_link, type, datasource);
    when_false_trace(ok, exit, ERROR, "Error adding link %s %s", upper_dev_name, lower_dev_name);
    s_link_add_unidir(upper_dev_name, lower_dev_name, gmaps_link_type_lower_link, type, datasource);
    when_false_trace(ok, exit, ERROR, "Error adding backlink %s %s", upper_dev_name, lower_dev_name);

    retval = true;
exit:
    return retval;
}

static void s_link_remove_unidir(const char* from_dev, const char* to_dev, gmaps_link_type_t direction, const char* datasource) {
    amxd_object_t* uldevice_templ = s_get_uldevice_template(from_dev, datasource, direction);
    amxd_object_t* link = amxd_object_get_instance(uldevice_templ, to_dev, 0);
    when_null_trace(uldevice_templ, exit, ERROR, "Error getting link template object for %s", from_dev);
    when_null(link, exit); // ok already deleted.

    if(direction == gmaps_link_type_upper_link) {
        gmaps_dm_tpb_delete_edge_for_linkudev(link);
    }
    amxd_object_delete(&link);

exit:
    return;
}

static bool s_link_remove_bidir(const char* upper_dev_name, const char* lower_dev_name, const char* datasource) {
    bool retval = false;
    when_str_empty_trace(upper_dev_name, exit, ERROR, "NULL/empty argument (%s, %s, %s)", upper_dev_name, lower_dev_name, datasource);
    when_str_empty_trace(lower_dev_name, exit, ERROR, "NULL/empty argument (%s, %s, %s)", upper_dev_name, lower_dev_name, datasource);
    when_str_empty_trace(datasource, exit, ERROR, "NULL/empty argument (%s, %s, %s)", upper_dev_name, lower_dev_name, datasource);

    s_link_remove_unidir(lower_dev_name, upper_dev_name, gmaps_link_type_upper_link, datasource);
    s_link_remove_unidir(upper_dev_name, lower_dev_name, gmaps_link_type_lower_link, datasource);

    retval = true;
exit:
    return retval;
}

static bool s_get_direct_connected(amxc_var_t* target_list, const char* dev_name, const char* datasource, gmaps_link_type_t direction) {
    bool retval = false;
    amxd_object_t* uldevice_templ = NULL;
    when_null_trace(target_list, exit, ERROR, "NULL");

    amxc_var_set_type(target_list, AMXC_VAR_ID_LIST);
    uldevice_templ = s_get_uldevice_template(dev_name, datasource, direction);
    when_null_trace(uldevice_templ, exit, ERROR, "Error getting Link.[].UDevice/LDevice templ obj for '%s' '%s'", dev_name, datasource);

    amxd_object_for_each(instance, uldevice_it, uldevice_templ) {
        amxd_object_t* dev = amxc_llist_it_get_data(uldevice_it, amxd_object_t, it);
        amxc_var_add(cstring_t, target_list, amxd_object_get_name(dev, AMXD_OBJECT_NAMED));
    }

    retval = true;
exit:
    return retval;
}

/**
 * remove all links links between dev_name and all devices in list, where
 * dev_name is the lower device (if direction == upper), or dev_name is the upper device (if direction == lower).
 */
static bool s_link_remove_bidir_multiple(amxc_var_t* list, const char* dev_name, gmaps_link_type_t direction, const char* datasource, const char* do_not_remove_this_dev) {
    bool ok = false;
    bool retval = false;
    amxc_var_for_each(device_name_var, list) {
        const char* other_dev = amxc_var_constcast(cstring_t, device_name_var);
        when_str_empty_trace(other_dev, exit, ERROR, "NULL/empty device name in list");
        if((do_not_remove_this_dev != NULL) && (0 == strcmp(other_dev, do_not_remove_this_dev))) {
            continue;
        }
        if(direction == gmaps_link_type_upper_link) {
            ok = s_link_remove_bidir(other_dev, dev_name, datasource);
        } else {
            ok = s_link_remove_bidir(dev_name, other_dev, datasource);
        }
        when_false_trace(ok, exit, ERROR, "Error removing bidir link");
    }

    retval = true;
exit:
    return retval;
}

static bool s_link_remove_all(const char* upper_dev_name, const char* lower_dev_name, const char* datasource) {
    bool retval = false;
    bool ok = false;
    amxc_var_t list;
    gmaps_link_type_t direction = gmaps_link_type_lower_link;
    const char* dev_name = NULL;
    amxc_var_init(&list);

    when_false_trace(upper_dev_name == NULL || lower_dev_name == NULL, exit,
                     ERROR, "One argument must be NULL such that direction can be determined %s %s %s",
                     upper_dev_name, lower_dev_name, datasource);
    when_false_trace(upper_dev_name != NULL || lower_dev_name != NULL, exit,
                     ERROR, "One of the device arguments must be non-NULL (%s)", datasource);

    direction = upper_dev_name == NULL ? gmaps_link_type_upper_link : gmaps_link_type_lower_link;
    dev_name = upper_dev_name == NULL ? lower_dev_name : upper_dev_name;

    ok = s_get_direct_connected(&list, dev_name, datasource, direction);
    when_false_trace(ok, exit, ERROR, "Error getting direct connections");
    ok = s_link_remove_bidir_multiple(&list, dev_name, direction, datasource, NULL);
    when_false_trace(ok, exit, ERROR, "Error removing links");

    retval = true;
exit:
    amxc_var_clean(&list);
    return retval;
}

static bool s_link_add(const char* upper_dev_name, const char* lower_dev_name, const char* type, const char* datasource) {
    bool ok = false;
    bool retval = false;

    ok = s_link_add_bidir(upper_dev_name, lower_dev_name, type, datasource);
    when_false_trace(ok, exit, ERROR, "Error adding link");

    retval = true;
exit:
    return retval;
}

/**
 * @param upper_dev_name: NULL to remove all links with `lower_dev_name` as LDevice.
 * @param lower_dev_name: NULL to remove all links with `upper_dev_name` as UDevice.
 */
static bool s_link_remove(const char* upper_dev_name, const char* lower_dev_name, const char* datasource) {
    bool ok = false;
    bool retval = false;

    if((upper_dev_name == NULL) || (lower_dev_name == NULL)) {
        ok = s_link_remove_all(upper_dev_name, lower_dev_name, datasource);
    } else {
        ok = s_link_remove_bidir(upper_dev_name, lower_dev_name, datasource);
    }
    when_false_trace(ok, exit, ERROR, "Error removing links %s - %s (%s)", upper_dev_name, lower_dev_name, datasource);

    retval = true;
exit:
    return retval;
}

static bool s_links_replace(const char* upper_dev_name, const char* lower_dev_name, const char* type, const char* datasource) {
    bool ok = false;
    bool retval = false;
    amxc_var_t list;
    amxc_var_init(&list);

    ok = s_get_direct_connected(&list, lower_dev_name, datasource, gmaps_link_type_upper_link);
    when_false_trace(ok, exit, ERROR, "Error getting direct links")

    ok = s_link_remove_bidir_multiple(&list, lower_dev_name, gmaps_link_type_upper_link, datasource, upper_dev_name);
    when_false_trace(ok, exit, ERROR, "Error removing links")

    ok = s_link_add_bidir(upper_dev_name, lower_dev_name, type, datasource);
    when_false_trace(ok, exit, ERROR, "Error adding link")

    retval = true;
exit:
    amxc_var_clean(&list);
    return retval;
}

static void s_on_device_delete(const char* dev_name) {
    bool ok = false;
    amxd_object_t* link_templ = NULL;
    when_str_empty_trace(dev_name, exit, ERROR, "NULL/Empty device name");

    link_templ = s_get_link_template(dev_name);
    when_null_trace(link_templ, exit, ERROR, "Error getting link templ obj for '%s'", dev_name);

    amxd_object_for_each(instance, link_it, link_templ) {
        amxd_object_t* datasource_obj = amxc_llist_it_get_data(link_it, amxd_object_t, it);
        const char* datasource = amxd_object_get_name(datasource_obj, AMXD_OBJECT_NAMED);
        ok = s_link_remove_all(dev_name, NULL, datasource);
        if(!ok) {
            SAH_TRACEZ_ERROR(ME, "Error removing links from %s (%s) - topology will likely be wrong", dev_name, datasource);
        }
        ok = s_link_remove_all(NULL, dev_name, datasource);
        if(!ok) {
            SAH_TRACEZ_ERROR(ME, "Error removing links to %s (%s)- topology will likely be wrong", dev_name, datasource);
        }
    }

exit:
    return;
}

bool gmaps_devices_multilink_add(const char* upper_dev_name, const char* lower_dev_name, const char* type, const char* datasource) {
    bool ok = false;
    bool retval = false;

    ok = s_link_add(upper_dev_name, lower_dev_name, type, datasource);
    when_false_trace(ok, exit, ERROR, "Error linking %s - %s (%s)", upper_dev_name, lower_dev_name, datasource);

    gmaps_dm_tpb_update();

    retval = true;
exit:
    return retval;
}

bool gmaps_devices_multilink_remove(const char* upper_dev_name, const char* lower_dev_name, const char* datasource) {
    bool ok = false;
    bool retval = false;

    ok = s_link_remove(upper_dev_name, lower_dev_name, datasource);
    when_false_trace(ok, exit, ERROR, "Error removing link %s - %s (%s)", upper_dev_name, lower_dev_name, datasource);

    gmaps_dm_tpb_update();

    retval = true;
exit:
    return retval;
}

bool gmaps_devices_multilink_replace(const char* upper_dev_name, const char* lower_dev_name, const char* type, const char* datasource) {
    bool ok = false;
    bool retval = false;

    ok = s_links_replace(upper_dev_name, lower_dev_name, type, datasource);
    when_false_trace(ok, exit, ERROR, "Error replacing link %s - %s (%s)", upper_dev_name, lower_dev_name, datasource);

    gmaps_dm_tpb_update();

    retval = true;
exit:
    return retval;
}

void gmaps_devices_multilink_on_device_delete(const char* dev_name) {
    when_str_empty_trace(dev_name, exit, ERROR, "NULL/Empty argument");
    s_on_device_delete(dev_name);

    gmaps_dm_tpb_update();

exit:
    return;
}
