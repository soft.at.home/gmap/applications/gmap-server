/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <debug/sahtrace.h>

#include "gmaps_priv.h"

#define ME "traverse"

typedef struct _gmaps_traverse_params {
    amxp_expr_t* expression;
    gmap_traverse_tree_cb_t cb;
    void* userdata;
    gmaps_link_type_t link_direction;
    bool recursive;
    amxc_llist_t* seen;
} gmaps_traverse_params_t;


static gmap_traverse_status_t gmaps_traverse_handle_tree(amxd_object_t* device,
                                                         bool skip_this,
                                                         gmaps_traverse_params_t* p);


/**
 * Callback that matches the expression in @param priv against the @param device.
 *
 * @return Returns @ref gmap_status_break if the evaluation is
 * true, else @param gmap_status_ok.
 */
static gmap_status_t gmaps_traverse_cb_evaluate_device(amxd_object_t* device,
                                                       void* priv) {
    return gmaps_device_matches(device, (amxp_expr_t*) priv)
           ? gmap_status_break
           : gmap_status_ok;
}

/**
 * Handle one @param device of the traverse.
 * If the device or an alternative matches the @param expression, then calls the @param cb function
 * with parameter @ref gmap_traverse_device_matching.
 * Else, it calls the callback with parameter @ref gmap_traverse_device_not_matching.
 *
 * @return Returns the return value of the callback.
 */
static gmap_traverse_status_t gmaps_traverse_handle_this(amxd_object_t* device,
                                                         amxp_expr_t* expression,
                                                         gmap_traverse_tree_cb_t cb,
                                                         void* userdata) {
    gmap_traverse_action_t action = gmap_traverse_device_not_matching;

    if(gmaps_device_matches(device, expression)
       || (gmaps_device_for_all_alternatives(device,
                                             gmaps_traverse_cb_evaluate_device,
                                             (void*) expression) == gmap_status_break)) {
        action = gmap_traverse_device_matching;
    }

    return cb(device, action, userdata);
}

GMAPS_INLINE gmap_traverse_status_t gmap_traverse_verify_status(gmap_traverse_status_t status) {
    return (status == gmap_traverse_no_recurse) ? gmap_traverse_continue : status;
}

/**
 * Handles the links of the @param device for traversal.
 */
static gmap_traverse_status_t gmaps_traverse_cb_handle_links(amxd_object_t* device,
                                                             void* priv) {
    gmap_traverse_status_t retval = gmap_traverse_continue;
    gmaps_traverse_params_t* p = (gmaps_traverse_params_t*) priv;
    amxd_object_t* link_template = amxd_object_get_child(device,
                                                         gmaps_device_get_link_text(p->link_direction));

    amxd_object_for_each(instance, it, link_template) {
        amxd_object_t* linked_dev = amxc_llist_it_get_data(it, amxd_object_t, it);
        linked_dev = gmaps_get_device(amxd_object_get_name(linked_dev, AMXD_OBJECT_NAMED));
        when_null_status(linked_dev, exit, retval = gmap_traverse_failed);
        if(p->recursive) {
            retval = gmaps_traverse_handle_tree(linked_dev, false, p);
        } else {
            retval = gmaps_traverse_handle_this(linked_dev, p->expression, p->cb, p->userdata);
            if(retval != gmap_traverse_continue) {
                p->cb(linked_dev, gmap_traverse_device_done, p->userdata);
            } else {
                retval = p->cb(linked_dev, gmap_traverse_device_done, p->userdata);
            }
            retval = gmap_traverse_verify_status(retval);
        }
        if(retval != gmap_traverse_continue) {
            break;
        }
    }

exit:
    return retval;
}

GMAPS_INLINE bool gmaps_traverse_seen_device(const gmap_device_priv_data_t* priv) {
    return priv->traversal.llist != NULL;
}

/**
 * Handle the tree starting at @param device.
 * If the device is an alternative device, continue with the master device.
 * If not @param skip_this, handle this device.
 * Next, consider the linked device in the given direction, see @param p.
 * If recurse is true (see @param p), recurse into the linked device; else handle the linked
 * device without recusing.
 * Next, handle the links of the alternative devices of the given device.
 */
static gmap_traverse_status_t gmaps_traverse_handle_tree(amxd_object_t* device,
                                                         bool skip_this,
                                                         gmaps_traverse_params_t* p) {
    gmap_traverse_status_t retval = gmap_traverse_continue;
    gmap_device_priv_data_t* dev_priv = NULL;

    gmaps_device_get_master(&device);
    dev_priv = gmaps_device_private_data(device);
    if(dev_priv == NULL) {
        SAH_TRACEZ_ERROR(ME, "Missing private data on traversed object");
        retval = gmap_traverse_failed;
        goto exit;
    }

    if(!skip_this) {
        retval = gmaps_traverse_handle_this(device, p->expression, p->cb, p->userdata);
        if(retval != gmap_traverse_continue) {
            p->cb(device, gmap_traverse_device_done, p->userdata);
            goto exit;
        }

        // If no link instances are available, no need to push, iterate and pop.
        if(!gmaps_device_has_link(device, p->link_direction, true)) {
            retval = p->cb(device, gmap_traverse_device_done, p->userdata);
            goto exit;
        }

        // Verify recursion.
        when_true_status(gmaps_traverse_seen_device(dev_priv), exit, retval = gmap_traverse_failed);

        // Add current device key to seen devices.
        amxc_llist_append(p->seen, &dev_priv->traversal);

        retval = p->cb(device, gmap_traverse_level_push, p->userdata);
        if(retval != gmap_traverse_continue) {
            p->cb(device, gmap_traverse_level_pop, p->userdata);
            amxc_llist_it_take(&dev_priv->traversal);
            goto exit;
        }
    }

    // Handle master and alternative device links.
    retval = gmaps_traverse_cb_handle_links(device, (void*) p);  // call cb directly for master
    if(retval == gmap_traverse_continue) {
        retval = gmaps_device_for_all_alternatives(device,
                                                   gmaps_traverse_cb_handle_links,
                                                   (void*) p);
    }

    if(!skip_this) {
        gmap_traverse_status_t temp = p->cb(device, gmap_traverse_level_pop, p->userdata);
        retval = (retval == gmap_traverse_continue) ? temp : retval;
        amxc_llist_it_take(&dev_priv->traversal);
        retval = gmap_traverse_verify_status(retval);

        temp = p->cb(device, gmap_traverse_device_done, p->userdata);
        retval = (retval == gmap_traverse_continue) ? temp : retval;
    }

exit:
    retval = gmap_traverse_verify_status(retval);
    return retval;
}

/**
 * Returns true if the given @param mode is exclusive, else false.
 */
static bool gmaps_traverse_mode_is_exclusive(gmap_traverse_mode_t mode) {
    bool retval = false;

    switch(mode) {
    case gmap_traverse_down_exclusive:
    case gmap_traverse_up_exclusive:
    case gmap_traverse_one_down_exclusive:
    case gmap_traverse_one_up_exclusive:
        retval = true;
        break;
    case gmap_traverse_this:
    case gmap_traverse_down:
    case gmap_traverse_up:
    case gmap_traverse_one_down:
    case gmap_traverse_one_up:
    case gmap_traverse_max:
        break;
    }
    return retval;
}

/**
 * Returns true if the direction of the given @param mode is down, else false.
 */
static bool gmaps_traverse_mode_is_down(gmap_traverse_mode_t mode) {
    bool retval = false;

    switch(mode) {
    case gmap_traverse_down:
    case gmap_traverse_down_exclusive:
    case gmap_traverse_one_down:
    case gmap_traverse_one_down_exclusive:
        retval = true;
        break;
    case gmap_traverse_this:
    case gmap_traverse_up:
    case gmap_traverse_up_exclusive:
    case gmap_traverse_one_up:
    case gmap_traverse_one_up_exclusive:
    case gmap_traverse_max:
        break;
    }

    return retval;
}

/**
 * Returns true if the given @param mode is recursive, else false.
 */
static bool gmaps_traverse_mode_is_recursive(gmap_traverse_mode_t mode) {
    bool retval = false;

    switch(mode) {
    case gmap_traverse_down:
    case gmap_traverse_up:
    case gmap_traverse_down_exclusive:
    case gmap_traverse_up_exclusive:
        retval = true;
        break;
    case gmap_traverse_this:
    case gmap_traverse_one_up:
    case gmap_traverse_one_up_exclusive:
    case gmap_traverse_one_down:
    case gmap_traverse_one_down_exclusive:
    case gmap_traverse_max:
        break;
    }

    return retval;
}

/**
   @ingroup gmap_traverse
   @brief
   Traverse the devices in a hierarchical and logical order

   The gmap deta model contains a flat list of all discovered devices. Each of these device can have an upper device
   or multiple lower devices. This function helps in traversing the device tree taking into account the upper or lower
   devices.

   When a device has alternatives, the alternatives themselfs are not taken into account. The links of each alternative
   device are taken into account while traversing the topology tree. A master device will match the provided expression, if
   the master device or one of its alternatives are matching.

   Starting to traverse the topology tree at an alternative device is the same as starting to traverse from the master
   device.

   The provided callback function is called multiple times. When the traverse is starting it is called with @ref gmap_traverse_start
   as the action, when the traverse is done it will be called with @ref gmap_traverse_stop action.

   For each device that is encountered the callback function is called with @ref gmap_traverse_device_matching action if the
   device is matching the provided expression or no expression was provided. And it is called with @ref gmap_traverse_device_not_matching
   when the device is not matching the provided expression.

   When the traverse is recursing into the next level (up or down depending on the provided traverse mode), the callback is
   called with the @ref gmap_traverse_level_push. In this case the device provided is the device from the current level.
   When the level is done, the callback is called with @ref gmap_traverse_level_pop.

   When a device is done the callback is called with action @ref gmap_traverse_device_done. After that action the device
   will not occur anymore in the traverse.

   The callback function must give an idication of what to do next, these indication must be one of the following values:
   - @ref gmap_traverse_continue
   - @ref gmap_traverse_no_recurse
   - @ref gmap_traverse_done
   - @ref gmap_traverse_failed

   For more information about these return values see @ref gmap_traverse_status_t.

   @warning
   - This function can only be called from the server side, it needs direct access to the data model

   @param device The starting point, this is the device from where the tree traversing is started
   @param mode contains the traverse mode, it indicates the direction. See @ref section_gmap_traverse_direction for more information
   @param expression optionally an expression can be provided, the expression is evaluated against each device the traverse is accessing
   @param cb the callback function that is called multiple times during the traverse
   @param userdata any kind of data, this will passed unmodified to the callback function every time

   @return
   Returns the traverse status, this can be one of the following values @ref gmap_traverse_done when traversing is
   ended succussful or @ref gmap_traverse_failed when traversing the device tree failed
 */
gmap_traverse_status_t gmaps_traverse_tree(amxd_object_t* device,
                                           gmap_traverse_mode_t mode,
                                           const char* expression,
                                           gmap_traverse_tree_cb_t cb,
                                           void* userdata) {
    gmap_traverse_status_t retval = gmap_traverse_done;
    amxp_expr_t expr;

    when_null_status(device, exit, retval = gmap_traverse_failed);
    when_true_status(mode >= gmap_traverse_max, exit, retval = gmap_traverse_failed);
    when_null_status(expression, exit, retval = gmap_traverse_failed);
    when_null_status(cb, exit, retval = gmap_traverse_failed);
    when_failed_status(amxp_expr_init(&expr, expression), exit, retval = gmap_traverse_failed);
    SAH_TRACEZ_INFO(ME, "Traverse tree parameters are ok");

    cb(NULL, gmap_traverse_start, userdata);

    if(mode == gmap_traverse_this) {
        retval = gmaps_traverse_handle_this(device, &expr, cb, userdata);
        if(retval != gmap_traverse_continue) {
            cb(device, gmap_traverse_device_done, userdata);
        } else {
            retval = cb(device, gmap_traverse_device_done, userdata);
        }
    } else {
        amxc_llist_t devices_seen;
        amxc_llist_init(&devices_seen);
        gmaps_traverse_params_t p = {
            .cb = cb,
            .expression = &expr,
            .seen = &devices_seen,
            .userdata = userdata,
            .recursive = gmaps_traverse_mode_is_recursive(mode),
            .link_direction = (gmaps_traverse_mode_is_down(mode) ?
                               gmaps_link_type_lower_link :
                               gmaps_link_type_upper_link),
        };
        retval = gmaps_traverse_handle_tree(device, gmaps_traverse_mode_is_exclusive(mode), &p);
        amxc_llist_clean(&devices_seen, NULL);
    }

    cb(NULL, gmap_traverse_stop, userdata);

    amxp_expr_clean(&expr);

exit:
    retval = gmap_traverse_verify_status(retval);
    return (retval == gmap_traverse_continue) ? gmap_traverse_done : retval;
}


