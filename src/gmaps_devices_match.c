/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>


#include "gmaps_priv.h"

#define ME "match"

/**
 * Custom field fetcher for gMap.
 * Called for each field (part of the expression) for which the expression
 * parser needs additional information.
 * For gMap, we look both at the object path and the tags.
 *
 * Example of an expression: ".Active == true and (wifi or eth)"
 * This expression has 3 fields for which the parser needs more info:
 *  1) .Active
 *  2) wifi
 *  3) eth
 *
 * The first one can be handled by the custom field fetcher from
 * the amxd lib, see @ref amxd_object_expr_get_field.
 * The second and third ones are tags and need an gMap-specific field
 * fetcher, see @ref gmaps_tag_field_fetcher.
 *
 * Note: gMap differentiates between tag and object path by looking at the first
 * character. If it's a dot, it is considered an object path. Else, a tag.
 */
static amxp_expr_status_t gmaps_custom_field_fetcher(amxp_expr_t* expr,
                                                     amxc_var_t* value,
                                                     const char* path,
                                                     void* priv) {
    amxp_expr_status_t status = amxp_expr_status_unknown_error;
    amxd_object_t* device = (amxd_object_t*) priv;
    if(path[0] == '.') {
        // Field is object path.
        status = amxd_object_expr_get_field(expr, value, path, device);
    } else {
        // Field is tag.
        amxc_var_set(bool, value, gmaps_device_tag_has(device, path));
        status = amxp_expr_status_ok;
    }

    return status;
}

static bool gmaps_device_match_has_instance(amxp_expr_t* expr,
                                            amxc_var_t* args) {
    bool retval = false;
    amxc_llist_it_t* it = NULL;
    amxc_var_t* var = NULL;
    const char* sub_obj_path = NULL;
    const char* sub_obj_expr = NULL;
    amxp_expr_t* sub_expr = NULL;
    amxd_object_t* sub_obj = NULL;
    amxd_object_t* device_obj = NULL;

    SAH_TRACEZ_INFO(ME, "has_instance");

    when_null(expr, exit);
    when_null(args, exit);

    device_obj = (amxd_object_t*) expr->priv;
    when_null(device_obj, exit);

    it = amxc_llist_get_first(&args->data.vl);
    var = amxc_var_from_llist_it(it);
    when_true(amxc_var_is_null(var), exit);

    sub_obj_path = amxc_var_constcast(cstring_t, var);
    when_str_empty(sub_obj_path, exit);

    it = amxc_llist_it_get_next(it);
    var = amxc_var_from_llist_it(it);
    when_true(amxc_var_is_null(var), exit);
    sub_obj_expr = amxc_var_constcast(cstring_t, var);
    when_str_empty(sub_obj_expr, exit);

    when_failed_trace(amxp_expr_new(&sub_expr, sub_obj_expr), exit,
                      ERROR, "Invalid expression: '%s'", sub_obj_expr);

    sub_obj = amxd_object_findf(device_obj, "%s", sub_obj_path);
    when_null(sub_obj, exit);

    amxd_object_for_each(instance, it2, sub_obj) {
        amxd_object_t* sub_obj_instance = amxc_container_of(it2, amxd_object_t, it);
        retval = gmaps_device_matches(sub_obj_instance, sub_expr);
        if(retval == true) {
            SAH_TRACEZ_INFO(ME,
                            "device: %s suboject path: %s has instance: %s and match expr: %s",
                            amxd_object_get_name(device_obj, AMXD_OBJECT_NAMED),
                            sub_obj_path,
                            amxd_object_get_name(sub_obj_instance, AMXD_OBJECT_NAMED),
                            sub_obj_expr);
            goto exit;
        }
    }
exit:
    amxp_expr_delete(&sub_expr);
    return retval;
}

static bool gmaps_device_match_has_instances(amxp_expr_t* expr,
                                             amxc_var_t* args) {
    bool retval = false;
    amxc_llist_it_t* it = NULL;
    amxc_var_t* var = NULL;
    const char* sub_obj_path = NULL;
    amxd_object_t* sub_obj = NULL;
    amxd_object_t* sub_obj_instance = NULL;
    amxd_object_t* device_obj = NULL;

    SAH_TRACEZ_INFO(ME, "has_instances");

    when_null(expr, exit);
    when_null(args, exit);

    device_obj = (amxd_object_t*) expr->priv;
    when_null(device_obj, exit);

    it = amxc_llist_get_first(&args->data.vl);
    var = amxc_var_from_llist_it(it);
    when_true(amxc_var_is_null(var), exit);

    sub_obj_path = amxc_var_constcast(cstring_t, var);
    when_str_empty(sub_obj_path, exit);

    sub_obj = amxd_object_findf(device_obj, "%s", sub_obj_path);
    when_null(sub_obj, exit);

    it = amxd_object_first_instance(sub_obj);
    when_null(it, exit);
    sub_obj_instance = amxc_container_of(it, amxd_object_t, it);
    when_null(sub_obj_instance, exit);

    retval = true;
    SAH_TRACEZ_INFO(ME,
                    "device: %s suboject path: %s has instance",
                    amxd_object_get_name(device_obj, AMXD_OBJECT_NAMED),
                    sub_obj_path);
exit:
    return retval;
}

/* check object/object instance existence for a given device */
static bool gmaps_device_match_exists(amxp_expr_t* expr,
                                      amxc_var_t* args) {
    bool retval = false;
    amxc_llist_it_t* it = NULL;
    amxc_var_t* var = NULL;
    const char* sub_obj_path = NULL;
    amxd_object_t* sub_obj = NULL;
    amxd_object_t* device_obj = NULL;

    SAH_TRACEZ_INFO(ME, "exists");

    when_null(expr, exit);
    when_null(args, exit);

    device_obj = (amxd_object_t*) expr->priv;
    when_null(device_obj, exit);

    it = amxc_llist_get_first(&args->data.vl);
    var = amxc_var_from_llist_it(it);
    when_true(amxc_var_is_null(var), exit);

    sub_obj_path = amxc_var_constcast(cstring_t, var);
    when_str_empty(sub_obj_path, exit);

    sub_obj = amxd_object_findf(device_obj, "%s", sub_obj_path);
    when_null(sub_obj, exit);
    retval = true;

    SAH_TRACEZ_INFO(ME,
                    "device: %s suboject path: %s exists",
                    amxd_object_get_name(device_obj, AMXD_OBJECT_NAMED),
                    sub_obj_path);
exit:
    return retval;

}

static amxd_object_t* gmaps_device_match_find_root(void) {
    amxd_object_t* result = NULL;

    /* Note: the result could probably be cached, because the self and physical
     * tags may not be removed.
     * In the end, this is typically one of the first devices anyway, so the
     * impact should be fairly limited anyway.
     */

    amxd_object_for_each(instance, it, gmap_dm_get_devices_device()) {
        amxd_object_t* device = amxc_llist_it_get_data(it, amxd_object_t, it);
        if(gmaps_device_tag_has(device, "self") && gmaps_device_tag_has(device, "physical")) {
            result = device;
            break;
        }
    }

    return result;
}

static bool gmaps_device_match_is_connected(amxp_expr_t* expr,
                                            amxc_var_t* args) {
    bool retval = false;
    amxd_object_t* device_obj = NULL;
    amxd_object_t* self_root_device_obj = NULL;

    SAH_TRACEZ_INFO(ME, "is_connected");

    when_null(expr, exit);
    when_null(args, exit);

    device_obj = (amxd_object_t*) expr->priv;
    when_null(device_obj, exit);

    self_root_device_obj = gmaps_device_match_find_root();
    when_null(self_root_device_obj, exit);
    retval = gmaps_device_is_linked_to(device_obj,
                                       self_root_device_obj,
                                       gmap_traverse_up);
    if(retval) {
        SAH_TRACEZ_INFO(ME,
                        "device: %s is connected",
                        amxd_object_get_name(device_obj, AMXD_OBJECT_NAMED));
    }
exit:
    return retval;
}

static uint64_t convert(const char* mac_str) {
    uint8_t values[6] = { 0, 0, 0, 0, 0, 0};
    int i = 0;
    uint64_t mac = 0;

    i = sscanf(mac_str, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx%*c",
               &values[5], &values[4], &values[3],
               &values[2], &values[1], &values[0]);

    when_false_status(i == 6, exit, mac = 0);
    for(i = 0; i < 6; ++i) {
        mac |= values[i];
        mac = mac << 8;
    }

exit:
    return mac;
}


// check that the given PhysAddr for a devices match the base mac ( arg 0 ) and oui mask ( arg 1) in argument of the boolean function
static bool gmap_device_match_mac_in_mask(amxp_expr_t* expr,
                                          amxc_var_t* args) {
    const char* mask = NULL;
    const char* mac = NULL;
    const char* base_mac = NULL;
    uint64_t mac_val = 0;
    uint64_t mask_val = 0;
    uint64_t base_mac_val = 0;
    amxd_object_t* device_obj = NULL;

    SAH_TRACEZ_INFO(ME, "In mask match");

    when_null(expr, exit);
    when_null(args, exit);

    base_mac = GETI_CHAR(args, 0);
    mask = GETI_CHAR(args, 1);
    device_obj = (amxd_object_t*) expr->priv;

    when_str_empty_trace(mask, exit, ERROR, "No OUI mask given for matching");
    when_str_empty_trace(base_mac, exit, ERROR, "No base mac given for matching");
    when_null_trace(device_obj, exit, ERROR, "No mac object given to check mac in mask");

    mac = GET_CHAR(amxd_object_get_param_value(device_obj, "PhysAddress"), NULL);
    when_str_empty_trace(mac, exit, ERROR, "No mac found in device");

    mac_val = convert(mac);
    mask_val = convert(mask);
    base_mac_val = convert(base_mac);

    SAH_TRACEZ_INFO(ME, "Matching %s is %d", mac, (base_mac_val & mask_val) == (mac_val & mask_val));
    return ((base_mac_val & mask_val) == (mac_val & mask_val));
exit:
    return false;
}

void gmaps_device_match_init(void) {
    SAH_TRACEZ_INFO(ME, "device_match_init");
    amxp_expr_add_bool_fn("has_instance", gmaps_device_match_has_instance);
    amxp_expr_add_bool_fn("has_instances", gmaps_device_match_has_instances);
    amxp_expr_add_bool_fn("is_connected", gmaps_device_match_is_connected);
    amxp_expr_add_bool_fn("exists", gmaps_device_match_exists);
    amxp_expr_add_bool_fn("mac_in_mask", gmap_device_match_mac_in_mask);
}

bool gmaps_device_matches(amxd_object_t* device,
                          amxp_expr_t* expression) {
    return amxp_expr_evaluate(expression, gmaps_custom_field_fetcher, device, NULL);
}
