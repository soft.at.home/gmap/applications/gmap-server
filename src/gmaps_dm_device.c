/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "gmaps_priv.h"

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <stdlib.h>
#include <string.h>

#define ME "dm_device"

amxd_status_t _Device_removeName(amxd_object_t* object,
                                 GMAPS_UNUSED amxd_function_t* func,
                                 amxc_var_t* args,
                                 amxc_var_t* ret) {

    gmap_status_t status = gmap_status_ok;

    char* source = amxc_var_dyncast(cstring_t, GET_ARG(args, "source"));
    const char* key = amxd_object_get_name(object, AMXD_OBJECT_NAMED);

    status = gmaps_device_remove_name(key, source);

    amxc_var_set(bool, ret, status == gmap_status_ok);

    free(source);

    return ( status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Device_setName(amxd_object_t* object,
                              GMAPS_UNUSED amxd_function_t* func,
                              amxc_var_t* args,
                              amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;

    char* name = amxc_var_dyncast(cstring_t, GET_ARG(args, "name"));
    char* source = amxc_var_dyncast(cstring_t, GET_ARG(args, "source"));

    if(*source == '\0') {
        free(source);
        source = (char*) malloc(strlen("webui") + 1);
        strcpy(source, "webui");
    }

    status = gmaps_device_set_name(object, name, source);

    amxc_var_set(bool, ret, status == gmap_status_ok);

    free(name);
    free(source);
    return (status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Device_setActive(amxd_object_t* object,
                                GMAPS_UNUSED amxd_function_t* func,
                                amxc_var_t* args,
                                GMAPS_UNUSED amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;
    const char* key = amxd_object_get_name(object, AMXD_OBJECT_NAMED);
    bool active = GET_BOOL(args, "active");
    uint32_t priority = GET_UINT32(args, "priority");
    const char* source = GET_CHAR(args, "source");

    status = gmaps_device_set_active(key, active, source, priority);

    return (status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Device_set(amxd_object_t* object,
                          GMAPS_UNUSED amxd_function_t* func,
                          amxc_var_t* args,
                          amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;
    const char* key = amxd_object_get_name(object, AMXD_OBJECT_NAMED);
    amxc_var_t values;

    amxc_var_init(&values);
    amxc_var_copy(&values, GET_ARG(args, "parameters"));

    status = gmaps_device_set(key, &values);

    amxc_var_set(bool, ret, status == gmap_status_ok);

    amxc_var_clean(&values);
    return (status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}


amxd_status_t _Device_setTag(amxd_object_t* object,
                             GMAPS_UNUSED amxd_function_t* func,
                             amxc_var_t* args,
                             GMAPS_UNUSED amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;

    const char* key = amxd_object_get_name(object, AMXD_OBJECT_NAMED);
    ssv_string_t tag = amxc_var_dyncast(ssv_string_t, GET_ARG(args, "tag"));
    char* expression = amxc_var_dyncast(cstring_t, GET_ARG(args, "expression"));
    char* traverse = amxc_var_dyncast(cstring_t, GET_ARG(args, "traverse"));

    gmap_traverse_mode_t mode = gmap_traverse_mode(traverse);

    status = gmaps_device_set_tag(key, tag, expression, mode);

    free(tag);
    free(expression);
    free(traverse);

    return ( status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Device_hasTag(amxd_object_t* object,
                             GMAPS_UNUSED amxd_function_t* func,
                             GMAPS_UNUSED amxc_var_t* args,
                             amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;

    const char* key = amxd_object_get_name(object, AMXD_OBJECT_NAMED);
    ssv_string_t tag = amxc_var_dyncast(ssv_string_t, GET_ARG(args, "tag"));
    char* expression = amxc_var_dyncast(cstring_t, GET_ARG(args, "expression"));
    char* traverse = amxc_var_dyncast(cstring_t, GET_ARG(args, "traverse"));
    bool result = false;
    gmap_traverse_mode_t mode = gmap_traverse_mode(traverse);

    status = gmaps_device_has_tag(key, tag, expression, mode, &result);

    amxc_var_set_type(ret, AMXC_VAR_ID_BOOL);
    amxc_var_set_bool(ret, result);

    free(tag);
    free(expression);
    free(traverse);

    return ( status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Device_clearTag(amxd_object_t* object,
                               GMAPS_UNUSED amxd_function_t* func,
                               amxc_var_t* args,
                               amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;

    const char* key = amxd_object_get_name(object, AMXD_OBJECT_NAMED);
    ssv_string_t tag = amxc_var_dyncast(ssv_string_t, GET_ARG(args, "tag"));
    char* expression = amxc_var_dyncast(cstring_t, GET_ARG(args, "expression"));
    char* traverse = amxc_var_dyncast(cstring_t, GET_ARG(args, "traverse"));
    bool result = false;
    gmap_traverse_mode_t mode = gmap_traverse_mode(traverse);

    status = gmaps_device_clear_tag(key, tag, expression, mode);

    amxc_var_set_type(ret, AMXC_VAR_ID_BOOL);
    amxc_var_set_bool(ret, result);

    free(tag);
    free(expression);
    free(traverse);

    return ( status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;

}


amxd_status_t _Device_get(amxd_object_t* object,
                          GMAPS_UNUSED amxd_function_t* func,
                          amxc_var_t* args,
                          amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;

    const char* key = amxd_object_get_name(object, AMXD_OBJECT_NAMED);
    char* flags_string = amxc_var_dyncast(cstring_t, GET_ARG(args, "flags"));
    uint32_t flags = gmap_devices_flags_from_cstring(flags_string);

    status = gmaps_device_get(key, ret, flags);

    free(flags_string);

    return ( status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Device_setFunction(GMAPS_UNUSED amxd_object_t* object,
                                  GMAPS_UNUSED amxd_function_t* func,
                                  GMAPS_UNUSED amxc_var_t* args,
                                  GMAPS_UNUSED amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;
    char* function = NULL;
    char* sub_object = NULL;
    function = amxc_var_dyncast(cstring_t, GET_ARG(args, "function"));
    sub_object = amxc_var_dyncast(cstring_t, GET_ARG(args, "subObject"));

    amxc_var_t* data = GET_ARG(args, "data");
    status = gmaps_device_set_function(object, sub_object, function, data);
    amxc_var_set(bool, ret, status == gmap_status_ok);
    free(function);
    free(sub_object);
    return ( status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Device_removeFunction(GMAPS_UNUSED amxd_object_t* object,
                                     GMAPS_UNUSED amxd_function_t* func,
                                     GMAPS_UNUSED amxc_var_t* args,
                                     GMAPS_UNUSED amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;
    char* function = NULL;
    char* sub_object = NULL;
    function = amxc_var_dyncast(cstring_t, GET_ARG(args, "function"));
    sub_object = amxc_var_dyncast(cstring_t, GET_ARG(args, "subObject"));
    status = gmaps_device_remove_function(object, sub_object, function);
    amxc_var_set(bool, ret, status == gmap_status_ok);
    free(function);
    free(sub_object);

    return (status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Device_isImplemented(amxd_object_t* object,
                                    GMAPS_UNUSED amxd_function_t* func,
                                    GMAPS_UNUSED amxc_var_t* args,
                                    GMAPS_UNUSED amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;
    char* function = NULL;
    char* sub_object = NULL;
    function = amxc_var_dyncast(cstring_t, GET_ARG(args, "function"));
    sub_object = amxc_var_dyncast(cstring_t, GET_ARG(args, "subObject"));
    status = gmaps_device_is_function_implemented(object, sub_object, function, ret);

    free(function);
    free(sub_object);
    return ( status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Device_setAlternative(amxd_object_t* object,
                                     GMAPS_UNUSED amxd_function_t* func,
                                     amxc_var_t* args,
                                     amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;

    char* alternative = amxc_var_dyncast(cstring_t, GET_ARG(args, "alternative"));

    amxd_object_t* altdevice = gmaps_get_device(alternative);

    status = gmaps_device_set_alternative(object, altdevice);

    amxc_var_set(bool, ret, status == gmap_status_ok);

    free(alternative);

    return ( status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Device_removeAlternative(amxd_object_t* object,
                                        GMAPS_UNUSED amxd_function_t* func,
                                        amxc_var_t* args,
                                        GMAPS_UNUSED amxc_var_t* ret) {

    gmap_status_t status = gmap_status_ok;

    char* alternative = amxc_var_dyncast(cstring_t, GET_ARG(args, "alternative"));

    amxd_object_t* altdevice = gmaps_get_device(alternative);

    status = gmaps_device_remove_alternative(object, altdevice);

    amxc_var_set(bool, ret, status == gmap_status_ok);

    free(alternative);

    return ( status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;

}

amxd_status_t _Device_isAlternative(amxd_object_t* object,
                                    GMAPS_UNUSED amxd_function_t* func,
                                    amxc_var_t* args,
                                    amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;

    bool result = false;

    char* alternative_string = amxc_var_dyncast(cstring_t, GET_ARG(args, "alternative"));

    amxd_object_t* alternative = gmaps_get_device(alternative_string);

    result = gmaps_device_is_alternative_from(object, alternative);

    amxc_var_set_bool(ret, result);

    free(alternative_string);

    return ( status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Device_setAlternativeRules(amxd_object_t* object,
                                          GMAPS_UNUSED amxd_function_t* func,
                                          amxc_var_t* args,
                                          amxc_var_t* ret) {
    bool result = false;
    char* key = NULL;
    amxc_var_t* rules = NULL;
    amxd_status_t status;

    key = amxd_object_get_cstring_t(object, "Key", &status);
    when_str_empty_trace(key, exit, ERROR, "NULL pointer/empty string status: %d", status);
    rules = GET_ARG(args, "rules");
    when_null_trace(rules, exit, ERROR, "NULL pointer");
    result = gmaps_devices_merge_add_rules(key, rules);
    amxc_var_set_bool(ret, result);
exit:
    free(key);
    return result ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Device_removeAlternativeRules(amxd_object_t* object,
                                             GMAPS_UNUSED amxd_function_t* func,
                                             GMAPS_UNUSED amxc_var_t* args,
                                             amxc_var_t* ret) {
    bool result = false;
    char* key = NULL;
    amxd_status_t status;

    key = amxd_object_get_cstring_t(object, "Key", &status);
    when_str_empty_trace(key, exit, ERROR, "NULL pointer/empty string status: %d", status);
    result = gmaps_devices_merge_delete_rules(key);
    amxc_var_set_bool(ret, result);
exit:
    free(key);
    return result ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Device_isLinkedTo(amxd_object_t* object,
                                 GMAPS_UNUSED amxd_function_t* func,
                                 amxc_var_t* args,
                                 GMAPS_UNUSED amxc_var_t* ret) {

    gmap_status_t status = gmap_status_ok;

    bool result = false;

    char* device_string = amxc_var_dyncast(cstring_t, GET_ARG(args, "device"));
    char* traverse = amxc_var_dyncast(cstring_t, GET_ARG(args, "traverse"));

    gmap_traverse_mode_t traverse_mode = gmap_traverse_mode(traverse);

    amxd_object_t* device = gmaps_get_device(device_string);

    result = gmaps_device_is_linked_to(object, device, traverse_mode);

    amxc_var_set(bool, ret, result);

    free(device_string);
    free(traverse);

    return ( status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;

}


amxd_status_t _Device_topology(amxd_object_t* object,
                               GMAPS_UNUSED amxd_function_t* func,
                               amxc_var_t* args,
                               amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;
    amxc_var_t* data = NULL;
    uint32_t flags = false;
    gmap_traverse_mode_t traverse_mode = gmap_traverse_down;

    char* expression = amxc_var_dyncast(cstring_t, GET_ARG(args, "expression"));
    char* traverse = amxc_var_dyncast(cstring_t, GET_ARG(args, "traverse"));
    char* flags_string = amxc_var_dyncast(cstring_t, GET_ARG(args, "flags"));

    flags = gmap_devices_flags_from_cstring(flags_string);
    traverse_mode = gmap_traverse_mode(traverse);
    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    data = gmaps_device_topology(object, expression, traverse_mode, flags, &status);

    amxc_var_move(ret, amxc_var_get_first(data));
    free(expression);
    free(traverse);
    free(flags_string);
    amxc_var_delete(&data);
    return ( status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}


amxd_status_t _Device_getFirstParameter(amxd_object_t* object,
                                        GMAPS_UNUSED amxd_function_t* func,
                                        amxc_var_t* args,
                                        amxc_var_t* ret) {
    gmap_status_t status = gmap_status_unknown_error;
    amxc_var_t* data = NULL;
    const char* parameter = NULL;
    const char* expression = NULL;
    const char* traverse = NULL;
    gmap_traverse_mode_t traverse_mode = gmap_traverse_down;

    parameter = GET_CHAR(args, "parameter");
    when_null(parameter, exit);
    expression = GET_CHAR(args, "expression");
    traverse = GET_CHAR(args, "traverse");

    if(traverse && *traverse) {
        traverse_mode = gmap_traverse_mode(traverse);
    }

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    data = gmaps_device_get_first_parameter(object, parameter, expression, traverse_mode, &status);
    if(data) {
        amxc_var_move(ret, data);
    }
    amxc_var_dump(data, 0);
    amxc_var_dump(ret, 0);
    amxc_var_delete(&data);
exit:
    return ( status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Device_getParameters(amxd_object_t* object,
                                    GMAPS_UNUSED amxd_function_t* func,
                                    amxc_var_t* args,
                                    amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;
    amxc_var_t* data = NULL;
    gmap_traverse_mode_t traverse_mode = gmap_traverse_down;

    char* parameter = amxc_var_dyncast(cstring_t, GET_ARG(args, "parameter"));
    char* expression = amxc_var_dyncast(cstring_t, GET_ARG(args, "expression"));
    char* traverse = amxc_var_dyncast(cstring_t, GET_ARG(args, "traverse"));

    traverse_mode = gmap_traverse_mode(traverse);
    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    data = gmaps_device_get_parameters(object, parameter, expression, traverse_mode, &status);

    amxc_var_move(ret, data);
    free(parameter);
    free(expression);
    free(traverse);
    amxc_var_delete(&data);
    return ( status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Device_addAction(amxd_object_t* object,
                                GMAPS_UNUSED amxd_function_t* func,
                                amxc_var_t* args,
                                amxc_var_t* ret) {
    amxd_status_t status = amxd_status_ok;
    const char* name = GET_CHAR(args, "name");
    const char* func_name = GET_CHAR(args, "function");
    char* action_name = NULL;
    amxd_function_t* function = NULL;
    gmaps_function_data_t* func_data = NULL;

    amxc_var_set(bool, ret, false);

    when_null_status(object, exit, status = amxd_status_object_not_found);
    when_str_empty_status(func_name, exit, status = amxd_status_invalid_value);

    function = amxd_object_get_function(object, func_name);
    when_null_status(function, exit, status = amxd_status_object_not_found);

    func_data = (gmaps_function_data_t*) function->priv;

    if(func_data && func_data->action_name) {
        status = amxd_status_invalid_action;
        goto exit;
    }

    if(name && (name[0] != '\0')) {
        action_name = strdup(name);
    } else {
        action_name = strdup(func_name);
    }

    if(!func_data) {
        func_data = (gmaps_function_data_t*) calloc(1, sizeof(gmaps_function_data_t));
        when_null_status(func_data, exit, status = amxd_status_out_of_mem);
        func_data->device = object;
    }

    func_data->action_name = action_name;
    function->priv = func_data;
    status = amxd_status_ok;
    amxc_var_set(bool, ret, true);

exit:
    if(status != amxd_status_ok) {
        free(action_name);
    }
    return status;
}

amxd_status_t _Device_removeAction(amxd_object_t* object,
                                   GMAPS_UNUSED amxd_function_t* func,
                                   amxc_var_t* args,
                                   amxc_var_t* ret) {
    amxd_status_t status = amxd_status_ok;
    const char* func_name = GET_CHAR(args, "function");
    amxd_function_t* function = NULL;
    gmaps_function_data_t* func_data = NULL;

    amxc_var_set(bool, ret, false);

    when_null_status(object, exit, status = amxd_status_object_not_found);
    when_str_empty_status(func_name, exit, status = amxd_status_invalid_value);

    function = amxd_object_get_function(object, func_name);
    when_null_status(function, exit, status = amxd_status_object_not_found);

    func_data = (gmaps_function_data_t*) function->priv;

    if(func_data->action_name) {
        free(func_data->action_name);
        func_data->action_name = NULL;
    }
    if(!function->impl) {
        amxc_var_clean(&func_data->user_data);
        free(func_data);
        function->priv = NULL;
    }

    amxc_var_set(bool, ret, true);

exit:
    return status;
}
