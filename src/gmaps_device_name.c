/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "gmaps_priv.h"

#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#define ME "name"

#ifndef MIN
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#endif

#ifndef MAX
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#endif


typedef struct _gmaps_device_name_index_info {
    uint32_t index;         // the index to use
    char* stored_name;      // existing name, from different source
    uint32_t stored_index;  // index corresponding with stored_name
    char* old_name;         // existing name, from same source
    uint32_t old_index;     // index corresponding with old_name
} gmaps_device_name_index_info_t;

static amxd_object_t* gmaps_device_get_name(amxd_object_t* device) {
    /* TODO custom part */
    gmap_status_t status;
    amxc_var_t var_name_order;
    const char* name_order = NULL;
    amxc_string_t name_order_str;
    amxc_llist_t name_parts;
    amxd_object_t* config_obj = NULL;
    amxd_object_t* name_obj = NULL;
    amxd_object_t* alternatives_obj = NULL;

    amxc_llist_init(&name_parts);
    amxc_string_init(&name_order_str, 0);
    amxc_var_init(&var_name_order);

    when_null(device, exit);

    config_obj = gmap_dm_get_devices_config();
    when_null_trace(config_obj, exit, ERROR, "can not get config obj");
    status = gmaps_get_config(config_obj, "global", "NameOrder", &var_name_order);
    when_failed_trace(status, exit, ERROR, "can not get NameOrder from Config");
    name_order = amxc_var_constcast(cstring_t, &var_name_order);
    amxc_string_append(&name_order_str, name_order, strlen(name_order));
    amxc_string_split_to_llist(&name_order_str, &name_parts, ',');
    amxc_llist_for_each(it, &name_parts) {
        const char* source = amxc_string_get(amxc_string_from_llist_it(it), 0);
        name_obj = amxd_object_findf(device, "Names.%s", source);
        when_false_trace((name_obj == NULL), exit, INFO,
                         "Source %s found -> will set name for device %s to %s",
                         source,
                         amxd_object_get_name(device, AMXD_OBJECT_NAMED),
                         amxc_var_constcast(cstring_t, amxd_object_get_param_value(name_obj, "Name")));
    }
    alternatives_obj = amxd_object_get_child(device, "Alternative");
    amxd_object_for_each(instance, it, alternatives_obj) {
        amxd_object_t* alt_device = amxc_llist_it_get_data(it, amxd_object_t, it);
        name_obj = gmaps_device_get_name(alt_device);
        if(name_obj) {
            break;
        }
    }

exit:
    amxc_var_clean(&var_name_order);
    amxc_string_clean(&name_order_str);
    amxc_llist_clean(&name_parts, amxc_string_list_it_free);
    return name_obj;
}

/**
   @ingroup gmap_device_names
   @brief
   Select and set the name of a device object

   When a custom name selection function is set (see @ref gmaps_device_setNameSelector), this function is called first.
   If the custom name selection function does not return a name object, the default algorithm is used.
   If no custom name selection function is set, the default name selection algorithm is used.

   The default algorithm selects the name as follows (in the order specified)
   - Search a name from the nameOrder from the global config odl
   - Use the default name (from source "default")

   @warning
   - This function can only be called on server side. Use @ref gmap_isServer to determine if the code is running on the
   server side.

   @param device the device object for which a name must be selected
 */

void gmaps_device_select_name(amxd_object_t* device) {
    amxd_object_t* name_obj = NULL;
    when_null(device, exit);
    name_obj = gmaps_device_get_name(device);
    if(!name_obj) {
        name_obj = amxd_object_findf(device, "Names.default");
        when_null(name_obj, exit);
        SAH_TRACEZ_INFO(ME,
                        "Source %s found -> will set name for device %s to %s",
                        "default",
                        amxd_object_get_name(device, AMXD_OBJECT_NAMED),
                        amxc_var_constcast(cstring_t, amxd_object_get_param_value(name_obj, "Name")));
    }
    {
        const char* old_name = NULL;
        const char* new_name = NULL;
        const char* suffix = NULL;
        amxc_string_t full_name;
        amxc_string_init(&full_name, 0);
        old_name = amxc_var_constcast(cstring_t, amxd_object_get_param_value(device, "Name"));
        new_name = amxc_var_constcast(cstring_t, amxd_object_get_param_value(name_obj, "Name"));
        amxc_string_append(&full_name, new_name, strlen(new_name));
        suffix = amxc_var_constcast(cstring_t, amxd_object_get_param_value(name_obj, "Suffix"));
        if(suffix && *suffix) {
            amxc_string_append(&full_name, "-", 1);
            amxc_string_append(&full_name, suffix, strlen(suffix));
        }
        new_name = amxc_string_get(&full_name, 0);
        if((old_name && new_name && strcmp(old_name, new_name)) ||
           (!old_name && !*old_name && new_name)) {
            {
                // use transaction so libamxs can sync this parameter
                amxd_trans_t trans;
                amxd_trans_init(&trans);
                amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
                amxd_trans_select_object(&trans, device);
                amxd_trans_set_value(cstring_t, &trans, "Name", new_name);
                amxd_trans_apply(&trans, gmap_get_dm());
                amxd_trans_clean(&trans);
            }

            {
                amxc_var_t event_data;
                amxc_var_init(&event_data);
                amxc_var_add_key(cstring_t,
                                 &event_data,
                                 "OldName", old_name);
                amxc_var_add_key(cstring_t,
                                 &event_data,
                                 "Name", new_name);
                gmaps_event_send(amxd_object_get_name(device, AMXD_OBJECT_NAMED),
                                 GMAP_DEVICE_NAME_CHANGED_TEXT,
                                 GMAP_DEVICE_NAME_CHANGED,
                                 &event_data);
                amxc_var_clean(&event_data);
            }
        }
        amxc_string_clean(&full_name);
    }
exit:
    return;
}

/**
 * Returns the index suffix of the given @param name if it's available.
 * Else, zero is returned.
 * Returns the stripped name (i.e. name without index) via @param stripped_name.
 * The @param name pointer returned is the given name, with the suffix (index) stripped off.
 * This pointer must be freed.
 */
static uint32_t gmaps_device_name_take_index(const char* name, char** stripped_name) {
    uint32_t retval = 0;
    amxc_string_t name_str;
    amxc_llist_t name_parts;
    amxc_string_t* suffix = NULL;
    char* name_dup = NULL;

    name_dup = (char*) malloc(strlen(name) + 1);
    strcpy(name_dup, name);

    amxc_llist_init(&name_parts);
    amxc_string_init(&name_str, 0);
    amxc_string_append(&name_str, name, strlen(name));

    amxc_string_split_to_llist(&name_str, &name_parts, '-');
    when_true_status(amxc_llist_size(&name_parts) < 2, exit, retval = 0);
    suffix = amxc_string_from_llist_it(amxc_llist_get_last(&name_parts));
    if(amxc_string_is_numeric(suffix)) {
        amxc_string_t name_joined;
        amxc_string_init(&name_joined, 0);

        retval = atoi(amxc_string_get(suffix, 0));

        // Replace name by joined name.
        amxc_string_list_it_free(amxc_llist_take_last(&name_parts));
        amxc_string_join_llist(&name_joined, &name_parts, '-');
        free(name_dup);
        name_dup = amxc_string_take_buffer(&name_joined);
    }

exit:
    *stripped_name = name_dup;
    amxc_string_clean(&name_str);
    amxc_llist_clean(&name_parts, amxc_string_list_it_free);
    return retval;
}

/**
 * Returns the index to use for the new device name via the parameter @param info.
 * Returns the stripped name (i.e. name without index) via the parameter @param stripped_name.
 * The value of the index depends on several factors, see
 * documentation of @ref gmaps_device_name_apply.
 * The @param name pointer returned is the given name, with the suffix (index) stripped off.
 */
static void gmaps_device_name_get_index(const char* name,
                                        amxd_object_t* names_template,
                                        amxd_object_t* names_instance,
                                        char** stripped_name,
                                        gmaps_device_name_index_info_t* info) {
    uint32_t first_available_index = 0;

    info->index = gmaps_device_name_take_index(name, stripped_name);
    first_available_index = gmaps_device_name_table_get_first_available_index(*stripped_name);

    // If index was given but not available, set index to zero to use first available one.
    if((info->index != 0) && (gmaps_device_name_table_is_index_available(*stripped_name, info->index) == false)) {
        info->index = 0;
    }

    if((info->old_name != NULL) && (strcmp(*stripped_name, info->old_name) == 0)) {
        // If the new name and old name are the same, also consider the index of the old name.
        first_available_index = MIN(first_available_index, info->old_index);
    } else {
        // If the name already exists within the device under a different source, we use the same index.
        char* curr_stored_name = NULL;
        amxd_object_for_each(instance, it, names_template) {
            amxd_object_t* curr_name = amxc_llist_it_get_data(it, amxd_object_t, it);
            if(curr_name == names_instance) {
                continue;
            }
            curr_stored_name = amxd_object_get_value(cstring_t, curr_name, "Name", NULL);
            if(strcmp(curr_stored_name, *stripped_name) == 0) {
                info->stored_name = (char*) malloc(strlen(curr_stored_name) + 1);
                strcpy(info->stored_name, curr_stored_name);
                info->stored_index = amxd_object_get_value(uint32_t, curr_name, "Suffix", NULL);
                first_available_index = MIN(first_available_index, info->stored_index);
                free(curr_stored_name);
                break;
            }
            free(curr_stored_name);
        }
    }
    info->index = MAX(info->index, first_available_index);
}

static void gmaps_device_name_trans_set_index(amxd_trans_t* trans,
                                              uint32_t index) {
    if(index != 0) {
        amxd_trans_set_value(uint32_t, trans, "Suffix", index);
    } else {
        amxd_trans_set_value(cstring_t, trans, "Suffix", "");
    }
}

/**
 * Sets all indices to @param index of the instances of @param names_template if they are named
 * @param name and have a different source than the source of @param names_instance.
 * The datamodel is updated accordingly.
 */
static void gmaps_device_name_set_indices(const char* name,
                                          amxd_object_t* names_template,
                                          amxd_object_t* names_instance,
                                          uint32_t index) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);

    amxd_object_for_each(instance, it, names_template) {
        amxd_object_t* curr_name = amxc_llist_it_get_data(it, amxd_object_t, it);
        if(curr_name == names_instance) {
            continue;
        } else {
            char* curr_stored_name = amxd_object_get_value(cstring_t, curr_name, "Name", NULL);
            amxd_trans_select_object(&trans, curr_name);
            if(strcmp(curr_stored_name, name) == 0) {
                gmaps_device_name_trans_set_index(&trans, index);
            }
            free(curr_stored_name);
        }
    }

    amxd_trans_apply(&trans, gmap_get_dm());
    amxd_trans_clean(&trans);
}

/**
 * Returns true if the device with given @param key uses duplicate name detection.
 * Returns false otherwise.
 */
static bool gmaps_device_uses_duplicate_name_detection(const char* key) {
    // Duplicate name detection is disabled on self devices and upnp devices.
    bool result_self = false;
    bool result_upnp = false;
    bool result_logical = false;
    gmaps_device_has_tag(key, "self", "", gmap_traverse_this, &result_self);
    gmaps_device_has_tag(key, "logical", "", gmap_traverse_this, &result_upnp);
    gmaps_device_has_tag(key, "upnp", "", gmap_traverse_this, &result_logical);

    if(result_self || (result_upnp && result_logical)) {
        return false;
    }

    return true;
}

static bool gmaps_device_name_update_dm(amxd_trans_t* trans,
                                        const char* device_key,
                                        const char* name,
                                        const char* source,
                                        uint32_t index) {
    bool retval = false;

    amxd_trans_set_value(cstring_t, trans, "Name", name);
    amxd_trans_set_value(cstring_t, trans, "Source", source);
    if(gmaps_device_uses_duplicate_name_detection(device_key)) {
        gmaps_device_name_trans_set_index(trans, index);
    }
    when_failed_status(amxd_trans_apply(trans, gmap_get_dm()),
                       exit,
                       retval = false);

    retval = true;
exit:
    return retval;
}

static void gmaps_device_name_apply_send_event(const char* const key,
                                               amxd_object_t* names_instance,
                                               const gmaps_device_name_index_info_t* const info) {
    amxc_var_t data;
    const char* event_name = GMAP_DEVICE_SUB_NAME_ADDED_TEXT;
    uint32_t event_id = GMAP_DEVICE_SUB_NAME_ADDED;

    amxc_var_init(&data);
    amxd_object_get_params(names_instance, &data, amxd_dm_access_protected);

    if(info->old_name != NULL) {
        event_name = GMAP_DEVICE_SUB_NAME_CHANGED_TEXT;
        event_id = GMAP_DEVICE_SUB_NAME_CHANGED;
        amxc_var_add_key(cstring_t,
                         &data,
                         "OldName",
                         (info->old_name == NULL) ? "" : info->old_name);
    }

    gmaps_event_send(key, event_name, event_id, &data);

    amxc_var_clean(&data);
}

/**
 * Apply a new device name to @param device with given @param name and @param source.
 *
 * Device names must be globally (i.e. across all devices) unique.
 * A device name consists of a name part and an index part.
 *
 * To guarantee uniqueness, internally a device name table is stored
 * that keeps track of all device names used by gMap.
 *
 * The name may already exist if it was set on another @param device.
 * In that case, the new one is a duplicate device name and the distinct index number
 * is added as suffix to differentiate between the names.
 * Note that if the name is discovered via another @param source but the same @param device,
 * the same index number will used for both (since it refers to the same device).
 *
 * The given @param name may contain an index as suffix to the name with a dash,
 * e.g. "galaxy-42", where 42 is the index.
 * In that case, the given number is used as index number.
 * If it does not have a valid index (none given, or in use), the lowest available index for
 * the given @param name is used.
 * The lowest available index is the minimum of 2 numbers:
 *  (1) the lowest available index in the global names table for that name (across all devices), and
 *  (2) IF under the same device, the name is found but the source is different: this index number, and
 *      ELSE IF under the same device, the name is found and the source is the same: this index number.
 *      ELSE infinity.
 *
 * The given @param source may or may not exist already in the @param device.
 * If the source does not exist, it is created.
 * If the source exists, its name parameter is changed to the new name.
 *
 * The given @param name may exist in the @param device from other sources already.
 * This is checked. If such a match is found, all names are aligned to have the same index.
 */
static bool gmaps_device_name_apply(amxd_object_t* device, const char* name, const char* source) {
    bool retval = false;
    gmaps_device_name_index_info_t info;
    char* stripped_name = NULL;
    const char* device_key = amxd_object_get_name(device, AMXD_OBJECT_NAMED);
    amxd_object_t* names_template = amxd_object_get(device, "Names");
    amxd_object_t* names_instance = amxd_object_get_instance(names_template, source, 0);
    amxd_trans_t trans;

    memset(&info, 0, sizeof(info));

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    if(names_instance == NULL) {
        amxd_trans_select_object(&trans, names_template);
        amxd_trans_add_inst(&trans, 0, source);
    } else {
        amxd_trans_select_object(&trans, names_instance);
        info.old_name = amxd_object_get_value(cstring_t, names_instance, "Name", NULL);
        info.old_index = amxd_object_get_value(uint32_t, names_instance, "Suffix", NULL);
    }

    gmaps_device_name_get_index(name, names_template, names_instance, &stripped_name, &info);
    gmaps_device_name_set_indices(stripped_name, names_template, names_instance, info.index);

    if((info.old_name == NULL) || (strcmp(stripped_name, info.old_name) != 0) || (info.index != info.old_index)) {
        when_false_status(gmaps_device_name_update_dm(&trans, device_key, stripped_name, source, info.index),
                          exit,
                          retval = false);

        if(gmaps_device_uses_duplicate_name_detection(device_key)) {
            gmaps_device_name_table_remove(info.old_name, info.old_index);
            gmaps_device_name_table_remove(info.stored_name, info.stored_index);
            gmaps_device_name_table_add(stripped_name, info.index);
        }

        gmaps_device_name_apply_send_event(device_key, amxd_object_get_instance(names_template, source, 0), &info);
    }

    retval = true;
exit:
    amxd_trans_clean(&trans);
    free(stripped_name);
    free(info.old_name);
    free(info.stored_name);
    return retval;
}

/**
   @ingroup gmap_device_names
   @brief
   Generate a default name for the device

   When a custom default name generation function is set (see @ref gmaps_devices_setDefaultNameGenerator), this function is called first.
   If the custom default name generation function does not return a name string, the default algorithm is used.
   If no custom default name generation function is set, the default algorithm is used.

   The default algorithm generates a name as follows (in the order specified)
   - if the device contains the tags "self" and "hgw" the default name is HGW. Normally there should only be one such device
   - if the device contains the tag "self" the default name is the device key
   - if the device contains the tag "lan" the default name is device_<device index>
   - if the device contains the tag "usb" the default name is usb_<device index>

   @warning
   - This function can only be called on server side. Use @ref gmap_isServer to determine if the code is running on the
   server side.

   @note
   The returned name must be freed

   @param device the device object for which a name must be selected

   @return
   the default name for the device.
 */
char* gmaps_device_generate_default_name(amxd_object_t* device) {
    /* TODO custom implementation based on gmap server odl config section */
    const char* dev_name = NULL;
    amxc_string_t name;

    amxc_string_init(&name, 0);
    when_null(device, exit);

    if(gmaps_device_tag_has(device, "self")) {
        if(gmaps_device_tag_has(device, "hgw")) {
            amxc_string_set(&name, "HGW");
        } else {
            amxc_string_set(&name, amxd_object_get_name(device, AMXD_OBJECT_NAMED));
        }
        goto exit;
    }

    if(gmaps_device_tag_has(device, "usb")) {
        amxc_string_set(&name, "usb-");
    } else if(gmaps_device_tag_has(device, "lan")) {
        amxc_string_set(&name, "PC-");
    } else {
        amxc_string_set(&name, "device ");
    }

    dev_name = amxd_object_get_name(device, AMXD_OBJECT_NAMED);
    amxc_string_append(&name, dev_name, strlen(dev_name));

exit:
    return amxc_string_take_buffer(&name);
}

/**
   @ingroup gmap_device_names
   @brief
   Add a name to a device

   This function creates a instance of the "Names" template object within the device object.

   Optionally the name of the source that adds the name can be given, if no source is provided (NULL pointer or empty string),
   the source "webui" is used. If a name was already available for the source, the name is overwritten with the new one.

   After that the name is added, the name selection algorithm is used to select the name of the device.
   (See @ref section_device_names_selection)

   @warning
   - This function can only be called on server side. Use @ref gmap_isServer to determine if the code is running on the
   server side.

   @param key the device key for which a new name is added
   @param name the new name
   @param source optionally the source that added the new name can be given

   @return
   true when the name has been added, false if it was not possible to add the name
   Check pcb_error for error conditions. Possible errors:
    - gmap_error_invalid_name: empty name or null pointer
    - pcb_error_not_found: device with given key not found.
    - gmap_error_commit_failed: failed to commit new name
 */
gmap_status_t gmaps_device_set_name(amxd_object_t* device, const char* name, const char* source) {
    gmap_status_t retval = gmap_status_unknown_error;

    when_null_status(device, exit, retval = gmap_status_invalid_parameter);
    when_str_empty_status(name, exit, retval = gmap_status_invalid_parameter);
    when_str_empty_status(source, exit, retval = gmap_status_invalid_parameter);

    when_false_status(gmaps_device_name_apply(device, name, source),
                      exit,
                      retval = gmap_status_internal_error);

    retval = gmap_status_ok;
exit:
    return retval;
}

/**
   @ingroup gmap_device_names
   @brief
   Remove a name from a device

   Server side imlementation to remove a name.

   This function removes the name for the given source from the device. If no source name is given (NULL pointer or empty
   string) the source "webui" is removed.

   After that the name is removed, the name selection algorithm is used to select the name of the device.
   (See @ref section_device_names_selection)

   The "default" source can not be removed.

   @warning
   - This function can only be called on server side. Use @ref gmap_isServer to determine if the code is running on the
   server side.
   @param key the device key for which a new name is added
   @param source the name of the source that added the device name

   @return
   true when the name has been removed, false if it was not possible to remove the source
   Check pcb_error for error condition. Possible errors:
    - pcb_error_not_found: no device found for the given key
    - gmap_error_default_name: default name can not be deleted
    - gmap_error_commit_failed: failed to commit changes
 */
gmap_status_t gmaps_device_remove_name(const char* key, const char* source) {
    gmap_status_t retval = gmap_status_unknown_error;
    amxd_object_t* device = NULL;
    amxd_object_t* names_template = NULL;
    amxd_object_t* name_instance = NULL;

    when_str_empty_status(key, exit, retval = gmap_status_invalid_parameter);

    if(!(source && *source)) {
        source = "webui";
    }

    if(strcmp(source, "default") == 0) {
        SAH_TRACEZ_ERROR(ME, "Default name can not be deleted");
        retval = gmap_status_no_default_name;
        goto exit;
    }

    device = gmaps_get_device(key);
    when_null_status(device, exit, retval = gmap_status_device_not_found);

    names_template = amxd_object_get(device, "Names");
    when_null_status(device, exit, retval = gmap_status_data_model_error);
    name_instance = amxd_object_get_instance(names_template, source, 0);
    when_null(name_instance, exit);
    {
        amxc_var_t parameters;
        amxd_trans_t trans;
        amxd_status_t result = amxd_status_unknown_error;
        char* source_name = NULL;

        amxc_var_init(&parameters);
        source_name = amxd_object_get_value(cstring_t, name_instance, "Name", NULL);
        gmaps_device_name_table_remove(
            source_name,
            amxd_object_get_value(uint32_t, name_instance, "Suffix", NULL));
        free(source_name);
        amxd_object_get_params(name_instance, &parameters, amxd_dm_access_protected);

        amxd_trans_init(&trans);
        amxd_trans_select_object(&trans, names_template);
        amxd_trans_del_inst(&trans, 0, source);
        result = amxd_trans_apply(&trans, gmap_get_dm());
        retval = (result == amxd_status_ok ? gmap_status_ok : gmap_status_unknown_error);
        amxd_trans_clean(&trans);
        if(result == amxd_status_ok) {
            gmaps_event_send(key, GMAP_DEVICE_SUB_NAME_DELETED_TEXT, GMAP_DEVICE_SUB_NAME_DELETED, &parameters);
        }
        amxc_var_clean(&parameters);
    }
exit:
    return retval;
}
