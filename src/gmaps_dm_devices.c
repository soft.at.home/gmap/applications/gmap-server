/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "gmaps_priv.h"
#include "gmaps_uuid.h"
#include "gmaps_devices_multilink.h"

#define ME "dm_devices"

#ifndef STR_EMPTY
#define STR_EMPTY(x) ((x) == NULL || (x)[0] == '\0')
#endif

// events handling

/* for now only used by gmap queries */

/**
 *
 * @pre Precondition: `subobj_path` must be an indexed path and not a named path,
 *   so we can easily deal with objects that have a dot in the name.
 */
static amxd_object_t* get_device_obj_from_indexed_subobj_path(const char* subobj_path) {
    char* device_path = NULL;
    char* nextpart = NULL;
    amxd_object_t* device_obj = NULL;

    when_str_empty(subobj_path, exit);

    device_path = strdup(subobj_path);
    when_str_empty(device_path, exit);
    nextpart = strchr(device_path, '.'); //Devices.
    nextpart = nextpart + 1;
    nextpart = strchr(nextpart, '.');    //Devices.Device.
    nextpart = nextpart + 1;
    nextpart = strchr(nextpart, '.');    //Devices.Device.17
    *nextpart = 0;

    SAH_TRACEZ_INFO(ME, "device_path: %s\n", device_path);

    when_str_empty(device_path, exit);
    // Note: this is safe against names with dots or other special characters, because
    // device_path is an indexed path ("Device.Device.123") instead of a named path ("Device.Device.dot.in.name][!").
    device_obj = amxd_dm_findf(gmap_get_dm(), "%s", (const char*) device_path);
exit:
    free(device_path);
    return device_obj;
}

void _device_added(const char* const sig_name,
                   const amxc_var_t* const data,
                   GMAPS_UNUSED void* const priv) {
    amxd_object_t* device_template_obj = NULL;
    amxd_object_t* inst_obj = NULL;
    when_str_empty(sig_name, exit);
    SAH_TRACEZ_INFO(ME, "event device %s received", sig_name);

    device_template_obj = amxd_dm_signal_get_object(gmap_get_dm(), data);
    when_null(device_template_obj, exit);
    amxd_object_get_instance(device_template_obj, GET_CHAR(data, "name"), 0);
    when_null(inst_obj, exit);

    gmaps_query_verify_all(inst_obj);
exit:
    return;
}

amxd_status_t _remove_device_instance(amxd_object_t* object,
                                      GMAPS_UNUSED amxd_param_t* param,
                                      GMAPS_UNUSED amxd_action_t reason,
                                      const amxc_var_t* const args,
                                      GMAPS_UNUSED amxc_var_t* const retval,
                                      GMAPS_UNUSED void* priv) {
    gmap_status_t status = gmap_status_ok;
    amxd_status_t rv = amxd_status_ok;
    amxd_object_t* dev = NULL;
    const char* key = NULL;
    const char* name = GET_CHAR(args, "name");
    int32_t index = GET_INT32(args, "index");

    when_null_status(object, exit, rv = amxd_status_object_not_found);

    if(((name == NULL) || (name[0] == '\0')) && (index != 0)) {
        amxd_object_t* temp_object = amxd_object_get_instance(object, NULL, index);
        key = amxd_object_get_name(temp_object, AMXD_OBJECT_NAMED);
    } else if((index == 0) && ((name != NULL) && (name[0] != '\0'))) {
        key = name;
    } else {
        rv = amxd_status_missing_key;
        goto exit;
    }

    status = gmaps_device_cleanup(object, &dev, key);
    when_false_status(status == gmap_status_ok, exit, rv = amxd_status_unknown_error);

exit:
    return rv;
}

void _device_changed(const char* const sig_name,
                     const amxc_var_t* const data,
                     GMAPS_UNUSED void* const priv) {
    amxd_object_t* device_obj = NULL;
    when_str_empty(sig_name, exit);
    SAH_TRACEZ_INFO(ME, "event device %s received", sig_name);
    device_obj = amxd_dm_signal_get_object(gmap_get_dm(), data);
    when_null(device_obj, exit);

    gmaps_query_verify_all(device_obj);
exit:
    return;
}

void _device_subobject_update(const char* const sig_name,
                              const amxc_var_t* const data,
                              GMAPS_UNUSED void* const priv) {
    amxd_object_t* device_obj = NULL;
    when_str_empty(sig_name, exit);
    SAH_TRACEZ_INFO(ME, "event device subobject  %s received", sig_name);
    // Use "path" instead of "object" to have indexed paths instead of named paths because
    // indexed paths are easier to deal with if the names contains dots.
    device_obj = get_device_obj_from_indexed_subobj_path(GET_CHAR(data, "path"));

    gmaps_query_verify_all(device_obj);
exit:
    return;
}

void _device_names_subobject_update(const char* const sig_name,
                                    const amxc_var_t* const data,
                                    GMAPS_UNUSED void* const priv) {
    amxd_object_t* device_obj = NULL;
    when_str_empty(sig_name, exit);
    SAH_TRACEZ_INFO(ME, "event names device subobject  %s received", sig_name);
    // Use "path" instead of "object" to have indexed paths instead of named paths because
    // indexed paths are easier to deal with if the names contains dots.
    device_obj = get_device_obj_from_indexed_subobj_path(GET_CHAR(data, "path"));
    gmaps_device_select_name(device_obj);

exit:
    return;
}


// methods handling

amxd_status_t _Devices_createDevice(amxd_object_t* object,
                                    GMAPS_UNUSED amxd_function_t* func,
                                    amxc_var_t* args,
                                    amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;
    amxd_object_t* device_templ = NULL;

    char* key = amxc_var_dyncast(cstring_t, GET_ARG(args, "key"));
    char* discovery_source =
        amxc_var_dyncast(cstring_t, GET_ARG(args, "discovery_source"));
    char* tags = amxc_var_dyncast(cstring_t, GET_ARG(args, "tags"));
    bool persistent = amxc_var_dyncast(bool, GET_ARG(args, "persistent"));
    char* default_name =
        amxc_var_dyncast(cstring_t, GET_ARG(args, "default_name"));
    amxc_var_t* values = GET_ARG(args, "values");

    device_templ = amxd_object_get_child(object, "Device");

    status = gmaps_new_device(device_templ,
                              key,
                              discovery_source,
                              tags,
                              persistent,
                              default_name,
                              values);

    amxc_var_set(bool, ret, status == gmap_status_ok);

    free(key);
    free(discovery_source);
    free(tags);
    free(default_name);
    amxc_var_delete(&values);

    return (status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}


amxd_status_t _Devices_createDeviceOrGetKey(GMAPS_UNUSED amxd_object_t* devices,
                                            GMAPS_UNUSED amxd_function_t* func,
                                            amxc_var_t* args,
                                            amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;
    const char* mac = amxc_var_constcast(cstring_t, GET_ARG(args, "mac"));
    const char* discovery_source =
        amxc_var_constcast(cstring_t, GET_ARG(args, "discovery_source"));
    const char* tags = amxc_var_constcast(cstring_t, GET_ARG(args, "tags"));
    bool persistent = amxc_var_dyncast(bool, GET_ARG(args, "persistent"));
    const char* default_name =
        amxc_var_constcast(cstring_t, GET_ARG(args, "default_name"));
    amxc_var_t* values = GET_ARG(args, "values");
    char* key = NULL;
    bool already_exists = false;

    status = gmaps_uuid_new_device_or_get_key(mac,
                                              discovery_source,
                                              tags,
                                              persistent,
                                              default_name,
                                              values,
                                              &key,
                                              &already_exists);

    amxc_var_add_key(cstring_t, args, "key", key);
    free(key);
    amxc_var_add_key(bool, args, "already_exists", already_exists);

    amxc_var_set(bool, ret, status == gmap_status_ok);

    return (status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Devices_findByMac(GMAPS_UNUSED amxd_object_t* devices,
                                 GMAPS_UNUSED amxd_function_t* func,
                                 amxc_var_t* args,
                                 amxc_var_t* ret) {
    const char* mac = amxc_var_constcast(cstring_t, GET_ARG(args, "mac"));

    const char* key = gmaps_uuid_get_key_by_mac(mac);
    if(key != NULL) {
        amxc_var_set(cstring_t, ret, key);
    } else {
        amxc_var_set_type(ret, AMXC_VAR_ID_NULL);
    }

    return amxd_status_ok;
}

amxd_status_t _Devices_destroyDevice(amxd_object_t* object,
                                     GMAPS_UNUSED amxd_function_t* func,
                                     amxc_var_t* args,
                                     amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;
    amxd_object_t* device_templ = NULL;

    char* key = amxc_var_dyncast(cstring_t, GET_ARG(args, "key"));

    device_templ = amxd_object_get_child(object, "Device");
    status = gmaps_delete_device(device_templ, key);

    amxc_var_set(bool, ret, status == gmap_status_ok);

    free(key);
    return (status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Devices_find(GMAPS_UNUSED amxd_object_t* object,
                            GMAPS_UNUSED amxd_function_t* func,
                            amxc_var_t* args,
                            amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;
    uint32_t flags = 0;
    char* expression = NULL;
    amxc_var_t* expression_var = GET_ARG(args, "expression");
    char* flags_char = amxc_var_dyncast(cstring_t, GET_ARG(args, "flags"));

    flags = gmap_devices_flags_from_cstring(flags_char);

    if(amxc_var_type_of(expression_var) == AMXC_VAR_ID_HTABLE) {
        // return a hash table
        amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
        // fetch the devices per expression available
        amxc_htable_for_each(it, amxc_var_constcast(amxc_htable_t, expression_var)) {
            // (key, value) of htable is (expression_id, expression)
            const char* expression_id = amxc_htable_it_get_key(it);
            expression = amxc_var_dyncast(cstring_t, amxc_var_from_htable_it(it));
            // add an empty list of devices
            amxc_var_t* devices = amxc_var_add_key(amxc_llist_t, ret, expression_id, NULL);
            // fetch devices
            status = gmaps_find_devices(expression, 0, flags, devices);
            when_failed_status(status, exit, amxc_var_clean(ret));
            free(expression);
            expression = NULL;
        }
    } else {
        // return a list
        amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
        // fetch devices
        // TODO: add support for UID
        expression = amxc_var_dyncast(cstring_t, expression_var);
        status = gmaps_find_devices(expression, 0, flags, ret);
        when_failed_status(status, exit, amxc_var_clean(ret));
    }

exit:
    free(expression);
    free(flags_char);
    return (status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Devices_linkAdd(GMAPS_UNUSED amxd_object_t* object,
                               GMAPS_UNUSED amxd_function_t* func,
                               amxc_var_t* args,
                               amxc_var_t* ret) {
    const char* upper_device = GET_CHAR(args, "upper_device");
    const char* lower_device = GET_CHAR(args, "lower_device");
    const char* type = GET_CHAR(args, "type");
    const char* datasource = GET_CHAR(args, "datasource");

    bool ok = gmaps_devices_multilink_add(upper_device, lower_device, type, datasource);

    amxc_var_set(bool, ret, ok);
    return ok ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Devices_linkReplace(GMAPS_UNUSED amxd_object_t* object,
                                   GMAPS_UNUSED amxd_function_t* func,
                                   amxc_var_t* args,
                                   amxc_var_t* ret) {
    const char* upper_device = GET_CHAR(args, "upper_device");
    const char* lower_device = GET_CHAR(args, "lower_device");
    const char* type = GET_CHAR(args, "type");
    const char* datasource = GET_CHAR(args, "datasource");

    bool ok = gmaps_devices_multilink_replace(upper_device, lower_device, type, datasource);

    amxc_var_set(bool, ret, ok);
    return ok ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Devices_linkRemove(GMAPS_UNUSED amxd_object_t* object,
                                  GMAPS_UNUSED amxd_function_t* func,
                                  amxc_var_t* args,
                                  amxc_var_t* ret) {
    const char* upper_device = GET_CHAR(args, "upper_device");
    const char* lower_device = GET_CHAR(args, "lower_device");
    const char* datasource = GET_CHAR(args, "datasource");

    bool ok = gmaps_devices_multilink_remove(upper_device, lower_device, datasource);

    amxc_var_set(bool, ret, ok);
    return ok ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Devices_block(amxd_object_t* object,
                             GMAPS_UNUSED amxd_function_t* func,
                             amxc_var_t* args,
                             GMAPS_UNUSED amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;
    char* key = amxc_var_dyncast(cstring_t, GET_ARG(args, "key"));

    status = gmaps_block_device(object, key);

    free(key);
    return (status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Devices_unblock(GMAPS_UNUSED amxd_object_t* object,
                               GMAPS_UNUSED amxd_function_t* func,
                               amxc_var_t* args,
                               GMAPS_UNUSED amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;
    char* key = amxc_var_dyncast(cstring_t, GET_ARG(args, "key"));

    status = gmaps_unblock_device(key);

    free(key);
    return (status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _Devices_isBlocked(GMAPS_UNUSED amxd_object_t* object,
                                 GMAPS_UNUSED amxd_function_t* func,
                                 amxc_var_t* args,
                                 GMAPS_UNUSED amxc_var_t* ret) {
    bool blocked = false;
    char* key = amxc_var_dyncast(cstring_t, GET_ARG(args, "key"));

    blocked = gmaps_device_is_blocked(key);

    amxc_var_set(bool, ret, blocked);

    free(key);
    return amxd_status_ok;
}

amxd_status_t _Devices_notify(GMAPS_UNUSED amxd_object_t* object,
                              GMAPS_UNUSED amxd_function_t* func,
                              amxc_var_t* args,
                              GMAPS_UNUSED amxc_var_t* ret) {

    const char* key = GET_CHAR(args, "key");
    uint32_t event_id = GET_UINT32(args, "id");
    const char* event_name = GET_CHAR(args, "name");
    amxc_var_t* data = GET_ARG(args, "data");

    gmaps_event_send(key, event_name, event_id, data);

    return amxd_status_ok;
}

amxd_status_t _Devices_get(GMAPS_UNUSED amxd_object_t* object,
                           GMAPS_UNUSED amxd_function_t* func,
                           amxc_var_t* args,
                           amxc_var_t* ret) {
    amxc_var_t* result = NULL;
    uint32_t flags = 0;
    char* expression = amxc_var_dyncast(cstring_t, GET_ARG(args, "expression"));
    char* flag_string = amxc_var_dyncast(cstring_t, GET_ARG(args, "flags"));

    flags = gmap_devices_flags_from_cstring(flag_string);

    result = gmaps_devices_get(expression, flags);

    amxc_var_copy(ret, result);

    free(expression);
    free(flag_string);
    amxc_var_delete(&result);

    return amxd_status_ok;
}

amxd_status_t _Devices_csiFinished(GMAPS_UNUSED amxd_object_t* object,
                                   GMAPS_UNUSED amxd_function_t* func,
                                   amxc_var_t* args,
                                   GMAPS_UNUSED amxc_var_t* ret) {

    uint32_t id = 0;
    uint32_t status = 0;
    amxc_var_t* retval = NULL;

    // take arguments out of the argument list
    id = GET_UINT32(args, "ID");
    status = GET_UINT32(args, "status");
    retval = GET_ARG(args, "retval");

    gmaps_device_function_done(id, status, retval);

    return amxd_status_ok;
}

amxd_status_t _Devices_removeInactiveDevices(GMAPS_UNUSED amxd_object_t* object,
                                             GMAPS_UNUSED amxd_function_t* func,
                                             amxc_var_t* args,
                                             GMAPS_UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_ok;
    const uint32_t min_inactive = GET_UINT32(args, "minimumInactiveInterval");
    uint32_t nr_of_removed = 0;

    if(!remove_inactive_devices(min_inactive, &nr_of_removed)) {
        status = amxd_status_unknown_error;
    }

    amxc_var_add_key(uint32_t, args, "nrOfRemovedDevices", nr_of_removed);

    return status;
}

