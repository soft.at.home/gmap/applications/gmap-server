/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>

#include "gmaps_priv.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#define ME "dm_query"


amxd_status_t _Query_matchingDevices(amxd_object_t* object,
                                     GMAPS_UNUSED amxd_function_t* func,
                                     GMAPS_UNUSED amxc_var_t* args,
                                     amxc_var_t* ret) {

    gmap_status_t status = gmap_status_ok;

    status = gmaps_query_get_match_devices_without_fields(object, ret);

    return (status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}

amxd_status_t _openQuery(GMAPS_UNUSED amxd_object_t* object,
                         GMAPS_UNUSED amxd_function_t* func,
                         amxc_var_t* args,
                         amxc_var_t* ret) {

    gmap_status_t status = gmap_status_ok;
    amxd_status_t amxdstatus = amxd_status_ok;
    gmap_query_t* query = NULL;
    amxc_var_t* flags_var = NULL;
    amxc_string_t* flags_str = NULL;

    const char* expression = GET_CHAR(args, "expression");
    const char* name = GET_CHAR(args, "name");
    amxc_var_t* devices = NULL;
    const char* flags_char = GET_CHAR(args, "flags");
    gmap_query_flags_t flags = gmap_query_flags_from_cstring(flags_char);
    SAH_TRACEZ_INFO(ME, "open query name: %s expression: %s flags: %s(%#x)",
                    name, expression, flags_char, flags);

    when_str_empty_status(name, exit, amxdstatus = amxd_status_invalid_arg);
    when_str_empty_status(expression, exit, amxdstatus = amxd_status_invalid_arg);
    status = gmaps_query_open(expression, name, flags, gmaps_query_notify_client, NULL, &query);
    when_failed_status(status, exit, amxdstatus = amxd_status_unknown_error);
    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, ret, "id", query->id);
    amxc_var_add_key(uint32_t, ret, "index", query->index);
    amxc_var_add_key(cstring_t, ret, "name", gmaps_query_name(query));
    flags_str = gmap_query_flags_string(query->flags);
    flags_var = amxc_var_add_new_key(ret, "flags");
    amxc_var_push(amxc_string_t, flags_var, flags_str);

    SAH_TRACEZ_INFO(ME, "openquery succeeded: returned name: %s index:%d id:%d flags:%#x",
                    gmaps_query_name(query), query->index, query->id, query->flags);

    devices = amxc_var_add_new_key(ret, "devices");
    status = gmaps_query_get_match_devices_with_fields(gmaps_query_object(query), devices);
    when_failed_status(status, exit, amxdstatus = amxd_status_unknown_error);

exit:
    amxc_string_delete(&flags_str);
    return amxdstatus;
}

amxd_status_t _closeQuery(GMAPS_UNUSED amxd_object_t* object,
                          GMAPS_UNUSED amxd_function_t* func,
                          amxc_var_t* args,
                          GMAPS_UNUSED amxc_var_t* ret) {

    gmap_status_t status = gmap_status_ok;
    gmap_query_t* gmap_query = NULL;

    uint32_t index = GET_UINT32(args, "index");
    uint32_t id = GET_UINT32(args, "id");

    SAH_TRACEZ_INFO(ME, "close query index:%d id: %d", index, id);

    gmap_query = gmaps_query_find(index, id);
    if(gmap_query) {
        gmaps_query_close(gmap_query);
    }

    return (status == gmap_status_ok) ? amxd_status_ok : amxd_status_unknown_error;
}
