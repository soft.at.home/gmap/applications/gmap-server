/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/


#include <debug/sahtrace.h>
#include "gmaps_priv.h"
#include <amxc/amxc_macros.h>

#define ME "dm_devices"


amxc_var_t* gmaps_device_topology(amxd_object_t* device, const char* expression, gmap_traverse_mode_t mode, uint32_t flags, gmap_status_t* status) {
    topology_data_t topology_data;
    amxc_var_t result;
    amxc_var_t* topology = NULL;

    amxc_llist_init(&(topology_data.stack));
    topology_data.flags = flags;

    amxc_var_init(&result);
    amxc_var_set_type(&result, AMXC_VAR_ID_HTABLE);

    // add a children list to the result map and push the result map on the stack
    topology = amxc_var_add_key(amxc_llist_t, &result, "Children", NULL);
    gmap_topology_push(&topology_data.stack, &result);

    // recursively traverse the tree and construct a topology tree
    if(gmaps_traverse_tree(device, mode, expression, gmap_traverse_topology, &topology_data) == gmap_traverse_failed) {
        SAH_TRACEZ_ERROR(ME, "Failed to traverse tree");
        *status = gmap_status_unknown_error;
        goto exit;
    }

exit:
    amxc_var_take_it(topology);
    amxc_var_clean(&result);
    amxc_llist_clean(&(topology_data.stack), free_stack_item);
    return topology;
}


gmap_traverse_status_t gmap_traverse_topology(amxd_object_t* device, gmap_traverse_action_t action, void* userdata) {
    gmap_traverse_status_t status = gmap_traverse_continue;
    topology_data_t* topology_data = (topology_data_t*) userdata;
    amxc_var_t* data = gmap_topology_current(&(topology_data->stack));

    switch(action) {
    case gmap_traverse_start: {
        SAH_TRACEZ_INFO(ME, "Traverse start");
        break;
    }
    case gmap_traverse_level_push: {
        SAH_TRACEZ_INFO(ME, "Traverse push %s", amxd_object_get_name(device, AMXD_OBJECT_NAMED));
        break;
    }
    case gmap_traverse_device_matching: {
        SAH_TRACEZ_INFO(ME, "Traverse device matching %s", amxd_object_get_name(device, AMXD_OBJECT_NAMED));
        gmap_topology_add_device(&(topology_data->stack), data, device, topology_data->flags);
        amxc_var_t* new_device = gmap_topology_current(&(topology_data->stack));
        amxc_var_add_key(bool, new_device, "Matching", true);
        break;
    }
    case gmap_traverse_device_not_matching: {
        if(topology_data->flags & GMAP_TOPOLOGY_NO_RECURSE) {
            status = gmap_traverse_no_recurse;
        }
        SAH_TRACEZ_INFO(ME, "Traverse device not matching %s", amxd_object_get_name(device, AMXD_OBJECT_NAMED));
        gmap_topology_add_device(&(topology_data->stack), data, device, GMAP_TOPOLOGY_NO_DETAILS | GMAP_TOPOLOGY_NO_ACTIONS);
        amxc_var_t* new_device = gmap_topology_current(&(topology_data->stack));
        amxc_var_add_key(bool, new_device, "Matching", false);
        break;
    }
    case gmap_traverse_device_done: {
        SAH_TRACEZ_INFO(ME, "Traverse device done %s", amxd_object_get_name(device, AMXD_OBJECT_NAMED));
        amxc_var_t* previous = gmap_topology_pop(&(topology_data->stack));
        amxc_var_t* children = amxc_var_get_key(previous, "Children", AMXC_VAR_FLAG_DEFAULT);

        GMAPS_UNUSED const amxc_htable_t* t = amxc_var_get_const_amxc_htable_t(previous);

        // remove the children list if it is empty or if it necessary due to flags
        handle_children(children, topology_data->flags);

        // verify matching parameter
        amxc_var_t* matching_var = amxc_var_get_key(previous, "Matching", AMXC_VAR_FLAG_DEFAULT);
        bool matching = amxc_var_get_bool(matching_var);

        // remove matching parameter
        amxc_var_delete(&matching_var);

        if(topology_data->flags & GMAP_TOPOLOGY_NO_RECURSE) {
            // remove if not matching and no recurse flag is set
            if(!matching) {
                SAH_TRACEZ_INFO(ME, "device is not matching and no recurse is set -> remove from list");
                amxc_var_delete(&previous);
            }
        }
        break;
    }
    case gmap_traverse_level_pop: {
        SAH_TRACEZ_INFO(ME, "Traverse pop %s", amxd_object_get_name(device, AMXD_OBJECT_NAMED));
        break;
    }
    case gmap_traverse_stop: {
        SAH_TRACEZ_INFO(ME, "Traverse stop");
        break;
    }
    default: {
        break;
    }
    }
    return status;
}


void gmap_topology_add_device(amxc_llist_t* stack, amxc_var_t* data, amxd_object_t* device, uint32_t flags) {
    /* get the children list from data */
    amxc_var_t* device_table = NULL;
    amxc_var_t* children = NULL;

    children = amxc_var_get_key(data, "Children", AMXC_VAR_FLAG_DEFAULT);

    // add the device map to the children list
    device_table = amxc_var_add(amxc_htable_t, children, NULL);

    /* get all parameters (including the sub objects) */
    gmaps_device_get_recursive(device, device_table, true, flags);

    /* add an empty list for the children */
    amxc_var_add_key(amxc_llist_t, device_table, "Children", NULL);

    /* push device map to stack */
    gmap_topology_push(stack, device_table);
}


void gmap_topology_push(amxc_llist_t* stack, amxc_var_t* device_table) {
    item_t* item = (item_t*) calloc(1, sizeof(item_t));
    item->data = device_table;

    amxc_llist_prepend(stack, &item->lit);
}


amxc_var_t* gmap_topology_pop(amxc_llist_t* stack) {
    amxc_llist_it_t* it = amxc_llist_take_first(stack);
    item_t* item = amxc_container_of(it, item_t, lit);
    amxc_var_t* data = item->data;
    item->data = NULL;

    free_stack_item(it);

    return data;
}


amxc_var_t* gmap_topology_current(amxc_llist_t* stack) {
    amxc_llist_it_t* it = amxc_llist_get_first(stack);
    item_t* item = amxc_container_of(it, item_t, lit);

    return item->data;
}


void free_stack_item(amxc_llist_it_t* it) {
    item_t* item = amxc_container_of(it, item_t, lit);
    free(item);
}


void handle_children(amxc_var_t* children, uint32_t flags) {
    GMAPS_UNUSED bool retval = true;
    when_null_status(children, exit, retval = false);

    // remove the children list if a random mac address is included
    // and the flag GMAP_TOPOLOGY_INCLUDE_RANDOM_MAC is not set
    if(!(flags & GMAP_TOPOLOGY_INCLUDE_RANDOM_MAC)) {
        amxc_var_for_each(child, children) {
            const char* child_key = GET_CHAR(child, "Key");
            bool result;
            gmaps_device_has_tag(child_key, "randomized_mac", "", gmap_traverse_this, &result);
            if(result) {
                gmaps_device_has_tag(child_key, "placeholder", "", gmap_traverse_this, &result);
                if(!result) {
                    SAH_TRACEZ_INFO(ME, "Excluding randomized_mac device [%s] without placeholder tag", child_key);
                    amxc_var_delete(&child);
                }
            }
        }
    }

    // remove empty children list
    if(amxc_llist_is_empty(&(children->data.vl))) {
        SAH_TRACEZ_INFO(ME, "Children list is empty, delete from topology");
        amxc_var_delete(&children);
    }
exit:
    return;
}
