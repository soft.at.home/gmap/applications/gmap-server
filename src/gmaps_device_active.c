/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/


#include <stdlib.h>
#include <string.h>

/** The lowest possible number for a priority (expressing the highest possible priority) */
#define GMAPS_DEVICE_ACTIVE_PRIORITY_MIN 1
/** The highest possible number for a priority (expressing the lowest possible priority) */
#define GMAPS_DEVICE_ACTIVE_PRIORITY_MAX 100

#include "gmaps_priv.h"
#include "debug/sahtrace.h"
#include "debug/sahtrace_macros.h"

#define ME "gmaps"

/**
 * Implements whether a device is active based on the sources are active.
 *
 * This is according to the specifications in the .odl (under "Devices.Device.Active").
 *
 * If `source_override` is not NULL, then for the source `source_override` the given priority
 * `priority_override` and active value `active_override` are used instead of the values
 * currently stored for the device.
 * If these values are being changed in a transaction, passing the values to this function allows
 * the values to be taken into account before the transaction is committed.
 */
static gmap_status_t s_calculate_active(bool* tgt_active, amxd_object_t* device,
                                        const char* source_override, uint32_t priority_override, bool active_override) {

    amxd_object_t* actives_template = amxd_object_get_child(device, "Actives");
    amxd_status_t status = amxd_status_unknown_error;
    uint32_t highest_priority = GMAPS_DEVICE_ACTIVE_PRIORITY_MAX + 1;
    // From the spec: "If no Actives instance exists, the Active parameter is considered false."
    bool active_so_far = false;
    when_null_trace(tgt_active, error, ERROR, "NULL argument");
    when_null_trace(device, error, ERROR, "NULL argument");
    when_null_trace(source_override, error, ERROR, "NULL argument");

    if(source_override != NULL) {
        active_so_far = active_override;
        highest_priority = priority_override;
    }

    amxd_object_for_each(instance, child_it, actives_template) {
        amxd_object_t* active_obj = amxc_llist_it_get_data(child_it, amxd_object_t, it);
        bool current_active = false;
        uint32_t current_priority = 0;
        char* current_source = NULL;
        bool skip = false;

        current_source = amxd_object_get_cstring_t(active_obj, "Source", &status);
        when_failed_trace(status, error, ERROR, "Error getting datamodel value");
        skip = (source_override != NULL) && (0 == strcmp(source_override, current_source));
        free(current_source);
        current_source = NULL;

        if(skip) {
            continue;
        }

        current_active = amxd_object_get_bool(active_obj, "Active", &status);
        when_failed_trace(status, error, ERROR, "Error getting datamodel value");
        current_priority = amxd_object_get_uint32_t(active_obj, "Priority", NULL);
        when_failed_trace(status, error, ERROR, "Error getting datamodel value");

        if(current_priority < highest_priority) {
            // From the spec: "The instance in Devices.Device.{i}.Actives with the highest priority
            // is selected" and "With 1 the highest priority"
            highest_priority = current_priority;
            active_so_far = current_active;
        } else if(current_priority == highest_priority) {
            // From the spec: "if multiple instances have the highest priority then it's a AND
            //                 between all the values."
            active_so_far = active_so_far && current_active;
        }
    }

    *tgt_active = active_so_far;
    return gmap_status_ok;
error:
    return gmap_status_internal_error;
}

static gmap_status_t s_update_device_active(amxd_trans_t* trans, amxc_ts_t* now, amxd_object_t* device,
                                            const char* source_override, uint32_t priority_override, bool source_active_override) {

    gmap_status_t retval = gmap_status_unknown_error;
    amxd_status_t status = amxd_status_unknown_error;
    bool old_device_active = false;
    bool new_device_active = false;
    amxc_var_t old_device_active_var;
    amxc_var_init(&old_device_active_var);

    // Note: get_bool not part of transaction, relying on read-only and single-threaded.
    old_device_active = amxd_object_get_bool(device, "Active", &status);
    when_failed_trace(status, exit, ERROR, "Error getting datamodel value");
    retval = s_calculate_active(&new_device_active, device, source_override,
                                priority_override, source_active_override);
    when_failed_trace(retval, exit, ERROR, "Error calculating device active");

    if(old_device_active == new_device_active) {
        retval = gmap_status_ok;
        goto exit;
    }

    amxd_trans_set_value(bool, trans, "Active", new_device_active);

    if(gmap_is_ntp_synchronized()) {
        amxd_trans_set_value(amxc_ts_t, trans, "LastConnection", now);
    }

    retval = gmap_status_ok;
exit:
    amxc_var_clean(&old_device_active_var);
    return retval;
}

static gmap_status_t s_update_source_active(amxd_trans_t* trans, amxc_ts_t* now,
                                            amxd_object_t* device, const char* source, uint32_t priority, bool active) {

    bool old_active = false;
    // Note: reading the datamodel is not inside the transaction so this is unreliable if other
    //       actors can write to it in the meantime. We prevent that by making "Actives" read-only
    //       so only gmap-server can write to it, and by having gmap-server single-threaded.
    amxd_object_t* actives_templ = amxd_object_get_child(device, "Actives");
    amxd_object_t* active_obj = amxd_object_get_instance(actives_templ, source, 0);

    if(active_obj == NULL) {
        amxd_trans_select_object(trans, actives_templ);
        amxd_trans_add_inst(trans, 0, source);
        amxd_trans_set_value(cstring_t, trans, "Source", source);
    } else {
        amxd_status_t status = amxd_status_unknown_error;
        old_active = amxd_object_get_bool(active_obj, "Active", &status);
        when_failed_trace(status, error, ERROR, "Error getting datamodel value");
        amxd_trans_select_object(trans, active_obj);
    }

    amxd_trans_set_value(uint32_t, trans, "Priority", priority);
    amxd_trans_set_value(bool, trans, "Active", active);
    if(((active_obj == NULL) || (old_active != active)) && gmap_is_ntp_synchronized()) {
        amxd_trans_set_value(amxc_ts_t, trans, "Time", now);
    }

    amxd_trans_select_pathf(trans, ".^");

    return gmap_status_ok;
error:
    return gmap_status_unknown_error;
}

gmap_status_t gmaps_device_set_active(const char* key,
                                      bool active_of_source,
                                      const char* source,
                                      uint32_t priority) {
    gmap_status_t retval = gmap_status_unknown_error;
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* device = NULL;
    amxd_trans_t trans;
    amxc_ts_t now;
    amxc_ts_now(&now);
    amxd_trans_init(&trans);
    retval = gmap_status_invalid_key;
    when_str_empty_trace(key, exit, ERROR, "Invalid argument key");
    retval = gmap_status_invalid_parameter;
    when_str_empty_trace(source, exit, ERROR, "Invalid argument source");
    when_false_trace(priority >= GMAPS_DEVICE_ACTIVE_PRIORITY_MIN
                     && priority <= GMAPS_DEVICE_ACTIVE_PRIORITY_MAX, exit,
                     ERROR, "Invalid argument priority %" PRIu32, priority);

    retval = gmap_status_device_not_found;
    device = gmaps_get_device(key);
    when_null_trace(device, exit, ERROR, "Device not found: %s", key);

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, device);

    retval = s_update_device_active(&trans, &now, device, source, priority, active_of_source);
    when_failed_trace(retval, exit, ERROR, "Error updating device");
    retval = s_update_source_active(&trans, &now, device, source, priority, active_of_source);
    when_failed_trace(retval, exit, ERROR, "Error updating actives entry");

    retval = gmap_status_internal_error;
    status = amxd_trans_apply(&trans, gmap_get_dm());
    when_failed_trace(status, exit, ERROR, "Transaction failed");

    retval = gmap_status_ok;
exit:
    amxd_trans_clean(&trans);
    return retval;
}

bool gmaps_device_is_active(amxd_object_t* device) {
    bool active = false;

    when_null(device, exit);

    active = amxd_object_get_bool(device, "Active", NULL);

exit:
    return active;
}
