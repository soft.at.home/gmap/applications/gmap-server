%define {
    select Devices {
        /**
         * Configuration root object
         *
         * Each module can add configuration options under this object.
         * All configuration options will be stored persistently
         *
         * @version 6.0
         */
        %persistent object Config {
            /**
             * Create a configuration group for a module
             *
             * @param module module identifier
             *
             * @version 6.0
             */
            bool create(%mandatory string module);

            /**
             * Add a configuration option for a module and set the value
             *
             * @param module module identifier
             * @param option the name of the configuration option
             * @param value the configuration options value
             *
             * @version 6.0
             */
            void set(%mandatory string module, %mandatory string option, %mandatory variant value);

            /**
             * Get a configuration option value
             *
             * @param module module identifier
             * @param option the name of the configuration option
             *
             * @version 6.0
             */
            variant get(%mandatory string module, %mandatory string option);

            /**
             * Load a configuration group
             *
             * @param module module identifier
             *
             * @version 6.0
             */
            bool load(%mandatory string module);

            /**
             * Save a configuration group
             *
             * @param module module identifier
             *
             * @version 6.0
             */
            bool save(%mandatory string module);

            /**
             * Scan a directory for MIBs
             *
             * @param path the full path of the directory
             *
             * @version 6.0
             */
            void scanMibDir(%mandatory string path);

            object global {
                /**
                 * Contains a list of comma seperated name values
                 *
                 * The order of the list determines the order for the name selection
                 *
                 * @version 1.0
                 */
                 %persistent csv_string NameOrder {
                    default "webui,user,mdns,dil,dhcp,grules";
                 }

                 /**
                 * Determines the max number of devices that gmap may contain
                 * 
                 * If a new device is requested to be created when the limit is reached, the oldest inactive device will be removed. 
                 * Devices with the "protected", even if inactive, cannot be removed in this cleanup.
                 * If no inactive devices can be removed, the new device will not be added.
                 *
                 * @version 1.0
                 */
                 %persistent uint32 MaxDevices {
                    default 500;
                 }

                 /**
                 * Determines the max number of LAN-devices that gmap may contain.
                 * A LAN-device is a device with the tags 'lan' && 'physical'.
                 *
                 * If a new LAN-device is requested to be created when the limit is reached, a cleanup will be called.
                 * This cleanup will remove all LAN-devices inactive for longer than 'MaxInactiveTimeTreshold'.
                 * Devices with the "protected", even if inactive, cannot be removed in this cleanup.
                 * If no LAN-device is removed, the creation of the new LAN-device will fail.
                 *
                 * @version 1.0
                 */
                 %persistent uint32 MaxLanDevices {
                    default 252;
                 }

                 /**
                 * This value is the amount of interval time in seconds between automatic cleanups of the inactive LAN-devices.
                 * This cleanup removes the inactive LAN-devices, based on how long they have been inactive.
                 * The value used to determine which inactive LAN-devices should be removed, depends on 'MaxInactiveTime' and 'MaxInactiveTimeThreshold'.
                 * For more information check their documentation.
                 *
                 * The default is 12 hours (in seconds).
                 * This value should not go higher than 4.250.000, as to avoid an overflow.
                 */
                 %persistent uint32 InactiveCheckInterval {
                    default 43200;
                    on action validate call check_range [600,4250000];
                 }

                 /**
                 * Number of LAN-devices to reach so that automatic cleanup based on threshold is triggered.
                 * For more information on the automatic cleanup, check the documentation of 'InactiveCheckInterval'.
                 *
                 * When the number of LAN-devices is equal to or greater than 'InactiveCheckThreshold',
                 * the automatic cleanup will remove all devices inactive for longer than 'MaxInactiveTimeThreshold'.
                 *
                 * @version 1.0
                 */
                 %persistent uint32 InactiveCheckThreshold {
                     default 150;
                 }
                
                 /**
                 * Maximum amount of time (in seconds) a LAN-device can be inactive before
                 * being removed unconditionally at each 'InactiveCheckInterval'.
                 * If this value is '0', no LAN-devices will be removed.
                 */
                 %persistent uint32 MaxInactiveTime {
                    default 0;
                 }
                 
                 /**
                 * When the number of LAN-devices is equal to or greater than 'InactiveCheckThreshold',
                 * the automatic cleanup will remove all LAN-devices inactive for longer than 'MaxInactiveTimeTreshold'.
                 * If this value is '0', no LAN-devices will be removed.
                 * For more information on the automatic cleanup, check 'InactiveCheckInterval'.
                 *
                 * The default value is 30 days (in seconds)
                 */
                 %persistent uint32 MaxInactiveTimeTreshold {
                    default 2592000;
                 }

                 /**
                 * Contains a list of space seperated of reasons which can block a device
                 *
                 * This list contains the possible reasons for which a device can be blocked.
                 * If a reason is not in the list, a device cannot be blocked for it.
                 *
                 * @version 1.0
                 */
                 %persistent csv_string BlockedReasons {
                    default "Global";
                 }
            }
        }   
    }
}
