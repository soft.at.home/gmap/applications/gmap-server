%define {
    /**
     * gMap root object
     *
     * @version 6.0
     */
    select Devices {

        /**
         * Contains the list of opened queries
         *
         * Queries can be opened and used to get notifications, when a device starts matching the query,
         * stops matching a query or when a device was matching a query and is updated
         *
         * The same query can be opened multiple times, but is only visible once in the data model. See {Devices.openQuery}.
         * For each call to {Devices.openQuery} a matching call to {Devices.closeQuery} must be done.
         *
         * @version 6.0
         */
        %read-only object Query[] {

            %read-only string Name;

            %unique %key string Alias;

            /**
             * The expression of the query
             *
             * @version 6.0
             */
            %read-only string Expression;

            /**
             * A '|'-separated string with extra flags set on this query.
             *
             * Valid elements are:
             * - ignore_device_updated  If set, no device_updated events will be emitted for this query.
             *
             * @version 2.15.0
             */
            %read-only string Flags;

            /**
             * Event sent when the list of matching devices is updated or
             * when a parameter of a matched device changes value.
             * The "Result" field in the event data will contain a variant with
             * the new matching devices.
             *
             * The emitted events may be configured through the 'flags' parameter
             * when opening the query with {Devices.openQuery}.
             *
             * @version 1.0
             */
            event "gmap_query";

            /**
             * Gets the list of all matching devices.
             *
             * Get the list of keys of the matching devices.
             *
             * @return the list of keys of all matching devices.
             *
             * @version 6.0
             */
            list matchingDevices();
        }
    }
}
