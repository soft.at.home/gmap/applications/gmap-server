/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__GMAPS_QUERY_H__)
#define __GMAPS_QUERY_H__

#ifdef __cplusplus
extern "C"
{
#endif

gmap_status_t GMAPS_PRIVATE gmaps_query_open(const char* expression, const char* name, gmap_query_flags_t flags, gmap_query_cb_t fn, void* user_data, gmap_query_t** gmap_query);
void GMAPS_PRIVATE gmaps_query_close(gmap_query_t* gmap_query);
gmap_query_t* GMAPS_PRIVATE gmaps_query_find(uint32_t index, uint32_t id);
const char* GMAPS_PRIVATE gmaps_query_name(gmap_query_t* query);
gmap_status_t GMAPS_PRIVATE gmaps_query_get_match_devices_with_fields(amxd_object_t* query, amxc_var_t* result);
gmap_status_t GMAPS_PRIVATE gmaps_query_get_match_devices_without_fields(amxd_object_t* query, amxc_var_t* result);
void GMAPS_PRIVATE gmaps_query_remove_all(amxd_object_t* device);
void GMAPS_PRIVATE gmaps_query_verify_all(amxd_object_t* device);
void GMAPS_PRIVATE gmaps_query_notify_client(gmap_query_t* query,
                                             const char* key,
                                             amxc_var_t* device,
                                             gmap_query_action_t action);
amxd_object_t* GMAPS_PRIVATE gmaps_query_object(gmap_query_t* query);
void GMAPS_PRIVATE gmaps_query_init(void);
void GMAPS_PRIVATE gmaps_query_cleanup(void);

GMAPS_INLINE void gmaps_query_device_init(GMAPS_UNUSED const amxd_object_t* device, gmap_device_priv_data_t* priv) {
    amxc_llist_it_init(&priv->query);
}

GMAPS_INLINE void gmaps_query_device_clean(GMAPS_UNUSED const amxd_object_t* device, gmap_device_priv_data_t* priv) {
    amxc_llist_it_clean(&priv->query, NULL);
}

#ifdef __cplusplus
}
#endif

#endif // __GMAPS_QUERY_H__
