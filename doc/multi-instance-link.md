## Building topology by choosing links

![example topology](./diagrams/multi-instance-links.drawio.svg)

In the diagram above, we see that there is a phone (`phone1`), two wifi repeaters, two homeplug devices (ethernet over power), the ethernet port of the homegateway, the bridge, and the homegateway itself (`self`).

We also see in the diagram that multiple gmap modules provide information about how these devices and interfaces link to each other:
- the gmap module `mod-self` says the interface `bridge_lan-bridge` is linked to device `self`.
- `mod-eth-dev` says `phone1` is linked to `ethIntf-eth0`
- `mod-ssw` says `phone1` is linked to `wifirepeater2`
- etc

The way the gmap modules provide this information to gmap-server, is by calling `Devices.linkAdd()`, `Devices.linkRemove()` and `Devices.linkReplace()`.

Based on all this information, we want gmap-server to obtain a network topology. The network topology is made by choosing some of the links described above, and ignoring the others. The links that are chosen to keep, are highlighted with a green background in the diagram above.

Determining which links are chosen is done as follows: gmap-server calculates the longest path to each node. Links that are not on the longest path are not chosen. If there are multiple longest paths, an arbitrary longest path is chosen.

For example, in the graph below, the longest path to `C` is `A→B→C`, the longest path to `B` is `A→B`, and the longest path to `A` is `A`. So `A→C` is not part of the longest path, so it is not chosen in the topology.

```mermaid
graph TD;
  subgraph &nbsp;
  direction LR
  subgraph input:
    A --> B
    B --> C
    A --> C
  end
  subgraph topology:
    A' --> B'
    B' --> C'
  end
  input: --> topology:
  end
```


## Datamodel

The gmap modules tell gmap-server which links the gmap modules see. All this information, including the links we do not want to include in the topology, is stored in a multi-instance object `Link`, with an instance per gmap module. So gmap modules do not overwrite each other's data. For the example diagram above, this looks as follows:

```
Devices.Device.phone1.Link.mod-ssw.wifirepeater2.Alias = wifirepeater2
Devices.Device.phone1.Link.mod-homeplug.homeplug2.Alias = homeplug2
Devices.Device.phone1.Link.mod-homeplug.homeplug1.Alias = homelpgu1
Devices.Device.phone1.Link.mod-ssw.wifirepeater1.Alias = wifirepeater1
Devices.Device.phone1.Link.mod-eth-dev.ethIntf-eth0.Alias = wifirepeater1

Devices.Device.wifirepeater2.Link.mod-homeplug.homeplug2.Alias = homeplug2
Devices.Device.wifirepeater2.Link.mod-homeplug.homeplug1.Alias = homeplug1
Devices.Device.wifirepeater2.Link.mod-ssw.wifirepeater1.Alias = wifirepeater1
Devices.Device.wifirepeater2.Link.mod-eth-dev.ethIntf-eth0.Alias = ethIntf-eth0

...
```

The topology obtained by choosing links is not done by changes under Devices.Device.[].Link, so links we decide to not chose for the topology are still kept under Devices.Device.[].Link.

The topology obtained is stored under Devices.Device.[].UDevice and Devices.Device.[].LDevice.

For the example diagram, this yields
```
Devices.Device.phone1.UDevice.wifirepeater2.Alias = wifirepeater2
Devices.Device.wifirepeater2.UDevice.homeplug2.Alias = homeplug2
Devices.Device.homeplug2.UDevice.homeplug1.Alias = homeplug1
Devices.Device.homeplug1.UDevice.wifirepeater1.Alias = wifirepeater1
Devices.Device.wifirepeater1.UDevice.ethIntf-eth0.Alias = ethIntf-eth0
Devices.Device.ethIntf-eth0.UDevice.bridge_lan-bridge.Alias = bridge_lan-bridge
Devices.Device.bridge_lan-bridge.UDevice.self.Alias = self
```
The reverse direction is also kept for easy lookup:
```
Devices.Device.wifirepeater2.LDevice.phone1.Alias = phone1
Devices.Device.homeplug2.LDevice.wifirepeater2.Alias = wifirepeater2
Devices.Device.homeplug1.LDevice.homeplug2.Alias = homeplug2
Devices.Device.wifirepeater1.LDevice.homeplug1.Alias = homeplug1
Devices.Device.ethIntf-eth0.LDevice.wifirepeater1.Alias = wifirepeater1
Devices.Device.bridge_lan-bridge.LDevice.ethIntf-eth0.Alias = ethIntf-eth0
Devices.Device.self.LDevice.bridge_lan-bridge.Alias = bridge_lan-bridge
```
