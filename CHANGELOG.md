# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v3.0.10 - 2025-01-06(10:46:25 +0000)

### Other

- Fix error message 'Error selecting link'

## Release v3.0.9 - 2024-12-18(17:33:16 +0000)

### Other

- Timeout on Time.Status causes init failures

## Release v3.0.8 - 2024-12-05(15:49:22 +0000)

### Other

- [gmap-server] small optimizations

## Release v3.0.7 - 2024-12-05(10:59:11 +0000)

### Other

- [gmap][gmap-server] Optimize tag checking

## Release v3.0.6 - 2024-11-08(13:55:16 +0000)

### Other

- Reduce regex usage in signal handling

## Release v3.0.5 - 2024-11-08(12:47:13 +0000)

### Other

- Gmap query optionally don't send out device-changed events.

## Release v3.0.4 - 2024-11-07(19:48:41 +0000)

### Other

- Query matched devices not updated on set/remove alternative

## Release v3.0.3 - 2024-11-06(15:37:09 +0000)

### Other

- optimization: save datamodel (reboot-persistence) less often

## Release v3.0.2 - 2024-10-18(14:00:20 +0000)

### Other

- Restoring a disconnected device through two firmware upgrades fails

## Release v3.0.1 - 2024-10-14(09:17:09 +0000)

### Other

- Add mdns to NameOrder in gmap
- [gMap] Name suffix concatenated without separator

## Release v3.0.1 - 2024-10-01(11:26:15 +0000)

### Other

- Add mdns to NameOrder in gmap

## Release v3.0.0 - 2024-07-11(05:54:13 +0000)

### Other

- Build topology based on (contradicting) data from gmap modules

## Release v2.14.0 - 2024-07-01(10:04:15 +0000)

### Other

- Set FirstSeen to now when NTP comes up for devices with unknown FirstSeen

## Release v2.13.0 - 2024-06-12(15:10:28 +0000)

### Other

- [gmap][server] catch delete device event in case of not using the rpc

## Release v2.12.1 - 2024-05-17(08:02:25 +0000)

### Other

- Use libamxut

## Release v2.12.0 - 2024-05-14(07:58:13 +0000)

### Other

- Show multi-instance links in datamodel

## Release v2.11.2 - 2024-05-14(06:07:27 +0000)

### Other

- Test get key for duplicate macs

## Release v2.11.1 - 2024-05-06(08:30:52 +0000)

### Other

- Fix doc of link() and setLink()

## Release v2.11.0 - 2024-05-02(11:44:00 +0000)

### Other

- New linking API (no new functionality yet)

## Release v2.10.3 - 2024-04-10(10:05:24 +0000)

### Changes

- Make amxb timeouts configurable

## Release v2.10.2 - 2024-04-09(15:35:21 +0000)

### Other

- [PRPL][4.0.9.8][TECC][Device List]: Clear device list only clears till a reboot then the devices are all shown again even though they have not connected since clearing.

## Release v2.10.1 - 2024-04-03(09:23:46 +0000)

### Fixes

- [tr181-pcm] Saved and defaults odl should not be included in the backup files

## Release v2.10.0 - 2024-03-27(13:22:06 +0000)

### Other

- extend gMap unlink to support remove all links

## Release v2.9.1 - 2024-03-27(10:46:56 +0000)

### Other

- libuuid implemented twice

## Release v2.9.0 - 2024-03-26(08:10:06 +0000)

### Other

- gMap server: delete devices TODOs remaining in the code

## Release v2.8.0 - 2024-03-14(10:22:00 +0000)

### Other

- Add support for upgrade-persistency

## Release v2.7.0 - 2024-03-12(19:21:16 +0000)

### Other

- Router host table automatic cleaning up requirement -gmap part

## Release v2.6.7 - 2024-03-11(16:20:55 +0000)

### Fixes

- Fix topology() does not contain field of Childs[sic] anymore

## Release v2.6.6 - 2024-03-07(09:01:18 +0000)

### Other

- Optimization 6: wait 100ms instead of 10ms for re-evaluation of changed device

## Release v2.6.5 - 2024-03-07(08:53:42 +0000)

### Other

- Optimization 5: Parse an expression only once

## Release v2.6.4 - 2024-03-07(08:15:46 +0000)

### Other

- Optimization 4: quadratic->linear lookups by using already existing bidir association

## Release v2.6.3 - 2024-03-07(07:46:27 +0000)

### Other

- Optimization 3: do not copy list and skip unneeded generic functionality

## Release v2.6.2 - 2024-03-06(20:47:53 +0000)

### Other

- Optimization 2: no recalc + less fieldfetch

## Release v2.6.1 - 2024-03-06(20:37:46 +0000)

### Other

- Renames for readability

## Release v2.6.0 - 2024-02-29(13:49:12 +0000)

### Other

- [amx][gmap-server] destroyDevice() keeps lower link to device

## Release v2.5.6 - 2024-02-27(12:36:08 +0000)

### Other

- [gmap][server] wrong use of the gmaps_device_has_tag() function

## Release v2.5.5 - 2024-02-26(13:04:50 +0000)

### Other

- [gmap-server] Setting AMX_GMAP_MAX_DEVICES has no effectundo remove of var

## Release v2.5.4 - 2024-02-22(13:02:27 +0000)

### Fixes

- Also load saved configuration (not only defaults) at startup

## Release v2.5.3 - 2024-02-22(12:47:37 +0000)

### Other

- [gmap-server] Setting AMX_GMAP_MAX_DEVICES has no effect

## Release v2.5.2 - 2024-02-22(09:28:48 +0000)

### Other

- Fix pipelines using mock clock + make tests 25sec faster

## Release v2.5.1 - 2024-02-21(08:57:36 +0000)

### Other

- [gmap][gmap-server] Add subscription to InactiveCheckInterval parameter

## Release v2.5.0 - 2024-02-16(14:05:25 +0000)

### Other

- Replace deprecated macros by new ones

## Release v2.4.2 - 2024-02-13(15:22:31 +0000)

### Other

- Router host table automatic cleaning up requirement

## Release v2.4.1 - 2024-02-08(08:52:53 +0000)

### Other

- Make UDevice and LDevice conditionally persistent

## Release v2.4.0 - 2024-01-18(11:26:18 +0000)

### Other

- [amx][gmap] Add a function API to remove all inactive devices explicitly

## Release v2.3.1 - 2024-01-16(14:42:02 +0000)

### Changes

- [gmap] Load config odls at startup of gmap-server

## Release v2.3.0 - 2024-01-11(09:58:26 +0000)

### New

- [Host Access Control] Allow a number of mac address (MAC + MASK) to be blocked for internet access

## Release v2.2.18 - 2023-12-05(20:20:20 +0000)

### Other

- Fix query notifications not sent

## Release v2.2.17 - 2023-11-28(09:25:36 +0000)

### Other

- Fix performance problem of re-evaluating query expressions too often

## Release v2.2.16 - 2023-10-23(11:05:55 +0000)

### Other

- Move DeviceType from gmap server to information mib

## Release v2.2.15 - 2023-10-20(11:00:46 +0000)

### Other

- [gMAP] [Prpl] Tag parsing not working as expected

## Release v2.2.14 - 2023-10-19(09:54:17 +0000)

### Other

- gMap Homeplug plugin: topology changes

## Release v2.2.13 - 2023-10-16(12:04:07 +0000)

### Other

- Fix datamodel gmap missing in getdebug

## Release v2.2.12 - 2023-10-13(15:11:08 +0000)

### Other

- [USP] access to Devices.Device. using USP return an error

## Release v2.2.11 - 2023-10-12(13:01:31 +0000)

### Other

- Fix crash on gmap devices with tricky names (dots, square...

## Release v2.2.10 - 2023-10-04(06:53:45 +0000)

### Other

- gMap: setAlternativeRules, removeAlternativeRules support

## Release v2.2.9 - 2023-09-26(11:57:11 +0000)

### Other

- Fix no persistence when creating device using ubus

## Release v2.2.8 - 2023-09-26(09:10:10 +0000)

### Other

- Fix licenseses (separate MR and commit)

## Release v2.2.7 - 2023-09-19(14:39:04 +0000)

### Other

- Fix link() removes upper->lower but keeps lower->upper

## Release v2.2.6 - 2023-09-12(11:01:31 +0000)

### Other

- [GMAP] gmaps_device_get_recursive does not return all data of child objects

## Release v2.2.5 - 2023-08-02(14:46:30 +0000)

### Fixes

- lan DHCP client hostname is not resolved

## Release v2.2.4 - 2023-06-08(09:02:57 +0000)

### Other

- [gmap] MaxDevices implementation doesnt differentiate between devices and interfaces

## Release v2.2.3 - 2023-05-26(09:24:04 +0000)

### Other

- - [HTTPManager][WebUI] Create plugin's ACLs permissions

## Release v2.2.2 - 2023-04-17(12:02:01 +0000)

### Fixes

- [odl]Remove deprecated odl keywords

## Release v2.2.1 - 2023-04-17(09:10:22 +0000)

### Other

- Removed deprecated odl keywords in tests

## Release v2.2.0 - 2023-03-24(12:28:41 +0000)

### Other

- gMap server: action RPCs

## Release v2.1.0 - 2023-03-24(08:57:20 +0000)

### Other

- gMap Functions RPC  not implemented

## Release v2.0.0 - 2023-02-23(10:03:06 +0000)

### Breaking

- Change setActive API to have source and priority

## Release v1.9.6 - 2023-02-16(11:46:55 +0000)

### Other

- Fix gmap-server sometimes thinks time never syncronized

## Release v1.9.5 - 2023-02-03(19:06:04 +0000)

### Other

- Remove event stdout spam

## Release v1.9.4 - 2023-02-03(18:51:11 +0000)

### Other

- Fix incorrect log on adding default name

## Release v1.9.3 - 2023-02-02(09:49:44 +0000)

### Other

- [gmap] Remove the time-plugin requirement from gmap-server

## Release v1.9.2 - 2023-01-30(14:47:19 +0000)

### Other

- [gmap] avoid using m4 odl files.

## Release v1.9.1 - 2023-01-25(09:21:08 +0000)

### Other

- [gmap] the max nr of devices needs to be checked in gmaps_new_device instead of in _Devices_createDevice

## Release v1.9.0 - 2023-01-19(12:07:38 +0000)

### Other

- gMap Server: DM RPC cleanup

## Release v1.8.0 - 2023-01-18(16:17:14 +0000)

### Other

- gMap server: getParameters, getFirstParameter RPC

## Release v1.7.0 - 2023-01-09(10:36:34 +0000)

### Other

- GMap Server: implementation of gmap additional expression operators

## Release v1.6.4 - 2023-01-06(10:20:00 +0000)

### Fixes

- Fix optional include of components.config.m4 in config/gmap_conf_global.odl.m4

## Release v1.6.3 - 2022-12-20(16:07:28 +0000)

### Other

- Use optional include when including components.config.m4

## Release v1.6.2 - 2022-12-06(11:42:55 +0000)

### Other

- gMap Event handling

## Release v1.6.1 - 2022-12-01(09:46:38 +0000)

### Other

- [amx][gmap] mib-ip fails to load due to undefined symbol

## Release v1.6.0 - 2022-11-29(14:14:27 +0000)

### Other

- Add 'dhcp' to default name source list

## Release v1.5.0 - 2022-11-23(13:41:14 +0000)

### Other

- gMap Server: fix gmaps_device_get_recursive

## Release v1.4.1 - 2022-10-24(10:32:40 +0000)

### Other

- [gmap] Automatic clean up of inactive devices when MaxDevices is reached.

## Release v1.4.0 - 2022-10-21(07:18:49 +0000)

### Other

- [gmap] Automatic clean up of inactive devices when MaxDevices is reached.

## Release v1.3.0 - 2022-10-11(09:30:05 +0000)

### Other

- [amx][gmap] Define Name, setName(), delName() behaviour

## Release v1.2.0 - 2022-09-20(10:38:44 +0000)

### Other

- the Devices.link() function only allows one lower device

## Release v1.1.2 - 2022-09-09(07:12:45 +0000)

### Other

- Create unit tests for amx gMap Query feature

## Release v1.1.1 - 2022-09-06(07:51:15 +0000)

### Other

- [prpl][gMap]RPC topology missing

## Release v1.1.0 - 2022-09-05(08:09:44 +0000)

### New

- [prpl][gMap]RPC topology missing

## Release v1.0.6 - 2022-08-30(11:28:04 +0000)

### Other

- Gmap: Fix default and make gmap default startup priorityr configurable

## Release v1.0.5 - 2022-08-30(10:28:53 +0000)

### Other

- Gmap: Fix default and make gmap default startup priority configurable

## Release v1.0.4 - 2022-08-08(14:26:12 +0000)

### Other

- [CI] Fix missing dependencies on uuid

## Release v1.0.3 - 2022-08-08(12:08:44 +0000)

### Other

- [prpl][gMap] gMap queries must be evaluated in the gMap server(not the lib)

## Release v1.0.2 - 2022-08-03(16:41:53 +0000)

### Other

- Implement using UUIDs for device keys

## Release v1.0.1 - 2022-07-28(10:12:02 +0000)

### Other

- test mib loading + move testhelper functions to correct test file

## Release v1.0.0 - 2022-07-26(08:41:52 +0000)

### Other

- [prpl][gMap] gMap needs to be split in a gMap-core and gMap-client process

## Release v0.3.1 - 2022-05-20(06:43:01 +0000)

### Fixes

- [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected

## Release v0.3.0 - 2022-01-27(08:08:24 +0000)

### New

- Gmap add query functionality

## Release v0.2.0 - 2021-08-17(14:48:34 +0000)

### New

- [GMAP] add implementation for name selection

## Release v0.1.5 - 2021-08-13(08:11:58 +0000)

### Fixes

- [GMAP-SERVER] allow hasTags to check multiple tags

## Release v0.1.4 - 2021-08-06(12:46:46 +0000)

### Changes

- GMAP-SERVER update mibs after changing tag

## Release v0.1.3 - 2021-07-19(14:48:15 +0000)

- gmap-server export functions
- relative path in odl to .so file fails

## Release v0.1.2 - 2021-06-11(12:38:55 +0000)

### Fixes

- [tr181 plugins][makefile] Dangerous clean target for all tr181 components

## Release v0.1.1 - 2021-06-09(12:38:55 +0000)

### Fixes

- Update startup script

## Release v0.1.0 - 2021-05-14(07:29:17 +0000)

### New

- Auto generate odl documentation

### Fixes

- Fix for hqsTqg correct return value from traverse is used now

### Changes

- Add FAKEROOT variable to packages makefile
- Modules and plugins should not have a version extension in their names

## Release 0.0.9 - 2021-04-08(12:11:06 +0000)

### Changes

- Update readme 
- move copybara to baf 
- Update changelog to ambiorix style 

## Release 0.0.8 - 2021-03-31(11:08:21 +0000)

### Fixes

- Fix for creating links with mac ids
- Fix events for subscribe on ubus

## Release 0.0.7 - 2021-03-25(07:28:17 +0000)

### Fixes

- Fix mib value setting on createdevice
- Fix for startup script on openwrt

## Release 0.0.6 - 2021-03-16(15:56:00 +0100)

### Fixes

- Fixes Device get on openwrt added device links to get results

## Release 0.0.5 - 2021-03-10(12:35:00 +0100)

## New

- Added gmap devices get
- Initscript for gmap-server
- Added print\_event for debugging events

### Fixes

- Dev fix parameter adding
- Fix parameter adding in device get function

## Release 0.0.4 - 2021-02-19(14:18:53 +0100)

### Changes

- Moved traverse functionality to the lib

## Release 0.0.3 - 2021-02-17(09:28:53 +0100)

### New 

- add compile flag for studio.h support on musl compilers
- Missing functions for mibdirs
- Implementation of missing functionality for link
- Fixed amxrt links
- Added missing functionality in alternatives
- bugfixes for running on target

## Release 0.0.2 - 2020-12-10(11:28:53 +0100)

### New 

- Migrate to Sofa Pipelines and BAF
- Added rpc for isLinkedTo
- Implemented still missing functions in links
- Removes _print_event dbg function
- Apply (add/remove) mibs when Tags parameter changes
- Fixes duplicate set tags
- Fixes memory leak
- Implemented Device Alternatives
- fix for amx version
- Implemented Device get functionality
- Changed setActive to use default value from odl
- Changed hasTag and setTag parameter assignments to oneliners
- Implemented Device HasTag
- Added setTag implementation
- Fix for amx upstep
- Applied new coding style
- Added test coverage for Devices.Device.set RPC.
- Implemented Device Set RPC.
- AMX API change amxd\_object\_get\_params.
- Added default value for Device setActive RPC.
- Fix: Device.setActive is void RPC.
- LLTD devices should always be blocked.
- Fix in device tree traversal: iterator cleanup.
- Implemented Devices.block/unblock/isBlocked RPCs + coverage.
- Implemented Device.setActive RPC + coverage.
- ODL parser does not apply default value for empty cstring\_t.
- Use default value in ODL for webui in setName and setType RPCs.
- Send event in setName RPC.
- Added tests for setName RPC.
- Device name implementation.
- Added device set type rpc.

## Release 0.0.1 - 2020-08-24(07:02:53 +0000)

### New

- Device object
- Config object
- Config RPCs: create, get, set, load, save
- Devices RPCs: find, createDevice, setLink
- Device tree traversal

## Release 0.0.0 - 2020-06-24(14:20:00 +0000)

### New

- Initial release
