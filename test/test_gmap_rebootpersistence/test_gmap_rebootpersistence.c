/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_gmap_rebootpersistence.h"
#include "../test_common/test_util.h"
#include "gmaps_devices.h"
#include <gmap/gmap.h>

#include <amxc/amxc.h>
#include <amxp/amxp_signal.h>
#include <amxp/amxp_slot.h>

#include <amxd/amxd_common.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>

#include "gmaps_devices.h"
#include "gmaps_devices_multilink.h"
#include "test_gmap_rebootpersistence_setup_teardown.h"

static void s_testhelper_link(bool lower_persistent, bool upper_persistent, bool expect_link, void** state) {
    // GIVEN a persistent upperdevice and a non-persistent lower device
    const char* udev_name = "the_upper_device";
    const char* ldev_name = "the_lower_device";
    amxd_object_t* devices_object = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxd_object_t* device_template = amxd_dm_findf(amxut_bus_dm(), "Devices.Device");
    gmap_status_t status = gmaps_new_device(device_template, ldev_name,
                                            "testdisco", "", lower_persistent, "", NULL);
    assert_int_equal(status, gmap_status_ok);
    status = gmaps_new_device(device_template, udev_name,
                              "testdisco", "", upper_persistent, "", NULL);
    assert_int_equal(status, gmap_status_ok);
    gmaps_devices_multilink_add(udev_name, ldev_name, "default", "testdatasource");
    amxut_bus_handle_events();

    // WHEN storing and reloading the devices
    // store:
    amxo_parser_save_object(amxut_bus_parser(), "savefile.odl", devices_object, false);
    amxut_bus_handle_events();
    // restart:
    devices_object = NULL;
    device_template = NULL;
    test_gmap_teardown(state);
    test_gmap_setup(state);
    // load:
    amxut_dm_load_odl("savefile.odl");
    _gmap_server_start(NULL, NULL, NULL);
    amxut_bus_handle_events();

    assert_true((gmaps_get_device(ldev_name) != NULL) == lower_persistent);
    assert_true((gmaps_get_device(udev_name) != NULL) == upper_persistent);

    // THEN the there are or are no links depending on what's expected
    assert_true((amxd_dm_findf(amxut_bus_dm(), "Devices.Device.%s.LDevice.%s", udev_name, ldev_name) != NULL) == expect_link);
    assert_true((amxd_dm_findf(amxut_bus_dm(), "Devices.Device.%s.Link.testdatasource.LDevice.%s", udev_name, ldev_name) != NULL) == expect_link);
    assert_true((amxd_dm_findf(amxut_bus_dm(), "Devices.Device.%s.UDevice.%s", ldev_name, udev_name) != NULL) == expect_link);
    assert_true((amxd_dm_findf(amxut_bus_dm(), "Devices.Device.%s.Link.testdatasource.UDevice.%s", ldev_name, udev_name) != NULL) == expect_link);
}

void test_rebootpersistence_udevice_ldevice(void** state) {
    s_testhelper_link(true, true, true, state);
    s_testhelper_link(true, false, false, state);
    s_testhelper_link(false, true, false, state);
    s_testhelper_link(false, false, false, state);
}