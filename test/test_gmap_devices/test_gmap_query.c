/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#define MAX_TIME_UNTIL_QUERY_NOTIFICATION_IN_MS 300

#include "test_devices.h"
#include "../test_common/test_util.h"
#include "gmaps_query.h"
#include "gmaps_dm_query.h"
#include <amxut/amxut_bus.h>
#include <amxut/amxut_timer.h>

static void query1_cb(GMAPS_UNUSED gmap_query_t* query,
                      GMAPS_UNUSED const char* key,
                      GMAPS_UNUSED amxc_var_t* device,
                      GMAPS_UNUSED gmap_query_action_t action) {
}

void test_can_open_query(GMAPS_UNUSED void** state) {

    const char* expression = "myOtherTag";
    const char* name = "active";
    gmap_query_t* q = NULL;

    amxd_object_t* querries = amxd_dm_findf(amxut_bus_dm(), "Devices.Query");

    assert_int_equal(amxd_object_get_instance_count(querries), 0);

    assert_int_equal(gmaps_query_open(expression, name, 0, query1_cb, NULL, &q), gmap_status_ok);

    assert_non_null(q);
    assert_int_not_equal(q->index, 0);
    assert_int_equal(q->id, 1);
    assert_int_equal(q->flags, 0);

    assert_int_equal(amxd_object_get_instance_count(querries), 1);

}

void test_can_match_query(GMAPS_UNUSED void** state) {
    amxd_object_t* querries = amxd_dm_findf(amxut_bus_dm(), "Devices.Query");

    assert_int_equal(amxd_object_get_instance_count(querries), 1);

    amxd_object_t* querry = amxd_object_get_instance(querries, "active", 0);

    assert_non_null(querry);

    amxc_var_t result;
    amxc_var_init(&result);

    gmaps_query_get_match_devices_without_fields(querry, &result);

    amxc_var_dump(&result, 0);

    amxc_var_clean(&result);
}

void test_can_close_query(GMAPS_UNUSED void** state) {

    amxd_object_t* querries = amxd_dm_findf(amxut_bus_dm(), "Devices.Query");

    assert_int_equal(amxd_object_get_instance_count(querries), 1);

    amxd_object_t* querry = amxd_object_get_instance(querries, "active", 0);

    uint32_t index = amxd_object_get_index(querry);

    gmap_query_t* gmap_query = gmaps_query_find(index, 1);

    gmaps_query_close(gmap_query);

    assert_int_equal(amxd_object_get_instance_count(querries), 0);

}

static void test_destroy_device(amxd_object_t* devices, const char* devicekey) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "key", devicekey);
    assert_int_equal(amxd_object_invoke_function(devices, "destroyDevice", &args, &ret), amxd_status_ok);
    assert_true(amxc_var_get_bool(&ret));
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_rpc_query(GMAPS_UNUSED void** state) {
    amxd_object_t* devices = NULL;
    amxd_object_t* querries = NULL;
    amxd_object_t* query = NULL;
    amxc_var_t args;
    amxc_var_t result;
    amxc_var_t* matching_devices = NULL;
    const char* devicekey1 = "deviceKey1";
    const char* devicekey2 = "deviceKey2";
    const char* tag = "tag_of_test_rpc_query";
    const char* testdevicekey = NULL;

    util_create_device(devicekey1, "tempDiscoverySource",
                       tag, false, "tempDefaultName", amxd_status_ok, NULL);
    handle_events();

    amxc_var_init(&args);
    amxc_var_init(&result);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    devices = amxd_dm_findf(gmap_get_dm(), "Devices");
    querries = amxd_dm_findf(amxut_bus_dm(), "Devices.Query");

    assert_non_null(devices);
    assert_non_null(querries);

    assert_int_equal(amxd_object_get_instance_count(querries), 0);

    amxc_var_add_key(cstring_t, &args, "expression", tag);
    amxc_var_add_key(cstring_t, &args, "name", "keysearch");

    amxd_object_invoke_function(devices, "openQuery", &args, &result);
    amxc_var_dump(&result, 0);
    uint32_t index = GET_UINT32(&result, "index");
    uint32_t id = GET_UINT32(&result, "id");
    const char* flags = GET_CHAR(&result, "flags");
    assert_int_not_equal(index, 0);
    assert_int_equal(id, 1);
    assert_string_equal(flags, "");

    matching_devices = GET_ARG(&result, "devices");
    amxc_var_dump(matching_devices, STDOUT_FILENO);
    assert_int_equal(amxc_var_type_of(matching_devices), AMXC_VAR_ID_LIST);
    assert_string_equal(devicekey1, GET_CHAR(GETI_ARG(matching_devices, 0), "Key"));
    amxc_var_clean(&args);
    amxc_var_clean(&result);

    handle_events();

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    query = amxd_dm_findf(amxut_bus_dm(), "Devices.Query.keysearch");
    assert_non_null(query);
    amxd_object_invoke_function(query, "matchingDevices", &args, &result);
    handle_events();
    amxc_var_dump(&result, STDOUT_FILENO);
    testdevicekey = GETI_CHAR(&result, 0);
    assert_string_equal(testdevicekey, devicekey1);
    amxc_var_clean(&args);
    amxc_var_clean(&result);

    util_create_device(devicekey2, "tempDiscoverySource",
                       tag, false, "tempDefaultName", amxd_status_ok, NULL);
    handle_events();

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxd_object_invoke_function(query, "matchingDevices", &args, &result);
    handle_events();
    amxc_var_dump(&result, STDOUT_FILENO);
    testdevicekey = GETI_CHAR(&result, 0);
    assert_string_equal(devicekey1, testdevicekey);
    testdevicekey = GETI_CHAR(&result, 1);
    assert_string_equal(devicekey2, testdevicekey);

    amxc_var_clean(&args);
    amxc_var_clean(&result);

    test_destroy_device(devices, devicekey1);
    test_destroy_device(devices, devicekey2);

    handle_events();

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &args, "index", index);
    amxc_var_add_key(uint32_t, &args, "id", 1);
    amxd_object_invoke_function(devices, "closeQuery", &args, &result);

    assert_int_equal(amxd_object_get_instance_count(querries), 0);

    amxc_var_clean(&args);
    amxc_var_clean(&result);
}

void test_query_cleanup(GMAPS_UNUSED void** state) {
    test_can_open_query(NULL);
    gmaps_query_cleanup();
}

/** Perform `openQuery` RPC */
static void s_openquery(uint32_t* tgt_index, uint32_t* tgt_id, const char* expression, const char* name, const char* flags) {
    amxc_var_t args;
    amxc_var_t result;
    amxc_var_init(&args);
    amxc_var_init(&result);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxd_object_t* devices = amxd_dm_findf(gmap_get_dm(), "Devices");
    amxd_object_t* queries = amxd_dm_findf(amxut_bus_dm(), "Devices.Query");
    assert_non_null(devices);
    assert_non_null(queries);

    amxc_var_add_key(cstring_t, &args, "expression", expression);
    amxc_var_add_key(cstring_t, &args, "name", name);
    if(flags != NULL) {
        amxc_var_add_key(cstring_t, &args, "flags", flags);
    }
    assert_int_equal(amxd_status_ok, amxd_object_invoke_function(devices, "openQuery", &args, &result));
    *tgt_index = GET_UINT32(&result, "index");
    *tgt_id = GET_UINT32(&result, "id");
    assert_int_not_equal(*tgt_index, 0);
    assert_int_equal(*tgt_id, 1);

    amxc_var_clean(&args);
    amxc_var_clean(&result);
}

/** @implements amxp_slot_fn_t */
static void s_query_cb(GMAPS_UNUSED const char* const sig_name,
                       GMAPS_UNUSED const amxc_var_t* const data,
                       void* const priv) {
    bool* triggered = priv;
    *triggered = true;
}

static void s_subscribe(uint32_t query_index, bool* callback_called) {
    amxb_bus_ctx_t* ctx = amxb_be_who_has("Devices");
    assert_non_null(ctx);
    amxc_string_t path;
    amxc_string_init(&path, 10);
    amxc_string_setf(&path, "Devices.Query.%i.", query_index);
    assert_int_equal(0, amxb_subscribe(ctx, amxc_string_get(&path, 0), "notification == 'gmap_query'", s_query_cb, callback_called));
    amxut_bus_handle_events();
    amxc_string_clean(&path);
}

static void s_handle_events_and_timetravel(void) {
    amxut_bus_handle_events();
    amxut_timer_go_to_future_ms(MAX_TIME_UNTIL_QUERY_NOTIFICATION_IN_MS);
    amxut_bus_handle_events();
}

void test_query_notification_after_device_creation(GMAPS_UNUSED void** state) {
    // GIVEN a query looking for a tag. The query matches no devices.

    uint32_t query_index = 0;
    uint32_t query_id = 0;
    s_openquery(&query_index, &query_id, "mytesttag", __func__, NULL);
    bool callback_called = false;
    s_subscribe(query_index, &callback_called);

    // WHEN a device is created that matches the expression
    util_create_device("testdev_query_notif_after_dev_creation", "tempDiscoverySource",
                       "mytesttag", false, "tempDefaultName", amxd_status_ok, NULL);
    s_handle_events_and_timetravel();

    // THEN an event was triggered.
    assert_true(callback_called);
}

void test_query_no_notification_after_irrelevant_device_creation(GMAPS_UNUSED void** state) {
    // GIVEN a query matching no devices

    uint32_t query_index = 0;
    uint32_t query_id = 0;
    s_openquery(&query_index, &query_id, "mytesttag", __func__, NULL);
    bool callback_called = false;
    s_subscribe(query_index, &callback_called);

    // WHEN a device is created that does not match the expression of the query
    util_create_device(__func__, "tempDiscoverySource",
                       "irrelevant tags", false, "tempDefaultName", amxd_status_ok, NULL);
    s_handle_events_and_timetravel();

    // THEN no event was triggered.
    assert_false(callback_called);
}

void test_query_notification_after_device_stops_matching(GMAPS_UNUSED void** state) {
    // GIVEN a query matching a device
    util_create_device("testdev", "test_disco_source",
                       "mytesttag", false, "test_default_name", amxd_status_ok, NULL);
    uint32_t query_index = 0;
    uint32_t query_id = 0;
    s_openquery(&query_index, &query_id, "mytesttag", __func__, NULL);
    s_handle_events_and_timetravel();
    bool callback_called = false;
    s_subscribe(query_index, &callback_called);
    s_handle_events_and_timetravel();
    assert_false(callback_called);

    // WHEN the matching device is deleted
    util_destroy_device("testdev");
    s_handle_events_and_timetravel();

    // THEN a notification was sent
    assert_true(callback_called);
}

void test_query_no_notification_after_irrelevant_device_stops_matching(GMAPS_UNUSED void** state) {
    // GIVEN a query not matching any devices
    uint32_t query_index = 0;
    uint32_t query_id = 0;
    s_openquery(&query_index, &query_id, "mytesttag", __func__, NULL);
    s_handle_events_and_timetravel();
    util_create_device("testdev", "test_disco_source",
                       "blabla", false, "test_default_name", amxd_status_ok, NULL);
    s_handle_events_and_timetravel();
    bool callback_called = false;
    s_subscribe(query_index, &callback_called);
    s_handle_events_and_timetravel();
    assert_false(callback_called);

    // WHEN the non-matching device is deleted
    util_destroy_device("testdev");
    s_handle_events_and_timetravel();

    // THEN no notification was sent
    assert_false(callback_called);
}

void test_query_notification_after_existing_device_starts_matching_in_tag(GMAPS_UNUSED void** state) {
    // GIVEN a query not matching any devices
    uint32_t query_index = 0;
    uint32_t query_id = 0;
    // Create device:
    util_create_device("testdev", "test_disco_source",
                       "tag1", false, "test_default_name", amxd_status_ok, NULL);

    s_handle_events_and_timetravel();
    // Open query:
    bool callback_called = false;
    s_openquery(&query_index, &query_id, "tag2", __func__, NULL);
    s_subscribe(query_index, &callback_called);
    s_handle_events_and_timetravel();
    assert_false(callback_called);

    // WHEN the device is modified such that it starts matching (by changing its tag)
    assert_int_equal(gmaps_device_set_tag("testdev", "tag2 tag2", "", 0), amxd_status_ok);
    s_handle_events_and_timetravel();

    // THEN a notification was sent
    assert_true(callback_called);
}

static void s_set_param_rpc(amxd_object_t* obj, const char* param_name, const char* param_value) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* parameters = NULL;
    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    parameters = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(cstring_t, parameters, param_name, param_value);
    assert_int_equal(amxd_object_invoke_function(obj,
                                                 "set",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

static void s_set_param_transaction(amxd_object_t* obj, const char* param_name, const char* param_value) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, obj);
    amxd_trans_set_cstring_t(&trans, param_name, param_value);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), amxd_status_ok);
    amxd_trans_clean(&trans);
}

static void s_testhelper_query_notification_after_existing_device_starts_matching_in_parameter(bool set_via_rpc) {
    // GIVEN a query not matching any devices
    uint32_t query_index = 0;
    uint32_t query_id = 0;
    // Create device:
    util_create_device("testdev", "test_disco_source",
                       "tag1", false, "test_default_name", amxd_status_ok, NULL);
    amxd_object_t* device = gmaps_get_device("testdev");
    assert_non_null(device);
    util_add_string_param_to_obj(device, "MyParameter", "my not matching value");

    s_handle_events_and_timetravel();
    // Open query:
    bool callback_called = false;
    s_openquery(&query_index, &query_id, ".MyParameter == 'my matching value'", __func__, NULL);
    s_subscribe(query_index, &callback_called);
    s_handle_events_and_timetravel();
    assert_false(callback_called);

    // WHEN the device is modified such that it starts matching (by changing the device parameter)
    if(set_via_rpc) {
        s_set_param_rpc(device, "MyParameter", "my matching value");
    } else {
        s_set_param_transaction(device, "MyParameter", "my matching value");
    }
    s_handle_events_and_timetravel();

    // THEN a notification was sent
    assert_true(callback_called);
}

void test_query_notification_after_existing_device_starts_matching_in_parameter_via_rpc(GMAPS_UNUSED void** state) {
    s_testhelper_query_notification_after_existing_device_starts_matching_in_parameter(true);
}

void test_query_notification_after_existing_device_starts_matching_in_parameter_via_transaction(GMAPS_UNUSED void** state) {
    s_testhelper_query_notification_after_existing_device_starts_matching_in_parameter(false);
}

static void s_change_alternative(const char* master, const char* alternative, bool make_alternative) {
    amxd_object_t* master_obj = NULL;
    amxd_object_t* alternative_obj = NULL;
    amxc_var_t args;
    amxc_var_t check_args;
    amxc_var_t result;
    amxc_var_t check_result;

    amxc_var_init(&args);
    amxc_var_init(&check_args);
    amxc_var_init(&result);
    amxc_var_init(&check_result);

    assert_non_null(master);
    assert_non_null(alternative);

    master_obj = gmaps_get_device(master);
    alternative_obj = gmaps_get_device(alternative);

    assert_non_null(master_obj);
    assert_non_null(alternative_obj);


    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&check_args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, "alternative", alternative);
    amxc_var_add_key(cstring_t, &check_args, "alternative", alternative);

    amxd_object_invoke_function(master_obj, make_alternative ? "setAlternative" : "removeAlternative", &args, &result);
    amxd_object_invoke_function(master_obj, "isAlternative", &check_args, &check_result);

    assert_true(make_alternative == amxc_var_constcast(bool, &check_result));
    assert_true(make_alternative == gmaps_device_are_alternatives(alternative_obj, master_obj));

    amxc_var_clean(&args);
    amxc_var_clean(&result);
    amxc_var_clean(&check_result);
    amxc_var_clean(&check_args);
}

static void s_invoke_matching_devices(uint32_t index, amxc_var_t* result) {
    amxd_object_t* query = NULL;
    amxc_var_t args;

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    assert_true(index > 0);
    assert_non_null(result);

    query = amxd_dm_findf(amxut_bus_dm(), "Devices.Query.%d.", index);
    assert_non_null(query);

    assert_int_equal(0, amxd_object_invoke_function(query, "matchingDevices", &args, result));
    assert_int_equal(amxc_var_type_of(result), AMXC_VAR_ID_LIST);
    amxc_var_for_each(var, result) {
        assert_int_equal(amxc_var_type_of(var), AMXC_VAR_ID_CSTRING);
    }

    amxc_var_clean(&args);
}

static bool s_assert_matching_devices(const amxc_var_t* matches, const char* expected, bool total) {
    bool ret = true;
    amxc_htable_t table;
    amxc_llist_t expected_list;
    amxc_string_t expected_string;

    amxc_htable_init(&table, 10);
    amxc_llist_init(&expected_list);
    amxc_string_init(&expected_string, 0);

    amxc_var_for_each(var, matches) {
        const char* value = amxc_var_constcast(cstring_t, var);
        amxc_htable_it_t* it = NULL;
        assert_non_null(value);
        assert_null(amxc_htable_get(&table, value));

        it = calloc(1, sizeof(amxc_htable_it_t));
        assert_non_null(it);
        assert_int_equal(0, amxc_htable_insert(&table, value, it));
    }

    amxc_string_appendf(&expected_string, "%s", expected);
    amxc_string_split_to_llist(&expected_string, &expected_list, ',');

    amxc_llist_for_each(it, &expected_list) {
        amxc_string_t* item_string = amxc_string_from_llist_it(it);
        amxc_htable_it_t* entry;

        amxc_string_trim(item_string, NULL);
        entry = amxc_htable_take(&table, amxc_string_get(item_string, 0));
        if(entry == NULL) {
            print_error("Missing expected entry '%s'\n", amxc_string_get(item_string, 0));
            ret = false;
        } else {
            amxc_htable_it_clean(entry, NULL);
            free(entry);
        }
    }

    amxc_htable_for_each(it, &table) {
        if(total) {
            print_error("Found unexpected entry '%s'\n", amxc_htable_it_get_key(it));
            ret = false;
        }
        amxc_htable_it_clean(it, NULL);
        free(it);
    }

    amxc_llist_clean(&expected_list, amxc_string_list_it_free);
    amxc_string_clean(&expected_string);
    amxc_htable_clean(&table, NULL);
    return ret;
}

void test_query_notification_after_set_alternative(UNUSED void** state) {
    // GIVEN a query matching two unrelated devices
    uint32_t query_index = 0;
    uint32_t query_id = 0;
    amxc_var_t matching;

    amxc_var_init(&matching);
    // Create test devices:
    util_create_device("testdev_1", "test_disco_source",
                       "tag1", false, "test_default_name", amxd_status_ok, NULL);
    util_create_device("testdev_2", "test_disco_source",
                       "tag1", false, "test_default_name", amxd_status_ok, NULL);
    s_handle_events_and_timetravel();

    // Open query matching both devices
    bool callback_called = false;
    s_openquery(&query_index, &query_id, "tag1 && .Master==\"\"", __func__, "");
    s_subscribe(query_index, &callback_called);
    s_handle_events_and_timetravel();
    assert_false(callback_called);

    s_invoke_matching_devices(query_index, &matching);
    assert_true(s_assert_matching_devices(&matching, "testdev_1,testdev_2", true));
    amxc_var_set_type(&matching, AMXC_VAR_ID_NULL);

    // WHEN the devices are marked as alternatives of each other
    s_change_alternative("testdev_1", "testdev_2", true);
    s_handle_events_and_timetravel();

    // THEN a notification was sent and the matching devices was changed.
    assert_true(callback_called);

    s_invoke_matching_devices(query_index, &matching);
    assert_true(s_assert_matching_devices(&matching, "testdev_1", true));
    amxc_var_set_type(&matching, AMXC_VAR_ID_NULL);

    amxc_var_clean(&matching);
}

void test_query_notification_after_remove_alternative(UNUSED void** state) {
    // GIVEN a query matching two alternative devices
    uint32_t query_index = 0;
    uint32_t query_id = 0;
    amxc_var_t matching;

    amxc_var_init(&matching);
    // Create test devices:
    util_create_device("testdev_1", "test_disco_source",
                       "tag1", false, "test_default_name", amxd_status_ok, NULL);
    util_create_device("testdev_2", "test_disco_source",
                       "tag1", false, "test_default_name", amxd_status_ok, NULL);
    s_change_alternative("testdev_1", "testdev_2", true);
    s_handle_events_and_timetravel();

    // Open query matching both devices
    bool callback_called = false;
    s_openquery(&query_index, &query_id, "tag1 && .Master==\"\"", __func__, "");
    s_subscribe(query_index, &callback_called);
    s_handle_events_and_timetravel();
    assert_false(callback_called);

    s_invoke_matching_devices(query_index, &matching);
    assert_true(s_assert_matching_devices(&matching, "testdev_1", true));
    amxc_var_set_type(&matching, AMXC_VAR_ID_NULL);

    // WHEN the devices are no longer marked as alternatives of each other
    s_change_alternative("testdev_1", "testdev_2", false);
    s_handle_events_and_timetravel();

    // THEN a notification was sent and the matching devices was changed.
    assert_true(callback_called);

    s_invoke_matching_devices(query_index, &matching);
    assert_true(s_assert_matching_devices(&matching, "testdev_1,testdev_2", true));
    amxc_var_set_type(&matching, AMXC_VAR_ID_NULL);

    amxc_var_clean(&matching);
}

static void s_testhelper_query_notification_when_device_updated(const char* flags, bool cb_called) {
    // GIVEN a query matching a devices
    uint32_t query_index = 0;
    uint32_t query_id = 0;
    // Create device:
    util_create_device("testdev", "test_disco_source",
                       "tag1", false, "test_default_name", amxd_status_ok, NULL);
    amxd_object_t* device = gmaps_get_device("testdev");
    assert_non_null(device);
    util_add_string_param_to_obj(device, "MyParameter", "my initial value");

    s_handle_events_and_timetravel();
    // Open query:
    bool callback_called = false;
    s_openquery(&query_index, &query_id, "tag1", __func__, flags);
    s_subscribe(query_index, &callback_called);
    s_handle_events_and_timetravel();
    // The callback is NOT called, but the device should be among the already matching devices.
    assert_false(callback_called);

    // WHEN the device is modified (by changing its tag)
    assert_int_equal(gmaps_device_set_tag("testdev", "tag2", "", 0), amxd_status_ok);
    s_handle_events_and_timetravel();

    // THEN a notification was sent as expected
    assert_int_equal(callback_called, cb_called);
    callback_called = false;

    // WHEN the device is modified (by changing a unrelated parameter through RPC)
    s_set_param_rpc(device, "MyParameter", "my first value");
    s_handle_events_and_timetravel();

    // THEN a notification was sent as expected
    assert_int_equal(callback_called, cb_called);
    callback_called = false;

    // WHEN the device is modified (by changing a unrelated parameter through transaction)
    s_set_param_transaction(device, "MyParameter", "my second value");
    s_handle_events_and_timetravel();

    // THEN a notification was sent as expected
    assert_int_equal(callback_called, cb_called);
    callback_called = false;
}

void test_query_notification_when_device_updated(GMAPS_UNUSED void** state) {
    s_testhelper_query_notification_when_device_updated("", true);
}

void test_query_no_notification_when_ignoring_device_updated(GMAPS_UNUSED void** state) {
    s_testhelper_query_notification_when_device_updated(GMAP_QUERY_IGNORE_DEVICE_UPDATED_STR, false);
}
