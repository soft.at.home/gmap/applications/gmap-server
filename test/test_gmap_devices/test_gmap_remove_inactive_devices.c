/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "../test_common/test_util.h"

#include "gmaps_devices.h"
#include "gmaps_dm_devices.h"
#include <amxut/amxut_timer.h>

#include "test_devices.h"
#include "../test_common/test_util.h"

amxd_object_t* devices = NULL;

static void init_devices() {
    devices = amxd_dm_findf(amxut_bus_dm(), "Devices.");

    util_create_device("Dev1", "", "lan physical", false, "", amxd_status_ok, NULL);
    assert_int_equal(gmaps_device_set_active("Dev1", true, "mysource", 100), gmap_status_ok);
    util_create_device("Dev2", "", "lan physical", false, "", amxd_status_ok, NULL);
    assert_int_equal(gmaps_device_set_active("Dev2", true, "mysource", 100), gmap_status_ok);
    util_create_device("Dev3", "", "lan physical", false, "", amxd_status_ok, NULL);
    assert_int_equal(gmaps_device_set_active("Dev3", true, "mysource", 100), gmap_status_ok);
    util_create_device("Dev4", "", "lan physical", false, "", amxd_status_ok, NULL);
    assert_int_equal(gmaps_device_set_active("Dev4", true, "mysource", 100), gmap_status_ok);
    util_create_device("Dev5", "", "lan physical protected", false, "", amxd_status_ok, NULL);
    assert_int_equal(gmaps_device_set_active("Dev5", true, "mysource", 100), gmap_status_ok);
}

/*
 * Check that devices inactive for longer than minimumInactiveInterval are removed
 */
void test_remove_inactive_devices(GMAPS_UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxd_object_t* dev1 = NULL;
    amxd_object_t* dev2 = NULL;
    amxd_object_t* dev3 = NULL;

    init_devices();

    dev1 = gmaps_get_device("Dev1");
    dev2 = gmaps_get_device("Dev2");
    dev3 = gmaps_get_device("Dev3");
    assert_non_null(dev1);
    assert_non_null(dev2);
    assert_non_null(dev3);

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &args, "active", 0);
    amxc_var_add_key(cstring_t, &args, "source", "mysource");
    amxc_var_add_key(uint32_t, &args, "priority", 100);

    assert_int_equal(amxd_object_invoke_function(dev2,
                                                 "setActive",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &args, "active", 0);
    amxc_var_add_key(cstring_t, &args, "source", "mysource");
    amxc_var_add_key(uint32_t, &args, "priority", 100);
    assert_int_equal(amxd_object_invoke_function(dev3,
                                                 "setActive",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    amxut_timer_go_to_future_ms(3000);

    // args cannot be reused, so re-init:
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &args, "minimumInactiveInterval", 2);
    assert_int_equal(amxd_object_invoke_function(devices,
                                                 "removeInactiveDevices",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    assert_int_equal(GET_UINT32(&args, "nrOfRemovedDevices"), 2);
    dev1 = gmaps_get_device("Dev1");
    dev2 = gmaps_get_device("Dev2");
    dev3 = gmaps_get_device("Dev3");
    assert_non_null(dev1);
    assert_null(dev2);
    assert_null(dev3);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

/*
 * Check that devices shorter inactive than are not minimumInactiveInterval removed
 */
void test_not_remove_inactive_device_before_time(GMAPS_UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxd_object_t* dev1 = NULL;

    init_devices();

    dev1 = gmaps_get_device("Dev1");
    assert_non_null(dev1);

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &args, "active", 0);
    amxc_var_add_key(cstring_t, &args, "source", "mysource");
    amxc_var_add_key(uint32_t, &args, "priority", 100);

    assert_int_equal(amxd_object_invoke_function(dev1,
                                                 "setActive",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    /* no sleep, thus the device should not be removed */

    // args cannot be reused, so re-init:
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &args, "minimumInactiveInterval", 2);
    assert_int_equal(amxd_object_invoke_function(devices,
                                                 "removeInactiveDevices",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    assert_int_equal(GET_UINT32(&args, "nrOfRemovedDevices"), 0);
    dev1 = gmaps_get_device("Dev1");
    assert_non_null(dev1);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &args, "active", 1);
    amxc_var_add_key(cstring_t, &args, "source", "mysource");
    amxc_var_add_key(uint32_t, &args, "priority", 100);
    assert_int_equal(amxd_object_invoke_function(dev1,
                                                 "setActive",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

/*
 * Check that when minimumInactiveInterval is zero, all inactive devices are removed
 */
void test_remove_all_inactive_devices(GMAPS_UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxd_object_t* dev1 = NULL;
    amxd_object_t* dev4 = NULL;

    init_devices();

    dev1 = gmaps_get_device("Dev1");
    dev4 = gmaps_get_device("Dev4");
    assert_non_null(dev1);
    assert_non_null(dev4);

    amxc_var_init(&ret);
    amxc_var_init(&args);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &args, "active", 0);
    amxc_var_add_key(cstring_t, &args, "source", "mysource");
    amxc_var_add_key(uint32_t, &args, "priority", 100);

    assert_int_equal(amxd_object_invoke_function(dev1,
                                                 "setActive",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    // args cannot be reused, so re-init:
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &args, "minimumInactiveInterval", 0);
    assert_int_equal(amxd_object_invoke_function(devices,
                                                 "removeInactiveDevices",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    assert_int_equal(GET_UINT32(&args, "nrOfRemovedDevices"), 1);
    dev1 = gmaps_get_device("Dev1");
    dev4 = gmaps_get_device("Dev4");
    assert_null(dev1);
    assert_non_null(dev4);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

/*
 * Check that devices with the "protected" tag are not removed.
 */
void test_cannot_remove_protected_devices(GMAPS_UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxd_object_t* dev5 = NULL;

    init_devices();

    dev5 = gmaps_get_device("Dev5");
    assert_non_null(dev5);

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &args, "active", 0);
    amxc_var_add_key(cstring_t, &args, "source", "mysource");
    amxc_var_add_key(uint32_t, &args, "priority", 100);

    assert_int_equal(amxd_object_invoke_function(dev5,
                                                 "setActive",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    // args cannot be reused, so re-init:
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(devices,
                                                 "removeInactiveDevices",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    assert_int_equal(GET_UINT32(&args, "nrOfRemovedDevices"), 0);
    dev5 = gmaps_get_device("Dev5");
    assert_non_null(dev5);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}