/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_devices.h"
#include "gmaps_dm_device.h"
#include "../test_common/test_util.h"

#define TEST_ODL_DEFAULT_SELECT_NAME_ORDER "webui,user,dil,grules,dect_ule"

static amxc_llist_t name_order_list;

static void print_names(const char* device_key) {
    amxd_object_t* object = gmaps_get_device(device_key);
    char* name_read = amxd_object_get_value(cstring_t,
                                            object,
                                            "Name",
                                            NULL);
    printf("Name:\t%s\n", name_read);

    amxd_object_t* instances = amxd_object_findf(object, "Names");
    amxd_object_for_each(instance, it, instances) {
        amxd_object_t* inst = amxc_llist_it_get_data(it, amxd_object_t, it);
        amxc_var_t params;
        amxc_var_init(&params);
        amxd_object_get_params(inst, &params, amxd_dm_access_public);
        amxc_var_dump(&params, 0);
        amxc_var_clean(&params);
    }

    free(name_read);
}

static void check_selected_name(const char* device_key, const char* expected_selected_name) {

    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxd_object_t* device_instance = gmaps_get_device(device_key);

    assert_ptr_not_equal(devices, NULL);
    assert_ptr_not_equal(device_instance, NULL);

    char* name_read = amxd_object_get_value(cstring_t,
                                            device_instance,
                                            "Name",
                                            NULL);
    assert_string_equal(name_read, expected_selected_name);
    free(name_read);
}


void test_init_select_name(GMAPS_UNUSED void** state) {

    gmap_status_t status;
    amxc_var_t var_name_order;
    const char* name_order = NULL;
    amxc_string_t name_order_str;
    amxd_object_t* config_obj = NULL;

    amxc_llist_init(&name_order_list);
    amxc_string_init(&name_order_str, 0);
    amxc_var_init(&var_name_order);

    // make a list
    config_obj = amxd_dm_findf(gmap_get_dm(), "Devices.Config.");
    assert_ptr_not_equal(config_obj, NULL);
    status = gmaps_get_config(config_obj, "global", "NameOrder", &var_name_order);
    assert_int_equal(status, gmap_status_ok);
    name_order = amxc_var_constcast(cstring_t, &var_name_order);
    assert_ptr_not_equal(name_order, NULL);
    assert_string_equal(name_order, TEST_ODL_DEFAULT_SELECT_NAME_ORDER);
    amxc_string_appendf(&name_order_str, "%s", name_order);
    amxc_string_split_to_llist(&name_order_str, &name_order_list, ',');
    assert_false(amxc_llist_is_empty(&name_order_list));

    // create device
    util_create_device("selectnamedevice", "default", "lan and hgw", false, "selectednamedevice", amxd_status_ok, NULL);
    handle_events();
    check_selected_name("selectnamedevice", "selectednamedevice");

    amxc_string_clean(&name_order_str);
    amxc_var_clean(&var_name_order);
}

void test_rpc_select_name(GMAPS_UNUSED void** state) {
    const char* source = NULL;
    amxc_string_t name_str;
    const char* name = NULL;

    amxc_llist_iterate_reverse(it, &name_order_list) {
        amxc_string_init(&name_str, 0);
        source = amxc_string_get(amxc_string_from_llist_it(it), 0);
        assert_ptr_not_equal(source, NULL);
        assert_string_not_equal(source, "");
        printf("source: %s\n", source);

        amxc_string_appendf(&name_str, "name_%s", source);
        name = amxc_string_get(&name_str, 0);
        printf("name:%s\n", name);
        util_set_name("selectnamedevice", name, source,
                      name, source, "", 0);
        handle_events();
        check_selected_name("selectnamedevice", name);
        amxc_string_clean(&name_str);

    }
    amxc_llist_it_t* it = amxc_llist_get_first(&name_order_list);
    if(!it) {
        return;
    }
    source = amxc_string_get(amxc_string_from_llist_it(it), 0);
    assert_ptr_not_equal(source, NULL);
    assert_string_not_equal(source, "");
    while(it) {
        gmaps_device_remove_name("selectnamedevice", source);
        handle_events();
        it = amxc_llist_it_get_next(it);
        if(it == NULL) {
            check_selected_name("selectnamedevice", "selectednamedevice");
            break;
        } else {
            amxc_string_init(&name_str, 0);
            source = amxc_string_get(amxc_string_from_llist_it(it), 0);
            assert_ptr_not_equal(source, NULL);
            assert_string_not_equal(source, "");
            amxc_string_appendf(&name_str, "name_%s", source);
            name = amxc_string_get(&name_str, 0);
            check_selected_name("selectnamedevice", name);
            amxc_string_clean(&name_str);
        }
    }
}

void test_select_name_adds_correct_sufix(GMAPS_UNUSED void** state) {
    /*
     * Create a new device (not self) that has the same default name.
     * This should cause a suffix to be added to the new device its name.
     * *Note*: self devices may have duplicate names
     */

    util_create_device("selectnamedevice2", "default", "lan and hgw", false, "selectednamedevice", amxd_status_ok, NULL);
    handle_events();

    print_names("selectnamedevice2");

    check_selected_name("selectnamedevice", "selectednamedevice");
    check_selected_name("selectnamedevice2", "selectednamedevice-1");

}

void test_select_name_cleanup(GMAPS_UNUSED void** state) {
    amxc_llist_clean(&name_order_list, amxc_string_list_it_free);
}
