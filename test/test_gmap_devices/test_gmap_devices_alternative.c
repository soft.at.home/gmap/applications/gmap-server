/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_devices.h"
#include "gmaps_dm_device.h"
#include "gmaps_devices_alternative.h"
#include "../test_common/test_util.h"

void test_gmap_can_alternative_wrong_parameters(GMAPS_UNUSED void** state) {
    amxd_object_t* device = gmaps_get_device(TEST2KEY);
    amxd_object_t* device_alt = gmaps_get_device(TEST3KEY);

    gmaps_device_set_alternative(NULL, NULL);
    gmaps_device_set_alternative(device, NULL);

    gmaps_device_remove_alternative(NULL, NULL);
    gmaps_device_remove_alternative(device, NULL);

    gmaps_device_remove_alternative(NULL, device_alt);
    gmaps_device_remove_alternative(device, device_alt);

    assert_false(gmaps_device_are_alternatives(NULL, device));
    assert_false(gmaps_device_are_alternatives(device, NULL));
    assert_false(gmaps_device_are_alternatives(NULL, NULL));
    assert_false(gmaps_device_are_alternatives(device, device));

}

void test_gmap_can_device_set_alternative(GMAPS_UNUSED void** state) {
    amxd_object_t* device = gmaps_get_device(TEST2KEY);
    amxd_object_t* device_alt = gmaps_get_device(TEST3KEY);

    assert_false(gmaps_device_are_alternatives(device, device_alt));
    assert_false(gmaps_device_is_alternative(device_alt));

    gmaps_device_set_alternative(device, device_alt);

    assert_true(gmaps_device_are_alternatives(device, device_alt));
    assert_true(gmaps_device_is_alternative(device_alt));

}

void test_gmap_can_invoke_set_alternative(GMAPS_UNUSED void** state) {
    amxd_object_t* device = gmaps_get_device(TEST2KEY);
    amxd_object_t* device_alt = gmaps_get_device(TEST3KEY);
    amxc_var_t args;
    amxc_var_t check_args;
    amxc_var_t result;
    amxc_var_t check_result;

    amxc_var_init(&args);
    amxc_var_init(&check_args);
    amxc_var_init(&result);
    amxc_var_init(&check_result);

    assert_false(gmaps_device_are_alternatives(device_alt, device));
    assert_false(gmaps_device_is_alternative(device_alt));
    assert_false(gmaps_device_is_alternative_from(device, device_alt));

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&check_args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, "alternative", TEST3KEY);
    amxc_var_add_key(cstring_t, &check_args, "alternative", TEST3KEY);


    amxd_object_invoke_function(device, "setAlternative", &args, &result);

    assert_int_equal(amxd_object_invoke_function(device, "isAlternative", &check_args, &check_result), amxd_status_ok);

    assert_true(amxc_var_get_bool(&check_result));

    assert_true(gmaps_device_are_alternatives(device_alt, device));
    assert_true(gmaps_device_is_alternative(device_alt));
    assert_true(gmaps_device_is_alternative_from(device, device_alt));

    assert_true(gmaps_device_is_linked_to(device, device_alt, gmap_traverse_down));

    amxc_var_clean(&args);
    amxc_var_clean(&result);
    amxc_var_clean(&check_result);
    amxc_var_clean(&check_args);

}

void test_gmap_can_device_remove_alternative(GMAPS_UNUSED void** state) {
    amxd_object_t* device = gmaps_get_device(TEST2KEY);
    amxd_object_t* device_alt = gmaps_get_device(TEST3KEY);

    assert_true(gmaps_device_are_alternatives(device, device_alt));
    assert_true(gmaps_device_is_alternative(device_alt));
    assert_true(gmaps_device_is_alternative_from(device, device_alt));

    gmaps_device_remove_alternative(device, device_alt);

    assert_false(gmaps_device_are_alternatives(device, device_alt));
    assert_false(gmaps_device_is_alternative(device_alt));
    assert_false(gmaps_device_is_alternative_from(device, device_alt));

}

void test_gmap_can_invoke_remove_alternatives(GMAPS_UNUSED void** state) {
    amxd_object_t* device = gmaps_get_device(TEST2KEY);
    amxd_object_t* device_alt = gmaps_get_device(TEST3KEY);

    amxc_var_t args;
    amxc_var_t check_args;
    amxc_var_t result;
    amxc_var_t check_result;

    amxc_var_init(&args);
    amxc_var_init(&check_args);
    amxc_var_init(&result);
    amxc_var_init(&check_result);

    assert_true(gmaps_device_are_alternatives(device_alt, device));
    assert_true(gmaps_device_is_alternative(device_alt));


    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&check_result, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, "alternative", TEST3KEY);
    amxc_var_add_key(cstring_t, &check_args, "alternative", TEST3KEY);


    amxd_object_invoke_function(device, "removeAlternative", &args, &result);
    amxd_object_invoke_function(device, "isAlternative", &check_args, &check_result);

    assert_false(amxc_var_constcast(bool, &check_result));

    assert_false(gmaps_device_are_alternatives(device_alt, device));
    assert_false(gmaps_device_is_alternative(device_alt));

    amxc_var_clean(&args);
    amxc_var_clean(&result);
    amxc_var_clean(&check_result);
    amxc_var_clean(&check_args);
}

static bool check_has_alternative(amxd_object_t* device, const char* name) {
    amxd_object_t* alternatives = NULL;
    bool result = false;

    assert_non_null(device);
    assert_non_null(name);

    alternatives = amxd_object_findf(device, "Alternative");
    when_null(alternatives, exit);

    amxd_object_for_each(instance, lit, alternatives) {
        amxd_object_t* alt = amxc_llist_it_get_data(lit, amxd_object_t, it);
        if(strcmp(amxd_object_get_name(alt, AMXD_OBJECT_NAMED), name) == 0) {
            result = true;
            goto exit;
        }
    }

exit:
    return result;
}

void test_alternatives_are_cleaned_for_destroyed_devices_alternative(GMAPS_UNUSED void** state) {
    const char* dev1name = "alternativestest_master";
    const char* dev2name = "alternativestest_alt1";
    const char* dev3name = "alternativestest_alt2";
    amxd_object_t* dev1 = NULL;
    amxd_object_t* dev2 = NULL;
    amxd_object_t* dev3 = NULL;

    // Given a cluster of three devices
    util_create_device(dev1name, "test", "", false, NULL, amxd_status_ok, NULL);
    util_create_device(dev2name, "test", "", false, NULL, amxd_status_ok, NULL);
    util_create_device(dev3name, "test", "", false, NULL, amxd_status_ok, NULL);

    dev1 = gmaps_get_device(dev1name);
    dev2 = gmaps_get_device(dev2name);
    dev3 = gmaps_get_device(dev3name);

    assert_non_null(dev1);
    assert_non_null(dev2);
    assert_non_null(dev3);
    assert_ptr_not_equal(dev1, dev2);
    assert_ptr_not_equal(dev1, dev3);
    assert_ptr_not_equal(dev2, dev3);

    gmaps_device_set_alternative(dev1, dev2);
    gmaps_device_set_alternative(dev1, dev3);
    assert_true(gmaps_device_are_alternatives(dev1, dev2));
    assert_true(gmaps_device_are_alternatives(dev1, dev3));

    // Remove one alternative and verify that the master no longer holds a reference
    util_destroy_device(dev2name);

    assert_false(check_has_alternative(dev1, dev2name));
    assert_true(check_has_alternative(dev1, dev3name));
    assert_true(gmaps_device_is_alternative(dev3));
}

void test_alternatives_are_cleaned_for_destroyed_devices_master(GMAPS_UNUSED void** state) {
    const char* dev1name = "alternativestest_master";
    const char* dev2name = "alternativestest_alt1";
    const char* dev3name = "alternativestest_alt2";
    amxd_object_t* dev1 = NULL;
    amxd_object_t* dev2 = NULL;
    amxd_object_t* dev3 = NULL;
    char* master_name = NULL;
    amxd_status_t status = amxd_status_unknown_error;

    // Given a cluster of three devices, one master and two alternatives
    util_create_device(dev1name, "test", "", false, NULL, amxd_status_ok, NULL);
    util_create_device(dev2name, "test", "", false, NULL, amxd_status_ok, NULL);
    util_create_device(dev3name, "test", "", false, NULL, amxd_status_ok, NULL);

    dev1 = gmaps_get_device(dev1name);
    dev2 = gmaps_get_device(dev2name);
    dev3 = gmaps_get_device(dev3name);

    assert_non_null(dev1);
    assert_non_null(dev2);
    assert_non_null(dev3);
    assert_ptr_not_equal(dev1, dev2);
    assert_ptr_not_equal(dev1, dev3);
    assert_ptr_not_equal(dev2, dev3);

    gmaps_device_set_alternative(dev1, dev2);
    gmaps_device_set_alternative(dev1, dev3);
    assert_true(gmaps_device_are_alternatives(dev1, dev2));
    assert_true(gmaps_device_are_alternatives(dev1, dev3));

    // Remove the master and verify that both alternatives no longer hold a reference
    util_destroy_device(dev1name);

    master_name = amxd_object_get_value(cstring_t, dev2, "Master", &status);
    assert_false(gmaps_device_is_alternative(dev2));
    assert_int_equal(status, amxd_status_ok);
    assert_non_null(master_name);
    assert_string_equal(master_name, "");
    free(master_name);
    master_name = NULL;

    status = amxd_status_unknown_error;
    master_name = amxd_object_get_value(cstring_t, dev3, "Master", &status);
    assert_false(gmaps_device_is_alternative(dev3));
    assert_int_equal(status, amxd_status_ok);
    assert_non_null(master_name);
    assert_string_equal(master_name, "");
    free(master_name);
    master_name = NULL;
}
