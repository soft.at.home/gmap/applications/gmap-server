/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_devices.h"
#include "gmaps_devices.h"
#include "gmaps_dm_devices.h"
#include "../test_common/test_util.h"

char* tagDev1_1_0 = "device wifi hgw";
char* tagDev2_1_0 = "device hgw";
char* tagDev2_2_0 = "device wifi";
char* tagDev2_1_1 = "device hgw";
char* tagDev3_1_0 = "device wifi";
char* tagDev3_2_0 = "device wifi";
char* tagDev3_2_1 = "device wifi";

/**
 * M = Master
 * A = Alternative
 *                                  __________
 *                                  |         |
 *                           _______|dev1.1.0 |_______
 *                           |      |_________|       |
 *                           |                        |
 *     ___________      _____V_____              _____V_____
 *     |         |A    M|         |              |         |
 *     |dev2.1.1 |<-----|dev2.1.0 |         _____|dev2.2.0 |_____
 *     |_________|      |_________|         |    |_________|     |
 *                                          |                    |
 *                                     _____V_____          _____V_____      ___________
 *                                     |         |          |         |M    A|         |
 *                                     |dev3.1.0 |          |dev3.1.0 |----->|dev3.1.1 |
 *                                     |_________|          |_________|      |_________|
 *
 * This function creates a device tree as depicted above
 */
static void create_device_tree() {
    amxd_object_t* device = NULL;
    amxd_object_t* alt_device = NULL;

    util_create_device(TEST_TOPOLOGY_KEY_DEV_1_1_0, "", tagDev1_1_0, false, "", amxd_status_ok, NULL);
    util_create_device(TEST_TOPOLOGY_KEY_DEV_2_1_0, "", tagDev2_1_0, false, "", amxd_status_ok, NULL);
    util_create_device(TEST_TOPOLOGY_KEY_DEV_2_2_0, "", tagDev2_2_0, false, "", amxd_status_ok, NULL);
    util_create_device(TEST_TOPOLOGY_KEY_DEV_2_1_1, "", tagDev2_1_1, false, "", amxd_status_ok, NULL);
    util_create_device(TEST_TOPOLOGY_KEY_DEV_3_1_0, "", tagDev3_1_0, false, "", amxd_status_ok, NULL);
    util_create_device(TEST_TOPOLOGY_KEY_DEV_3_2_0, "", tagDev3_2_0, false, "", amxd_status_ok, NULL);
    util_create_device(TEST_TOPOLOGY_KEY_DEV_3_2_1, "", tagDev3_2_1, false, "", amxd_status_ok, NULL);

    util_chosenlink_link_devices(TEST_TOPOLOGY_KEY_DEV_1_1_0, TEST_TOPOLOGY_KEY_DEV_2_1_0, true);
    util_chosenlink_link_devices(TEST_TOPOLOGY_KEY_DEV_1_1_0, TEST_TOPOLOGY_KEY_DEV_2_2_0, true);
    util_chosenlink_link_devices(TEST_TOPOLOGY_KEY_DEV_2_2_0, TEST_TOPOLOGY_KEY_DEV_3_1_0, true);
    util_chosenlink_link_devices(TEST_TOPOLOGY_KEY_DEV_2_2_0, TEST_TOPOLOGY_KEY_DEV_3_2_0, true);

    device = gmaps_get_device(TEST_TOPOLOGY_KEY_DEV_2_1_0);
    alt_device = gmaps_get_device(TEST_TOPOLOGY_KEY_DEV_2_1_1);
    gmaps_device_set_alternative(device, alt_device);

    device = gmaps_get_device(TEST_TOPOLOGY_KEY_DEV_3_2_0);
    alt_device = gmaps_get_device(TEST_TOPOLOGY_KEY_DEV_3_2_1);
    gmaps_device_set_alternative(device, alt_device);

}


static amxc_var_t* check_device(amxc_var_t* device, char* device_name, char* alternative) {
    amxc_var_t* children = amxc_var_get_key(device, "Children", AMXC_VAR_FLAG_DEFAULT);
    assert_string_equal(GETP_CHAR(device, "Alias"), device_name);

    if(alternative != NULL) {
        amxc_var_t* slaves = amxc_var_get_key(device, "Childs", AMXC_VAR_FLAG_DEFAULT);
        amxc_var_t* slave = amxc_var_get_first(slaves);
        assert_string_equal(GETP_CHAR(slave, "Alias"), alternative);
    }

    return children;
}


static int check_device_tree_top_down(amxc_var_t* tree) {
    amxc_var_t* children = NULL;
    amxc_var_t* device = NULL;

    children = check_device(tree, TEST_TOPOLOGY_KEY_DEV_1_1_0, NULL);

    device = amxc_var_get_first(children);
    check_device(device, TEST_TOPOLOGY_KEY_DEV_2_1_0, TEST_TOPOLOGY_KEY_DEV_2_1_1);

    device = amxc_var_get_last(children);
    children = check_device(device, TEST_TOPOLOGY_KEY_DEV_2_2_0, NULL);

    device = amxc_var_get_first(children);
    check_device(device, TEST_TOPOLOGY_KEY_DEV_3_1_0, NULL);

    device = amxc_var_get_last(children);
    check_device(device, TEST_TOPOLOGY_KEY_DEV_3_2_0, TEST_TOPOLOGY_KEY_DEV_3_2_1);

    return 1;
}


static int check_device_tree_down_up(amxc_var_t* tree) {
    amxc_var_t* children = NULL;
    amxc_var_t* device = NULL;

    children = check_device(tree, TEST_TOPOLOGY_KEY_DEV_3_2_0, TEST_TOPOLOGY_KEY_DEV_3_2_1);

    device = amxc_var_get_first(children);
    children = check_device(device, TEST_TOPOLOGY_KEY_DEV_2_2_0, NULL);

    device = amxc_var_get_first(children);
    check_device(device, TEST_TOPOLOGY_KEY_DEV_1_1_0, NULL);

    return 1;
}


static int check_device_tree_traverse(amxc_var_t* tree) {
    amxc_var_t* children = NULL;
    amxc_var_t* device = NULL;

    children = check_device(tree, TEST_TOPOLOGY_KEY_DEV_1_1_0, NULL);

    device = amxc_var_get_first(children);
    check_device(device, TEST_TOPOLOGY_KEY_DEV_2_1_0, TEST_TOPOLOGY_KEY_DEV_2_1_1);

    return 1;
}


void test_topology_traverse_down(GMAPS_UNUSED void** state) {
    amxd_object_t* upper_device = NULL;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    create_device_tree();
    upper_device = gmaps_get_device(TEST_TOPOLOGY_KEY_DEV_1_1_0);

    amxc_var_add_key(cstring_t, &args, "traverse", "down");
    amxc_var_add_key(cstring_t, &args, "expression", "");
    amxc_var_add_key(cstring_t, &args, "flags", "");

    assert_int_equal(amxd_object_invoke_function(upper_device,
                                                 "topology",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    assert_int_equal(check_device_tree_top_down(&ret), 1);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}


void test_topology_traverse_up(GMAPS_UNUSED void** state) {
    amxd_object_t* lower_device = NULL;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    lower_device = gmaps_get_device(TEST_TOPOLOGY_KEY_DEV_3_2_0);

    amxc_var_add_key(cstring_t, &args, "traverse", "up");
    amxc_var_add_key(cstring_t, &args, "expression", "");
    amxc_var_add_key(cstring_t, &args, "flags", "");

    assert_int_equal(amxd_object_invoke_function(lower_device,
                                                 "topology",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    assert_int_equal(check_device_tree_down_up(&ret), 1);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}


void test_topology_match_expression(GMAPS_UNUSED void** state) {
    amxd_object_t* upper_device = NULL;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    upper_device = gmaps_get_device(TEST_TOPOLOGY_KEY_DEV_1_1_0);

    amxc_var_add_key(cstring_t, &args, "traverse", "down");
    amxc_var_add_key(cstring_t, &args, "expression", "device hgw");
    amxc_var_add_key(cstring_t, &args, "flags", "no_recurse");

    assert_int_equal(amxd_object_invoke_function(upper_device,
                                                 "topology",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    assert_int_equal(check_device_tree_traverse(&ret), 1);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_topology_defaults(GMAPS_UNUSED void** state) {
    amxd_object_t* upper_device = NULL;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    upper_device = gmaps_get_device(TEST_TOPOLOGY_KEY_DEV_1_1_0);

    assert_int_equal(amxd_object_invoke_function(upper_device,
                                                 "topology",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    assert_int_equal(check_device_tree_top_down(&ret), 1);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}



void test_topology_traverse_failed(GMAPS_UNUSED void** state) {
    AMXB_UNUSED amxd_object_t* upper_device = NULL;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    upper_device = gmaps_get_device("no_device");
    upper_device = gmaps_get_device(TEST_TOPOLOGY_KEY_DEV_1_1_0);

    amxc_var_add_key(cstring_t, &args, "traverse", "down");
    amxc_var_add_key(cstring_t, &args, "expression", NULL);
    amxc_var_add_key(cstring_t, &args, "flags", "");

    assert_int_equal(_Device_topology(NULL, NULL, &args, &ret), amxd_status_unknown_error);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}


void test_topology_alternatives(GMAPS_UNUSED void** state) {
    // GIVEN a device that is an alternative of another device
    amxc_var_t testdev2_params;
    amxc_var_init(&testdev2_params);
    amxc_var_set_type(&testdev2_params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &testdev2_params, "PhysAddress", "60:50:40:30:20:10");
    util_create_device("testdev1", "testdiscosource1", "mytag1", false, NULL, amxd_status_ok, NULL);
    amxd_object_t* testdev1_obj = gmaps_get_device("testdev1");
    util_create_device("testdev2", "testdiscosource2", "mytag2 mac", false, NULL, amxd_status_ok, &testdev2_params);
    amxd_object_t* testdev2_obj = gmaps_get_device("testdev2");
    assert_int_equal(amxd_status_ok, gmaps_device_set_alternative(testdev1_obj, testdev2_obj));

    // WHEN querying the topology of the "master" device
    gmap_status_t status = gmap_status_ok;
    amxc_var_t* topology = gmaps_device_topology(testdev1_obj, "", gmap_traverse_down, 0, &status);
    assert_int_equal(status, gmap_status_ok);
    amxc_var_dump(topology, 0);

    // THEN the topology contains the fields of its child in "Childs" [sic]
    assert_string_equal(GETP_CHAR(topology, "0.Childs.0.Alias"), "testdev2");
    assert_string_equal(GETP_CHAR(topology, "0.Childs.0.DiscoverySource"), "testdiscosource2");
    assert_string_equal(GETP_CHAR(topology, "0.Childs.0.Tags"), "mytag2 mac");
    assert_string_equal(GETP_CHAR(topology, "0.Childs.0.PhysAddress"), "60:50:40:30:20:10");

    amxc_var_delete(&topology);
    amxc_var_clean(&testdev2_params);
}