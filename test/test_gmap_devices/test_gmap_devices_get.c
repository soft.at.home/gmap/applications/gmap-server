/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_devices.h"
#include "../test_common/test_util.h"

#include "gmaps_dm_devices.h"
#include "gmaps_devices.h"
#include "gmaps_devices_multilink.h"



void test_gmap_can_devices_get(GMAPS_UNUSED void** state) {

    amxc_var_t* test = gmaps_devices_get("", 0);

    assert_non_null(test);
    assert_int_equal(AMXC_VAR_ID_LIST, test->type_id);

    amxc_var_delete(&test);


}

void test_gmap_can_devices_get_result(GMAPS_UNUSED void** state) {
    const char* dev_name = "dev_get_result";
    util_create_device(dev_name, "default", "", false, "", amxd_status_ok, NULL);

    amxc_var_t* test = gmaps_devices_get(".Key==\"dev_get_result\"", 0);
    int count = 0;

    amxc_var_dump(test, STDOUT_FILENO);

    assert_non_null(test);

    assert_int_equal(AMXC_VAR_ID_LIST, test->type_id);

    amxc_var_for_each(result, test) {
        assert_int_equal(result->type_id, AMXC_VAR_ID_HTABLE);
        char* key = amxc_var_dyncast(cstring_t, amxc_var_get_key(result, "Key", 0));
        assert_string_equal(key, dev_name);
        count++;
        free(key);
    }

    assert_int_equal(count, 1);

    amxc_var_delete(&test);
}

void test_gmap_can_devices_get_custom_ops(GMAPS_UNUSED void** state) {
    amxc_var_t* test = NULL;
    int count = 0;
    const char* dev_key1 = "dev_custom_ops1";
    const char* dev_key2 = "dev_custom_ops2";

    util_create_device(dev_key1, "default", "self physical", false, "", amxd_status_ok, NULL);
    util_create_device(dev_key2, "test2DiscoverySource", "wifi", false, "test2DefaultName", amxd_status_ok, NULL);

    gmaps_devices_multilink_replace(dev_key1, dev_key2, "default", "testdatasource");

    handle_events();
    // NO LINKS displayed using gmaps_devices_get
    test = gmaps_devices_get(".Key==\"dev_custom_ops2\" and is_connected()", 0);
    amxc_var_dump(test, STDOUT_FILENO);
    assert_non_null(test);
    assert_int_equal(AMXC_VAR_ID_LIST, test->type_id);
    amxc_var_for_each(result, test) {
        assert_int_equal(result->type_id, AMXC_VAR_ID_HTABLE);
        char* key = amxc_var_dyncast(cstring_t, amxc_var_get_key(result, "Key", 0));
        assert_string_equal(key, dev_key2);
        count++;
        free(key);
    }

    assert_int_equal(count, 1);
    amxc_var_delete(&test);
    count = 0;

    test = gmaps_devices_get(".Key==\"dev_custom_ops2\" and exists(\"Names\") and has_instances(\"Names\") and has_instance(\"Names\",\".Source==\'default\'\") and .Names.default.Name==\"test2DefaultName\"", 0);

    amxc_var_dump(test, STDOUT_FILENO);
    assert_non_null(test);
    assert_int_equal(AMXC_VAR_ID_LIST, test->type_id);
    amxc_var_for_each(result, test) {
        assert_int_equal(result->type_id, AMXC_VAR_ID_HTABLE);
        char* key = amxc_var_dyncast(cstring_t, amxc_var_get_key(result, "Key", 0));
        assert_string_equal(key, dev_key2);
        count++;
        free(key);
    }
    assert_int_equal(count, 1);
    amxc_var_delete(&test);

    test = gmaps_devices_get(".Key==\"dev_custom_ops2\" and exists(\"Bob\")", 0);
    amxc_var_dump(test, STDOUT_FILENO);
    assert_non_null(test);
    assert_null(amxc_var_get_first(test));
    amxc_var_delete(&test);

    test = gmaps_devices_get(".Key==\"dev_custom_ops2\" and has_instances(\"Bob\")", 0);
    amxc_var_dump(test, STDOUT_FILENO);
    assert_non_null(test);
    assert_null(amxc_var_get_first(test));
    amxc_var_delete(&test);

    test = gmaps_devices_get(".Key==\"dev_custom_ops2\" and has_instance(\"Names\",\".Source==\'jupiter\'\")", 0);
    amxc_var_dump(test, STDOUT_FILENO);
    assert_non_null(test);
    assert_null(amxc_var_get_first(test));
    amxc_var_delete(&test);

    gmaps_devices_multilink_remove(NULL, dev_key2, "testdatasource");
    gmaps_devices_multilink_remove(dev_key1, NULL, "testdatasource");

    handle_events();

    test = gmaps_devices_get(".Key==\"dev_custom_ops2\" and is_connected()", 0);
    amxc_var_dump(test, STDOUT_FILENO);
    assert_non_null(test);
    assert_null(amxc_var_get_first(test));
    amxc_var_delete(&test);

    gmaps_devices_chosenlink_make(dev_key1, dev_key2, "default", true);

}

static void s_testhelper_gmap_devices_get_matching_mac(const char* base_mac, const char* mask, const char* mac1, bool expected_match_mac1, const char* mac2, bool expected_match_mac2) {
    amxc_var_t* test = NULL;
    amxc_var_t params;
    amxc_string_t expression;
    int i = 0;
    amxc_var_init(&params);
    amxc_string_init(&expression, 32);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &params, "PhysAddress", mac1);
    util_create_device("test_device", "default", "mac", false, "test_device", amxd_status_ok, &params);
    handle_events();

    amxc_var_set(cstring_t, GET_ARG(&params, "PhysAddress"), mac2);
    util_create_device("test_device_2", "default", "mac", false, "test_device_2", amxd_status_ok, &params);
    handle_events();

    amxc_string_setf(&expression, "mac_in_mask(\"%s\", \"%s\")", base_mac, mask);
    test = gmaps_devices_get(amxc_string_get(&expression, 0), 0);

    amxc_var_dump(test, STDOUT_FILENO);
    assert_non_null(test);

    if(expected_match_mac1) {
        assert_string_equal("test_device", GET_CHAR(GETI_ARG(test, i), "Alias"));
        i++;
    }
    if(expected_match_mac2) {
        assert_string_equal("test_device_2", GET_CHAR(GETI_ARG(test, i), "Alias"));
        i++;
    }
    assert_null(GETI_ARG(test, i));

    amxc_var_clean(&params);
    amxc_var_delete(&test);
    amxc_string_clean(&expression);
}

void test_gmap_devices_get_matching_mac(GMAPS_UNUSED void** state) {
    s_testhelper_gmap_devices_get_matching_mac("11:00:00:00:00:00", "ff:00:00:00:00:00",
                                               "11:22:33:44:55:66", true,
                                               "22:33:44:55:66:77", false);

    s_testhelper_gmap_devices_get_matching_mac("11:00:00:00:00:00", "ff:00:00:00:00:00",
                                               "11:22:33:44:55:66", true,
                                               "10:33:44:55:66:77", false);

    s_testhelper_gmap_devices_get_matching_mac("11:22:33:00:00:00", "ff:ff:ff:00:00:00",
                                               "11:22:33:44:55:66", true,
                                               "ff:ff:ff:55:66:77", false);

    s_testhelper_gmap_devices_get_matching_mac("fe:dc:ba:98:76:54", "ff:ff:ff:ff:ff:ff",
                                               "fe:dc:ba:98:77:54", false,
                                               "fe:dc:ba:98:76:54", true);
}

/**
 *  Test if we correctly can invoke the get function
 */
void test_gmap_can_invoke_devices_get(GMAPS_UNUSED void** state) {
    amxd_object_t* device = amxd_dm_findf(gmap_get_dm(), "Devices");
    amxc_var_t args;
    amxc_var_t result;

    amxc_var_init(&args);
    amxc_var_init(&result);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, "expression", ".Key == \"" TEST2KEY "\"");
    amxc_var_add_key(cstring_t, &args, "flags", "");

    assert_non_null(device);

    amxd_object_invoke_function(device, "get", &args, &result);

    amxc_var_clean(&args);
    amxc_var_clean(&result);
}
