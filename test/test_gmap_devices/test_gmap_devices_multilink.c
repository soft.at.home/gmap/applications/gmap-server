/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_devices.h"
#include "gmaps_devices.h"
#include "gmaps_dm_devices.h"
#include "../test_common/test_util.h"
#include <amxut/amxut_dm.h>
#include <amxut/amxut_bus.h>

/**
 * @file This file tests the functionality to add/remove Devices.Device.[].UDevice and
 * Devices.Device.[].LDevice but NOT Devices.Device.[].Link.
 *
 * So this tests the lower-level API of gmaps_devices_link.c and not the higher-level API
 * of gmaps_devices_multilink.c.
 */

static int32_t s_get_nb_links(const char* dev, const char* datasource, gmaps_link_type_t direction) {
    amxd_object_t* uldev_obj = amxd_object_get(amxd_object_get(amxd_object_get(gmaps_get_device(dev), "Link"), datasource), gmaps_device_get_link_text(direction));
    assert_non_null(uldev_obj);
    return amxd_object_get_instance_count(uldev_obj);
}

void test_multilink_rpc_linkAdd(GMAPS_UNUSED void** state) {
    // GIVEN two devices
    util_create_device("hgw", "testdiscosource", "", false, NULL, amxd_status_ok, NULL);
    util_create_device("port", "testdiscosource", "", false, NULL, amxd_status_ok, NULL);
    assert_false(util_is_linked_to("hgw", "port", "testdatasource"));

    // WHEN calling RPC function to link
    util_call_linkAdd("hgw", "port", "testdatasource", "default", true);
    amxut_bus_handle_events();

    // THEN the device is directly linked
    assert_true(util_is_linked_to("hgw", "port", "testdatasource"));
    // THEN the device is indirectly linked
    assert_true(gmaps_device_is_linked_to(gmaps_get_device("hgw"), gmaps_get_device("port"), gmap_traverse_down));
    assert_true(gmaps_device_is_linked_to(gmaps_get_device("port"), gmaps_get_device("hgw"), gmap_traverse_up));
}

void test_multilink_rpc_linkAdd_fail(GMAPS_UNUSED void** state) {
    // GIVEN one device
    util_create_device("hgw", "testdiscosource", "", false, NULL, amxd_status_ok, NULL);

    // WHEN calling RPC function to link the device to a non-existing device
    util_call_linkAdd("hgw", "port", "testdatasource", "default", false);

    // THEN the RPC function propagated the error from the C function it called
    // (this is checked in the helper function that calls the RPC function)
}

void test_multilink_rpc_linkAdd_invalid_args(GMAPS_UNUSED void** state) {
    util_create_device("dev1", "testdiscosource", "", false, NULL, amxd_status_ok, NULL);
    util_create_device("dev2", "testdiscosource", "", false, NULL, amxd_status_ok, NULL);

    util_call_linkAdd("dev1", NULL, "testdatasource", "default", false);
    util_call_linkAdd(NULL, "dev2", "testdatasource", "default", false);
    util_call_linkAdd("dev1", "dev2", NULL, "default", false);
    util_call_linkAdd("dev1", "dev2", "", "default", false);
}

void test_multilink_rpc_linkReplace(GMAPS_UNUSED void** state) {
    // GIVEN a device ("port") connected to another device ("bridge1")
    util_create_device("bridge1", "testdiscosource", "", false, NULL, amxd_status_ok, NULL);
    util_create_device("bridge2", "testdiscosource", "", false, NULL, amxd_status_ok, NULL);
    util_create_device("port", "testdiscosource", "", false, NULL, amxd_status_ok, NULL);
    util_call_linkAdd("bridge1", "port", "testdatasource", "default", true);
    amxut_bus_handle_events();
    assert_true(util_is_linked_to("bridge1", "port", "testdatasource"));

    // WHEN calling RPC function to replace link
    util_call_linkReplace("bridge2", "port", "testdatasource", "default", true);
    amxut_bus_handle_events();

    // THEN the RPC function called the C function, so we can observe the effect of the C function,
    // old link is removed:
    assert_false(util_is_linked_to("bridge1", "port", "testdatasource"));
    // new link is added:
    assert_true(util_is_linked_to("bridge2", "port", "testdatasource"));
}

void test_multilink_rpc_linkReplace_fail(GMAPS_UNUSED void** state) {
    // GIVEN one device
    util_create_device("hgw", "testdiscosource", "", false, NULL, amxd_status_ok, NULL);

    // WHEN calling RPC function to replace the link of the device to a non-existing device
    util_call_linkReplace("hgw", "port", "testdatasource", "default", false);

    // THEN the RPC function propagated the error from the C function it called
    // (this is checked in the helper function that calls the RPC function)
}

void test_multilink_rpc_linkReplace_invalid_args(GMAPS_UNUSED void** state) {
    util_create_device("dev1", "testdiscosource", "", false, NULL, amxd_status_ok, NULL);
    util_create_device("dev2", "testdiscosource", "", false, NULL, amxd_status_ok, NULL);

    util_call_linkReplace("dev1", NULL, "testdatasource", "default", false);
    util_call_linkReplace(NULL, "dev2", "testdatasource", "default", false);
    util_call_linkReplace("dev1", "dev2", NULL, "default", false);
    util_call_linkReplace("dev1", "dev2", "", "default", false);
}

void test_multilink_rpc_linkRemove(GMAPS_UNUSED void** state) {
    // GIVEN a device ("port") connected to another device ("bridge1")
    util_create_device("bridge1", "testdiscosource", "", false, NULL, amxd_status_ok, NULL);
    util_create_device("port", "testdiscosource", "", false, NULL, amxd_status_ok, NULL);
    util_call_linkAdd("bridge1", "port", "testdatasource", "default", true);
    amxut_bus_handle_events();
    assert_true(util_is_linked_to("bridge1", "port", "testdatasource"));

    // WHEN calling RPC function to remove the link
    util_call_linkRemove("bridge1", "port", "testdatasource", true);
    amxut_bus_handle_events();

    // THEN the RPC function called the C function, so we can observe the effect of the C function:
    // link is removed:
    assert_false(util_is_linked_to("bridge1", "port", "testdatasource"));
}

void test_multilink_rpc_linkRemove_twice(GMAPS_UNUSED void** state) {
    // GIVEN a device ("port") connected to another device ("bridge1")
    util_create_device("bridge1", "testdiscosource", "", false, NULL, amxd_status_ok, NULL);
    util_create_device("port", "testdiscosource", "", false, NULL, amxd_status_ok, NULL);
    util_call_linkAdd("bridge1", "port", "testdatasource", "default", true);
    assert_true(util_is_linked_to("bridge1", "port", "testdatasource"));

    // WHEN calling RPC function to remove the link, twice
    util_call_linkRemove("bridge1", "port", "testdatasource", true);
    util_call_linkRemove("bridge1", "port", "testdatasource", true);

    // THEN the link is removed
    assert_false(util_is_linked_to("bridge1", "port", "testdatasource"));
}

void test_multilink_rpc_linkRemove__multiple_sources(GMAPS_UNUSED void** state) {
    // GIVEN a device ("port") connected to another device ("bridge1"), according to two sources
    util_create_device("bridge1", "testdiscosource1", "", false, NULL, amxd_status_ok, NULL);
    util_create_device("port", "testdiscosource1", "", false, NULL, amxd_status_ok, NULL);
    util_call_linkAdd("bridge1", "port", "testdatasource1", "default", true);
    util_call_linkAdd("bridge1", "port", "testdatasource2", "default", true);
    assert_true(util_is_linked_to("bridge1", "port", "testdatasource1"));
    assert_true(util_is_linked_to("bridge1", "port", "testdatasource2"));

    // WHEN calling RPC function to remove the link of one datasource
    util_call_linkRemove("bridge1", "port", "testdatasource1", true);

    // THEN the link of one datasource is removed, but the link of the other is kept
    assert_false(util_is_linked_to_for_datasource("bridge1", "port", "testdatasource1"));
    assert_true(util_is_linked_to_for_datasource("bridge1", "port", "testdatasource2"));

    // THEN the selected link is kept (or replaced by the other multilink for the other datasource)
    assert_true(util_is_linked_to("bridge1", "port", "testdatasource2"));

    // WTODO assert that the link type (here "default") also changes
}

void test_multilink_rpc_linkRemove_removeAllUpper(GMAPS_UNUSED void** state) {
    // GIVEN a device ("dev1") with some upper devices
    util_create_device("devA", "testdiscosource", "", false, NULL, amxd_status_ok, NULL);
    util_create_device("devB", "testdiscosource", "", false, NULL, amxd_status_ok, NULL);
    util_create_device("devC", "testdiscosource", "", false, NULL, amxd_status_ok, NULL);
    util_create_device("dev1", "testdiscosource", "", false, NULL, amxd_status_ok, NULL);
    util_call_linkAdd("devB", "dev1", "otherdatasource", "default", true);
    util_call_linkAdd("devA", "dev1", "testdatasource", "default", true);
    util_call_linkAdd("devB", "dev1", "testdatasource", "default", true);
    util_call_linkAdd("devC", "dev1", "otherdatasource", "default", true);
    assert_true(util_is_linked_to_for_datasource("devA", "dev1", "testdatasource"));
    assert_true(util_is_linked_to_for_datasource("devB", "dev1", "testdatasource"));
    assert_true(util_is_linked_to_for_datasource("devB", "dev1", "otherdatasource"));
    assert_true(util_is_linked_to_for_datasource("devC", "dev1", "otherdatasource"));
    assert_true(util_is_link_selected("devA", "dev1")
                || util_is_link_selected("devB", "dev1")
                || util_is_link_selected("devC", "dev1"));

    // WHEN removing all upper links for a datasource
    util_call_linkRemove(NULL, "dev1", "testdatasource", true);

    // THEN all upper links are removed for that datasource
    assert_false(util_is_linked_to_for_datasource("devA", "dev1", "testdatasource"));
    assert_false(util_is_linked_to_for_datasource("devB", "dev1", "testdatasource"));
    // THEN the upper links are not removed for the other datasources
    assert_true(util_is_linked_to_for_datasource("devB", "dev1", "otherdatasource"));
    assert_true(util_is_linked_to_for_datasource("devC", "dev1", "otherdatasource"));
    // THEN the selected link is one of the remaining multilinks.
    assert_true(util_is_link_selected("devA", "dev1")
                || util_is_link_selected("devB", "dev1"));
    assert_false(util_is_link_selected("devC", "dev1"));
}

void test_multilink_rpc_linkRemove_removeAllLower(GMAPS_UNUSED void** state) {
    // GIVEN a device ("dev1") with some lower devices
    util_create_device("devA", "testdiscosource", "", false, NULL, amxd_status_ok, NULL);
    util_create_device("devB", "testdiscosource", "", false, NULL, amxd_status_ok, NULL);
    util_create_device("dev1", "testdiscosource", "", false, NULL, amxd_status_ok, NULL);
    util_create_device("devC", "testdiscosource", "", false, NULL, amxd_status_ok, NULL);
    util_create_device("devD", "testdiscosource", "", false, NULL, amxd_status_ok, NULL);
    util_call_linkAdd("dev1", "devA", "testdatasource", "default", true);
    util_call_linkAdd("dev1", "devB", "testdatasource", "default", true);
    util_call_linkAdd("devD", "dev1", "otherdatasource", "default", true);
    util_call_linkAdd("dev1", "devC", "otherdatasource", "default", true);
    assert_true(util_is_linked_to("dev1", "devA", "testdatasource"));
    assert_true(util_is_linked_to("dev1", "devB", "testdatasource"));
    assert_true(util_is_linked_to("devD", "dev1", "otherdatasource"));
    assert_true(util_is_linked_to("dev1", "devC", "otherdatasource"));

    // WHEN removing all lower links for a datasource
    util_call_linkRemove("dev1", NULL, "testdatasource", true);

    // THEN all lower links are removed for that datasource
    assert_false(util_is_linked_to("dev1", "devA", "testdatasource"));
    assert_false(util_is_linked_to("dev1", "devB", "testdatasource"));
    // AND THEN the lower links are not removed for the other datasource
    assert_true(util_is_linked_to_for_datasource("devD", "dev1", "otherdatasource"));
    assert_true(util_is_linked_to_for_datasource("dev1", "devC", "otherdatasource"));
}

void test_multilink_rpc_linkRemove_invalid_args(GMAPS_UNUSED void** state) {
    util_create_device("dev1", "testdiscosource", "", false, NULL, amxd_status_ok, NULL);
    util_create_device("dev2", "testdiscosource", "", false, NULL, amxd_status_ok, NULL);

    util_call_linkRemove(NULL, NULL, "testdatasource", false);
    util_call_linkRemove("dev1", "dev2", NULL, false);
    util_call_linkRemove("dev1", "dev2", "", false);
}

void test_multilink_on_device_delete(GMAPS_UNUSED void** state) {
    // GIVEN the following devices linked as follows:
    // ```
    //
    //  ┌─────┐   testdatasource    ┌─────┐  testdatasource   ┌────┐
    //  │dev1 ├────────────────────►│dev2 ├──────────────────►│dev3│
    //  └─────┘                     └┬─┬──┘                   └────┘
    //                               │ │     testdatasource2    ▲
    //                               │ └────────────────────────┘
    //                               │
    //                               │
    //                               │      testdatasource    ┌────┐
    //                               └───────────────────────►│dev4│
    //                                                        └────┘
    // ```
    const char* dev1name = "linktest_dev1\" || \"a\" == \"b.dot.";
    const char* dev2name = "linktest_dev2\" || \"a\" == \"b.dot.";
    const char* dev3name = "linktest_dev3\" || \"a\" == \"b.dot.";
    const char* dev4name = "linktest_dev4\" || \"a\" == \"b.dot.";
    util_create_device(dev1name, "test", "", false, NULL, amxd_status_ok, NULL);
    util_create_device(dev2name, "test", "", false, NULL, amxd_status_ok, NULL);
    util_create_device(dev3name, "test", "", false, NULL, amxd_status_ok, NULL);
    util_create_device(dev4name, "test", "", false, NULL, amxd_status_ok, NULL);
    util_call_linkAdd(dev1name, dev2name, "testdatasource", "default", true);
    util_call_linkAdd(dev2name, dev3name, "testdatasource", "default", true);
    util_call_linkAdd(dev2name, dev3name, "testdatasource2", "default", true);
    util_call_linkAdd(dev2name, dev4name, "testdatasource", "default", true);
    handle_events();
    assert_true(util_is_linked_to(dev1name, dev2name, "testdatasource"));
    assert_true(util_is_linked_to(dev2name, dev3name, "testdatasource"));
    assert_true(util_is_linked_to(dev2name, dev3name, "testdatasource2"));
    assert_true(util_is_linked_to(dev2name, dev4name, "testdatasource"));

    // WHEN removing dev2
    assert_int_equal(gmaps_delete_device(amxd_dm_findf(amxut_bus_dm(), "Devices"), dev2name), gmap_status_ok);

    // THEN dev2 is unlinked
    assert_int_equal(0, s_get_nb_links(dev1name, "testdatasource", gmaps_link_type_lower_link));
    assert_int_equal(0, s_get_nb_links(dev1name, "testdatasource", gmaps_link_type_upper_link));
    assert_int_equal(0, s_get_nb_links(dev3name, "testdatasource", gmaps_link_type_upper_link));
    assert_int_equal(0, s_get_nb_links(dev3name, "testdatasource2", gmaps_link_type_lower_link));
    assert_int_equal(0, s_get_nb_links(dev3name, "testdatasource2", gmaps_link_type_upper_link));
    assert_int_equal(0, s_get_nb_links(dev3name, "testdatasource", gmaps_link_type_lower_link));
    assert_int_equal(0, s_get_nb_links(dev4name, "testdatasource", gmaps_link_type_upper_link));
    assert_int_equal(0, s_get_nb_links(dev4name, "testdatasource", gmaps_link_type_lower_link));

    // The low-level links (Devices.Device.[].UDevice / Devices.Device.[].LDevice) are cleared as well:
    assert_false(gmaps_device_is_linked_to(gmaps_get_device(dev2name), gmaps_get_device(dev3name), gmap_traverse_down));
    assert_false(gmaps_device_is_linked_to(gmaps_get_device(dev2name), gmaps_get_device(dev4name), gmap_traverse_down));
    assert_false(gmaps_device_is_linked_to(gmaps_get_device(dev2name), gmaps_get_device(dev1name), gmap_traverse_up));
    assert_false(gmaps_device_is_linked_to(gmaps_get_device(dev3name), gmaps_get_device(dev2name), gmap_traverse_up));
    assert_false(gmaps_device_is_linked_to(gmaps_get_device(dev4name), gmaps_get_device(dev2name), gmap_traverse_up));
    assert_int_equal(0, amxd_object_get_child_count(amxd_object_get(gmaps_get_device(dev1name), "LDevice")));
    assert_int_equal(0, amxd_object_get_child_count(amxd_object_get(gmaps_get_device(dev3name), "UDevice")));
    assert_int_equal(0, amxd_object_get_child_count(amxd_object_get(gmaps_get_device(dev4name), "UDevice")));
}

void test_multilink_rpc_linkAdd_twice(UNUSED void** state) {
    // GIVEN two devices that are linked with each other
    util_create_device("hgw", "testdiscosource", "", false, NULL, amxd_status_ok, NULL);
    util_create_device("port", "testdiscosource", "", false, NULL, amxd_status_ok, NULL);
    util_call_linkAdd("hgw", "port", "testdatasource", "default", true);
    assert_true(util_is_linked_to("hgw", "port", "testdatasource"));

    // WHEN linking again
    util_call_linkAdd("hgw", "port", "testdatasource", "default", true);

    // THEN nothing happens and the link stays there
    assert_true(util_is_linked_to("hgw", "port", "testdatasource"));
    // AND THEN there are no redundant instances created in the datamodel
    assert_int_equal(1, s_get_nb_links("hgw", "testdatasource", gmaps_link_type_lower_link));
    assert_int_equal(1, s_get_nb_links("hgw", "testdatasource", gmaps_link_type_lower_link));
    assert_int_equal(1, s_get_nb_links("port", "testdatasource", gmaps_link_type_upper_link));
    assert_int_equal(1, s_get_nb_links("port", "testdatasource", gmaps_link_type_upper_link));
}
