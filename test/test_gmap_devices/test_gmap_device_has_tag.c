/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_devices.h"
#include "gmaps_dm_device.h"
#include "gmaps_device_tag.h"



void test_gmap_can_device_has_tag(GMAPS_UNUSED void** state) {
    bool result = false;

    assert_int_equal(gmaps_device_has_tag(TEST2KEY, "myTag", "", 0, NULL), gmap_status_invalid_parameter);
    assert_int_equal(gmaps_device_has_tag(TEST2KEY, "", "", 0, &result), gmap_status_invalid_parameter);
    assert_int_equal(gmaps_device_has_tag(TEST2KEY, NULL, "", 0, &result), gmap_status_invalid_parameter);
    assert_int_equal(gmaps_device_has_tag("", "myTag", "", 0, &result), gmap_status_invalid_key);
    assert_int_equal(gmaps_device_has_tag(NULL, "myTag", "", 0, &result), gmap_status_invalid_key);
    assert_int_equal(gmaps_device_has_tag("nosuchdevice", "myTag", "", 0, &result), gmap_status_device_not_found);

    gmaps_device_has_tag(TEST2KEY, "myTag", "", 0, &result);
    assert_true(result);

    gmaps_device_has_tag(TEST2KEY, "myTag_test", "", 0, &result);
    assert_false(result);

    gmaps_device_has_tag(TEST2KEY, "wrongTag", "", 0, &result);
    assert_false(result);

    gmaps_device_has_tag(TEST2KEY, "myTag tag1", "", 0, &result);
    assert_true(result);

    gmaps_device_has_tag(TEST2KEY, "tag1 myTag", "", 0, &result);
    assert_true(result);

    gmaps_device_has_tag(TEST2KEY, "tag1 myTag wrongTag", "", 0, &result);
    assert_false(result);

    gmaps_device_has_tag(TEST2KEY, "wrongTag tag1 myTag", "", 0, &result);
    assert_false(result);

}

void test_gmap_can_invoke_device_has_tag(GMAPS_UNUSED void** state) {
    amxd_object_t* device = gmaps_get_device(TEST2KEY);
    amxc_var_t args;
    amxc_var_t result;

    amxc_var_init(&args);
    amxc_var_init(&result);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, "tag", "myTag");
    amxc_var_add_key(cstring_t, &args, "expression", "");
    amxc_var_add_key(cstring_t, &args, "traverse", "this");

    assert_non_null(device);

    assert_int_equal(amxd_object_invoke_function(device, "hasTag", &args, &result), amxd_status_ok);

    amxc_var_dump(&result, 1);

    bool bresult = amxc_var_get_const_bool(&result);


    assert_true(bresult);

    amxc_var_clean(&args);
    amxc_var_clean(&result);

}
