/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_devices.h"
#include "../test_common/test_util.h"
#include "gmaps_devices.h"
#include <gmap/gmap.h>

#include <amxc/amxc.h>
#include <amxp/amxp_signal.h>
#include <amxp/amxp_slot.h>

#include <amxd/amxd_common.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>

#define GMAP_EVENT 1300

static int event_rcvd = 0;

/*
   {
    Data = 1,
    EventId = 1300,
    Key = TESTKEY,
    eobject = "Devices.Device.[testKey].",
    object = "Devices.Device.testKey.",
    path = "Devices.Device.1."
   }
 */

static void gmap_event_slot(const char* const sig_name,
                            const amxc_var_t* const data,
                            GMAPS_UNUSED void* const priv) {
    if(!strcmp(sig_name, "gmap_event")) {
        uint32_t eventid = GET_UINT32(data, "EventId");
        if(eventid == GMAP_EVENT) {
            event_rcvd += 1;
            amxc_var_dump(data, 0);
            amxc_var_t* event_data = GET_ARG(data, "Data");
            assert_non_null(event_data);
            assert_int_equal(1, amxc_var_constcast(uint32_t, event_data));
            _print_event(sig_name,
                         data,
                         NULL);
        }
    }
}

void test_rpc_can_send_notify(GMAPS_UNUSED void** state) {

    amxc_var_t event_data1;
    amxc_var_t* event_data = NULL;

    amxc_var_t args;
    amxc_var_t ret;

    amxd_object_t* root_obj = NULL;
    const char* dev_key = "test_rpc_send_notify_dev_key";
    amxd_object_t* dev1 = NULL;

    util_create_device(dev_key, "", "", false, "", amxd_status_ok, NULL);
    dev1 = gmaps_get_device(dev_key);
    assert_non_null(dev1);

    amxc_var_init(&event_data1);
    amxc_var_set(uint32_t, &event_data1, 1);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "key", dev_key);
    amxc_var_add_key(uint32_t, &args, "id", GMAP_EVENT);

    event_data = amxc_var_add_new_key(&args, "data");
    amxc_var_copy(event_data, &event_data1);

    root_obj = amxd_dm_findf(amxut_bus_dm(), "Devices");
    assert_non_null(root_obj);

    {

        amxc_string_t expression;
        char* path = NULL;

        amxc_string_init(&expression, 0);

        path = amxd_object_get_path(dev1, AMXD_OBJECT_NAMED | AMXD_OBJECT_TERMINATE);
        amxc_string_setf(&expression, "object == \"%s\"", path);
        amxp_slot_connect(&(amxut_bus_dm()->sigmngr), "gmap_event", amxc_string_get(&expression, 0), gmap_event_slot, NULL);

        free(path);
        amxc_string_clean(&expression);
    }
    handle_events();

    assert_int_equal(amxd_object_invoke_function(root_obj,
                                                 "notify",
                                                 &args,
                                                 &ret), amxd_status_ok);
    handle_events();
    assert_int_equal(event_rcvd, 1);
    amxp_slot_disconnect(&(amxut_bus_dm()->sigmngr), "gmap_event", gmap_event_slot);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    amxc_var_clean(&event_data1);
}
