/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_devices.h"
#include "gmaps_devices.h"
#include "gmaps_dm_devices.h"


void test_rpc_can_find_device_by_parameter(GMAPS_UNUSED void** state) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t params;
    amxc_var_t* expression = NULL;

    assert_ptr_not_equal(devices, NULL);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_init(&params);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    expression = amxc_var_add_new_key(&args, "expression");
    amxc_var_set_type(expression, AMXC_VAR_ID_CSTRING);
    amxc_var_set(cstring_t, expression, ".Active == false");
    amxc_var_add_key(cstring_t, &args, "flags", "");

    assert_int_equal(amxd_object_invoke_function(devices,
                                                 "find",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    // check return value
    amxc_var_dump(&ret, STDOUT_FILENO);
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_LIST);
    amxc_llist_t* list = amxc_var_get_amxc_llist_t(&ret);
    amxc_llist_it_t* it = amxc_llist_get_first(list);
    assert_non_null(it);
    amxc_var_t* item = amxc_llist_it_get_data(it, amxc_var_t, lit);
    assert_string_equal(amxc_var_constcast(cstring_t, item),
                        TESTKEY);
    it = amxc_llist_it_get_next(it);
    assert_non_null(it);
    item = amxc_llist_it_get_data(it, amxc_var_t, lit);
    assert_string_equal(amxc_var_constcast(cstring_t, item),
                        TEST2KEY);
    it = amxc_llist_it_get_next(it);
    assert_non_null(it);
    item = amxc_llist_it_get_data(it, amxc_var_t, lit);
    assert_string_equal(amxc_var_constcast(cstring_t, item),
                        TEST3KEY);
    it = amxc_llist_it_get_next(it);
    amxc_llist_delete(&list, variant_list_it_free);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&params);
}

void test_rpc_cannot_find_device_by_parameter(GMAPS_UNUSED void** state) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t params;
    amxc_var_t* expression = NULL;

    assert_ptr_not_equal(devices, NULL);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_init(&params);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    expression = amxc_var_add_new_key(&args, "expression");
    amxc_var_set_type(expression, AMXC_VAR_ID_CSTRING);
    amxc_var_set(cstring_t, expression, ".Active == true");
    amxc_var_add_key(cstring_t, &args, "flags", "");

    assert_int_equal(amxd_object_invoke_function(devices,
                                                 "find",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    // check return value
    amxc_var_dump(&ret, STDOUT_FILENO);
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_LIST);
    amxc_llist_t* list = amxc_var_get_amxc_llist_t(&ret);
    amxc_llist_it_t* it = amxc_llist_get_first(list);
    assert_null(it);
    amxc_llist_delete(&list, variant_list_it_free);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&params);
}

void test_rpc_cannot_find_device_by_nonexisting_parameter_true(GMAPS_UNUSED void** state) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t params;
    amxc_var_t* expression = NULL;

    assert_ptr_not_equal(devices, NULL);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_init(&params);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    expression = amxc_var_add_new_key(&args, "expression");
    amxc_var_set_type(expression, AMXC_VAR_ID_CSTRING);
    amxc_var_set(cstring_t, expression, ".MadeUpParameter == true");
    amxc_var_add_key(cstring_t, &args, "flags", "");

    assert_int_equal(amxd_object_invoke_function(devices,
                                                 "find",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    // check return value
    amxc_var_dump(&ret, STDOUT_FILENO);
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_LIST);
    amxc_llist_t* list = amxc_var_get_amxc_llist_t(&ret);
    amxc_llist_it_t* it = amxc_llist_get_first(list);
    assert_null(it);
    amxc_llist_delete(&list, variant_list_it_free);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&params);
}

void test_rpc_cannot_find_device_by_nonexisting_parameter_false(GMAPS_UNUSED void** state) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t params;
    amxc_var_t* expression = NULL;

    assert_ptr_not_equal(devices, NULL);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_init(&params);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    expression = amxc_var_add_new_key(&args, "expression");
    amxc_var_set_type(expression, AMXC_VAR_ID_CSTRING);
    amxc_var_set(cstring_t, expression, ".MadeUpParameter == false");
    amxc_var_add_key(cstring_t, &args, "flags", "");

    assert_int_equal(amxd_object_invoke_function(devices,
                                                 "find",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    // check return list
    amxc_var_dump(&ret, STDOUT_FILENO);
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_LIST);
    amxc_llist_t* list = amxc_var_get_amxc_llist_t(&ret);
    amxc_llist_it_t* it = amxc_llist_get_first(list);
    assert_null(it);
    amxc_llist_delete(&list, variant_list_it_free);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&params);
}

void test_rpc_can_find_device_by_parameter_htable(GMAPS_UNUSED void** state) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t params;
    amxc_var_t* expression_table = NULL;
    amxc_var_t* expression_1 = NULL;
    amxc_var_t* expression_2 = NULL;

    assert_ptr_not_equal(devices, NULL);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_init(&params);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    expression_table = amxc_var_add_new_key(&args, "expression");
    amxc_var_set_type(expression_table, AMXC_VAR_ID_HTABLE);
    expression_1 = amxc_var_add_new_key(expression_table, "testID1");
    amxc_var_set(cstring_t, expression_1, ".Active == false");
    expression_2 = amxc_var_add_new_key(expression_table, "testID2");
    amxc_var_set(cstring_t, expression_2, ".Active == true");

    assert_int_equal(amxd_object_invoke_function(devices,
                                                 "find",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    // check returned table
    amxc_var_dump(&ret, STDOUT_FILENO);
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_HTABLE);
    amxc_htable_t* table = amxc_var_get_amxc_htable_t(&ret);

    // check expression_1
    amxc_htable_it_t* hit = amxc_htable_get_first(table);
    const char* key = amxc_htable_it_get_key(hit);
    assert_string_equal(key, "testID1");
    amxc_var_t* data = amxc_htable_it_get_data(hit, amxc_var_t, hit);
    amxc_llist_t* list = amxc_var_get_amxc_llist_t(data);
    amxc_llist_it_t* it = amxc_llist_get_first(list);
    assert_non_null(it);
    amxc_var_t* item = amxc_llist_it_get_data(it, amxc_var_t, lit);
    assert_string_equal(amxc_var_constcast(cstring_t, item),
                        TESTKEY);
    it = amxc_llist_it_get_next(it);
    assert_non_null(it);
    item = amxc_llist_it_get_data(it, amxc_var_t, lit);
    assert_string_equal(amxc_var_constcast(cstring_t, item),
                        TEST2KEY);
    it = amxc_llist_it_get_next(it);
    assert_non_null(it);
    item = amxc_llist_it_get_data(it, amxc_var_t, lit);
    assert_string_equal(amxc_var_constcast(cstring_t, item),
                        TEST3KEY);
    it = amxc_llist_it_get_next(it);
    amxc_llist_delete(&list, variant_list_it_free);

    // check expression_2
    hit = amxc_htable_it_get_next(hit);
    key = amxc_htable_it_get_key(hit);
    assert_string_equal(key, "testID2");
    data = amxc_htable_it_get_data(hit, amxc_var_t, hit);
    list = amxc_var_get_amxc_llist_t(data);
    it = amxc_llist_get_first(list);
    assert_null(it);
    amxc_llist_delete(&list, variant_list_it_free);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&params);
    amxc_htable_delete(&table, variant_htable_it_free);
}

void test_rpc_can_find_device_by_tag(GMAPS_UNUSED void** state) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t params;
    amxc_var_t* expression = NULL;

    assert_ptr_not_equal(devices, NULL);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_init(&params);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    expression = amxc_var_add_new_key(&args, "expression");
    amxc_var_set_type(expression, AMXC_VAR_ID_CSTRING);
    amxc_var_set(cstring_t, expression, "test3Tag2");
    amxc_var_add_key(cstring_t, &args, "flags", "");

    assert_int_equal(amxd_object_invoke_function(devices,
                                                 "find",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    // check return value
    amxc_var_dump(&ret, STDOUT_FILENO);
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_LIST);
    amxc_llist_t* list = amxc_var_get_amxc_llist_t(&ret);
    amxc_llist_it_t* it = amxc_llist_get_first(list);
    assert_non_null(it);
    amxc_var_t* item = amxc_llist_it_get_data(it, amxc_var_t, lit);
    assert_string_equal(amxc_var_constcast(cstring_t, item),
                        TEST3KEY);
    it = amxc_llist_it_get_next(it);
    assert_null(it);
    amxc_llist_delete(&list, variant_list_it_free);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&params);
}

void test_rpc_can_find_devices_by_tag(GMAPS_UNUSED void** state) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t params;
    amxc_var_t* expression = NULL;

    assert_ptr_not_equal(devices, NULL);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_init(&params);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    expression = amxc_var_add_new_key(&args, "expression");
    amxc_var_set_type(expression, AMXC_VAR_ID_CSTRING);
    amxc_var_set(cstring_t, expression, "wifi");
    amxc_var_add_key(cstring_t, &args, "flags", "");

    assert_int_equal(amxd_object_invoke_function(devices,
                                                 "find",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    // check return value
    amxc_var_dump(&ret, STDOUT_FILENO);
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_LIST);
    amxc_llist_t* list = amxc_var_get_amxc_llist_t(&ret);
    amxc_llist_it_t* it = amxc_llist_get_first(list);
    assert_non_null(it);
    amxc_var_t* item = amxc_llist_it_get_data(it, amxc_var_t, lit);
    assert_string_equal(amxc_var_constcast(cstring_t, item),
                        TEST2KEY);
    it = amxc_llist_it_get_next(it);
    assert_non_null(it);
    item = amxc_llist_it_get_data(it, amxc_var_t, lit);
    assert_string_equal(amxc_var_constcast(cstring_t, item),
                        TEST3KEY);
    it = amxc_llist_it_get_next(it);
    assert_null(it);
    amxc_llist_delete(&list, variant_list_it_free);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&params);
}

void test_rpc_can_find_device_by_tags(GMAPS_UNUSED void** state) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t params;
    amxc_var_t* expression = NULL;

    assert_ptr_not_equal(devices, NULL);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_init(&params);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    expression = amxc_var_add_new_key(&args, "expression");
    amxc_var_set_type(expression, AMXC_VAR_ID_CSTRING);
    amxc_var_set(cstring_t, expression, "wifi && eth");
    amxc_var_add_key(cstring_t, &args, "flags", "");

    assert_int_equal(amxd_object_invoke_function(devices,
                                                 "find",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    // check return value
    amxc_var_dump(&ret, STDOUT_FILENO);
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_LIST);
    amxc_llist_t* list = amxc_var_get_amxc_llist_t(&ret);
    amxc_llist_it_t* it = amxc_llist_get_first(list);
    assert_non_null(it);
    amxc_var_t* item = amxc_llist_it_get_data(it, amxc_var_t, lit);
    assert_string_equal(amxc_var_constcast(cstring_t, item),
                        TEST3KEY);
    it = amxc_llist_it_get_next(it);
    assert_null(it);
    amxc_llist_delete(&list, variant_list_it_free);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&params);
}
void test_internal_find_device_input_validation(GMAPS_UNUSED void** state) {
    amxc_var_t params;

    assert_ptr_not_equal(gmap_dm_get_devices_device(), NULL);
    amxc_var_init(&params);

    assert_int_equal(gmaps_find_devices(NULL,
                                        0,
                                        0,
                                        &params),
                     gmap_status_invalid_expression);
    assert_int_equal(gmaps_find_devices("testExpression",
                                        0,
                                        0,
                                        NULL),
                     gmap_status_invalid_parameter);

    amxc_var_clean(&params);
}
