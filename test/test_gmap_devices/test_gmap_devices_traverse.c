/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_devices.h"
#include "../test_common/test_util.h"
#include "gmaps_devices.h"
#include "gmaps_dm_devices.h"


typedef struct _traverse_cb_data {
    amxc_llist_it_t it;
    amxd_object_t* device;
    gmap_traverse_action_t action;
    void* userdata;
} traverse_cb_data_t;


static uint32_t callback_counter = 0;
static amxc_llist_t cb_list = {0};

/* Tree 0 */
static const char* const tree0_dev0_key = "keyTree0Dev0";
static const char* const tree0_dev1_key = "keyTree0Dev1";
static const char* const tree0_dev2_key = "keyTree0Dev2";
static const char* const tree0_dev3_key = "keyTree0Dev3";
static const char* const tree0_dev4_key = "keyTree0Dev4";
static const char* const tree0_dev5_key = "keyTree0Dev5";
static const char* const tree0_ds_tree = "DsTree0";
static const char* const tree0_dev0_tag = "device hgw";
static const char* const tree0_dev1_tag = "device wifi usb";
static const char* const tree0_dev2_tag = "device wifi repeater";
static const char* const tree0_dev3_tag = "device wifi client";
static const char* const tree0_dev4_tag = "device wifi printer";
static const char* const tree0_dev5_tag = "device mainframe";
/* Tree 1 */
static const char* const tree1_dev0_key = "keyTree1Dev0";
static const char* const tree1_dev1_key = "keyTree1Dev1";
static const char* const tree1_dev2_key = "keyTree1Dev2";
static const char* const tree1_dev3_key = "keyTree1Dev3";
static const char* const tree1_dev4_key = "keyTree1Dev4";
static const char* const tree1_ds_tree = "DsTree1";
static const char* const tree1_dev0_tag = "device hgw";
static const char* const tree1_dev1_tag = "device wifi usb";
static const char* const tree1_dev2_tag = "device wifi repeater";
static const char* const tree1_dev3_tag = "device wifi client";
static const char* const tree1_dev4_tag = "device wifi printer";


static void free_cb_data(amxc_llist_it_t* it) {
    traverse_cb_data_t* d = amxc_llist_it_get_data(it, traverse_cb_data_t, it);
    free(d);
}

static void append_cb_data(amxc_llist_t* list,
                           amxd_object_t* device,
                           gmap_traverse_action_t action,
                           void* userdata) {
    traverse_cb_data_t* d = calloc(1, sizeof(traverse_cb_data_t));
    d->device = device;
    d->action = action;
    d->userdata = userdata;
    amxc_llist_it_init(&d->it);
    amxc_llist_append(list, &d->it);
}

/**
 * Callback function that adds the cb data to a list.
 * Used to trace the traverse callback calls and save the parameter data.
 */
static gmap_traverse_status_t cb_add(amxd_object_t* device,
                                     gmap_traverse_action_t action,
                                     void* userdata) {
    const char* action_text = NULL;
    char* device_key = amxd_object_get_value(cstring_t, device, "Key", NULL);

    append_cb_data(&cb_list, device, action, userdata);

    #define X(a, b) case a: {action_text = b; break;}
    switch(action) {
        GMAP_TRAVERSE_ACTION_TABLE;
    }
    #undef X
    printf("\tTraverse callback device %s with action = %s\n",
           device_key == NULL ? "none" : device_key,
           action_text);

    free(device_key);
    return gmap_traverse_continue;
}

static gmap_traverse_status_t cb_add_no_recurse(amxd_object_t* device,
                                                gmap_traverse_action_t action,
                                                void* userdata) {
    cb_add(device, action, userdata);
    callback_counter--;
    return (callback_counter == 0) ? gmap_traverse_no_recurse : gmap_traverse_continue;
}

/**
 * Returns the expected callback list when starting the traverse from
 * tree 0, device 0 in the given @param mode.
 */
static amxc_llist_t* get_expected_cb_list_tree0_dev0_all_match(gmap_traverse_mode_t mode, void* userdata) {
    amxc_llist_t* expected = NULL;
    amxc_llist_new(&expected);

    switch(mode) {
    case gmap_traverse_this:
    case gmap_traverse_up:
    case gmap_traverse_one_up:
        append_cb_data(expected, NULL, gmap_traverse_start, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev0_key), gmap_traverse_device_matching, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev0_key), gmap_traverse_device_done, userdata);
        append_cb_data(expected, NULL, gmap_traverse_stop, userdata);
        break;
    case gmap_traverse_down:
        append_cb_data(expected, NULL, gmap_traverse_start, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev0_key), gmap_traverse_device_matching, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev0_key), gmap_traverse_level_push, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev1_key), gmap_traverse_device_matching, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev1_key), gmap_traverse_level_push, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev2_key), gmap_traverse_device_matching, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev2_key), gmap_traverse_level_push, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev4_key), gmap_traverse_device_matching, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev4_key), gmap_traverse_device_done, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev2_key), gmap_traverse_level_pop, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev2_key), gmap_traverse_device_done, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev3_key), gmap_traverse_device_matching, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev3_key), gmap_traverse_device_done, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev1_key), gmap_traverse_level_pop, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev1_key), gmap_traverse_device_done, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev0_key), gmap_traverse_level_pop, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev0_key), gmap_traverse_device_done, userdata);
        append_cb_data(expected, NULL, gmap_traverse_stop, userdata);
        break;
    case gmap_traverse_down_exclusive:
        append_cb_data(expected, NULL, gmap_traverse_start, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev1_key), gmap_traverse_device_matching, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev1_key), gmap_traverse_level_push, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev2_key), gmap_traverse_device_matching, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev2_key), gmap_traverse_level_push, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev4_key), gmap_traverse_device_matching, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev4_key), gmap_traverse_device_done, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev2_key), gmap_traverse_level_pop, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev2_key), gmap_traverse_device_done, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev3_key), gmap_traverse_device_matching, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev3_key), gmap_traverse_device_done, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev1_key), gmap_traverse_level_pop, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev1_key), gmap_traverse_device_done, userdata);
        append_cb_data(expected, NULL, gmap_traverse_stop, userdata);
        break;
    case gmap_traverse_up_exclusive:
    case gmap_traverse_one_up_exclusive:
        append_cb_data(expected, NULL, gmap_traverse_start, userdata);
        append_cb_data(expected, NULL, gmap_traverse_stop, userdata);
        break;
    case gmap_traverse_one_down:
        append_cb_data(expected, NULL, gmap_traverse_start, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev0_key), gmap_traverse_device_matching, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev0_key), gmap_traverse_level_push, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev1_key), gmap_traverse_device_matching, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev1_key), gmap_traverse_device_done, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev0_key), gmap_traverse_level_pop, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev0_key), gmap_traverse_device_done, userdata);
        append_cb_data(expected, NULL, gmap_traverse_stop, userdata);
        break;
    case gmap_traverse_one_down_exclusive:
        append_cb_data(expected, NULL, gmap_traverse_start, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev1_key), gmap_traverse_device_matching, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev1_key), gmap_traverse_device_done, userdata);
        append_cb_data(expected, NULL, gmap_traverse_stop, userdata);
        break;

    default:
        assert_true(0);
    }

    return expected;
}

static amxc_llist_t* get_expected_cb_list_tree0_dev0_all_match_early_stop(gmap_traverse_mode_t mode,
                                                                          void* userdata) {
    amxc_llist_t* expected = NULL;
    amxc_llist_new(&expected);

    switch(mode) {
    case gmap_traverse_this:
    case gmap_traverse_up:
    case gmap_traverse_one_up:
        append_cb_data(expected, NULL, gmap_traverse_start, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev0_key), gmap_traverse_device_matching, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev0_key), gmap_traverse_device_done, userdata);
        append_cb_data(expected, NULL, gmap_traverse_stop, userdata);
        break;
    case gmap_traverse_down:
        append_cb_data(expected, NULL, gmap_traverse_start, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev0_key), gmap_traverse_device_matching, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev0_key), gmap_traverse_level_push, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev1_key), gmap_traverse_device_matching, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev1_key), gmap_traverse_level_push, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev2_key), gmap_traverse_device_matching, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev2_key), gmap_traverse_device_done, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev3_key), gmap_traverse_device_matching, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev3_key), gmap_traverse_device_done, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev1_key), gmap_traverse_level_pop, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev1_key), gmap_traverse_device_done, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev0_key), gmap_traverse_level_pop, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev0_key), gmap_traverse_device_done, userdata);
        append_cb_data(expected, NULL, gmap_traverse_stop, userdata);
        break;
    case gmap_traverse_down_exclusive:
        append_cb_data(expected, NULL, gmap_traverse_start, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev1_key), gmap_traverse_device_matching, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev1_key), gmap_traverse_level_push, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev2_key), gmap_traverse_device_matching, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev2_key), gmap_traverse_level_push, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev4_key), gmap_traverse_device_matching, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev4_key), gmap_traverse_device_done, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev2_key), gmap_traverse_level_pop, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev2_key), gmap_traverse_device_done, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev3_key), gmap_traverse_device_matching, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev3_key), gmap_traverse_device_done, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev1_key), gmap_traverse_level_pop, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev1_key), gmap_traverse_device_done, userdata);
        append_cb_data(expected, NULL, gmap_traverse_stop, userdata);
        break;
    case gmap_traverse_up_exclusive:
    case gmap_traverse_one_up_exclusive:
        append_cb_data(expected, NULL, gmap_traverse_start, userdata);
        append_cb_data(expected, NULL, gmap_traverse_stop, userdata);
        break;
    case gmap_traverse_one_down:
        append_cb_data(expected, NULL, gmap_traverse_start, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev0_key), gmap_traverse_device_matching, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev0_key), gmap_traverse_level_push, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev1_key), gmap_traverse_device_matching, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev1_key), gmap_traverse_device_done, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev0_key), gmap_traverse_level_pop, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev0_key), gmap_traverse_device_done, userdata);
        append_cb_data(expected, NULL, gmap_traverse_stop, userdata);
        break;
    case gmap_traverse_one_down_exclusive:
        append_cb_data(expected, NULL, gmap_traverse_start, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev1_key), gmap_traverse_device_matching, userdata);
        append_cb_data(expected, gmaps_get_device(tree0_dev1_key), gmap_traverse_device_done, userdata);
        append_cb_data(expected, NULL, gmap_traverse_stop, userdata);
        break;

    default:
        assert_true(0);
    }

    return expected;
}

static amxc_llist_t* get_expected_cb_list_tree0_dev0_this_early_stop(void* userdata) {
    amxc_llist_t* expected = NULL;
    amxc_llist_new(&expected);

    append_cb_data(expected, NULL, gmap_traverse_start, userdata);
    append_cb_data(expected, gmaps_get_device(tree0_dev0_key), gmap_traverse_device_matching, userdata);
    append_cb_data(expected, gmaps_get_device(tree0_dev0_key), gmap_traverse_device_done, userdata);
    append_cb_data(expected, NULL, gmap_traverse_stop, userdata);

    return expected;
}

static amxc_llist_t* get_expected_cb_list_tree0_dev0_early_stop_at_push_cb(void* userdata) {
    amxc_llist_t* expected = NULL;
    amxc_llist_new(&expected);

    append_cb_data(expected, NULL, gmap_traverse_start, userdata);
    append_cb_data(expected, gmaps_get_device(tree0_dev0_key), gmap_traverse_device_matching, userdata);
    append_cb_data(expected, gmaps_get_device(tree0_dev0_key), gmap_traverse_level_push, userdata);
    append_cb_data(expected, gmaps_get_device(tree0_dev0_key), gmap_traverse_level_pop, userdata);
    append_cb_data(expected, NULL, gmap_traverse_stop, userdata);

    return expected;
}

static void assert_cb_seq_same(amxc_llist_t* l1, amxc_llist_t* l2) {
    assert_int_equal(amxc_llist_size(l1), amxc_llist_size(l2));
    for(uint32_t i = 0; i < amxc_llist_size(l1); ++i) {
        traverse_cb_data_t* d1 = amxc_llist_it_get_data(amxc_llist_get_at(l1, i),
                                                        traverse_cb_data_t,
                                                        it);
        traverse_cb_data_t* d2 = amxc_llist_it_get_data(amxc_llist_get_at(l2, i),
                                                        traverse_cb_data_t,
                                                        it);
        assert_int_equal(d1->device, d2->device);
        assert_int_equal(d1->action, d2->action);
        assert_int_equal(d1->userdata, d2->userdata);
    }
}

/**
 * This hulpfunction creates device tree 0.
 * The tree has the following topology.
 *
 *                      == DEVICE TREE 0 ==
 *
 *            +-------+
 *            | dev 0 |
 *            +-------+
 *                |
 *            +-------+
 *            | dev 1 |
 *            +-------+
 *            |       |
 *       +-------+ +-------+            +-------+
 *       | dev 2 | | dev 3 | ---alt---> | dev 5 |
 *       +-------+ +-------+            +-------+
 *            |
 *       +-------+
 *       | dev 4 |
 *       +-------+
 *
 */
static void s_create_tree0() {
    util_create_device(tree0_dev0_key, tree0_ds_tree, tree0_dev0_tag, false, "", amxd_status_ok, NULL);
    util_create_device(tree0_dev1_key, tree0_ds_tree, tree0_dev1_tag, false, "", amxd_status_ok, NULL);
    util_create_device(tree0_dev2_key, tree0_ds_tree, tree0_dev2_tag, false, "", amxd_status_ok, NULL);
    util_create_device(tree0_dev3_key, tree0_ds_tree, tree0_dev3_tag, false, "", amxd_status_ok, NULL);
    util_create_device(tree0_dev4_key, tree0_ds_tree, tree0_dev4_tag, false, "", amxd_status_ok, NULL);
    util_create_device(tree0_dev5_key, tree0_ds_tree, tree0_dev5_tag, false, "", amxd_status_ok, NULL);

    util_chosenlink_link_devices(tree0_dev0_key, tree0_dev1_key, true);
    util_chosenlink_link_devices(tree0_dev1_key, tree0_dev2_key, true);
    util_chosenlink_link_devices(tree0_dev1_key, tree0_dev3_key, true);
    util_chosenlink_link_devices(tree0_dev2_key, tree0_dev4_key, true);
}


void test_rpc_can_create_tree0(GMAPS_UNUSED void** state) {
    s_create_tree0();

    // TODO: set alternative when implemented alternative devices
}

/**
 * This helper function creates device tree 1.
 * The tree has the following topology.
 * Note that there is a recursive device tree relationship between devices 2 and 4.
 *
 *                      == DEVICE TREE 1 ==
 *
 *                          +-------+
 *                          | dev 0 |
 *                          +-------+
 *                              |
 *                          +-------+
 *                          | dev 1 |
 *                +------+  +-------+
 *                |      |  |       |
 *                |    +-------+ +-------+
 *                |    | dev 2 | | dev 3 |
 *                |    +-------+ +-------+
 *                |         |
 *                |    +-------+
 *                |    | dev 4 |
 *                |    +-------+
 *                |         |
 *                +---------+
 */
static void s_create_tree1() {
    util_create_device(tree1_dev0_key, tree1_ds_tree, tree1_dev0_tag, false, "", amxd_status_ok, NULL);
    util_create_device(tree1_dev1_key, tree1_ds_tree, tree1_dev1_tag, false, "", amxd_status_ok, NULL);
    util_create_device(tree1_dev2_key, tree1_ds_tree, tree1_dev2_tag, false, "", amxd_status_ok, NULL);
    util_create_device(tree1_dev3_key, tree1_ds_tree, tree1_dev3_tag, false, "", amxd_status_ok, NULL);
    util_create_device(tree1_dev4_key, tree1_ds_tree, tree1_dev4_tag, false, "", amxd_status_ok, NULL);

    util_chosenlink_link_devices(tree1_dev0_key, tree1_dev1_key, true);
    util_chosenlink_link_devices(tree1_dev1_key, tree1_dev2_key, true);
    util_chosenlink_link_devices(tree1_dev1_key, tree1_dev3_key, true);
    util_chosenlink_link_devices(tree1_dev2_key, tree1_dev4_key, true);
    util_chosenlink_link_devices(tree1_dev4_key, tree1_dev2_key, true);
}

void test_rpc_can_create_tree1(GMAPS_UNUSED void** state) {
    s_create_tree1();
}

/**
 * Test traverse where:
 *  -starting from dev0
 *  -all devices match the expression
 *  -in all traverse modes
 *  -callback returns successful
 *
 * We record the callback occurences and compare them to the expected occurences.
 */
void test_internal_traverse_tree0_dev0_all_match(GMAPS_UNUSED void** state) {
    s_create_tree0();
    amxd_object_t* dev0 = gmaps_get_device(tree0_dev0_key);
    amxc_llist_t* cb_list_expected = NULL;

    for(uint32_t i = 0; i < gmap_traverse_max; ++i) {
        printf("== Testing traverse mode %d ==\n", i);
        amxc_llist_init(&cb_list);
        cb_list_expected = get_expected_cb_list_tree0_dev0_all_match(i, dev0);

        assert_int_equal(gmaps_traverse_tree(dev0, i, "", cb_add, dev0),
                         gmap_traverse_done);

        assert_cb_seq_same(cb_list_expected, &cb_list);

        amxc_llist_clean(&cb_list, free_cb_data);
        amxc_llist_clean(cb_list_expected, free_cb_data);
        free(cb_list_expected);
        cb_list_expected = NULL;
    }
}

/**
 * Test traverse where:
 *  -starting from dev0
 *  -all devices match the expression
 *  -in all traverse modes
 *  -callback returns gmap_traverse_no_recurse to stop the traverse early at callback 6
 *
 * We record the callback occurences and compare them to the expected occurences.
 */
void test_internal_traverse_tree0_dev0_all_match_early_stop(GMAPS_UNUSED void** state) {
    s_create_tree0();
    amxd_object_t* dev0 = gmaps_get_device(tree0_dev0_key);
    amxc_llist_t* cb_list_expected = NULL;

    for(uint32_t i = 0; i < gmap_traverse_max; ++i) {
        printf("== Testing traverse mode %d ==\n", i);
        amxc_llist_init(&cb_list);
        cb_list_expected = get_expected_cb_list_tree0_dev0_all_match_early_stop(i, dev0);
        callback_counter = 6;

        assert_int_equal(gmaps_traverse_tree(dev0, i, "", cb_add_no_recurse, dev0),
                         (callback_counter == 0) ? gmap_traverse_no_recurse : gmap_traverse_done);

        assert_cb_seq_same(cb_list_expected, &cb_list);

        amxc_llist_clean(&cb_list, free_cb_data);
        amxc_llist_clean(cb_list_expected, free_cb_data);
        free(cb_list_expected);
        cb_list_expected = NULL;
    }
}

void test_internal_traverse_tree0_dev0_this_early_stop(GMAPS_UNUSED void** state) {
    s_create_tree0();
    amxd_object_t* dev0 = gmaps_get_device(tree0_dev0_key);
    amxc_llist_t* cb_list_expected = NULL;

    amxc_llist_init(&cb_list);
    cb_list_expected = get_expected_cb_list_tree0_dev0_this_early_stop(dev0);
    callback_counter = 2;

    assert_int_equal(gmaps_traverse_tree(dev0, gmap_traverse_this, "", cb_add_no_recurse, dev0),
                     gmap_traverse_done);

    assert_cb_seq_same(cb_list_expected, &cb_list);

    amxc_llist_clean(&cb_list, free_cb_data);
    amxc_llist_clean(cb_list_expected, free_cb_data);
    free(cb_list_expected);
    cb_list_expected = NULL;
}

void test_internal_traverse_tree0_dev0_early_stop_at_push_cb(GMAPS_UNUSED void** state) {
    s_create_tree0();
    amxd_object_t* dev0 = gmaps_get_device(tree0_dev0_key);
    amxc_llist_t* cb_list_expected = NULL;

    amxc_llist_init(&cb_list);
    cb_list_expected = get_expected_cb_list_tree0_dev0_early_stop_at_push_cb(dev0);
    callback_counter = 3;

    assert_int_equal(gmaps_traverse_tree(dev0, gmap_traverse_down, "", cb_add_no_recurse, dev0),
                     gmap_traverse_done);

    assert_cb_seq_same(cb_list_expected, &cb_list);

    amxc_llist_clean(&cb_list, free_cb_data);
    amxc_llist_clean(cb_list_expected, free_cb_data);
    free(cb_list_expected);
    cb_list_expected = NULL;
}

void test_internal_traverse_tree1_recursive(GMAPS_UNUSED void** state) {
    s_create_tree1();
    amxd_object_t* dev0 = gmaps_get_device(tree1_dev0_key);

    amxc_llist_init(&cb_list);

    assert_int_equal(gmaps_traverse_tree(dev0, gmap_traverse_down, "", cb_add, dev0), gmap_traverse_failed);

    amxc_llist_clean(&cb_list, free_cb_data);
}

void test_internal_traverse_input_validation(GMAPS_UNUSED void** state) {
    s_create_tree0();
    amxd_object_t* dev0 = gmaps_get_device(tree0_dev0_key);


    assert_int_equal(gmaps_traverse_tree(NULL, gmap_traverse_this, "", cb_add, dev0), gmap_traverse_failed);
    assert_int_equal(gmaps_traverse_tree(dev0, gmap_traverse_max, "", cb_add, dev0), gmap_traverse_failed);
    assert_int_equal(gmaps_traverse_tree(dev0, gmap_traverse_this, NULL, cb_add, dev0), gmap_traverse_failed);
    assert_int_equal(gmaps_traverse_tree(dev0, gmap_traverse_this, "", NULL, dev0), gmap_traverse_failed);
}

void test_internal_traverse_mode_string_conversions(GMAPS_UNUSED void** state) {
    // Test string to mode conversions
    assert_int_equal(gmap_traverse_mode(GMAP_TRAVERSE_THIS_STR), gmap_traverse_this);
    assert_int_equal(gmap_traverse_mode(GMAP_TRAVERSE_DOWN_STR), gmap_traverse_down);
    assert_int_equal(gmap_traverse_mode(GMAP_TRAVERSE_UP_STR), gmap_traverse_up);
    assert_int_equal(gmap_traverse_mode(GMAP_TRAVERSE_DOWN_EXCLUSIVE_STR), gmap_traverse_down_exclusive);
    assert_int_equal(gmap_traverse_mode(GMAP_TRAVERSE_UP_EXCLUSIVE_STR), gmap_traverse_up_exclusive);
    assert_int_equal(gmap_traverse_mode(GMAP_TRAVERSE_ONE_DOWN_STR), gmap_traverse_one_down);
    assert_int_equal(gmap_traverse_mode(GMAP_TRAVERSE_ONE_UP_STR), gmap_traverse_one_up);
    assert_int_equal(gmap_traverse_mode(GMAP_TRAVERSE_ONE_DOWN_EXCLUSIVE_STR), gmap_traverse_one_down_exclusive);
    assert_int_equal(gmap_traverse_mode(GMAP_TRAVERSE_ONE_UP_EXCLUSIVE_STR), gmap_traverse_one_up_exclusive);

    // test mode to string conversions
    assert_string_equal(GMAP_TRAVERSE_THIS_STR, gmap_traverse_string(gmap_traverse_this));
    assert_string_equal(GMAP_TRAVERSE_DOWN_STR, gmap_traverse_string(gmap_traverse_down));
    assert_string_equal(GMAP_TRAVERSE_UP_STR, gmap_traverse_string(gmap_traverse_up));
    assert_string_equal(GMAP_TRAVERSE_DOWN_EXCLUSIVE_STR, gmap_traverse_string(gmap_traverse_down_exclusive));
    assert_string_equal(GMAP_TRAVERSE_UP_EXCLUSIVE_STR, gmap_traverse_string(gmap_traverse_up_exclusive));
    assert_string_equal(GMAP_TRAVERSE_ONE_DOWN_STR, gmap_traverse_string(gmap_traverse_one_down));
    assert_string_equal(GMAP_TRAVERSE_ONE_UP_STR, gmap_traverse_string(gmap_traverse_one_up));
    assert_string_equal(GMAP_TRAVERSE_ONE_DOWN_EXCLUSIVE_STR, gmap_traverse_string(gmap_traverse_one_down_exclusive));
    assert_string_equal(GMAP_TRAVERSE_ONE_UP_EXCLUSIVE_STR, gmap_traverse_string(gmap_traverse_one_up_exclusive));

}

