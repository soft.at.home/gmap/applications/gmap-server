/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "test_config.h"
#include "gmaps_dm_config.h"
#include "../test_common/test_util.h"
#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>
#include <amxut/amxut_timer.h>


// Avoid "Process terminating with default action of signal 14 (SIGALRM)"
static void setup_timer(void) {
    sigset_t mask;
    sigemptyset(&mask);
    sigaddset(&mask, SIGALRM);

    sigprocmask(SIG_BLOCK, &mask, NULL);
}

int test_gmap_setup(void** state) {
    amxd_object_t* root_obj = NULL;

    amxut_bus_setup(state);

    root_obj = amxd_dm_get_root(amxut_bus_dm());
    assert_ptr_not_equal(root_obj, NULL);

    amxo_resolver_ftab_add(amxut_bus_parser(), "create", AMXO_FUNC(_Config_create));
    amxo_resolver_ftab_add(amxut_bus_parser(), "save", AMXO_FUNC(_Config_save));
    amxo_resolver_ftab_add(amxut_bus_parser(), "set", AMXO_FUNC(_Config_set));
    amxo_resolver_ftab_add(amxut_bus_parser(), "get", AMXO_FUNC(_Config_get));

    amxo_resolver_ftab_add(amxut_bus_parser(), "createDevice", AMXO_FUNC(_Devices_createDevice));
    amxo_resolver_ftab_add(amxut_bus_parser(), "setActive", AMXO_FUNC(_Device_setActive));

    amxo_resolver_ftab_add(amxut_bus_parser(), "scanMibDir", AMXO_FUNC(_Config_scanMibDir));
    amxo_resolver_ftab_add(amxut_bus_parser(), "load", AMXO_FUNC(_Config_load));

    amxut_dm_load_odl("%s", ODL_DEFS);
    amxut_dm_load_odl("%s", ODL_TEST_CONFIG);
    amxut_dm_load_odl("%s", ODL_NTP_DEFINITION);
    amxut_dm_load_odl("%s", ODL_NTP_SYNCRONIZED);
    assert_int_equal(system("mkdir -p /tmp/gmap-server/"), 0);
    amxp_sigmngr_add_signal(NULL, "connection-added");
    amxp_sigmngr_add_signal(NULL, "listen-added");
    amxp_sigmngr_add_signal(NULL, "connection-deleted");

    assert_non_null(amxb_be_who_has("Time."));

    setup_timer();

    gmap_server_init(amxut_bus_dm(), amxut_bus_parser());
    _gmap_server_start(NULL, NULL, NULL);

    /* Allow NTP time synchronization to settle. */
    amxut_timer_go_to_future_ms(30000);

    test_util_scanMibDir("../test_common/mibs");

    return 0;
}

int test_gmap_teardown(void** state) {
    gmap_server_cleanup();

    amxut_bus_teardown(state);

    return 0;
}
