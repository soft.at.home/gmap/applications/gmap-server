/* No expression needed */

/**
 * MIB is loaded on all network devices that have a DHCP ipv4 address assigned
 *
 * All devices matching expression: "dhcp" are extended with this MIB
 *
 * @version 1.0
 */
 mib dhcp {
   /**
   * Vendor class ID (dhcp option 60)
   *
   * @version 1.0
   */
   %read-only %persistent string VendorClassID;
   /**
   * User class ID (dhcp option 77)
   *
   * @version 1.0
   */
   %read-only %persistent string UserClassID;
   /**
   * DHCP client id
   *
   * @version 1.0
   */
   %read-only %persistent string ClientID;
   /**
   * Serial number (dhcp option 125)
   *
   * When option 125 is available for the device, it is a managed device (see TR-111)
   *
   * @version 1.0
   */
   %read-only %persistent string SerialNumber {
      on action validate call check_maximum_length 64;
   }
   /**
   * Product class (dhcp option 125)
   *
   * When option 125 is available for the device, it is a managed device (see TR-111)
   *
   * @version 1.0
   */
   %read-only %persistent string ProductClass {
      on action validate call check_maximum_length 64;
   }
   /**
   * Vendor OUI (dhcp option 125)
   *
   * When option 125 is available for the device, it is a managed device (see TR-111)
   *
   * @version 1.0
   */
   %read-only %persistent string OUI {
      on action validate call check_maximum_length 64;
   }
   /**
   * DHCP option 55 values (dhcp option 55)
   *
   * @version V9.2
   */
   %read-only %persistent string DHCPOption55;
   
   /**
   * Path to DHCPv4 server's client entry
   *
   * Path of the form "Device.DHCPv4.Server.Pool.X.Client.Y." (where X and Y are instance names
   * or indices). Can also be empty. If non-empty, the path always starts with "Device.".
   * 
   * In case there is no DHCP lease but information was learned from past a past DHCP lease,
   * it is possible this path is empty while other fields are filled in.
   */
   %read-only string DHCPv4Client;
   
   /**
   * Path to DHCPv6 server's client entry
   *
   * Same as DHCPv4Client but for DHCPv6 (paths are of the form
   * "Device.DHCPv6.Server.Pool.X.Client.Y.")
   */
   %read-only string DHCPv6Client;
}
