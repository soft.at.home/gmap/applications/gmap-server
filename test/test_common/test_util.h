/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__GMAP_TEST_UTIL_H__)
#define __GMAP_TEST_UTIL_H__

#include <amxc/amxc.h>

#include <amxp/amxp_signal.h>
#include <amxp/amxp_slot.h>
#include <amxp/amxp_expression.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_expression.h>
#include <amxd/amxd_transaction.h>

#include <amxb/amxb.h>

#include <amxo/amxo.h>
#include <amxo/amxo_mibs.h>
#include <amxo/amxo_save.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#define SPECIALCHARS "']"
#define INJECTIONATTACK "\" || \"a\" == \".dot."

#define TESTKEY "testkey" SPECIALCHARS INJECTIONATTACK
#define TEST2KEY "test2Key" SPECIALCHARS INJECTIONATTACK
#define TEST3KEY "test3Key" SPECIALCHARS INJECTIONATTACK

#define ODL_DEFS "../../odl/gmap-server-definitions.odl"
#define ODL_TEST_CONFIG "../gmap-test-defaults.odl"
#define ODL_NTP_DEFINITION "../test_common/time-definition.odl"
#define ODL_NTP_SYNCRONIZED "../test_common/time-synchronized.odl"
#define ODL_DHCP_CLIENT "../test_common/dhcp-manager-server-pool.odl"

void util_create_device(const char* const key,
                        const char* const discovery_source,
                        const char* const tags,
                        bool persistent,
                        const char* const default_name,
                        amxd_status_t expected_status, amxc_var_t* values);
void util_create_device_no_tag_comparison(const char* const key,
                                          const char* const discovery_source,
                                          const char* const tags,
                                          bool persistent,
                                          const char* const default_name,
                                          amxd_status_t expected_status, amxc_var_t* values);
void util_destroy_device(const char* const key);
void test_util_scanMibDir(const char* path);
void util_add_string_param_to_obj(amxd_object_t* object, const char* param_name, const char* value);
void handle_events(void);
amxd_status_t __wrap_cleanClientLeases(amxd_object_t* object,
                                       amxd_function_t* func,
                                       amxc_var_t* args,
                                       amxc_var_t* ret);
int test_util_check_mac_list(LargestIntegralType value, LargestIntegralType check_value_data);

bool util_is_linked_to_for_datasource(const char* upper_dev, const char* lower_dev, const char* datasource);
bool util_is_link_selected(const char* upper_dev, const char* lower_dev);
bool util_is_linked_to(const char* upper_dev, const char* lower_dev, const char* datasource);

int util_setup(void** state);
int util_teardown(void** state);

void util_setup_resolver(void);

void util_call_linkRemove(const char* upper_device, const char* lower_device, const char* datasource, bool expect_success);
void util_call_linkReplace(const char* upper_device, const char* lower_device, const char* datasource, const char* type, bool expect_success);
void util_call_linkAdd(const char* upper_device, const char* lower_device, const char* datasource, const char* type, bool expect_success);

#endif
