/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include <stdbool.h>
#include <string.h>
#include <stdio.h>

#include "test_gmap_active.h"
#include "../test_common/test_util.h"
#include "../test_common/test_common.h"
#include "gmaps_device_active.h"
#include <debug/sahtrace.h>

static void s_util_set_active_rpc(const char* key,
                                  bool source_active,
                                  bool use_default_active,
                                  const char* source,
                                  bool use_default_source,
                                  uint32_t priority,
                                  bool use_default_priority,
                                  bool expect_ok,
                                  bool expect_device_active) {
    amxd_object_t* device = gmaps_get_device(key);
    amxc_var_t args;
    amxc_var_t ret;
    bool actual_device_active = false;
    amxd_object_t* active_obj = NULL;
    amxd_status_t status = amxd_status_invalid_action;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_ptr_not_equal(device, NULL);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    if(!use_default_active) {
        amxc_var_add_key(bool, &args, "active", source_active);
    } else {
        assert_true(source_active);
    }

    if(!use_default_source) {
        amxc_var_add_key(cstring_t, &args, "source", source);
    }

    if(!use_default_priority) {
        amxc_var_add_key(uint32_t, &args, "priority", priority);
    }


    status = amxd_object_invoke_function(device,
                                         "setActive",
                                         &args,
                                         &ret);
    if(expect_ok) {
        assert_int_equal(amxd_status_ok, status);
    } else {
        assert_int_not_equal(amxd_status_ok, status);
    }

    // readback, verify
    actual_device_active = amxd_object_get_value(bool, device, "Active", NULL);
    assert_true(actual_device_active == expect_device_active);
    if(expect_ok) {
        active_obj = amxd_object_get_instance(amxd_object_get(device, "Actives"), source, 0);
        assert_non_null(active_obj);
        assert_int_equal(priority, amxd_object_get_uint32_t(active_obj, "Priority", NULL));
        assert_true(source_active == amxd_object_get_bool(active_obj, "Active", NULL));
    }

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_set_active_false_when_no_sources(GMAPS_UNUSED void** state) {
    amxd_status_t status = amxd_status_unknown_error;
    // GIVEN a gmap-server
    // (done by test-setup)

    // WHEN creating a device
    util_create_device("mydev", "mydisco", "mytags", false, "mydefaultname", amxd_status_ok, NULL);

    // THEN the device is inactive
    assert_false(amxd_object_get_bool(gmaps_get_device("mydev"), "Active", &status));
    assert_int_equal(amxd_status_ok, status);
}

static void s_assert_active_entry(const char* dev_key, const char* source,
                                  uint32_t expected_priority, bool expected_active) {

    amxd_status_t status = amxd_status_unknown_error;

    amxd_object_t* dev_obj = gmaps_get_device(dev_key);
    amxd_object_t* actives_templ = amxd_object_get(dev_obj, "Actives");
    assert_non_null(actives_templ);
    amxd_object_t* active_obj = amxd_object_get_instance(actives_templ, source, 0);
    assert_non_null(active_obj);

    assert_true(amxd_object_get_bool(active_obj, "Active", &status) == expected_active);
    assert_int_equal(amxd_status_ok, status);
    assert_int_equal(amxd_object_get_uint32_t(active_obj, "Priority", &status), expected_priority);
    assert_int_equal(amxd_status_ok, status);
}

void test_set_active__first_inactive(GMAPS_UNUSED void** state) {
    amxd_status_t status = amxd_status_unknown_error;
    // GIVEN a gmap-server with a device
    util_create_device("mydev", "mydisco", "mytags", false, "mydefaultname", amxd_status_ok, NULL);

    // WHEN setting the device inactive
    assert_int_equal(gmaps_device_set_active("mydev", false, "mysource", 10), gmap_status_ok);

    // THEN the device is inactive
    assert_false(amxd_object_get_bool(gmaps_get_device("mydev"), "Active", &status));
    // THEN the source is recorded
    s_assert_active_entry("mydev", "mysource", 10, false);
}

void test_set_active__first_active(GMAPS_UNUSED void** state) {
    amxd_status_t status = amxd_status_unknown_error;
    // GIVEN a gmap-server with a device
    util_create_device("mydev", "mydisco", "mytags", false, "mydefaultname", amxd_status_ok, NULL);

    // WHEN setting the device active
    assert_int_equal(gmaps_device_set_active("mydev", true, "mysource", 11), gmap_status_ok);

    // THEN the device is active
    assert_true(amxd_object_get_bool(gmaps_get_device("mydev"), "Active", &status));
    assert_int_equal(amxd_status_ok, status);
    // THEN the source is recorded
    s_assert_active_entry("mydev", "mysource", 11, true);
}

void test_set_active_rpc__invalid_argument(GMAPS_UNUSED void** state) {
    amxd_status_t status = amxd_status_unknown_error;
    util_create_device("mydev", "mydisco", "mytags", false, "mydefaultname", amxd_status_ok, NULL);

    // empty source:
    s_util_set_active_rpc("mydev", true, false, "", false, 10, false, false, false);
    // priority < min:
    s_util_set_active_rpc("mydev", true, false, "mysource", false, 0, false, false, false);
    // priority > max:
    s_util_set_active_rpc("mydev", true, false, "mysource", false, 101, false, false, false);

    // none of the failed rpc calls actually changed the status:
    assert_false(amxd_object_get_bool(gmaps_get_device("mydev"), "Active", &status));
    assert_int_equal(amxd_status_ok, status);
}

void test_set_active_rpc__normal_case(GMAPS_UNUSED void** state) {
    amxd_status_t status = amxd_status_unknown_error;
    // GIVEN a device
    util_create_device("mydev", "mydisco", "mytags", false, "mydefaultname", amxd_status_ok, NULL);

    // WHEN calling setActive, through RPC/bus
    s_util_set_active_rpc("mydev", true, false, "mysource", false, 10, false, true, true);

    // THEN the call went through
    assert_true(amxd_object_get_bool(gmaps_get_device("mydev"), "Active", &status));
    assert_int_equal(amxd_status_ok, status);
}

typedef struct {
    const char* source;
    uint32_t priority;
    bool active;
} test_data_t;

static void s_testhelper(bool expected_active, size_t count, test_data_t* data) {
    amxd_status_t status = amxd_status_unknown_error;
    util_create_device("mydev", "mydisco", "mytags", false, "mydefaultname", amxd_status_ok, NULL);

    for(size_t i = 0; i < count; i++) {
        assert_int_equal(gmaps_device_set_active("mydev", data[i].active, data[i].source, data[i].priority), gmap_status_ok);
    }

    assert_true(expected_active == amxd_object_get_bool(gmaps_get_device("mydev"), "Active", &status));
    assert_int_equal(amxd_status_ok, status);
}

void test_set_active__new_with_higher_priority(GMAPS_UNUSED void** state) {
    s_testhelper(false, 2, (test_data_t[]) {{"s1", 2, true}, {"s2", 1, false}});
}

void test_set_active__new_with_lower_priority(GMAPS_UNUSED void** state) {
    s_testhelper(false, 3, (test_data_t[]) {{"s1", 10, true}, {"s2", 4, false}, {"s3", 11, true}});
}

void test_set_active__change_not_longer_highest_priority(GMAPS_UNUSED void** state) {
    s_testhelper(false, 3, (test_data_t[]) {{"s1", 20, false}, {"s2", 10, true}, {"s2", 25, true}});
}

void test_set_active__change_becomes_highest_priority(GMAPS_UNUSED void** state) {
    s_testhelper(true, 3, (test_data_t[]) {{"s1", 20, true}, {"s2", 10, false}, {"s1", 9, true}});
}

void test_set_active__multiple_highest_priority_all_true(GMAPS_UNUSED void** state) {
    s_testhelper(true, 3, (test_data_t[]) {{"s1", 20, false}, {"s2", 10, true}, {"s2", 10, true}});
}

void test_set_active__multiple_highest_priority_some_true(GMAPS_UNUSED void** state) {
    s_testhelper(false, 3, (test_data_t[]) {{"s1", 20, false}, {"s2", 10, false}, {"s3", 10, true}});
}

void test_set_active__multiple_highest_priority_none_true(GMAPS_UNUSED void** state) {
    s_testhelper(false, 4, (test_data_t[]) {{"s1", 20, false}, {"s2", 10, false}, {"s3", 10, false}, {"s4", 11, true} });
}

void test_set_active__multiple_highest_priority_all_become_true(GMAPS_UNUSED void** state) {
    s_testhelper(true, 5, (test_data_t[]) {
        {"s1", 20, false},
        {"s2", 10, false},
        {"s3", 10, false},
        {"s2", 10, true},
        {"s3", 10, true}
    });
}