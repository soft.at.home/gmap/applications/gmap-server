/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include <stdbool.h>
#include <string.h>
#include <stdio.h>

#include "test_gmap_uuid.h"
#include "../test_common/test_util.h"
#include "../test_common/test_common.h"
#include "gmaps_uuid.h"
#include "gmaps_dm_devices.h"
#include <debug/sahtrace.h>

static bool s_is_hex_char(char c) {
    return (c >= '0' && c <= '9') || (c >= 'a' && c <= 'f');
}

static bool s_is_hex_str(const char* c, size_t len) {
    size_t i = 0;
    for(i = 0; i < len; i++) {
        if(!s_is_hex_char(c[i])) {
            return false;
        }
    }
    return true;
}

static void s_assert_valid_uuid_key(const char* key) {
    // format like for example:
    //        "ID-448513c1-5193-4c03-82a3-9c040f3fb43a"
    //  index: 0123456789 123456789 123456789 12345678
    //                   1         2         3
    assert_non_null(key);
    assert_int_equal(strlen(key), 39);
    assert_true(key[0] == 'I');
    assert_true(key[1] == 'D');
    assert_true(key[2] == '-');
    assert_true(s_is_hex_str(&key[3], 8));
    assert_true(key[11] == '-');
    assert_true(s_is_hex_str(&key[12], 4));
    assert_true(key[16] == '-');
    assert_true(s_is_hex_str(&key[17], 4));
    assert_true(key[21] == '-');
    assert_true(s_is_hex_str(&key[22], 4));
    assert_true(key[26] == '-');
    assert_true(s_is_hex_str(&key[27], 12));
}

void test_generate_device_key(GMAPS_UNUSED void** state) {
    // GIVEN nothing

    // WHEN generating a new key
    char* key = gmaps_uuid_generate_key();

    // THEN the key is in expected format
    s_assert_valid_uuid_key(key);

    free(key);
}

void test_get_key_by_mac__found(GMAPS_UNUSED void** state) {
    // GIVEN a gmap-server with a device with a mac
    amxc_var_t params;
    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &params, "PhysAddress", "a1:b1:C3:e4:f5:06");
    util_create_device("mac-found", "test_devices_uuid", "mac",
                       false, "badDevice", amxd_status_ok, &params);

    // WHEN asking this device by its mac (of course with different casing)
    const char* key = gmaps_uuid_get_key_by_mac("a1:b1:c3:E4:f5:06");

    // THEN the device was found
    assert_non_null(key);
    assert_string_equal("mac-found", key);

    amxc_var_clean(&params);
}

void test_get_key_by_mac__interface(GMAPS_UNUSED void** state) {
    // GIVEN a gmap-server with a device with a mac
    amxc_var_t params;
    amxc_var_t* physaddress = NULL;
    const char* key = NULL;
    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &params, "PhysAddress", "");
    physaddress = amxc_var_get_key(&params, "PhysAddress", AMXC_VAR_FLAG_DEFAULT);

    /* Non-interface comes first */
    amxc_var_set_cstring_t(physaddress, "a1:b1:C3:e4:f5:06");
    util_create_device("mac-00", "test_devices_uuid", "mac",
                       false, "macDevice00", amxd_status_ok, &params);

    amxc_var_set_cstring_t(physaddress, "a1:b1:C3:e4:f5:06");
    util_create_device("mac-01", "test_devices_uuid", "mac interface",
                       false, "macDevice01", amxd_status_ok, &params);

    /* Interface comes first */
    amxc_var_set_cstring_t(physaddress, "a1:b1:C3:e4:f5:07");
    util_create_device("mac-10", "test_devices_uuid", "mac interface",
                       false, "macDevice10", amxd_status_ok, &params);

    amxc_var_set_cstring_t(physaddress, "a1:b1:C3:e4:f5:07");
    util_create_device("mac-11", "test_devices_uuid", "mac",
                       false, "macDevice11", amxd_status_ok, &params);

    /* Non-interface between interfaces */
    amxc_var_set_cstring_t(physaddress, "a1:b1:C3:e4:f5:08");
    util_create_device("mac-20", "test_devices_uuid", "mac interface",
                       false, "macDevice20", amxd_status_ok, &params);

    amxc_var_set_cstring_t(physaddress, "a1:b1:C3:e4:f5:08");
    util_create_device("mac-21", "test_devices_uuid", "mac",
                       false, "macDevice21", amxd_status_ok, &params);

    amxc_var_set_cstring_t(physaddress, "a1:b1:C3:e4:f5:08");
    util_create_device("mac-22", "test_devices_uuid", "mac interface",
                       false, "macDevice22", amxd_status_ok, &params);

    /* only interfaces */
    amxc_var_set_cstring_t(physaddress, "a1:b1:C3:e4:f5:09");
    util_create_device("mac-30", "test_devices_uuid", "mac interface",
                       false, "macDevice30", amxd_status_ok, &params);

    amxc_var_set_cstring_t(physaddress, "a1:b1:C3:e4:f5:09");
    util_create_device("mac-31", "test_devices_uuid", "mac interface",
                       false, "macDevice31", amxd_status_ok, &params);

    amxc_var_set_cstring_t(physaddress, "a1:b1:C3:e4:f5:09");
    util_create_device("mac-32", "test_devices_uuid", "mac interface",
                       false, "macDevice32", amxd_status_ok, &params);

    // WHEN asking this device by its mac (of course with different casing)
    key = gmaps_uuid_get_key_by_mac("a1:b1:c3:E4:f5:06");

    // THEN the device was found
    assert_non_null(key);
    assert_string_equal("mac-00", key);

    // WHEN asking this device by its mac (of course with different casing)
    key = gmaps_uuid_get_key_by_mac("a1:b1:c3:E4:f5:07");

    // THEN the device was found
    assert_non_null(key);
    assert_string_equal("mac-11", key);

    // WHEN asking this device by its mac (of course with different casing)
    key = gmaps_uuid_get_key_by_mac("a1:b1:c3:E4:f5:08");

    // THEN the device was found
    assert_non_null(key);
    assert_string_equal("mac-21", key);

    // WHEN asking this device by its mac, none is found (all interfaces)
    key = gmaps_uuid_get_key_by_mac("a1:b1:c3:E4:f5:09");

    // THEN no device was found
    assert_null(key);

    amxc_var_clean(&params);
}

void test_get_key_by_mac__not_found(GMAPS_UNUSED void** state) {
    // GIVEN a gmap-server

    // WHEN asking for a device key by a mac for which there is no device
    const char* key = gmaps_uuid_get_key_by_mac("11:22:33:44:55:66");

    // THEN no device was found
    assert_null(key);
}

void test_get_key_by_mac__not_a_mac(GMAPS_UNUSED void** state) {
    // GIVEN a gmap-server

    // WHEN asking for a device key by an invalid mac
    const char* key1 = gmaps_uuid_get_key_by_mac("hello");
    const char* key2 = gmaps_uuid_get_key_by_mac(NULL);
    const char* key3 = gmaps_uuid_get_key_by_mac("");
    const char* key4 = gmaps_uuid_get_key_by_mac("zz:zz:zz:zz:zz:zz");

    // THEN no device was found
    assert_null(key1);
    assert_null(key2);
    assert_null(key3);
    assert_null(key4);
}

/**
 * Whether parameter `param` of device `device_key` has value `expected_value`.
 */
static bool s_is_dev_param(const char* device_key, const char* param, const char* expected_value) {
    bool retval = false;
    amxc_var_t obj_params;
    amxd_status_t status;
    const char* actual_value = NULL;
    amxc_var_init(&obj_params);
    amxd_object_t* obj = gmaps_get_device(device_key);
    if(obj == NULL) {
        printf("%s: device %s not found\n", __FUNCTION__, device_key);
        goto exit;
    }
    status = amxd_object_get_params(obj, &obj_params, amxd_dm_access_protected);
    if(status != amxd_status_ok) {
        printf("%s: device %s does not have parameter %s\n", __FUNCTION__, device_key, param);
        goto exit;
    }

    actual_value = amxc_var_constcast(cstring_t, VARIANT_GET(&obj_params, param));
    if(actual_value == NULL) {
        printf("%s: parameter %s is not a string", __FUNCTION__, param);
        goto exit;
    }

    if(0 == strcmp(actual_value, expected_value)) {
        retval = true;
    } else {
        printf("%s: expected '%s' got '%s'", __FUNCTION__, expected_value, actual_value);
    }
exit:
    amxc_var_clean(&obj_params);

    return retval;
}

void test_new_device_or_get_key__new_device(GMAPS_UNUSED void** state) {
    gmap_status_t status = gmap_status_unknown_error;
    char* key = NULL;
    const char* key_fetched = NULL;
    bool already_exists = true;
    amxc_var_t input_params;
    amxc_var_t decoy_params;
    amxc_var_init(&input_params);
    amxc_var_init(&decoy_params);
    amxc_var_set_type(&input_params, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&decoy_params, AMXC_VAR_ID_HTABLE);

    // GIVEN gmap-server with a device
    amxc_var_add_key(cstring_t, &decoy_params, "PhysAddress", "11:22:33:44:55:66:76");
    util_create_device("decoydevice", "test_new_device_or_get_key", "mac",
                       false, "decoydevice", amxd_status_ok, &decoy_params);

    // WHEN adding a device with a different mac than the existing device
    amxc_var_add_key(cstring_t, &input_params, "TestField", __FUNCTION__); // field of "testmib" mib
    status = gmaps_uuid_new_device_or_get_key("11:22:33:44:55:77", "test-gmap-uuid",
                                              "mac testmib", false, NULL, &input_params, &key, &already_exists);

    // THEN a new uuid is generated, and a device is added by that uuid
    assert_int_equal(gmap_status_ok, status);
    assert_non_null(key);
    assert_string_not_equal("decoydevice", key);
    s_assert_valid_uuid_key(key);
    key_fetched = gmaps_uuid_get_key_by_mac("11:22:33:44:55:77");
    assert_string_equal(key, key_fetched);
    assert_false(already_exists);
    // AND THEN the implicit parameters are set
    assert_true(s_is_dev_param(key, "Key", key));
    assert_true(s_is_dev_param(key, "DiscoverySource", "test-gmap-uuid"));
    assert_true(s_is_dev_param(key, "Tags", "mac testmib"));
    assert_true(s_is_dev_param(key, "PhysAddress", "11:22:33:44:55:77"));
    // AND THEN the explicit parameters are set
    assert_true(s_is_dev_param(key, "TestField", __FUNCTION__));

    free(key);
    amxc_var_clean(&decoy_params);
    amxc_var_clean(&input_params);
}

void test_new_device_or_get_key__device_exists_with_an_uuid(GMAPS_UNUSED void** state) {
    gmap_status_t status = gmap_status_unknown_error;
    char* origkey = NULL;
    char* newkey = NULL;
    bool already_exists = false;
    amxc_var_t new_params;
    amxc_var_t orig_params;
    amxc_var_init(&new_params);
    amxc_var_init(&orig_params);
    amxc_var_set_type(&new_params, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&orig_params, AMXC_VAR_ID_HTABLE);

    // GIVEN gmap-server with a device with a uuid-based key
    amxc_var_add_key(cstring_t, &orig_params, "TestField", "should remain");
    status = gmaps_uuid_new_device_or_get_key("11:22:cD:44:55:74", "test_dev_exists_1",
                                              "mac testmib origtag", false, NULL, &orig_params, &origkey, &already_exists);
    assert_int_equal(gmap_status_ok, status);
    handle_events();

    // WHEN adding a device with that mac
    amxc_var_add_key(cstring_t, &new_params, "TestField", "should be ignored");
    status = gmaps_uuid_new_device_or_get_key("11:22:Cd:44:55:74", "test_dev_exists_2",
                                              "mac testmib changedtag", false, NULL, &new_params, &newkey, &already_exists);

    // THEN key of original device is returned
    assert_int_equal(gmap_status_ok, status);
    assert_non_null(newkey);
    assert_string_equal(origkey, newkey);
    assert_true(already_exists);
    // AND THEN the implicit parameters are unchanged
    assert_true(s_is_dev_param(newkey, "Key", origkey));
    assert_true(s_is_dev_param(newkey, "DiscoverySource", "test_dev_exists_1"));
    assert_true(s_is_dev_param(newkey, "Tags", "mac testmib origtag"));
    assert_true(s_is_dev_param(newkey, "PhysAddress", "11:22:cD:44:55:74"));
    // AND THEN the explicit parameters are ignored
    assert_true(s_is_dev_param(newkey, "TestField", "should remain"));

    free(origkey);
    free(newkey);
    amxc_var_clean(&orig_params);
    amxc_var_clean(&new_params);
}

void test_new_device_or_get_key__device_exists_without_uuid(GMAPS_UNUSED void** state) {
    gmap_status_t status = gmap_status_unknown_error;
    char* key = NULL;
    bool already_exists = false;
    const char* original_dev_key = "originaldevice.dot.]['!";
    amxc_var_t new_params;
    amxc_var_t original_params;
    amxc_var_init(&new_params);
    amxc_var_init(&original_params);
    amxc_var_set_type(&new_params, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&original_params, AMXC_VAR_ID_HTABLE);

    // GIVEN gmap-server with a device
    amxc_var_add_key(cstring_t, &original_params, "PhysAddress", "aB:22:33:44:55:75");
    amxc_var_add_key(cstring_t, &original_params, "TestField", "should remain");
    util_create_device(original_dev_key, "test_dev_exists_1", "mac testmib origtag",
                       false, "originaldevice", amxd_status_ok, &original_params);

    // WHEN adding a device with that mac
    amxc_var_add_key(cstring_t, &new_params, "TestField", "should be ignored");
    status = gmaps_uuid_new_device_or_get_key("Ab:22:33:44:55:75", "test_dev_exists_2",
                                              "mac testmib changedtag", false, NULL, &new_params, &key, &already_exists);

    // THEN key of original device is returned
    assert_int_equal(gmap_status_ok, status);
    assert_non_null(key);
    assert_string_equal(original_dev_key, key);
    assert_true(already_exists);
    // AND THEN the implicit parameters are unchanged
    assert_true(s_is_dev_param(key, "Key", original_dev_key));
    assert_true(s_is_dev_param(key, "DiscoverySource", "test_dev_exists_1"));
    assert_true(s_is_dev_param(key, "Tags", "mac testmib origtag"));
    assert_true(s_is_dev_param(key, "PhysAddress", "aB:22:33:44:55:75"));
    // AND THEN the explicit parameters are ignored
    assert_true(s_is_dev_param(key, "TestField", "should remain"));

    free(key);
    amxc_var_clean(&original_params);
    amxc_var_clean(&new_params);
}

static uint32_t s_nb_devices() {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxd_object_t* template_obj = amxd_object_findf(devices, "Device");
    return amxd_object_get_instance_count(template_obj);
}

static void s_testhelper_new_device_or_get_key(const char* mac, const char* mac_as_param, const char* flags, bool expect_success) {
    gmap_status_t status = gmap_status_unknown_error;
    char* key = NULL;
    amxc_var_t params;
    bool already_exists = false;
    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    uint32_t nb_devices_before = s_nb_devices();

    // GIVEN gmap-server without any device
    if(mac_as_param != NULL) {
        amxc_var_add_key(cstring_t, &params, "PhysAddress", mac_as_param);
    }
    // WHEN doing create-dev-or-get-key
    status = gmaps_uuid_new_device_or_get_key(mac,
                                              "testhelper_new_device_or_get_key", flags, false, NULL, &params, &key, &already_exists);

    if(expect_success) {
        // THEN it is not rejected (just a sanity check, proper device creation/lookup tests are elsewhere)
        assert_int_equal(gmap_status_ok, status);
        assert_non_null(key);
    } else {
        // THEN it is rejected
        assert_int_not_equal(gmap_status_ok, status);
        assert_null(key);
        // AND THEN no device is created
        assert_int_equal(nb_devices_before, s_nb_devices());
    }

    free(key);
    amxc_var_clean(&params);
}

void test_new_device_or_get_key__invalid_params(GMAPS_UNUSED void** state) {
    // Case: not a mac: too short
    s_testhelper_new_device_or_get_key("11:22:33:44:55", NULL, "mac testmib", false);
    // Case: not a mac: too long
    s_testhelper_new_device_or_get_key("11:22:33:44:55:66:77", NULL, "mac testmib", false);
    // Case: not a mac: invalid chars
    s_testhelper_new_device_or_get_key("11:22:33:44:55:6z", NULL, "mac testmib", false);
    // Case: not a mac: wrong separator
    s_testhelper_new_device_or_get_key("11_22_33_44_55_66", NULL, "mac testmib", false);
    // Case: not a mac: no separator
    s_testhelper_new_device_or_get_key("112233445566", NULL, "mac testmib", false);
    // Case: not a mac: NULL
    s_testhelper_new_device_or_get_key(NULL, NULL, "mac testmib", false);
    // Case: not a mac: empty
    s_testhelper_new_device_or_get_key("", NULL, "mac testmib", false);

    // Case: mac given as function parameter and given as object parameter disagree
    s_testhelper_new_device_or_get_key("11:22:33:44:55:66", "11:22:33:44:55:61", "mac testmib", false);

    // Case: flags do not contain "mac"
    s_testhelper_new_device_or_get_key("11:22:33:44:55:66", NULL, "nice weather", false);
    s_testhelper_new_device_or_get_key("11:22:33:44:55:66", NULL, "testmib", false);
    s_testhelper_new_device_or_get_key("11:22:33:44:55:66", NULL, "", false);
    s_testhelper_new_device_or_get_key("11:22:33:44:55:66", NULL, NULL, false);

    // Case: illegal flags combinations
    s_testhelper_new_device_or_get_key("11:22:33:44:55:66", "11:22:33:44:55:66", "mac self lan physical", false);

    // Just see if we can create a valid one as well so it's not some other reason why above pass.
    s_testhelper_new_device_or_get_key("11:22:33:44:55:66", "11:22:33:44:55:66", "mac testmib", true);
    s_testhelper_new_device_or_get_key("11:22:33:44:55:66", NULL, "blabla mac testmib", true);
}

static amxd_status_t s_call_rpc_create_device_or_get_key(const char* mac, const char* discovery_source,
                                                         const char* tags, bool persistent, const char* default_name,
                                                         amxc_htable_t* dev_params,
                                                         char** tgt_out_key, bool* tgt_out_already_exists, bool* tgt_out_retval) {
    amxd_object_t* devices = gmap_dm_get_devices();
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t func_params;
    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_init(&func_params);
    assert_non_null(devices);
    amxd_status_t status;
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    if(mac != NULL) {
        amxc_var_add_key(cstring_t, &args, "mac", mac);
    }
    if(discovery_source != NULL) {
        amxc_var_add_key(cstring_t, &args, "discovery_source", discovery_source);
    }
    if(tags != NULL) {
        amxc_var_add_key(cstring_t, &args, "tags", tags);
    }
    if(default_name != NULL) {
        amxc_var_add_key(cstring_t, &args, "default_name", default_name);
    }
    amxc_var_add_key(bool, &args, "persistent", persistent);
    if(dev_params != NULL) {
        amxc_var_add_key(amxc_htable_t, &args, "values", dev_params);
    }

    status = amxd_object_invoke_function(devices,
                                         "createDeviceOrGetKey",
                                         &args,
                                         &ret);

    assert_int_equal(AMXC_VAR_ID_BOOL, amxc_var_type_of(&ret));
    *tgt_out_retval = amxc_var_constcast(bool, &ret);

    *tgt_out_key = strdup(GET_CHAR(&args, "key"));
    *tgt_out_already_exists = GET_BOOL(&args, "already_exists");

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&func_params);
    return status;
}

void test_rpc_create_device_or_get_key__new_device(GMAPS_UNUSED void** state) {
    amxc_var_t dev_params;
    amxc_var_init(&dev_params);
    amxc_var_set_type(&dev_params, AMXC_VAR_ID_HTABLE);
    char* key = NULL;
    const char* key_fetched;
    bool already_exists = false;
    bool rpc_retval = false;
    amxd_status_t status = amxd_status_unknown_error;

    // GIVEN gmap-server

    // WHEN calling the RPC function to create a device with an autogenerated key
    status = s_call_rpc_create_device_or_get_key("11:22:33:66:55:ab", "rpc-create-test", "mac testmib bla", true,
                                                 "defName", NULL, &key, &already_exists, &rpc_retval);
    handle_events();

    // THEN the RPC<->C glue called the C function,
    // so output parameters of the RPC function are set
    assert_int_equal(amxd_status_ok, status);
    assert_false(already_exists);
    assert_true(rpc_retval);
    assert_non_null(key);
    s_assert_valid_uuid_key(key);
    // and the the actual device creation happened
    assert_true(s_is_dev_param(key, "Key", key));
    assert_true(s_is_dev_param(key, "DiscoverySource", "rpc-create-test"));
    assert_true(s_is_dev_param(key, "Tags", "mac testmib bla"));
    assert_true(s_is_dev_param(key, "PhysAddress", "11:22:33:66:55:ab"));
    key_fetched = gmaps_uuid_get_key_by_mac("11:22:33:66:55:aB");
    assert_string_equal(key, key_fetched);

    free(key);
    amxc_var_clean(&dev_params);
}
