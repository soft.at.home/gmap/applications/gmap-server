/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include <stdbool.h>
#include <string.h>
#include <stdio.h>

#include "test_gmap_topologybuilder.h"
#include "../test_common/test_util.h"
#include "../test_common/test_common.h"
#include "gmaps_topologybuilder.h"
#include "gmaps_devices_multilink.h"
#include "gmaps_dm_devices.h"
#include <debug/sahtrace.h>

void __wrap_gmaps_dm_tpb_on_link_selected(gmaps_tpb_edge_t* edge);
void __wrap_gmaps_dm_tpb_on_no_link_selected(gmaps_tpb_node_t* node);
void __wrap_gmaps_dm_tpb_on_node_delete(gmaps_tpb_node_t* node);
void __wrap_gmaps_dm_tpb_on_edge_delete(gmaps_tpb_edge_t* edge);

void __wrap_gmaps_dm_tpb_on_link_selected(gmaps_tpb_edge_t* edge) {
    check_expected(edge);
}

void __wrap_gmaps_dm_tpb_on_no_link_selected(gmaps_tpb_node_t* node) {
    check_expected(node);
}

void __wrap_gmaps_dm_tpb_on_node_delete(gmaps_tpb_node_t* node) {
    check_expected(node);
}

void __wrap_gmaps_dm_tpb_on_edge_delete(gmaps_tpb_edge_t* edge) {
    check_expected(edge);

    // challenge that we can access inspectors (i.e. no dangling pointers),
    // even though you normally don't need that.
    if(gmaps_tpb_edge_get_lower_node(edge) != NULL) {
        gmaps_tpb_node_userdata(gmaps_tpb_edge_get_lower_node(edge));
    }
    if(gmaps_tpb_edge_get_upper_node(edge) != NULL) {
        gmaps_tpb_node_userdata(gmaps_tpb_edge_get_upper_node(edge));
    }
}

void test_gmaps_tpb_node_new__normalcase(UNUSED void** state) {
    // GIVEN a topologybuilder
    gmaps_tpb_t* tpb = gmaps_tpb_new();
    int x = 123;

    // WHEN creating a new node
    gmaps_tpb_node_t* node = gmaps_tpb_node_new(tpb, &x);

    // THEN the node exists
    assert_non_null(node);
    // AND THEN the node knows its given userdata.
    assert_ptr_equal(&x, gmaps_tpb_node_userdata(node));

    // Cleanup:
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node);
    gmaps_tpb_delete(&tpb);
}

void test_gmaps_tpb_node_new__invalid_arg(UNUSED void** state) {
    int x = 123;
    gmaps_tpb_node_t* node = gmaps_tpb_node_new(NULL, &x);
    assert_null(node);
}


void test_gmaps_tpb_node_delete(UNUSED void** state) {
    // GIVEN tho following graph
    //     ┌──────────────────────────┐
    //     │                          │
    //     │                          │
    //     │                          ▼
    //  ┌──┴──┐       ┌─────┐      ┌─────┐
    //  │Node1├──────►│Node2├─────►│Node3│
    //  └──┬──┘       └──┬─┬┘      └─────┘
    //     │             │ │          ▲
    //     │             │ └──────────┘
    //     │             │
    //     │             ▼
    //     │          ┌─────┐
    //     └─────────►│Node4│
    //                └─────┘
    gmaps_tpb_t* tpb = gmaps_tpb_new();
    gmaps_tpb_node_t* node1 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_node_t* node2 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_node_t* node3 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_node_t* node4 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_edge_t* edge12 = gmaps_tpb_edge_new(tpb, node1, node2, NULL);
    gmaps_tpb_edge_t* edge23 = gmaps_tpb_edge_new(tpb, node2, node3, NULL);
    gmaps_tpb_edge_t* edge23b = gmaps_tpb_edge_new(tpb, node2, node3, NULL);
    gmaps_tpb_edge_t* edge13 = gmaps_tpb_edge_new(tpb, node1, node3, NULL);
    gmaps_tpb_edge_t* edge14 = gmaps_tpb_edge_new(tpb, node1, node4, NULL);
    gmaps_tpb_edge_t* edge24 = gmaps_tpb_edge_new(tpb, node2, node4, NULL);

    // EXPECT the edges to/from node2 will be deleted
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge12);
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge23);
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge23b);
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge24);
    // EXPECT the node2 to be deleted
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node2);

    // WHEN deleting node2
    gmaps_tpb_node_delete(&node2);

    // cleanup:
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node1);
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node3);
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node4);
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge13);
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge14);
    gmaps_tpb_delete(&tpb);
}

void test_gmaps_tpb_edge_delete(UNUSED void** state) {
    // GIVEN an edge
    gmaps_tpb_t* tpb = gmaps_tpb_new();
    gmaps_tpb_node_t* node1 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_node_t* node2 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_edge_t* edge = gmaps_tpb_edge_new(tpb, node1, node2, NULL);

    // EXPECT the edge to be deleted
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge);

    // WHEN deleting the edge
    gmaps_tpb_edge_delete(&edge);

    // cleanup:
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node1);
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node2);
    gmaps_tpb_delete(&tpb);
}

void test_gmaps_tpb_edge_delete__null(UNUSED void** state) {
    gmaps_tpb_edge_delete(NULL);

    gmaps_tpb_edge_t* null_edge = NULL;
    gmaps_tpb_edge_delete(&null_edge);
}

void test_gmaps_tpb_edge_get_upper_node__normalcase(UNUSED void** state) {
    // GIVEN an edge
    gmaps_tpb_t* tpb = gmaps_tpb_new();
    gmaps_tpb_node_t* node1 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_node_t* node2 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_edge_t* edge = gmaps_tpb_edge_new(tpb, node1, node2, NULL);

    // WHEN requesting the upper node of the edge
    gmaps_tpb_node_t* upper_node_of_edge = gmaps_tpb_edge_get_upper_node(edge);

    // THEN we got the node we specified as upper node when creating the edge
    assert_ptr_equal(node1, upper_node_of_edge);

    // cleanup:
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge);
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node1);
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node2);
    gmaps_tpb_delete(&tpb);
}

void test_gmaps_tpb_edge_get_upper_node__invalid_args(UNUSED void** state) {
    assert_null(gmaps_tpb_edge_get_upper_node(NULL));
}

void test_gmaps_tpb_edge_get_lower_node__normalcase(UNUSED void** state) {
    // GIVEN an edge
    gmaps_tpb_t* tpb = gmaps_tpb_new();
    gmaps_tpb_node_t* node1 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_node_t* node2 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_edge_t* edge = gmaps_tpb_edge_new(tpb, node1, node2, NULL);

    // WHEN requesting the lower node of the edge
    gmaps_tpb_node_t* lower_node_of_edge = gmaps_tpb_edge_get_lower_node(edge);

    // THEN we got the node we specified as upper node when creating the edge
    assert_ptr_equal(node2, lower_node_of_edge);

    // cleanup:
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge);
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node1);
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node2);
    gmaps_tpb_delete(&tpb);
}

void test_gmaps_tpb_edge_get_lower_node__invalid_args(UNUSED void** state) {
    assert_null(gmaps_tpb_edge_get_lower_node(NULL));
}

void test_gmaps_tpb_node_userdata__normalcase(UNUSED void** state) {
    // GIVEN a node
    gmaps_tpb_t* tpb = gmaps_tpb_new();
    int x = 123;
    gmaps_tpb_node_t* node1 = gmaps_tpb_node_new(tpb, &x);

    // WHEN requesting the userdata of the node
    void* node1_userdata = gmaps_tpb_node_userdata(node1);

    // THEN we got back the userdata we passed when creating the node
    assert_ptr_equal(&x, node1_userdata);

    // cleanup:
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node1);
    gmaps_tpb_delete(&tpb);
}

void test_gmaps_tpb_node_userdata__invalid_args(UNUSED void** state) {
    assert_null(gmaps_tpb_node_userdata(NULL));
}

void test_gmaps_tpb_edge_userdata__normalcase(UNUSED void** state) {
    // GIVEN an edge
    gmaps_tpb_t* tpb = gmaps_tpb_new();
    gmaps_tpb_node_t* node1 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_node_t* node2 = gmaps_tpb_node_new(tpb, NULL);
    int x = 123;
    gmaps_tpb_edge_t* edge = gmaps_tpb_edge_new(tpb, node1, node2, &x);

    // WHEN requesting the userdata of the edge
    void* edge_userdata = gmaps_tpb_edge_userdata(edge);

    // THEN we got back the userdata we received when creating the edge
    assert_ptr_equal(edge_userdata, &x);

    // cleanup:
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge);
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node1);
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node2);
    gmaps_tpb_delete(&tpb);
}

void test_gmaps_tpb_edge_userdata__invalid_args(UNUSED void** state) {
    assert_null(gmaps_tpb_node_userdata(NULL));
}

void test_gmaps_tpb_delete(UNUSED void** state) {
    // GIVEN a graph with 2 nodes and one edge
    //   ┌─────┐       ┌─────┐
    //   │Node1├──────►│Node2│
    //   └─────┘       └─────┘
    gmaps_tpb_t* tpb = gmaps_tpb_new();
    gmaps_tpb_node_t* node1 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_node_t* node2 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_edge_t* edge = gmaps_tpb_edge_new(tpb, node1, node2, NULL);

    // EXPECT the nodes and edges to be deleted
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge);
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node1);
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node2);

    // WHEN deleting the graph / topologybuilder
    gmaps_tpb_delete(&tpb);

    assert_null(tpb);
}

void test_gmaps_tpb_delete__null(UNUSED void** state) {
    gmaps_tpb_delete(NULL);
    gmaps_tpb_t* tpb = NULL;
    gmaps_tpb_delete(&tpb);
}

void test_gmaps_tpb_update__empty(UNUSED void** state) {
    // GIVEN a topologybuilder with the empty graph
    gmaps_tpb_t* tpb = gmaps_tpb_new();

    // WHEN building the topology
    gmaps_tpb_update(tpb);

    // THEN no links were selected

    // Cleanup:
    gmaps_tpb_delete(&tpb);
}

void test_gmaps_tpb_update__one_edge(UNUSED void** state) {
    // GIVEN a graph with 2 nodes and one edge
    //   ┌─────┐       ┌─────┐
    //   │Node1├──────►│Node2│
    //   └─────┘       └─────┘
    gmaps_tpb_t* tpb = gmaps_tpb_new();
    gmaps_tpb_node_t* node1 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_node_t* node2 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_edge_t* edge = gmaps_tpb_edge_new(tpb, node1, node2, NULL);

    // EXPECT the edge to be selected for node 2
    expect_value(__wrap_gmaps_dm_tpb_on_link_selected, edge, edge);

    // WHEN building the topology
    gmaps_tpb_update(tpb);

    // cleanup:
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node1);
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node2);
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge);
    gmaps_tpb_delete(&tpb);
}

void test_gmaps_tpb_update__two_edges(UNUSED void** state) {
    // GIVEN a graph with 3 nodes and two edges
    //    ┌─────┐       ┌─────┐      ┌─────┐
    //    │Node1├──────►│Node2├─────►│Node3│
    //    └─────┘       └─────┘      └─────┘
    gmaps_tpb_t* tpb = gmaps_tpb_new();
    gmaps_tpb_node_t* node1 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_node_t* node2 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_node_t* node3 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_edge_t* edge12 = gmaps_tpb_edge_new(tpb, node1, node2, NULL);
    gmaps_tpb_edge_t* edge23 = gmaps_tpb_edge_new(tpb, node2, node3, NULL);

    // EXPECT the only available edge to be selected for node 2
    expect_value(__wrap_gmaps_dm_tpb_on_link_selected, edge, edge12);

    // EXPECT the only available edge to be selected for node 3
    expect_value(__wrap_gmaps_dm_tpb_on_link_selected, edge, edge23);

    // WHEN building the topology
    gmaps_tpb_update(tpb);

    // cleanup:
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node1);
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node2);
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node3);
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge12);
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge23);
    gmaps_tpb_delete(&tpb);
}

void test_gmaps_tpb_update__normal_case(UNUSED void** state) {
    // GIVEN tho following graph
    //     ┌──────────────────────────┐
    //     │                          │
    //     │                          │
    //     │                          ▼
    //  ┌──┴──┐       ┌─────┐      ┌─────┐
    //  │Node1├──────►│Node2├─────►│Node3│
    //  └──┬──┘       └──┬──┘      └─────┘
    //     │             │
    //     │             ▼
    //     │          ┌─────┐
    //     └─────────►│Node4│
    //                └─────┘
    gmaps_tpb_t* tpb = gmaps_tpb_new();
    gmaps_tpb_node_t* node1 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_node_t* node2 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_node_t* node3 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_node_t* node4 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_edge_t* edge12 = gmaps_tpb_edge_new(tpb, node1, node2, NULL);
    gmaps_tpb_edge_t* edge23 = gmaps_tpb_edge_new(tpb, node2, node3, NULL);
    gmaps_tpb_edge_t* edge13 = gmaps_tpb_edge_new(tpb, node1, node3, NULL);
    gmaps_tpb_edge_t* edge14 = gmaps_tpb_edge_new(tpb, node1, node4, NULL);
    gmaps_tpb_edge_t* edge24 = gmaps_tpb_edge_new(tpb, node2, node4, NULL);

    // EXPECT for node2, select the edge from node1
    expect_value(__wrap_gmaps_dm_tpb_on_link_selected, edge, edge12);
    // EXPECT for node3, select the edge from node2 (because it's longer path than the edge from node1)
    expect_value(__wrap_gmaps_dm_tpb_on_link_selected, edge, edge23);
    // EXPECT for node4, select the edge from node2 (because it's longer path than the edge from node2)
    expect_value(__wrap_gmaps_dm_tpb_on_link_selected, edge, edge24);

    // WHEN building the topology
    gmaps_tpb_update(tpb);

    // cleanup:
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node1);
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node2);
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node3);
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node4);
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge12);
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge23);
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge13);
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge14);
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge24);
    gmaps_tpb_delete(&tpb);
}


void test_gmaps_tpb_update__chain(UNUSED void** state) {
    // GIVEN the following graph
    // ```
    //   ┌─────┐   ┌─────┐   ┌─────┐   ┌─────┐   ┌─────┐
    //   │node1├──►│node2├──►│node3├──►│node4│──►│node5│
    //   └──┬──┘   └─────┘   └─────┘   └─────┘   └─────┘
    //      │         ▲         ▲         ▲         ▲
    //      ├─────────┘         │         │         │
    //      ├───────────────────┘         │         │
    //      ├─────────────────────────────┘         │
    //      └───────────────────────────────────────┘
    //
    // ```
    gmaps_tpb_t* tpb = gmaps_tpb_new();
    gmaps_tpb_node_t* node1 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_node_t* node2 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_node_t* node3 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_node_t* node4 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_node_t* node5 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_edge_t* edge12 = gmaps_tpb_edge_new(tpb, node1, node2, NULL);
    gmaps_tpb_edge_t* edge13 = gmaps_tpb_edge_new(tpb, node1, node3, NULL);
    gmaps_tpb_edge_t* edge14 = gmaps_tpb_edge_new(tpb, node1, node4, NULL);
    gmaps_tpb_edge_t* edge15 = gmaps_tpb_edge_new(tpb, node1, node5, NULL);
    gmaps_tpb_edge_t* edge45 = gmaps_tpb_edge_new(tpb, node4, node5, NULL);
    gmaps_tpb_edge_t* edge34 = gmaps_tpb_edge_new(tpb, node3, node4, NULL);
    gmaps_tpb_edge_t* edge23 = gmaps_tpb_edge_new(tpb, node2, node3, NULL);

    // EXPECT the longest path to be chosen
    expect_value(__wrap_gmaps_dm_tpb_on_link_selected, edge, edge12);
    expect_value(__wrap_gmaps_dm_tpb_on_link_selected, edge, edge23);
    expect_value(__wrap_gmaps_dm_tpb_on_link_selected, edge, edge34);
    expect_value(__wrap_gmaps_dm_tpb_on_link_selected, edge, edge45);

    // WHEN building the topology
    gmaps_tpb_update(tpb);

    // cleanup:
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node1);
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node2);
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node3);
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node4);
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node5);
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge12);
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge13);
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge14);
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge15);
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge45);
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge34);
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge23);
    gmaps_tpb_delete(&tpb);
}

void test_gmaps_tpb_update__after_remove_edge(UNUSED void** state) {
    // GIVEN tho following graph
    //     ┌──────────────────────────┐
    //     │                          │
    //     │                          │
    //     │                          ▼
    //  ┌──┴──┐       ┌─────┐      ┌─────┐
    //  │Node1├──────►│Node2├─────►│Node3│
    //  └──┬──┘       └──┬──┘      └─────┘
    //     │             │
    //     │             ▼
    //     │          ┌─────┐
    //     └─────────►│Node4│
    //                └─────┘
    gmaps_tpb_t* tpb = gmaps_tpb_new();
    gmaps_tpb_node_t* node1 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_node_t* node2 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_node_t* node3 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_node_t* node4 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_edge_t* edge12 = gmaps_tpb_edge_new(tpb, node1, node2, NULL);
    gmaps_tpb_edge_t* edge23 = gmaps_tpb_edge_new(tpb, node2, node3, NULL);
    gmaps_tpb_edge_t* edge13 = gmaps_tpb_edge_new(tpb, node1, node3, NULL);
    gmaps_tpb_edge_t* edge14 = gmaps_tpb_edge_new(tpb, node1, node4, NULL);
    gmaps_tpb_edge_t* edge24 = gmaps_tpb_edge_new(tpb, node2, node4, NULL);
    expect_value(__wrap_gmaps_dm_tpb_on_link_selected, edge, edge12);
    expect_value(__wrap_gmaps_dm_tpb_on_link_selected, edge, edge23);
    expect_value(__wrap_gmaps_dm_tpb_on_link_selected, edge, edge24);
    gmaps_tpb_update(tpb);

    // EXPECT the edge node2->node4 to be replaced by node1->node4
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge24);
    expect_value(__wrap_gmaps_dm_tpb_on_link_selected, edge, edge14);

    // WHEN removing the edge between node2 and node4
    gmaps_tpb_edge_delete(&edge24);
    gmaps_tpb_update(tpb);

    // cleanup:
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node1);
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node2);
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node3);
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node4);
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge12);
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge23);
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge13);
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge14);
    gmaps_tpb_delete(&tpb);
}

void test_gmaps_tpb_update__after_remove_last_edge(UNUSED void** state) {
    // GIVEN tho following graph
    //     ┌──────────────────────────┐
    //     │                          │
    //     │                          │
    //     │                          ▼
    //  ┌──┴──┐       ┌─────┐      ┌─────┐
    //  │Node1├──────►│Node2├─────►│Node3│
    //  └─────┘       └──┬──┘      └─────┘
    //                   │
    //                   ▼
    // (no 1->4 edge) ┌─────┐
    //                │Node4│
    //                └─────┘
    gmaps_tpb_t* tpb = gmaps_tpb_new();
    gmaps_tpb_node_t* node1 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_node_t* node2 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_node_t* node3 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_node_t* node4 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_edge_t* edge12 = gmaps_tpb_edge_new(tpb, node1, node2, NULL);
    gmaps_tpb_edge_t* edge23 = gmaps_tpb_edge_new(tpb, node2, node3, NULL);
    gmaps_tpb_edge_t* edge13 = gmaps_tpb_edge_new(tpb, node1, node3, NULL);
    gmaps_tpb_edge_t* edge24 = gmaps_tpb_edge_new(tpb, node2, node4, NULL);
    expect_value(__wrap_gmaps_dm_tpb_on_link_selected, edge, edge12);
    expect_value(__wrap_gmaps_dm_tpb_on_link_selected, edge, edge23);
    expect_value(__wrap_gmaps_dm_tpb_on_link_selected, edge, edge24);
    gmaps_tpb_update(tpb);

    // EXPECT node4 to not have an incoming link anymore after removing the edge 2->4.
    expect_value(__wrap_gmaps_dm_tpb_on_no_link_selected, node, node4);
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge24);

    // WHEN removing the edge between node2 and node4
    gmaps_tpb_edge_delete(&edge24);
    gmaps_tpb_update(tpb);

    // cleanup:
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node1);
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node2);
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node3);
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node4);
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge12);
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge23);
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge13);
    gmaps_tpb_delete(&tpb);
}

void test_gmaps_tpb_update__redundancy(UNUSED void** state) {
    // GIVEN a graph with 2 nodes and two edges between those node
    //             a
    //      ┌────────────┐
    //      │            ▼
    //   ┌──┴──┐       ┌─────┐
    //   │Node1│       │Node2│
    //   └──┬──┘       └─────┘
    //      │            ▲
    //      │            │
    //      └────────────┘
    //             b

    gmaps_tpb_t* tpb = gmaps_tpb_new();
    gmaps_tpb_node_t* node1 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_node_t* node2 = gmaps_tpb_node_new(tpb, NULL);
    gmaps_tpb_edge_t* edge12a = gmaps_tpb_edge_new(tpb, node1, node2, NULL);
    gmaps_tpb_edge_t* edge12b = gmaps_tpb_edge_new(tpb, node1, node2, NULL);

    expect_value(__wrap_gmaps_dm_tpb_on_link_selected, edge, edge12a);
    gmaps_tpb_update(tpb);

    // EXPECT the other edge to become selected
    expect_value(__wrap_gmaps_dm_tpb_on_link_selected, edge, edge12b);
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge12a);

    // WHEN removing the selected edge
    gmaps_tpb_edge_delete(&edge12a);
    gmaps_tpb_update(tpb);

    // cleanup:
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node1);
    expect_value(__wrap_gmaps_dm_tpb_on_node_delete, node, node2);
    expect_value(__wrap_gmaps_dm_tpb_on_edge_delete, edge, edge12b);
    gmaps_tpb_delete(&tpb);
}

void test_gmaps_tpb_update__invalid_args(UNUSED void** state) {
    gmaps_tpb_update(NULL);
}

